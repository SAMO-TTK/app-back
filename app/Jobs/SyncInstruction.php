<?php

namespace App\Jobs;

use Curl\Curl;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class SyncInstruction implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$curl = new Curl();
		$curl->get('http://wkrest.toutenkamion.com/instructions');
		$data = json_decode($curl->response);
		foreach ($data as $i => $row){
			if(!DB::table('instructions')
				->where('wana_id', $row->nIDInstruction)
				->get()
				->first()){
				if($job = DB::table('jobs')
					->where('wana_id', $row->nAtelier)
					->get()
					->first()){
					DB::table('instructions')
						->insert([
							'label' => addslashes($row->sDescInstruction),
							'wana_id' => addslashes($row->nIDInstruction),
							'wana_number' => addslashes($row->nInstruction)
						]);
				}
			}
		}

		$kamions = DB::table('kamions')->where('is_locked', 0)->get();
		foreach ($kamions as $kamion){
			$curl = new Curl();
			$curl->get('http://wkrest.toutenkamion.com/instruction/kamion/'.$kamion->id);
			$data = json_decode($curl->response);
			foreach ($data as $i => $row){
				DB::table('instruction')
					->where('wana_number', addslashes($row->nInstruction))
					->update(['kamion_id', $kamion->id]);
			}
		}
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
	}
}
