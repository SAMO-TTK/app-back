<?php

namespace App\Jobs;

use Curl\Curl;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class SyncMaterial implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

		$curl = new Curl();
		$curl->get('http://wkrest.toutenkamion.com/stock/'.date('Ymd'));
		$data = json_decode($curl->response);
		foreach ($data as $i => $row) {
			if(!DB::table('material_resources')
				->where('wana_id', addslashes($row->sIDArticle))
				->get()
				->first()){
				DB::table('material_resources')
					->insert([
						'label' => addslashes($row->sNomArticle),
						'wana_id' => addslashes($row->sIDArticle),
						'unit_label' => addslashes($row->sUnite),
					]);
			}
		}
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    }
}
