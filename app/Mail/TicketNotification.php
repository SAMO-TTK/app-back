<?php

namespace App\Mail;

use App\Api\v1\Models\Ticket_discussion;
use App\Api\v1\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TicketNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $ticket;
    public $discussions = [];
    public $fields = [];
    private $users = [];
    private $user;
    private $notified_fields = [];
    private $ticket_email = 'ticket@toutenkamion.com';

    /**
     * Create a new message instance.
     * @param $data
     * @return void
     */
    public function __construct($data) {
        $this->ticket = $data['ticket'];
        $this->notified_fields = $data['notified_fields'];
        $this->users = $data['users'];//cc
        $this->user = $data['user'];//to
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

    	//set the users informations for the email (for cc)
    	$users_emails = [];
    	foreach ($this->users as $user) {
    		$users_emails[] = [
    			'email' => $user->email,
				'name' => $user->last_name . ' ' . $user->first_name
			];
    	}

    	//get fields values if need to show ticket model data  (value == 0 in notified fields)
		if (in_array(0, $this->notified_fields)) {
			foreach ($this->ticket->fields as $field) {

				//special treatment for selectN type field
				if ($field->ticket_fields->type == "selectN") {
					$select_users = [];
					$select_users_id = explode(',', $field->value);

					$users = User::whereIn('id', $select_users_id)->get();

					foreach ($users as $user) {
						$select_users[] = $user->last_name . ' ' . $user->first_name;
					}
					$this->fields[$field->ticket_fields->position] = [
						'type' => $field->ticket_fields->type,
						'label' => $field->ticket_fields->label,
						'value' => $select_users
					];
				}
				else {
					$this->fields[$field->ticket_fields->position] = [
						'type' => $field->ticket_fields->type,
						'label' => $field->ticket_fields->label,
						'value' => $field->value
					];
				}
			}
			ksort($this->fields);
		}

		//get discussions if there are notified (value > 0 in notified fields)
		$discussion_ids = array_filter($this->notified_fields, function($a) { return ($a !== 0); });
		if (count($discussion_ids) > 0) {
			$this->discussions = Ticket_discussion::whereIn('id', $discussion_ids)->with([
				'messages.user',
				'creator'
			])->get();
		}

		//send the email based to the model 'emails.tickets.notification'
		//see resources/views/emails/tickets/notification.blade.php
        return $this->from($this->ticket_email, 'TTK Hub')
			->to($this->user->email, $this->user->last_name . ' ' . $this->user->first_name)
			->cc($users_emails)
			->subject('Ticket[' . $this->ticket->id . '] ' . $this->ticket->title)
			->view('emails.tickets.notification');
    }
}
