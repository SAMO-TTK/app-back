<?php

namespace App\Api\v1\Traits;

use Validator;

use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;


trait ApiModel {

    #region - validate()
    public function validate($data = null) {
        $http_method = request()->method();

        $rules = $this->rules;

        // Unique field
        if($http_method === 'PUT' || $http_method === 'PATCH') {
            foreach($rules as $k => $v) {
                if(strpos($v, 'unique:') !== false) {
                    $rules[$k] .= ',' . $k . ',' . $this->id . ',id';
                }
            }
        }


        if($data === null) {
            $data = $this->attributes;
        }


        $v = Validator::make($data, $rules);

        if($v->fails()) {
            switch($http_method) {
                case 'POST':
                    throw new StoreResourceFailedException('Could not create new ' . (new \ReflectionClass($this))->getShortName(), $v->errors());
                    break;

                case 'PUT':
                case 'PATCH':
                    throw new UpdateResourceFailedException('Could not update the ' . (new \ReflectionClass($this))->getShortName(), $v->errors());
                    break;

                case 'DELETE':
                    throw new DeleteResourceFailedException('Could not delete the ' . (new \ReflectionClass($this))->getShortName(), $v->errors());
                    break;

                default:
                    throw new ResourceException((new \ReflectionClass($this))->getShortName() . ' exception', $v->errors());
                    break;
            }
        }

        return $this;
    }
    #endregion

}
