<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class ApplicationController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	// @group ::Applications::
	// @model ::Application::
	// @migration ::create_applications_table::

	protected $model        = 'App\Api\v1\Models\Application';
	protected $index_fields = ['*'];
	/*
		@route ::/applications::
		@write ::index::
		*/
	public function index(Request $request){
		return $this->api_index($request);
	}
	/*
		@route ::/applications/{id}::
		@write ::show::
		*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*
		@route ::/applications::
		@write ::store::
		*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/applications/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/applications/{id}::
		@write ::delete:: */
	public function delete($id){
		return $this->api_delete($id);
	}
}
