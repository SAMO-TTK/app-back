<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class DurationController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	// @group ::Durations::
	// @model ::Duration::
	// @migration ::create_durations_table::


	protected $model        = 'App\Api\v1\Models\Duration';
	protected $index_fields = ['*'];
	/*
		@route ::/durations::
		@write ::index::
		*/
	public function index(Request $request){
		return $this->api_index($request);
	}
	/*
		@route ::/durations/{id}::
		@write ::show::
		*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*
		@route ::/durations::
		@write ::store::
		*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/durations/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/durations/{id}::
		@write ::delete:: */
	public function delete($id){
		return $this->api_delete($id);
	}
}
