<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Ticket_types::
	@model ::Ticket_type::
	@migration ::create_ticket_types_table::
 */
class Ticket_typeController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Ticket_type';
	protected $index_fields = ['*'];
	
	/*	@route ::/ticket_types::
	@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/ticket_types/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/ticket_types::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/ticket_types/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/ticket_types/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
