<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Notification;
use App\Api\v1\Models\Parameter;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	private function sync($data, $message){
		if (isset($data['documents'])) {
			foreach ($data['documents'] as $document) {
				$sync[$document] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$message->documents()->syncWithoutDetaching($sync);
		}
	}
	/*
		@group ::Messages::
		@model ::Message::
		@migration ::create_messages_table::
	 */
	protected $model        = 'App\Api\v1\Models\Message';
	protected $index_fields = ['*'];
	/*	@route ::/messages::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/messages/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/messages::
		@write ::store::*/
	public function store(Request $request){
		$mess = $this->api_store($request);
		$data = $request->all();
		$this->sync($data, $mess);

		$d = $this->model::where('id', $mess->id)->with(['discussion.element.chapter.kamion', 'discussion.element.chapter.parent.parent', 'discussion.messages.user'])->get()[0];
			$uu = [];
			foreach ($d->discussion->messages as $message){
				if (!in_array($message->user->id, $uu) && $message->user->id != $mess->user_id) {
					$uu[] = $message->user->id;
					$not = new Notification;
					$not->user_id = $message->user->id;
					$not->route = 'element';
					$not->content = "<div style='width: 50px!important;'>
						<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 363 331\" width='50px'>
						  <path d=\"M277.73,78.123 C277.73,89.12 269.724,95.808 263.878,100.716 C261.664,102.575 257.543,105.967 257.554,107.234 C257.594,112.204 253.598,116.173 248.627,116.173 C248.602,116.173 248.577,116.173 248.552,116.173 C243.616,116.173 239.594,112.326 239.554,107.381 C239.475,97.634 246.588,91.797 252.304,86.998 C256.789,83.232 259.73,80.582 259.73,78.157 C259.73,73.248 255.736,69.254 250.827,69.254 C245.916,69.254 241.921,73.248 241.921,78.157 C241.921,83.128 237.892,87.157 232.921,87.157 C227.95,87.157 223.921,83.128 223.921,78.157 C223.921,63.323 235.99,51.254 250.825,51.254 C265.661,51.253 277.73,63.288 277.73,78.123 Z M248.801,124.481 C243.83,124.481 240,128.51 240,133.481 L240,133.55 C240,138.521 243.831,142.516 248.801,142.516 C253.771,142.516 257.801,138.452 257.801,133.481 C257.801,128.51 253.772,124.481 248.801,124.481 Z M67.392,187.174 C62.421,187.174 58.392,191.203 58.392,196.174 C58.392,201.145 62.421,205.174 67.392,205.174 L68.142,205.174 C73.113,205.174 77.142,201.145 77.142,196.174 C77.142,191.203 73.113,187.174 68.142,187.174 L67.392,187.174 Z M98.671,187.174 C93.7,187.174 89.671,191.203 89.671,196.174 C89.671,201.145 93.7,205.174 98.671,205.174 L99.42,205.174 C104.391,205.174 108.42,201.145 108.42,196.174 C108.42,191.203 104.391,187.174 99.42,187.174 L98.671,187.174 Z M363,43.425 L363,144.726 C363,168.711 343.768,188.174 319.783,188.174 L203.066,188.174 C200.784,188.174 198.905,188.161 197.333,188.128 C195.686,188.094 193.832,188.081 193.109,188.161 C192.356,188.661 190.51,190.352 188.731,191.991 C188.026,192.64 187.228,193.354 186.367,194.14 L153.345,224.238 C150.711,226.641 146.814,227.263 143.552,225.825 C140.29,224.386 138,221.156 138,217.591 L138,122.174 L43.72,122.174 C29.658,122.174 18,133.697 18,147.757 L18,249.058 C18,263.119 29.659,274.174 43.72,274.174 L174.094,274.174 C176.339,274.174 178.439,275.205 180.097,276.719 L207,301.523 L207,215.984 C207,211.013 211.029,206.984 216,206.984 C220.971,206.984 225,211.013 225,215.984 L225,321.922 C225,325.487 222.96,328.669 219.697,330.108 C218.53,330.623 217.358,330.826 216.131,330.826 C213.927,330.826 211.753,329.921 210.062,328.377 L170.605,292.173 L43.72,292.173 C19.734,292.173 0,273.043 0,249.057 L0,147.757 C0,123.772 19.734,104.174 43.72,104.174 L138,104.174 L138,43.425 C138,19.439 157.885,0.174 181.871,0.174 L319.784,0.174 C343.768,0.174 363,19.439 363,43.425 Z M345,43.425 C345,29.364 333.843,18.174 319.783,18.174 L181.871,18.174 C167.81,18.174 156,29.364 156,43.425 L156,113.258 L156,197.192 L174.095,180.839 C174.933,180.074 175.872,179.374 176.557,178.742 C184.82,171.128 186.934,169.911 197.712,170.133 C199.182,170.164 200.933,170.175 203.066,170.175 L319.783,170.175 C333.843,170.175 345,158.787 345,144.727 L345,43.425 Z\"/>
						</svg>
						</div>
						<div style='width: 225px;margin-bottom: 10px;'>
							<p class=\"bar-action-notifs-list-infos-desc\">Il a nouveau message dans un debat auquel vous participez</p>
							<p style='color: #20a0ff;font-weight: bold;text-transform: uppercase;margin: 0;'>" . $d->discussion->element->chapter->kamion->name . "- (" . $d->discussion->element->chapter->kamion->wana_id . ")</p>
							<p style='color: #8fa3b4;font-style: italic;margin: 0;margin-top: 2px;'><app-icon icon=\"truck\" size=\"1rem\" :fill=\"\$appStyle.appBarBackgroundLaunch\"></app-icon>" . date('d/m/Y') . "</p>
						</div>
						<div class=\"bar-action-notifs-list-read\">
							<div class=\"circle-button\"></div>
						</div>";
					$not->save();

					$param = new Parameter;
					$param->label = 'read';
					$param->value = $d->discussion->id;
					$param->notification_id = $not->id;
					$param->save();

					$param = new Parameter;
					$param->label = 'id_kamion';
					$param->value = $d->discussion->element->chapter->kamion->id;
					$param->notification_id = $not->id;
					$param->save();

					$param = new Parameter;
					$param->label = 'elementId';
					$param->value = $d->discussion->element_id;
					$param->notification_id = $not->id;
					$param->save();
					if ($d->discussion->element->chapter->parent == null) {
						$param = new Parameter;
						$param->label = 'chapterId';
						$param->value = $d->discussion->element->chapter->id;
						$param->notification_id = $not->id;
						$param->save();
					} else if ($d->discussion->element->chapter->parent->parent == null) {
						$param = new Parameter;
						$param->label = 'chapterId';
						$param->value = $d->discussion->element->chapter->parent->id;
						$param->notification_id = $not->id;
						$param->save();
						$param = new Parameter;
						$param->label = 'subChapterId';
						$param->value = $d->discussion->element->chapter->id;
						$param->notification_id = $not->id;
						$param->save();
					} else {
						$param = new Parameter;
						$param->label = 'chapterId';
						$param->value = $d->discussion->element->chapter->parent->parent->id;
						$param->notification_id = $not->id;
						$param->save();
						$param = new Parameter;
						$param->label = 'subChapterId';
						$param->value = $d->discussion->element->chapter->parent->id;
						$param->notification_id = $not->id;
						$param->save();
						$param = new Parameter;
						$param->label = 'subChapterSecId';
						$param->value = $d->discussion->element->chapter->id;
						$param->notification_id = $not->id;
						$param->save();
					}
				}
			}


		return $mess;
	}

	/*	@route ::/messages/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$message = $this->api_update($request, $id);
		$data = $request->all();
		$this->sync($data, $message);
		return $message;
	}

	/*	@route ::/messages/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
