<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Kamion;
use App\Api\v1\Models\Chapter;
use App\Api\v1\Transformers\MetaTransformer;
/*
	@group ::Ticket_discussions::
	@model ::Ticket_discussion::
	@migration ::create_ticket_discussions_table::
 */
class Ticket_discussionController extends Controller{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Ticket_discussion';
	protected $index_fields = ['*'];
	private function sync($data, $discussion)
	{
		if (isset($data['users'])) {
			foreach ($data['users'] as $user) {
				$sync[$user] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$discussion->users()->syncWithoutDetaching($sync);
		}
		if (isset($data['documents'])) {
			foreach ($data['documents'] as $document) {
				$sync[$document] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$discussion->documents()->syncWithoutDetaching($sync);
		}
	}
	/*	@route ::/ticket_discussions::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/ticket_discussions/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/ticket_discussions::
		@write ::store::*/
	public function store(Request $request){
		$document = $this->api_store($request);
		$data = $request->all();
		$this->sync($data, $document);
		return($document);
	}

	/*	@route ::/ticket_discussions/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/ticket_discussions/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
