<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Roles::
	@model ::Role::
	@migration ::create_roles_table::
 */
class RoleController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Role';
	protected $index_fields = ['*'];
	/*	@route ::/roles::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/roles/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/roles::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/roles/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$data = $request->all();
		$role = $this->api_update($request, $id);
		if(isset($data['permissions'])){
			if ($data['permissions'][0] == 0){
				$role->permissions()->sync(array());
			}else {
				$role->permissions()->sync($data['permissions']);
			}
		}
		return $role;
	}

	/*	@route ::/roles/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
