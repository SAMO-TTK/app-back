<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Quality_controls::
	@model ::Quality_control::
	@migration ::create_quality_controls_table::
 */
class Quality_controlController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Quality_control';
	protected $index_fields = ['*'];
	private function sync($data, $discussion){
		if (isset($data['documents'])) {
			foreach ($data['documents'] as $document) {
				$sync[$document] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$discussion->documents()->sync($sync);
		}else{
			$discussion->documents()->sync([]);
		}
	}
	/*	@route ::/quality_controls::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/quality_controls/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/quality_controls::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/quality_controls/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$qc = $this->api_update($request, $id);
		$data = $request->all();
		$this->sync($data, $qc);
		return $qc;
	}

	/*	@route ::/quality_controls/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
