<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::InstructionUsers::
	@model ::InstructionUser::
	@migration ::create_instruction_user_table::
 */
class InstructionUserController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\InstructionUser';
	protected $index_fields = ['*'];
	/*	@route ::/instructionusers::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/instructionusers/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/instructionusers::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}
	/*	@route ::/instructionusers/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$tmp = $this->model::find($id);
		$tmp->updated_at = date('Y-m-d H:i:s');
		$tmp->save();
		return $this->api_update($request, $id);
	}

	/*	@route ::/instructionusers/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}

}
