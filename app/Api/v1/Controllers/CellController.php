<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Cell;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class CellController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::Categories::
	//@model ::Category::
	//@migration ::create_categories_table::
	protected $model        = 'App\Api\v1\Models\Cell';
	protected $index_fields = ['*'];

	/*	@route ::/categories::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/categories/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/categories::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/categories/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/categories/{id}::
		@write ::delete::*/
	public function delete_field($field){
		if (!$field){
			return;
		}
		foreach ($field->rows as $row){
			foreach ($row->cells as $cell){
				$this->delete_field($cell->field);
				$cell->delete();
			}
			$row->delete();
		}
		$field->delete();
		return;
	}
	public function delete($id){
		$cell = Cell::where('id', $id)->with(['field'])->get()->first();
		$this->delete_field($cell->field);
		return $this->api_delete($id);
	}
}
