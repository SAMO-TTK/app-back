<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class StatusLogtimeController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::StatusLogtimes::
	//@model ::StatusLogtime::
	//@migration ::create_status_logtimes_table::
	protected $model        = 'App\Api\v1\Models\StatusLogtime';
	protected $index_fields = ['*'];

	/*	@route ::/statuslogtimes::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/statuslogtimes/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/statuslogtimes::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/statuslogtimes/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/statuslogtimes/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
