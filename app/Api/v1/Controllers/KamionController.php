<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Column;
use App\Api\v1\Models\Kamion;
use App\Api\v1\Models\Parameter;
use App\Api\v1\Models\Stage;
use App\Api\v1\Models\User;
use Curl\Curl;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Notification;
use App\Api\v1\Transformers\MetaTransformer;
use Illuminate\Support\Facades\DB;

/*
	@group ::Kamions::
	@model ::Kamion::
	@migration ::create_kamions_table::
 */
class KamionController extends Controller
{

	private $id;
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Kamion';
	protected $index_fields = ['*'];

	/*	@route ::/kamions::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/kamions/{id}::
		@write ::show::*/
	public function show(Request $request, $id){

		$kamion = $this->api_show($request, $id);
		$user = \JWTAuth::parseToken()->authenticate()->id;
		// dd($kamion);
		$k = \App\Api\v1\Models\Kamion::find($id);
		$sync[$user] = array(
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s')
		);
		$k->last_projects()->syncWithoutDetaching($sync);
		return $kamion;
	}

	/*	@route ::/kamions::
		@write ::store::*/
	public function store(Request $request){
		$kamion = $this->api_store($request);
		$column = new Column();
		$column->kamion_id = $kamion->id;
		$column->numbering = 1;
		$column->reference = 0;
		$column->content = 1;
		$column->supplement = 1;
		$column->crecre = 0;
		$column->upupup = 1;
		$column->client_requirement = 1;
		$column->type = 0;
		$column->status = 1;
		$column->save();
		$id = $kamion->id;
		if(isset($request->users)){
			foreach ($request->users as $user){
				$dbuser = \DB::table('kamion_user')->where('kamion_id', $id)->where('user_id' , $user)->get();
				if (isset($dbuser[0])){
					\DB::table('kamion_user')->where('kamion_id', $id)->where('user_id', $user)->delete();
					$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
						'is_kamion_manager' => $dbuser[0]->is_kamion_manager,
						'is_project_manager' => $dbuser[0]->is_project_manager,
						'is_pilot' => $dbuser[0]->is_pilot,
						'kamion_id' => $id,
						'user_id' => $user,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					]))->get();
				} else {
					$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
						'is_kamion_manager' => 0,
						'is_project_manager' => 0,
						'is_pilot' => 0,
						'kamion_id' => $id,
						'user_id' => $user,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					]))->get();
				}

			}
		}
		if(isset($request->Kman)){
			$dbuser = \DB::table('kamion_user')->where('kamion_id', $id)->where('user_id' , $request->Kman)->get();
			if (isset($dbuser[0])){
				\DB::table('kamion_user')->where('kamion_id', $id)->where('user_id', $request->Kman)->delete();
				$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
					'is_kamion_manager' => 1,
					'is_project_manager' => $dbuser[0]->is_project_manager,
					'is_pilot' => $dbuser[0]->is_pilot,
					'kamion_id' => $id,
					'user_id' => $request->Kman,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]))->get();
			} else {
				$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
					'is_kamion_manager' => 1,
					'is_project_manager' => 0,
					'is_pilot' => 0,
					'kamion_id' => $id,
					'user_id' => $request->Kman,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]))->get();
			}

		}
		if(isset($request->Pman)){
			$dbuser = \DB::table('kamion_user')->where('kamion_id', $id)->where('user_id' , $request->Pman)->get();
			if (isset($dbuser[0])){
				\DB::table('kamion_user')->where('kamion_id', $id)->where('user_id', $request->Pman)->delete();
				$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
					'is_kamion_manager' => $dbuser[0]->is_kamion_manager,
					'is_project_manager' => 1,
					'is_pilot' => $dbuser[0]->is_pilot,
					'kamion_id' => $id,
					'user_id' => $request->Pman,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]))->get();
			} else {
				$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
					'is_kamion_manager' => 0,
					'is_project_manager' => 1,
					'is_pilot' => 0,
					'kamion_id' => $id,
					'user_id' => $request->Pman,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]))->get();
			}

		}
		return $kamion;
	}

	/*	@route ::/kamions/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$old = $this->model::where('id',$id)->with(['users'])->get();
		$kamion = $this->api_update($request, $id);
		if ($old[0]->stage_id != $kamion->stage_id){
			$kamion_stage = Stage::find($kamion->stage_id);
			$tmp = [];
			foreach ($old[0]->users as $user){
				$tmp[] = $user->id;
			}
			$not = new Notification;
			$not->icon = 'truck';
			$not->title = "La phase du Kamion vient de passer à " . $kamion_stage->label;
			$not->row1 = $kamion->name."- (".$kamion->wana_id.")";
			$not->row3 = date('d/m/Y H:i:s');
			$not->save();
			$sync = [];
			foreach ($tmp as $user){
				$sync[$user] = array(
					'is_read'	=> 0,
				);
			}
			$not->users()->sync($sync);
		}
		if ($old[0]->is_delivered != 1 && $kamion->is_delivered == 1){

			foreach ($old[0]->users as $user){
				$tmp[] = $user->id;
			}
			$not = new Notification;
			$not->icon = 'truck';
			$not->title = "Le Kamion à été noté comme livré";
			$not->row1 = $kamion->name."- (".$kamion->wana_id.")";
			$not->row3 = date('d/m/Y H:i:s');
			$not->save();
			foreach ($tmp as $user){
				$sync[$user] = array(
					'is_read'	=> 0,
				);
			}
			$not->users()->sync($sync);
		}
		return $kamion;

	}

	/*	@route ::/kamions/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}

	/*	@rule ::Save kamion's avatar::
		@route ::[POST /kamions/{id}/image]::
		@field
		+ FILE: `file`
		@endfield
		@parameters
		+ id:(integer, required) - the id of the kamion
		@endparameters
		@write ::custom::*/
	public function image(Request $request, $id){
//		$kamion = $this->model::find($id);
		$name = $request->file('file')->store('images');
//		$kamion->image = $name;
//		$kamion->save();
		return $name;
	}

	/*	@rule ::Link a Kamion to another kamion::
		@route ::[PUT /kamions/{id}/projects]::
		@field
		+ ARRAY: `projects`
		@endfield
		@parameters
		+ id:(integer, required) - the id of the principal kamion
		@endparameters
		@write ::custom::*/
	public function link_kamion(Request $request, $master_id){
		foreach($request->projects as $id){
			$linked_projects = \DB::table('linked_projects')->where('id', \DB::table('linked_projects')->insertGetId([
				'master_id' => $master_id, 'linked_id' => $id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
			]))->get();
		}
		return(response($linked_projects, 200));
	}

	/*	@rule ::Link a User to a Kamion::
		@route ::[PUT /kamions/{id}/users]::
		@field
		+ ARRAY: `users`
		@endfield
		@parameters
		+ id:(integer, required) - the id of the principal kamion
		@endparameters
		@responseheader
				Content-Type : application/json
		@endresponseheader
		@write ::custom::*/
	public function link_user(Request $request, $id){
		$tmp_lord = [];
		$tmp_user = [];
		$kamion = $this->model::find($id);
		foreach ($request->users as $user){
			$dbuser = \DB::table('kamion_user')->where('kamion_id', $id)->where('user_id' , $user['id'])->get();
			if (isset($dbuser[0])){
				\DB::table('kamion_user')->where('kamion_id', $id)->where('user_id', $user['id'])->delete();
				$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
					'is_kamion_manager' => isset($user['is_kamion_manager']) ? $user['is_kamion_manager'] : $dbuser[0]->is_kamion_manager,
					'is_project_manager' => isset($user['is_project_manager']) ? $user['is_project_manager'] : $dbuser[0]->is_project_manager,
					'is_pilot' => isset($user['is_pilot']) ? $user['is_pilot'] : $dbuser[0]->is_pilot,
					'kamion_id' => $id,
					'user_id' => $user['id'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]))->get();
			} else {
				$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
					'is_kamion_manager' => isset($user['is_kamion_manager']) ? $user['is_kamion_manager'] : 0,
					'is_project_manager' => isset($user['is_project_manager']) ? $user['is_project_manager'] : 0,
					'is_pilot' => isset($user['is_pilot']) ? $user['is_pilot'] : 0,
					'kamion_id' => $id,
					'user_id' => $user['id'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]))->get();
				if(isset($user['is_kamion_manager']) && $user['is_kamion_manager'] == 1){
					$tmp_user[] = $user['id'];
				}else{
					$tmp_lord[] = $user['id'];
				}
			}

		}
		if(count($tmp_user) != 0) {
			$not = new Notification;
			$not->icon = 'truck';
			$not->title = "Vous venez d'être affecté en tant que compagnon";
			$not->row1 = $kamion->name . "- (" . $kamion->wana_id . ")";
			$not->row3 = date('d/m/Y H:i:s');
			$not->save();
			foreach ($tmp_user as $user){
				$sync[$user] = array(
					'is_read'	=> 0,
				);
			}
			$not->users()->sync($sync);
		}
		if(count($tmp_lord) != 0) {
			$not = new Notification;
			$not->icon = 'truck';
			$not->title = "Vous venez d'être affecté en tant que chef de Kamion";
			$not->row1 = $kamion->name . "- (" . $kamion->wana_id . ")";
			$not->row3 = date('d/m/Y H:i:s');
			$not->save();
			foreach ($tmp_lord as $user){
				$sync[$user] = array(
					'is_read'	=> 0,
				);
			}
			$not->users()->sync($sync);
		}
		return(response($kamion_user, 200));
	}

	/*	@rule ::Show all linked project of kamion::
		@route ::[GET /kamions/{id}/projects]::
		@parameters
		+ id:(integer, required) - the id of the principal kamion
		@endparameters
		@write ::custom::*/
	public function linked($id){
		$first = \DB::table('kamions')
			->whereExists(function ($query) use ($id) {
				$query->select(\DB::raw(1))
					->from('linked_projects')
					->whereRaw('linked_projects.master_id = kamions.id')
					->whereRaw('linked_projects.linked_id = '.$id);
			});
		$kamions = \DB::table('kamions')
			->whereExists(function ($query) use ($id) {
				$query->select(\DB::raw(1))
					->from('linked_projects')
					->whereRaw('linked_projects.linked_id  = kamions.id')
					->whereRaw('linked_projects.master_id = '.$id);
			})
			->union($first)
			->get();
		return($kamions);
	}
	/*	@rule ::Show all cda structure::
		@route ::[GET /kamions/{id}/cda]::
		@parameters
		+ id:(integer, required) - the id of the principal kamion
		@endparameters
		@write ::custom::*/
	public function cda($id){
		$dispatcher = app('Dingo\Api\Dispatcher');
		return $dispatcher->get('chapters', ['filters' => 'kamion_id|=|'.$id.',parent_id|is|null', 'embed' => 'all_children']);
	}

	/* @rule ::retrive the 8 resently consulted kamion by curent user::
	@route ::[GET /kamions/users]::
	@write ::custom::*/
	public function last_projects(){
		$id = \JWTAuth::parseToken()->authenticate()->id;
		$kamion = $this->model
			::where('is_locked', '=', '0')
			->join('last_projects', 'kamions.id', '=', 'last_projects.kamion_id')
			->select('kamions.*', 'last_projects.updated_at')
			->where('last_projects.user_id', '=', $id)
			->orderBy('last_projects.updated_at', 'desc')
			->with('users')
			->limit(4)->get();
		return $kamion;
	}

	/* @rule ::retrive the elements linked::
	@route ::[GET /kamions/{id}/elements]::
	@parameters
	+ id: (integer,required) - the id of the kamion
	@endparameters
	@write ::custom::*/
	public function elements(Request $request,$id){
		$elements = \App\Api\v1\Models\Element
			::join('chapters', 'elements.chapter_id', '=', 'chapters.id')
			->select('elements.*')
			->with('validation_controls', 'quality_controls', 'supplement')
			->where('chapters.kamion_id', '=', $id);
		if(!is_null($request->query('page'))) {
			$per_page = !is_null($request->query('per_page')) ? $request->query('per_page') : 15;
			$query_result       = $elements->paginate($per_page);
			$transformed_result = $this->response->collection($query_result->getCollection(), new MetaTransformer);
			$return = [
				'count'     => $query_result->total(),
				'per_page'  => (int)$query_result->perPage(),
				'current'   => $query_result->currentPage(),
			];
			if($query_result->currentPage() > 1) {
				$return['prev'] = $query_result->currentPage() - 1;
			}
			if($query_result->currentPage() < $query_result->lastPage()) {
				$return['next'] = $query_result->currentPage() + 1;
			}
			$return['data'] = $transformed_result->getOriginalContent();
			return $return;
		} else {
			return $this->response->collection($elements->get(), new MetaTransformer);
		}
	}

	public function update_all_kamion()
	{
		$mfiles = new Curl();
		$data = new \stdClass();
		$data->Username = "ttkhubitb";
		$data->Password = "ttkhubitb";
		$data->VaultGuid = "{7061608C-319D-43C7-9017-6D940C3731C7}";
		$mfiles->post('https://mfiles.toutenkamion.com/REST/server/authenticationtokens', json_encode($data));
		unset($curl);
		$curl = new Curl();
		$curl->get("http://wkrest.toutenkamion.com/kamion");
		$kamions = json_decode($curl->response);

		$curl->close();
		$now = date_create();
		$ret = array();
		$diff = \DateInterval::createFromDateString('18 months');
//		dd($curl->response);
		foreach ($kamions as $i => $k) {
			if ($k->dDateLivraison && $k->dDateLivraison != "0000-00-00" && $k->dDateLivraison != "" && ($h = date_diff($now, ($t = date_create($k->dDateLivraison))->add($diff)))->invert == 0) {
				unset($t);
				unset($h);
				unset($curl);
				$curl = new Curl();
				$curl->get("http://wkrest.toutenkamion.com/kamion/" . $k->nIDKamion);
				unset($kamion);
				$kamion = json_decode($curl->response);

				$curl->close();
				if ($kamion && is_array($kamion) && isset($kamion[0])) {
					$kamion = $kamion[0];
					$old_kamion = Kamion::where('wana_id', $kamion->nKnum)->with(['column'])->get()->first();
					if (!$old_kamion || $old_kamion->stage_id != 6) {
						unset($Mkamion);
						$Mkamion = new Curl();
						$Mkamion->setHeader('X-Authentication', json_decode($mfiles->response)->Value);
						$Mkamion->get('https://mfiles.toutenkamion.com/REST/objects?p1047=' . $kamion->nKnum);
						unset($phase);
						$phase = new Curl();
						$phase->setHeader('X-Authentication', json_decode($mfiles->response)->Value);
						if (isset(json_decode($Mkamion->response)->Items[0])) {
							$phase->get('https://mfiles.toutenkamion.com/REST/objects/106/' . json_decode($Mkamion->response)->Items[0]->ObjVer->ID . '/latest/properties/39');
							$Mkamion->close();
							if (!$old_kamion) {
								$old_kamion = new Kamion();
								$old_kamion->wana_id = $kamion->nKnum;
							}
							if (isset(json_decode($phase->response)->Value)) {
								switch (intval(json_decode($phase->response)->Value->DisplayValue)) {
									case 0:
										$old_kamion->stage_id = 1;
										break;
									case 1:
										$old_kamion->stage_id = 2;
										break;
									case 2:
										$old_kamion->stage_id = 3;
										break;
									case 3:
										$old_kamion->stage_id = 4;
										break;
									case 4:
										$old_kamion->stage_id = 5;
										break;
									case 100:
										$old_kamion->stage_id = 6;
										break;
									case 11:
										$old_kamion->stage_id = 7;
										break;
								}
							}else{
								$old_kamion->stage_id = 1;
							}
						} else {
							$old_kamion = Kamion::where('wana_id', $kamion->nKnum)->get()->first();
							if (!$old_kamion) {
								unset($old_kamion);
								$old_kamion = new Kamion();
								$old_kamion->wana_id = $kamion->nKnum;
							}
							$old_kamion->stage_id = 1;
						}
						$phase->close();
						$old_kamion->wana_created_at = $kamion->dDateCreation;
						$old_kamion->wana_updated_at = $kamion->dDateMaj;
						$old_kamion->name = $kamion->sNom;
						$old_kamion->length = $kamion->nLongeur;
						$old_kamion->amount = $kamion->nNombre;
						$old_kamion->planned_time = $kamion->sHeurePrevue;
						$old_kamion->worked_time = $kamion->sHeureRealise;
						$old_kamion->is_delivered = $kamion->nLivre;
						$old_kamion->delivery = $kamion->dDateLivraison;
						$old_kamion->coeff_mp = $kamion->nCoeff;
						$old_kamion->hourly_rate = $kamion->nTauxHoraire;
						$old_kamion->is_locked = $kamion->bVisibleGlobale_HUB == false ? 1 : 0;
						$old_kamion->open_phase_2 = $kamion->bAccesPhase2_HUB == false ? 0 : 1;
						$old_kamion->is_visible = $kamion->bVisiblePhase2_HUB == false ? 0 : 1;
						$old_kamion->category_id = 1;
						$old_kamion->type = 1;
						$old_kamion->save();
						if (!$old_kamion->column) {
							$column = new Column();
							$column->kamion_id = $old_kamion->id;
							$column->numbering = 1;
							$column->reference = 0;
							$column->content = 1;
							$column->supplement = 1;
							$column->crecre = 0;
							$column->upupup = 1;
							$column->client_requirement = 1;
							$column->type = 0;
							$column->status = 1;
							$column->save();
						}
						if ($kamion->nChefKamion && $kamion->nChefKamion != "") {
							unset($tmpchefk);
							$tmpchefk = User::where('id', $kamion->nChefKamion)->get()->first();
							if ($tmpchefk) {
								unset($dbuser);
								$dbuser = \DB::table('kamion_user')->where('kamion_id', $old_kamion->id)->where('user_id', $tmpchefk)->get()->first();
								if ($dbuser) {
									\DB::table('kamion_user')->where('kamion_id', $old_kamion->id)->where('user_id', $tmpchefk)->delete();
									unset($kamion_user);
									$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
										'is_kamion_manager' => 1,
										'is_project_manager' => $dbuser->is_project_manager,
										'is_pilot' => $dbuser->is_pilot,
										'kamion_id' => $old_kamion->id,
										'user_id' => $tmpchefk->id,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s')
									]))->get();
								} else {
									unset($kamion_user);
									$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
										'is_kamion_manager' => 1,
										'is_project_manager' => 0,
										'is_pilot' => 0,
										'kamion_id' => $old_kamion->id,
										'user_id' => $tmpchefk->id,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s')
									]))->get();
								}
							}
						}
						if ($kamion->sChefProjet && $kamion->sChefProjet != "") {
							unset($tmpchefp);
							$tmpchefp = User::where('id', $kamion->sChefProjet)->get()->first();
							if ($tmpchefp) {
								unset($dbuser);
								$dbuser = \DB::table('kamion_user')->where('kamion_id', $old_kamion->id)->where('user_id', $tmpchefp)->get()->first();
								if ($dbuser) {
									\DB::table('kamion_user')->where('kamion_id', $old_kamion->id)->where('user_id', $tmpchefp)->delete();
									unset($kamion_user);
									$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
										'is_kamion_manager' => $dbuser->is_kamion_manager,
										'is_project_manager' => 1,
										'is_pilot' => $dbuser->is_pilot,
										'kamion_id' => $old_kamion->id,
										'user_id' => $tmpchefp->id,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s')
									]))->get();
								} else {
									unset($kamion_user);
									$kamion_user = \DB::table('kamion_user')->where('id', \DB::table('kamion_user')->insertGetId([
										'is_kamion_manager' => 0,
										'is_project_manager' => 1,
										'is_pilot' => 0,
										'kamion_id' => $old_kamion->id,
										'user_id' => $tmpchefp->id,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s')
									]))->get();
								}
							}
						}
						$ret[] = $old_kamion;
					}
					unset($dbuser);
					unset($tmpchefk);
					unset($tmpchefp);
					unset($kamion_user);
					unset($old_kamion);
				}
			}
		}
		$curl->close();
		unset($now);
		unset($kamions);
		unset($curl);
		unset($data);
		unset($diff);
		unset($phase);
		$mfiles->close();
		unset($mfiles);
		unset($Mkamion);
		return $ret;

	}
}
