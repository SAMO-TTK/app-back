<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::MaterialResources::
	@model ::MaterialResource::
	@migration ::create_material_resouces_table::
 */
class MaterialResourceController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\MaterialResource';
	protected $index_fields = ['*'];
	/*	@route ::/materialresources::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/materialresources/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/materialresources::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}
	/*	@route ::/materialresources/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/materialresources/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
	public function update_all_material(){

	}
}
