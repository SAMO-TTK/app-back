<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Supplements::
	@model ::Supplement::
	@migration ::create_supplements_table::
 */
class SupplementController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Supplement';
	protected $index_fields = ['*'];
	/*	@route ::/supplements::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/supplements/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/supplements::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}
	/*	@route ::/supplements/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$model =  $this->api_update($request, $id);
		$element = \App\Api\v1\Models\Element::find($model->element_id);
		$element->updated_at = date('Y-m-d H:i:s');
		$element->save();
		return $model;
	}

	/*	@route ::/supplements/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}

}
