<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Api\v1\Models\Notification;
use App\Http\Controllers\Controller;
/*
	@group ::Tickets::
	@model ::Ticket::
	@migration ::create_tickets_table::
 */
class TicketController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Ticket';
	protected $index_fields = ['*'];
	private function sync($data, $document){
		if(isset($data['users'])) {
			foreach ($data['users'] as $user) {
				$sync[$user] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$document->users()->sync($sync);
		}

		if(isset($data['categories'])) {
			if($data['categories'] == 'null'){
				$document->categories()->sync([]);
			}else{
				foreach ($data['categories'] as $user) {
					$sync[$user] = array(
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);
				}
				$document->categories()->sync($sync);

			}
		}
		if(isset($data['kamions'])) {
			foreach ($data['kamions'] as $user) {
				$sync[$user] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$document->kamions()->sync($sync);
		}
	}
	/*	@route ::/tickets::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/tickets/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/tickets::
		@write ::store::*/
	public function store(Request $request){
		$document = $this->api_store($request);
		$data = $request->all();
		$this->sync($data, $document);
		return($document);
	}

	/*	@route ::/tickets/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$ticket = $this->model::find($id);
		$document = $this->api_update($request, $id);
		if ($ticket->status_id < 3 && $document->status_id >= 3){
			$ticket = $this->model::where('id', $id)->with(['users', 'manager'])->get()->first();
//			dd($ticket[0]);

			$tmp = [];
			foreach ($ticket->users as $user){
				$tmp[] = $user->id;
			}
			$not = new Notification;
			$not->icon = 'tickets';
			$not->title = "Vous venez d'être affecté à un ticket";
			$not->row1 = $ticket->number;
			$not->row2 = $ticket->title;
			$not->row3 = date('d/m/Y H:i:s');
			$not->type = 'TICKET';
			$not->save();
			$tt = new NotificationTicket();
			$tt->notification_id = $not->id;
			$tt->ticket_id = $ticket->id;
			$tt->save();
			if(count($tmp)){
				foreach ($tmp as $user){
					$sync[$user] = array(
						'is_read'	=> 0,
					);
				}
				$not->users()->sync($sync);

			}
			$ticket = new NotificationTicket();
			$ticket->notification_id = $not->id;
			$ticket->ticket_id = $ticket->id;
			$ticket->save();
		}
		$data = $request->all();
		$this->sync($data, $document);
		return($document);
	}

	/*	@route ::/tickets/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
	/*	@rule ::Searche in kicket with filters::
		@route ::[PUT /tickets/search]::
		@field
		+ ARRAY: `destinater`
		+ ARRAY: `redacter`
		+ ARRAY: `responsable`
		+ ARRAY: `id_kamion`
		+ ARRAY: `model`
		+ ARRAY: `category`
		+ ARRAY: `id_ticket`
		+ ARRAY: `status`
		+ TIMESTAMP: `deadline`
		+ TIMESTAMP: `created_at`
		@endfield
		@write ::custom::*/
	public function search(Request $request){
		$query = $this->model::select(['id','number','instruction_id','title','created_at','manager_id','creator_id','kamion_id','status_id'])->search($request->search);
		if (isset($request->filters['destinater']) && count($request->filters['destinater']) > 0){
			$query->whereHas('users', function ($query)  use ($request){
				$query->whereIn('users.id', $request->filters['destinater']);
			});
		}
		if (isset($request->filters['redacter']) && count($request->filters['redacter']) > 0){
			$query->whereIn('creator_id', $request->filters['redacter']);
		}
		if (isset($request->filters['responsable']) && count($request->filters['responsable']) > 0){
			$query->whereIn('manager_id', $request->filters['responsable']);
		}
		if (isset($request->filters['id_kamion']) && count($request->filters['id_kamion']) > 0){
			$query->whereIn('kamion_id', $request->filters['id_kamion']);
		}
		if (isset($request->filters['model']) && count($request->filters['model']) > 0){
			$query->whereIn('model_id', $request->filters['model']);
		}
		if (isset($request->filters['category']) && count($request->filters['category']) > 0){
			$query->whereHas('categories', function ($query)  use ($request){
				$query->whereIn('ticket_categories.id', $request->filters['category']);
			});
		}
		if (isset($request->filters['id_ticket']) && count($request->filters['id_ticket']) > 0){
			$query->whereIn('number', $request->filters['id_ticket']);
		}
		if (isset($request->filters['status']) && count($request->filters['status']) > 0){
			$query->whereIn('status_id', $request->filters['status']);
		}
		if (isset($request->filters['deadline']) && $request->filters['deadline'] != null && $request->filters['deadline'] != "undefined" ){
			$query->whereBetween('deadline', [date('Y-m-d H:i:s',($request->filters['deadline'])), date('Y-m-d H:i:s',(86400 + $request->filters['deadline']))]);
		}
		//86400
		if (isset($request->filters['created_at']) && $request->filters['created_at'] != null && $request->filters['created_at'] != 'undefined' ){
			$query->whereBetween('created_at', [date('Y-m-d H:i:s',($request->filters['created_at'])), date('Y-m-d H:i:s',(86400 + $request->filters['created_at']))]);
		}
		if (isset($request->limit)){
			$query->limit($request->limit);
		}else{
			$query->limit(20);
		}
		return $query->with(['kamion','creator','manager','status','categories'])->get();
	}

	/*	@rule ::Get linked ticket::
		@route ::[GET /tickets/{id}/tickets]::
		@parameters
		+ id:(integer, required) - the id of the ticket
		@endparameters
		@write ::custom::*/
	public function linked($id){
		//			->join('kamions', 'kamions.id', '=', 'tickets.kamion_id')
		$first = $this->model::whereExists(function ($query) use ($id) {
			$query->select(\DB::raw(1))
				->from('linked_tickets')
				->whereRaw('linked_tickets.ticket_id = tickets.id')
				->whereRaw('linked_tickets.linked_id = '.$id);
		})->with('kamion','creator');
		$kamions =  $this->model::whereExists(function ($query) use ($id) {
			$query->select(\DB::raw(1))
				->from('linked_tickets')
				->whereRaw('linked_tickets.linked_id  = tickets.id')
				->whereRaw('linked_tickets.ticket_id = '.$id);
		})->with('kamion','creator')
			->union($first)
			->get();
		return($kamions);
	}

	/*	@rule ::link ticket to another ticket::
	@route ::[PUT /tickets/{id}/tickets]::
	@field
	+ ARRAY: `projects`
	@endfield
	@parameters
	+ id:(integer, required) - the id of the ticket
	@endparameters
	@write ::custom::*/
	public function link_ticket(Request $request, $ticket_id){
		foreach($request->projects as $id){
			$linked_projects = \DB::table('linked_tickets')->where('id', \DB::table('linked_tickets')->insertGetId([
				'ticket_id' => $ticket_id, 'linked_id' => $id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
			]))->get();
		}
		return(response($linked_projects, 200));
	}
	/*	@rule ::get the last ticket's number::
	@route ::[GET /tickets/last]::
	@responseheader
			Content-Type : application/json
	@endresponseheader
	@write ::custom::*/
	public function last_ticket(){
		$data = \DB::table('tickets')->where('deleted_at', null)->orWhere('title', 'CronSchedultYear')->max('number');
		return(response($data,200));
	}

}
