<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Instruction;
use App\Api\v1\Models\Job;
use Curl\Curl;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/*
	@group ::Instructions::
	@model ::Instruction::
	@migration ::create_instructions_table::
 */
class InstructionController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Instruction';
	protected $index_fields = ['*'];

	/*	@route ::/instructions::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/instructions/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/instructions::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/instructions/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/instructions/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}

	public function update_all_instruction(){
		$curl = new Curl();
		$curl->get('http://wkrest.toutenkamion.com/instructions');
		$data = json_decode($curl->response);
		dump(count($data));
		foreach ($data as $i => $row){
			$is = Instruction::where('wana_id', $row->nIDInstruction)
				->get()
				->first();
			if(!$is){
				if($job = Job::where('wana_id', $row->nAtelier)
					->get()
					->first()){
					DB::table('instructions')
						->insert([
							'label' => addslashes($row->sDescInstruction),
							'wana_id' => addslashes($row->nIDInstruction),
							'wana_number' => addslashes($row->nInstruction),
							'job_id' => $job->id
						]);
				}
			}
		}
//
		$kamions = DB::table('kamions')->where('is_locked', 0)->whereIn('stage_id', [3,4,5])->get();
		$idd = 0;
		foreach ($kamions as $kamion){
			$idd++;
			$curl = new Curl();
			$curl->get('http://wkrest.toutenkamion.com/instruction/kamion/' . $kamion->wana_id);
			$data = json_decode($curl->response);
			$t = count($data);
			if($t) {
				foreach ($data as $i => $row){
					$instruction = Instruction::where('wana_number', $row->nInstruction)->get()->first();
					$instruction->kamion_id = $kamion->id;
					$instruction->save();
				}
			}
		}
		return 'ok';
	}

}
