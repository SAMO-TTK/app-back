<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Notification;
use App\Api\v1\Models\Parameter;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Discussions::
	@model ::Discussion::
	@migration ::create_discussions_table::
 */
class DiscussionController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Discussion';
	protected $index_fields = ['*'];
	private function sync($data, $discussion){
		if (isset($data['documents'])) {
			foreach ($data['documents'] as $document) {
				$sync[$document] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$discussion->documents()->syncWithoutDetaching($sync);
		}
	}
	/*	@route ::/discussions::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/discussions/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/discussions::
		@write ::store::*/
	public function store(Request $request){
		$discussion = $this->api_store($request);
		$data = $request->all();
		$d = $this->model::where('id', $discussion->id)->with(['element.chapter.kamion', 'element.chapter.parent.parent'])->get()[0];
		if(isset($data['users'])) {
			$tmp = [];
			foreach ($data['users'] as $user){
				$sync[$user['id']] = array(
					'is_author'		=> isset($user['is_author']) ? $user['is_author'] : 0,
					'is_concerned'	=> isset($user['is_concerned']) ? $user['is_concerned'] : 0,
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
				if(isset($user['is_concerned']) && $user['is_concerned'] == 1){
					$tmp[] = $user['id'];
				}
			}

			$discussion->users()->sync($sync);
			$sync = [];
			$not = new Notification;
			$not->icon = 'debats';
			$not->title = "Vous êtes convié à participer à un débat";
			$not->row1 = $d->element->chapter->kamion->name."- (".$d->element->chapter->kamion->wana_id.")";
			$not->row3 = date('d/m/Y H:i:s');
			$not->type = 'DEBAT';
			$not->save();
			foreach ($tmp as $user){
				$sync[$user] = array(
					'is_read'	=> 0,
				);
			}
			$not->discussions()->sync([$discussion->id]);
			$not->users()->sync($sync);
		}

		$this->sync($data, $discussion);
		return $discussion;
	} //create user or link

	/*	@route ::/discussions/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return  $this->api_update($request, $id);
	} //create user or link


	/*	@route ::/discussions/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
