<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Notes::
	@model ::Note::
	@migration ::create_notes_table::
 */
class NoteController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Note';
	protected $index_fields = ['*'];
	private function sync($data, $discussion){
		if (isset($data['documents'])) {
			foreach ($data['documents'] as $document) {
				$sync[$document] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$discussion->documents()->sync($sync);
		}else{
			$discussion->documents()->sync([]);
		}
	}
	/*	@route ::/notes::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/notes/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/notes::
		@write ::store::*/
	public function store(Request $request){
		$qc = $this->api_store($request);
		$data = $request->all();
		$this->sync($data, $qc);
		return $qc;
	}

	/*	@route ::/notes/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$qc = $this->api_update($request, $id);
		$data = $request->all();
		$this->sync($data, $qc);
		return $qc;
	}

	/*	@route ::/notes/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}

	public function last_note($id_user, $id_kamion){
		return $this->model
			::WhereHas(
				'last_notes', function($q) use ($id_user){
				$q->where('user_id', '=', $id_user);
			}
			)->where('kamion_id', '=', $id_kamion)
			->with(array('last_notes' => function($query) use ($id_user)
			{
				$query->where('last_notes.user_id', $id_user);
			}))->get();
	}


	public function shared($id){
		return $this->model
			::WhereHas(
				'shared', function($query) use ($id) {
				$query
					->where('users.id', '=', $id);
			}
			)->with('documents')->get();
	}
}
