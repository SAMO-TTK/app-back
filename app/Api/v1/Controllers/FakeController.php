<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Element;
class FakeController extends Controller
{
    public function seeder(){
		$elements = Element::all();
		foreach ($elements as $element) {
			$rand = rand(1, 10);
			if ($rand > 7){
				$element->references = "{CONT_AV-00".sprintf('%03d', rand(1, 900))."-1}";
				$element->save();
			}
		}
		dd($elements);
	}
}
