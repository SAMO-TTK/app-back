<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Groupe;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use App\Api\v1\Transformers\MetaTransformer;
use App\Api\v1\Models\Extension;
use Dingo\Api\Routing\Helpers;
/*
	@group ::Documents::
	@model ::Document::
	@migration ::create_documents_table::
 */
class DocumentController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Document';
	protected $index_fields = ['*'];
	private function un_sync($data, $document){
		if(isset($data['d_elements'])){
			foreach($data['d_elements'] as $element){
				$detach = [];
				$detach[] = $element;
			}
			$document->elements()->detach($detach);
		}
		if(isset($data['d_tickets'])){
			foreach($data['d_tickets'] as $element){
				$detach = [];
				$detach[] = $element;
			}
			$document->tickets()->detach($detach);
		}
	}
	private function sync($data, $document){
		if(isset($data['users'])) {
			foreach ($data['users'] as $user){
				$sync[$user] = array(
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$document->users()->syncWithoutDetaching($sync);
		}
		if(isset($data['quality_controls'])) {
			foreach ($data['quality_controls'] as $quality_control){
				$sync[$quality_control] = array(
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$document->quality_controls()->syncWithoutDetaching($sync);
		}
		if(isset($data['tickets'])) {
			foreach ($data['tickets'] as $quality_control){
				$sync[$quality_control] = array(
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$document->tickets()->syncWithoutDetaching($sync);
		}
		if(isset($data['validation_controls'])) {
			foreach ($data['validation_controls'] as $validation_control){
				$sync[$validation_control] = array(
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$document->validation_controls()->syncWithoutDetaching($sync);
		}
		if(isset($data['messages'])) {
			foreach ($data['messages'] as $message){
				$sync[$message] = array(
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$document->messages()->syncWithoutDetaching($sync);
		}
		if(isset($data['discussions'])) {
			foreach ($data['discussions'] as $discussion){
				$sync[$discussion] = array(
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$document->discussions()->syncWithoutDetaching($sync);
		}
		if(isset($data['elements'])) {
			foreach ($data['elements'] as $element){
				$sync[$element] = array(
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$document->elements()->syncWithoutDetaching($sync);
		}
		if(isset($data['kamions'])) {
			foreach ($data['kamions'] as $kamion){
				$sync[$kamion] = array(
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$document->kamions()->syncWithoutDetaching($sync);
		}
	}
	/*	@route ::/documents::
		@write ::index::*/
	public function index(Request $request)
	{
		$q_fields = $request->query('fields');
		$fields = !is_null($q_fields) ? explode(',', $q_fields) : $this->index_fields;
		$query = $this->model::select($fields);

		// Orders
		foreach ($orders = explode(',', $request->query('order')) as $k => $v) {
			$o = explode('|', $v);
			if (count($o) > 1) {
				$query->orderBy($o[0], $o[1]);
			}
		}

		// Embed ressources
		$count = [];
		foreach ($embed = explode(',', $request->query('embed')) as $k => $v) {
			foreach ($tmp = explode('.', $v) as $key => $value) {                //custom for multi relation (embed=relation.relation.relation)
				if (!method_exists($key == 0 ? $this->model : $model, $value)) {    //custom for multi relation (embed=relation.relation.relation)
					unset($embed[$k]);                                            //custom for multi relation (embed=relation.relation.relation)
					break;                                                        //custom for multi relation (embed=relation.relation.relation)
				}                                                                //custom for multi relation (embed=relation.relation.relation)
				$count[$k] = $tmp[0];
				$name = $key == 0 ? $this->model : $model;                        //custom for multi relation (embed=relation.relation.relation)
				$model = get_class((new $name)->{$value}()->getRelated());        //custom for multi relation (embed=relation.relation.relation)
			}                                                                    //custom for multi relation (embed=relation.relation.relation)
		}
		$query->with($embed)->withCount($count);
		if ($request->query('id')) {
			$id_user = $request->query('id');
			$query->with(array('last_documents' => function ($query) use ($id_user) {
				$query->where('last_documents.user_id', $id_user);
			}));
		}

		// Filtered model's fields
		foreach($block = explode(';', $request->query('filters')) as $key => $value) {
			$query->orwhere(function ($q) use ($value) {

				foreach ($filters = explode(',', $value) as $k => $v) {
					if (empty($v)) {
						unset($filters[$k]);

					} else {

						$parts = explode('|', $v);

						$embeded = explode('.', $parts[0]);
						$length = count($embeded);
						$this->where_has($length, 0, $parts, $q, $filters, $embeded, $k, null, null);
					}
				}

			});
		}
		// Search
		if ($search = $request->query('search'))
			$query->search($search);


		// Filtered metas
		foreach($meta_filters = explode(',', $request->query('metas')) as $k => $v) {
			if(empty($v)) {
				unset($meta_filters[$k]);

			} else {
				$parts = explode('|', $v);

				if($parts[1] == 'all') {
					foreach(explode(';', $parts[2]) as $meta_value) {
						$query->whereHas('metas', function($q) use($parts, $meta_value) {
							$q->where([
								['slug', '=', $parts[0]],
								['meta_value_id', '=', str_replace('@', '', $meta_value)]
							]);
						});
					}

				} else {
					$query->whereHas('metas', function($q) use($parts) {
						$filters = [
							['slug', '=', $parts[0]]
						];

						if($parts[1] == 'in') {
							$q->whereIn('meta_value_id', explode(';', str_replace('@', '', $parts[2])));

						} else {
							if(strpos($parts[2], '@') !== false) {
								$filters[] = ['meta_value_id', $parts[1], str_replace('@', '', $parts[2])];
							} else {
								$filters[] = ['label', $parts[1], str_replace('~', '%', $parts[2])]; // For 'like' operator
							}
						}

						$q->where($filters);
					});
				}

			}
		}


		// Pagination, Transformation & return
		if(!is_null($request->query('page'))) {
			$per_page = !is_null($request->query('per_page')) ? $request->query('per_page') : 15;

			$query_result       = $query->paginate($per_page);

			$return = [
				'count'     => $query_result->total(),
				'per_page'  => (int)$query_result->perPage(),
				'current'   => $query_result->currentPage(),
			];

			if($query_result->currentPage() > 1) {
				$return['prev'] = $query_result->currentPage() - 1;
			}

			if($query_result->currentPage() < $query_result->lastPage()) {
				$return['next'] = $query_result->currentPage() + 1;
			}

			$return['data'] = [];

			foreach($query_result->getCollection() as $item) {
				$return['data'][] = (new MetaTransformer)->transform($item);
			}

			return $return;

		} else {
			return $this->response->collection($query->get(), new MetaTransformer);
		}
	}

	/*	@route ::/documents/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/documents::
		@write ::store::*/
	public function store(Request $request){
		$document = $this->api_store($request);
		$data = $request->all();
		$this->sync($data, $document);
		$ext = Extension::where('label', $document->extension)->get();
		if(count($ext) == 0){
			$gr = new Groupe;
			$gr->label = $document->extension;
			$gr->save();
			$ext = new Extension;
			$ext->label = $document->extension;
			$ext->groupe_id = $gr->id;
			$ext->save();
		}
		return($document);
	}

	/*	@route ::/documents/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$document = $this->api_update($request, $id);
		$data = $request->all();
		$this->sync($data, $document);
		$this->un_sync($data, $document);
		return $document;
	}

	/*	@route ::/documents/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}

	/*	@rule ::Get last documents::
	@route ::[GET /users/{id_user}/kamions/{id_kamion}/last_document]::
	@parameters
	+ id_user:(integer, required) - id of the User
	+ id_kamion:(integer, required) - id of the Kamion
	@endparameters
	@responseheader
			Content-Type : application/json
	@endresponseheader
	@write ::custom::*/
	public function last_document($id_user, $id_kamion){
		return array("data" => $this->model
			::WhereHas(
				'last_documents', function($q) use ($id_user){
				$q->where('user_id', '=', $id_user);
			}
			)->WhereHas('kamions',function($q) use ($id_kamion) {
				$q->where('kamion_id', '=', $id_kamion);
			}
			)->with(array('last_documents' => function($query) use ($id_user)
			{
				$query->where('last_documents.user_id', $id_user);
			}))->with('author')->get());
	}

	/*	@rule ::Get last documents::
	@route ::[GET /users/{id_user}/last_document]::
	@parameters
	+ id_user:(integer, required) - id of the User
	@endparameters
	@responseheader
			Content-Type : application/json
	@endresponseheader
	@write ::custom::*/
	public function lastDocument($id_user){
		return $this->model
			::WhereHas(
				'last_documents', function($q) use ($id_user){
				$q->where('user_id', '=', $id_user);
			}
			)->with(array('last_documents' => function($query) use ($id_user)
			{
				$query->where('last_documents.user_id', $id_user);
			}))->with('author')->get();
	}
}
