<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WanaController extends Controller
{
    public function sync(Request $request){
		$dispatcher = app('Dingo\Api\Dispatcher');
        $curl = new \Curl\Curl();
		$curl->get('http://www.liftingsignalisations.fr/metier/');
		foreach (json_decode($curl->response) as $jobs) {
			$dispatcher->post('jobs', ['wana_id' => $jobs->IDMetier, 'label' => $jobs->sLibelle]);
		}
        $curl = new \Curl\Curl();
		$curl->get('http://www.liftingsignalisations.fr/compagnon');
		$users = json_decode($curl->response);
		foreach ($users as $user) {
			$jobs =  \App\Api\v1\Models\Job::where('label', $user->sMetier)->get();
			// dd($jobs[0]->id);
			$nuser = new \App\Api\v1\Models\User;
			$nuser->first_name = isset($user->sPrenom) && $user->sPrenom != "" ? $user->sPrenom : "none";
			$nuser->last_name = isset($user->sNom) && $user->sNom != "" ? $user->sNom : "none";
			$nuser->username = isset($user->sSurnom) && $user->sSurnom != "" ? $user->sSurnom : "none";
			$nuser->password = '0000';
			$nuser->wana_id = isset($user->IDCompagnon) && $user->IDCompagnon != "" ? $user->IDCompagnon."" : "none";
			$nuser->is_active = 1;
			$nuser->role_id = $jobs[0]->id;
			$nuser->save();
			// dump($user->IDCompagnon);
		}
	}
}
