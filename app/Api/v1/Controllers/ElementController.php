<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Kamion;
use App\Api\v1\Models\Notification;
use App\Api\v1\Models\Parameter;
use App\Api\v1\Models\User;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Elements::
	@model ::Element::
	@migration ::create_elements_table::
 */
class ElementController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Element';
	protected $index_fields = ['*'];
	/*	@route ::/elements::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/elements/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/elements::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/elements/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}
	/*	@route ::/elements/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
