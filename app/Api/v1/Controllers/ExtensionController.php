<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class ExtensionController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	// @group ::Extensions::
	// @model ::Extension::
	// @migration ::create_extensions_table::

	protected $model        = 'App\Api\v1\Models\Extension';
	protected $index_fields = ['*'];
	/*
		@route ::/extensions::
		@write ::index::
		*/
	public function index(Request $request){
		return $this->api_index($request);
	}
	/*
		@route ::/extensions/{id}::
		@write ::show::
		*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*
		@route ::/extensions::
		@write ::store::
		*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/extensions/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/extensions/{id}::
		@write ::delete:: */
	public function delete($id){
		return $this->api_delete($id);
	}
}
