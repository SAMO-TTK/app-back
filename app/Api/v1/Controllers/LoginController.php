<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

/**
 * Login resource representation.
 *
 * @Resource("Logins", uri="/logins")
 */
class LoginController extends Controller {

    use ApiController {

        index   as protected api_index;
        //show    as protected api_show;
        //store   as protected api_store;
        //update  as protected api_update;
        //delete  as protected api_delete;
    }

    protected $model        = 'App\Api\v1\Models\Login';
    protected $index_fields = ['id', 'created_at', 'username', 'IP', 'country', 'city'];


    /**
     * Retrieve all logins
     *
     * Default returned fields : `id`, `created_at`, `email`, `IP`, `country`, `city`
     *
     * Extra fields : `updated_at`, `iso_code`, `state_name`, `lat`, `lng`
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("fields",    type="array", required=false, description="Fields to return, comma separated list.<br/>Return all fields with `*`"),
     *      @Parameter("embed",     type="array", required=false, description="Embed linked resources : `parent`, `children`, `contacts`, `metas`"),
     *      @Parameter("filters",   type="array", required=false, description="Allow to filter on the resource fields"),
     *      @Parameter("metas",     type="array", required=false, description="Allow to filter on the resource meta datas")
     * })
     * @Response(200, body={"logins": {{"id": 1},{"id": 2}}})
     */
    public function index(Request $request) {
        return $this->api_index($request);
    }
}
