<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Kamion;
use App\Api\v1\Models\Chapter;
use App\Api\v1\Transformers\MetaTransformer;
/*
	@group ::Model_fields::
	@model ::Model_field::
	@migration ::create_model_fields_table::
 */
class Model_fieldController extends Controller{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Model_field';
	protected $index_fields = ['*'];
	/*	@route ::/model_fields::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/model_fields/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/model_fields::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/model_fields/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/model_fields/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
