<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Notification;
use App\Api\v1\Models\Permission;
use App\Api\v1\Models\Ticket;
use App\Api\v1\Models\User;
use App\Api\v1\Models\NotificationTicket;
use App\Api\v1\Models\UpdateField;
use App\Mail\TicketNotification;
use App\Api\v1\Traits\ApiController;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
	@group ::Notifications::
	@model ::Notification::
	@migration ::create_notifications_table::
 */
class NotificationController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Notification';
	protected $index_fields = ['*'];

	/*	@route ::/notifications::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/notifications/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/notifications::
		@write ::store::*/
	public function store(Request $request){
		$resource = $this->api_store($request);
		$data = $request->all();
		if(isset($data['users'])) {
			$resource->users()->sync($data['users']);
		}
		if(isset($data['tickets'])){
			$ticket = new NotificationTicket();
			$ticket->notification_id = $resource->id;
			$ticket->ticket_id = $data['tickets'];
			$ticket->save();
			foreach ($data['update_fields'] as $field){
				$update = new UpdateField();
				$update->cat_id = $field;
				$update->notification_ticket_id = $ticket->id;
				$update->save();
			}

			//send mail
			if (isset($data['users'])) {
				$this->notifyNewTicketByEmail($data);
			}
		}
		return $resource;
	}

	/*	@route ::/notifications/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$resource = $this->api_update($request, $id);
		$data = $request->all();
		if(isset($data['users'])) {
			foreach ($data['users'] as $user){
				$sync[$user['id']] = array(
					'is_manager'	=> isset($user['is_manager']) ? $user['is_manager'] : 0,
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$resource->users()->sync($sync);
		}
		return $resource;
	}

	/*	@route ::/notifications/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}

	/*	@rule ::Get not read notifications about a ticket::
	@route ::[GET /notifications/tickets/{id}/users/{id_user}]::
	@parameters
		+ id:(integer, required) - id of the Ticket
		+ id_user:(integer, required) - id of the User
	@endparameters
	@responseheader
			Content-Type : application/json
	@endresponseheader
	@write ::custom::*/
	public function select_no_read($id, $user_id){
		$nots = Notification::whereHas('notification_ticket.ticket', function($q) use($id){
			$q->where('tickets.id', $id);
		})->whereHas('noreadusers', function($q) use ($user_id){
			$q->where('users.id',$user_id);
		})->with('notification_ticket.update_fields')->get();
		return($nots);
	}

	/*	@rule ::Mark read all user notifications::
	@route ::[PUT /notifications/users/{user_id}]::
	@parameters
		+ user_id:(integer, required) - id of the User
	@endparameters
	@responseheader
			Content-Type : application/json
	@endresponseheader
	@write ::custom::*/
	public function mark_all_read($user_id){
		$nots = $this->model::whereHas('users',function($q) use($user_id){
			$q->where('users.id', $user_id);
		})->get();
		foreach ($nots as $not){
			DB::table('notification_user')
				->where('user_id', $user_id)
				->where('notification_id', $not->id)
				->update(['is_read' => 1]);
		}
		return $nots;
	}

	/*	@rule ::Mark read a notification::
	@route ::[PUT /notifications/{id}/users/{user_id}]::
	@parameters
		+ id:(integer, required) - id of the Notification
		+ user_id:(integer, required) - id of the User
	@endparameters
	@write ::custom::*/
	public function mark_read($id, $user_id){
		DB::table('notification_user')
			->where('user_id', $user_id)
			->where('notification_id', $id)
			->update(['is_read' => 1]);
	}

	/** Notify the new ticket by send an email
	 * @param array $data
	 */
	private function notifyNewTicketByEmail($data) {
		//get users
		$users = User::whereIn('id', $data['users'])->with(['roles.permissions'])->get();
		//get ticket
		$ticket = Ticket::where('id', $data['tickets'])->with([
			'kamion',
			'kamions',
			'fields.ticket_fields',
			'categories',
			'users',
			'creator',
			'model',
			'status',
			'manager'
		])->get()->first();

		//get permission
		$permission_tomanageticketbyemail = Permission::where('slug', 'tomanageticketbyemail')->get()->first();

		$user_permissions = [];
		$users_permitted = [];//cc
		$user_permitted = null;//from

		//get users permissions and get the from user for email
		foreach ($users as $user) {
			foreach ($user->roles as $role) {
				foreach ($role->permissions as $permission) {
					$user_permissions[$user->id][] = $permission->id;
				}
			}

			//check if they can manage ticket by email
			if (!empty($user->email) && in_array($permission_tomanageticketbyemail->id, $user_permissions[$user->id])) {
				if ($user->id === $ticket->creator_id) {//from
					$user_permitted = $user;
				}
				else {//cc
					$users_permitted[] = $user;
				}
			}
		}

		if ($user_permitted === null && count($users_permitted) > 0) {
			$user_permitted = $users_permitted[0];
			array_shift($users_permitted);
		}

		if ($user_permitted !== null) {
			//Send mail
			//see app/Mail/TicketNotification.php
			Mail::send(new TicketNotification([
				'user'	=> $user_permitted,
				'users' => $users_permitted,
				'ticket' => $ticket,
				'notified_fields' => $data['update_fields']
			]));
		}
	}
}
