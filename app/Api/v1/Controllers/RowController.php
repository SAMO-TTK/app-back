<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Cell;
use App\Api\v1\Models\Field;
use App\Api\v1\Models\Row;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class RowController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::Categories::
	//@model ::Category::
	//@migration ::create_categories_table::
	protected $model        = 'App\Api\v1\Models\Row';
	protected $index_fields = ['*'];

	/*	@route ::/categories::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/categories/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/categories::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}
	private function duplicate_field($field, $model_id){
		if (!$field)
			return null;
		$new = new Field();
		$new->model_id = $model_id;
		$new->type = $field->type;
		$new->value = $field->value;
		$new->save();
		foreach ($field->rows as $row){
			$newRow = new Row();
			$newRow->field_id = $new->id;
			$newRow->position = $row->position;
			$newRow->content = $row->content;
			$newRow->save();
			foreach ($row->cells as $cell){
				$newCell = new Cell();
				$newCell->row_id = $newRow->id;
				$newCell->position = $cell->position;
				$newCell->field_id = $this->duplicate_field($cell->field, $model_id);
				$newCell->save();
			}
		}
		return $new->id;
	}

	public function duplicate(Request $request, $id){
		$row = Row::where('id', $id)->with(['cells'])->get()->first();
		$new = new Row();
		$new->field_id = $row->field_id;
		$new->position = $row->position + 1;
		$new->content = $row->content;
		$new->save();
		foreach ($row->cells as $cell){
			$newCell = new Cell();
			$newCell->row_id = $new->id;
			$newCell->position = $cell->position;
			$newCell->field_id = $this->duplicate_field($cell->field, $request->model_id);
			$newCell->save();

		}
		return($new);
	}
	/*	@route ::/categories/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/categories/{id}::
		@write ::delete::*/
	public function delete_field($field){
		if (!$field){
			return;
		}
		foreach ($field->rows as $row){
			foreach ($row->cells as $cell){
				$this->delete_field($cell->field);
				$cell->delete();
			}
			$row->delete();
		}
		$field->delete();
		return;
	}
	public function delete($id){
		$row = Row::where('id', $id)->with(['cells'])->get()->first();
		foreach ($row->cells as $cell){
			$this->delete_field($cell->field);
			$cell->delete();
		}
		return $this->api_delete($id);
	}
}
