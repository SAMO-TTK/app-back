<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Resources::
	@model ::Resource::
	@migration ::create_resources_table::
 */
class ResourceController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Resource';
	protected $index_fields = ['*'];
	/*	@route ::/resources::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/resources/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/resources::
		@write ::store::*/
	public function store(Request $request){
		$resource = $this->api_store($request);
		$data = $request->all();
		if(isset($data['users'])) {
			foreach ($data['users'] as $user){
				$sync[$user['id']] = array(
					'is_manager'	=> isset($user['is_manager']) ? $user['is_manager'] : 0,
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$resource->users()->sync($sync);
		}
		return $resource;
	}

	/*	@route ::/resources/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$resource = $this->api_update($request, $id);
		$data = $request->all();
		if(isset($data['users'])) {
			foreach ($data['users'] as $user){
				$sync[$user['id']] = array(
					'is_manager'	=> isset($user['is_manager']) ? $user['is_manager'] : 0,
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			}
			$resource->users()->sync($sync);
		}
		return $resource;
	}

	/*	@route ::/resources/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
