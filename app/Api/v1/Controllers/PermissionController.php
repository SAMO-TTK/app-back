<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Permission;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Permissions::
	@model ::Permission::
	@migration ::create_permissions_table::
 */
class PermissionController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Permission';
	protected $index_fields = ['*'];

	/*	@route ::/permissions::
		@write ::index::*/
	public function get_id($slug){
		return Permission::where('slug', $slug)->get()->toArray()[0]['id'];
	}

	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/permissions/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/permissions::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/permissions/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/permissions/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
