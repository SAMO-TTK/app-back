<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class MaterialController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::Materials::
	//@model ::Material::
	//@migration ::create_materials_table::
	protected $model        = 'App\Api\v1\Models\Material';
	protected $index_fields = ['*'];

	/*	@route ::/materials::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/materials/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/materials::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/materials/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/materials/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
