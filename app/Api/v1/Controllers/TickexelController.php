<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Ticket;
use App\Api\v1\Models\User;
use App\Api\v1\Models\Ticket_model;

class TickexelController extends Controller
{

	/*	@rule ::Export ticket to excel forma::
	@route ::[GET /tickets/export]::
	@responseheader
			Content-Type : application/json
	@endresponseheader
	@field
	+ ARRAY: `tickets`
	@endfield
	@write ::custom::*/
    public function export(Request $request){
		$objPHPExcel = new \PHPExcel();
		$tickets = Ticket::select([	'*'	])->whereIn('id', $request->tickets)->with([
			'kamion',
			'kamions',
			'model',
			'fields.values',
			'fields.ticket_fields',
			'status',
			'categories',
			'workgroup.users',
			'users',
			'manager',
			'creator',
			'discussions.messages',
			'discussions.users'
		])->withCount([
			'kamion',
			'kamions',
			'model',
			'fields',
			'status',
			'categories',
			'workgroup',
			'users',
			'manager',
			'creator',
			'discussions'
		])->get();
		$models = Ticket_model::select(['*'])->whereHas('tickets', function ($query)  use ($request){
			$query->whereIn('tickets.id', $request->tickets);
		})->with('fields')->withCount('fields')->get();
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getSheet(0)->getDefaultStyle()->getFont()->setSize(9);
		$objPHPExcel->getSheet(0)->setTitle($models[0]->label);
		$discussions = $this->setDiscution($tickets, $models);
//		dump($discussions);
		if(count($discussions) > 0) {
			$this->dumpHeader($objPHPExcel->getSheet(0), $models[0], $discussions[0]);
		}else{
			$this->dumpHeader($objPHPExcel->getSheet(0), $models[0], array());

		}
		$row = 3;
		foreach ($tickets as $ticket){
			if ($ticket->model_id == $models[0]->id){

				if(count($discussions) > 0) {
					$this->dumpRow($objPHPExcel->getSheet(0), $ticket, $models[0], $discussions[0], $row);
				}else {
					$this->dumpRow($objPHPExcel->getSheet(0), $ticket, $models[0], array(), $row);
				}
				$row++;
			}
		}
		for ($i = 1; $i < count($models); $i++){
			$row = 3;
			$objPHPExcel->createSheet();
			$objPHPExcel->getSheet($i)->getDefaultStyle()->getFont()->setSize(9);
			$objPHPExcel->getSheet($i)->setTitle($models[$i]->label);
			$this->dumpHeader($objPHPExcel->getSheet($i),$models[$i], $discussions[$i]);
			foreach ($tickets as $ticket){
				if ($ticket->model_id == $models[$i]->id){
					$this->dumpRow($objPHPExcel->getSheet($i), $ticket, $models[$i], $discussions[$i], $row);
					$row++;
				}
			}
		}

		$objPHPExcel->getProperties()
			->setCreator("TTK hub")
			->setLastModifiedBy("TTK hub")
			->setTitle("TTK Ticket generated")
			->setSubject("TTK Ticket generated")
			->setDescription("TTK Ticket generated");


		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename='Ticket-Hub-TTK-".time().".xlsx'");
		header('Cache-Control: max-age=0');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		$objPHPExcel->disconnectWorksheets();
		unset($objWriter);
		unset($objPHPExcel);

	}

	private function dumpRow($sheet, $ticket, $model, $discussions, $row){
		$sheet->getCell('A'.$row)->setValueExplicit($ticket->number, \PHPExcel_Cell_DataType::TYPE_STRING);
		$tmp1 = $ticket->kamion->wana_id;
    	$tmp2 = $ticket->kamion->name;
    	foreach ($ticket->kamions as $k){
			$tmp1 .= ';'.$k->wana_id;
			$tmp2 .= ';'.$k->name;
		}
		$sheet->getCell('B'.$row)->setValueExplicit($tmp1, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('C'.$row)->setValueExplicit($tmp2, \PHPExcel_Cell_DataType::TYPE_STRING);
		$tmp = '';
		foreach ($ticket->categories as $k){
			$tmp .= $tmp == '' ? $k->label : ';'.$k->label;
		}
		$sheet->getCell('D'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->manager){
			$tmp = $ticket->manager->last_name.' '.$ticket->manager->first_name;
		}else{
			$tmp = '';
		}
		$sheet->getCell('E'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('F'.$row)->setValueExplicit($ticket->deadline, \PHPExcel_Cell_DataType::TYPE_STRING);
		$tmp = '';
		foreach ($ticket->users as $k){
			$tmp .= $tmp == '' ? $k->last_name.' '.$k->first_name : ';'.$k->last_name.' '.$k->first_name;
		}
		$sheet->getCell('G'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->creator){
			$tmp = $ticket->creator->last_name.' '.$ticket->creator->first_name;
		}else{
			$tmp = '';
		}
		$sheet->getCell('H'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->status){
			$tmp = $ticket->status->label;
		}else{
			$tmp = '';
		}
		$sheet->getCell('I'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		$tmp = $ticket->is_public == 1 ? 'Puplique' : 'Priver';
		$sheet->getCell('J'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('K'.$row)->setValueExplicit($ticket->created_at, \PHPExcel_Cell_DataType::TYPE_STRING);
//		$sheet->getCell('L'.$row)->setValueExplicit('N°  d\'instruction liée', \PHPExcel_Cell_DataType::TYPE_STRING);
		$tmp = '';
		if ($ticket->workgroup != null){
			foreach ($ticket->workgroup->users as $k){
				$tmp .= $tmp == '' ? $k->last_name.' '.$k->first_name : ';'.$k->last_name.' '.$k->first_name;
			}
		}
		$sheet->getCell('M'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->workgroup != null){
			$tmp = $ticket->workgroup->target;
		}else{
			$tmp = '';
		}
		$sheet->getCell('N'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->workgroup != null){
			$tmp = $ticket->workgroup->delay;

		}else{
			$tmp = '';
		}
		$sheet->getCell('O'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->workgroup != null){
			$tmp = $ticket->workgroup->hours;
		}else{
			$tmp = '';
		}
		$sheet->getCell('P'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->workgroup != null){
			$tmp = $ticket->workgroup->planning;
		}else{
			$tmp = '';
		}
		$sheet->getCell('Q'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->workgroup != null){
			$tmp = $ticket->workgroup->report;
		}else{
			$tmp = '';
		}
		$sheet->getCell('R'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		if ($ticket->workgroup != null){
			$tmp = $ticket->workgroup->decision;
		}else{
			$tmp = '';
		}
		$sheet->getCell('S'.$row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('T'.$row)->setValueExplicit($ticket->quality_committee, \PHPExcel_Cell_DataType::TYPE_STRING);

		//model

		$C = 'U';
		foreach ($ticket->fields as $field){
			if($field->ticket_fields->type != 'title') {
				if ($field->value && $field->value != ""){
//					dump($field->ticket_fields->position );
					if ($field->ticket_fields->type == "selectN"){
						$tt = explode(',', $field->value);
						foreach ($tt as $t){
							if ($t != ""){
								$tu = User::find($t);
								$tmp .= $tmp == '' ? $tu->last_name.' '.$tu->first_name : ';'.$tu->last_name.' '.$tu->first_name;
							}
						}
						$sheet->getCell($this->pp($C, $field->ticket_fields->position - 1) . $row)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);

					}elseif($field->ticket_fields->type == "selectN") {
						$sheet->getCell($this->pp($C, $field->ticket_fields->position - 1) . $row)->setValueExplicit($field->value == 1 ? "true" : "false", \PHPExcel_Cell_DataType::TYPE_STRING);
					}else {
						$sheet->getCell($this->pp($C, $field->ticket_fields->position - 1) . $row)->setValueExplicit($field->value, \PHPExcel_Cell_DataType::TYPE_STRING);
					}
				}else{
					if($field->values) {
						$sheet->getCell($this->pp($C, $field->ticket_fields->position - 1) . $row)->setValueExplicit($field->values->value, \PHPExcel_Cell_DataType::TYPE_STRING);
					}
				}
			}else{
				$sheet->getCell($this->pp($C, $field->ticket_fields->position - 1) . $row)->setValueExplicit("////", \PHPExcel_Cell_DataType::TYPE_STRING);
			}
		}

		// discussion
		$D = $this->pp('U',$this->field_count($model->fields));

		foreach ($ticket->discussions as $k => $d){
			$C = $D;
			if (isset($d->users[0])) {
				$sheet->getCell($C . $row)->setValueExplicit($d->users[0]->last_name . ' ' . $d->users[0]->first_name, \PHPExcel_Cell_DataType::TYPE_STRING);
			}
			$C++;
			$sheet->getCell($C . $row)->setValueExplicit($d->deadline , \PHPExcel_Cell_DataType::TYPE_STRING);
			$C++;
			$sheet->getCell($C . $row)->setValueExplicit($d->content , \PHPExcel_Cell_DataType::TYPE_STRING);
			$C++;
			foreach ($d->messages as $m){
				$sheet->getCell($C . $row)->setValueExplicit($m->content, \PHPExcel_Cell_DataType::TYPE_STRING);
				$C++;
			}
			$D = $this->pp($D, $discussions[$k]+2);
		}
	}

	private function setDiscution($tickets, $models){
    	$ret = [];
    	foreach ($models as $k => $model){
    		foreach ($tickets as $t => $ticket){
    			foreach ($ticket->discussions as $l => $discussion){
    				if ($ticket->model_id == $model->id){
    					if (!isset($ret[$k])){
    						$ret[$k] = [];
						}
						if (!isset($ret[$k][$l]) || $ret[$k][$l] < count($discussion->messages) + 1){
    						$ret[$k][$l] = count($discussion->messages) + 1;

						}
					}
				}
			}
		}
		return $ret;
	}

	private function pp($c,$n){for($z=0;$z<$n;$z++){$c++;}return $c;}

	private function field_count($fields){
    	$i = 0;
    	foreach ($fields as $field){
    		$i++;
		}
		return($i);
	}

	private function dumpHeader($sheet, $model, $discussions){

		$sheet->mergeCells('A1:L1');
		$sheet->mergeCells('M1:S1');
		$sheet->getCell('A1')->setValueExplicit('HEADER', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('M1')->setValueExplicit('Groupe de travail', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('T1')->setValueExplicit('Comité Qualité', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A2')->setValueExplicit('N° TTK', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getCell('B2')->setValueExplicit('N° Kamion', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getCell('C2')->setValueExplicit('Nom  Kamion', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getCell('D2')->setValueExplicit('Catégories', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getCell('E2')->setValueExplicit('Responsable', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getCell('F2')->setValueExplicit('Date butoir', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getCell('G2')->setValueExplicit('Destinataires', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getCell('H2')->setValueExplicit('Rédacteur', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getCell('I2')->setValueExplicit('Etat', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getCell('J2')->setValueExplicit('Visibilité', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getCell('K2')->setValueExplicit('Date de création', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getCell('L2')->setValueExplicit('N°  d\'instruction liée', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('L')->setAutoSize(true);
		$sheet->getCell('M2')->setValueExplicit('Participants', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('M')->setAutoSize(true);
		$sheet->getCell('N2')->setValueExplicit('Objectif', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('N')->setAutoSize(true);
		$sheet->getCell('O2')->setValueExplicit('Délai', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('O')->setAutoSize(true);
		$sheet->getCell('P2')->setValueExplicit('Nb d\'heures', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('P')->setAutoSize(true);
		$sheet->getCell('Q2')->setValueExplicit('Planning', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('Q')->setAutoSize(true);
		$sheet->getCell('R2')->setValueExplicit('Compte rendu', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('R')->setAutoSize(true);
		$sheet->getCell('S2')->setValueExplicit('Décision', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('S')->setAutoSize(true);
		$sheet->getCell('T2')->setValueExplicit('Commentaires', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getColumnDimension('T')->setAutoSize(true);

		// model
		$sheet->mergeCells('U1:'.$this->pp('U',$this->field_count($model->fields) - 1).'1');
		$sheet->getCell('U1')->setValueExplicit($model->label, \PHPExcel_Cell_DataType::TYPE_STRING);
		$C = 'U';
//		dd($tmp);
		foreach ($model->fields as $field){
				$sheet->getCell($this->pp($C, $field->position - 1) . '2')->setValueExplicit($field->label, \PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getColumnDimension($this->pp($C, $field->position - 1))->setWidth(25);
		}
		// discussions
		$C = $this->pp('U',$this->field_count($model->fields));
//		dump($discussions);
		if($discussions) {
			foreach ($discussions as $k => $discussion) {
				$sheet->mergeCells($C . '1:' . $this->pp($C, $discussion) . '1');
				$sheet->getCell($C . '1')->setValueExplicit('Debat ' . $k, \PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getCell($C . '2')->setValueExplicit('Responsable ' , \PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getColumnDimension($C)->setWidth(25);
				$C++;
				$sheet->getCell($C . '2')->setValueExplicit('Date butoir  ' , \PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getColumnDimension($C)->setWidth(25);
				$C++;
				for ($i = 0; $i < $discussion; $i++) {
					$sheet->getColumnDimension($C)->setWidth(25);
					$sheet->getCell($C . '2')->setValueExplicit('message ' . $i, \PHPExcel_Cell_DataType::TYPE_STRING);
					$C++;
				}
			}
		}
	}
}
//		$sheet->getStyle('A1')->getAlignment()->setWrapText(true);
