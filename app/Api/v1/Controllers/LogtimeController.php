<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Timecard;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class LogtimeController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::Logtimes::
	//@model ::Logtime::
	//@migration ::create_logtimes_table::
	protected $model        = 'App\Api\v1\Models\Logtime';
	protected $index_fields = ['*'];

	/*	@route ::/logtimes::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/logtimes/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/logtimes::
		@write ::store::*/
	public function store(Request $request){
		$logtime = $this->api_store($request);
		$timecard = Timecard::where('id', $logtime->timecard_id)->with('logtimes')->get()->first();
		$tmpcumul = 0;
		$tmpprocess = 2;
		foreach ($timecard->logtimes as $log){
			$tmpcumul += $log->length;
			if ($tmpprocess && ($log->status_logtime_id == 3 || $log->status_logtime_id == 4 || $log->status_logtime_id == 5 || $log->status_logtime_id == 6 )){
				$tmpprocess = 1;
			}
			if ($log->status_logtime_id == 1 || $log->status_logtime_id == 2){
				$tmpprocess = 0;
			}
		}
		$timecard->cumul = $tmpcumul;
		$timecard->process_step = $tmpprocess;
		$timecard->save();
		return $logtime;
	}

	/*	@route ::/logtimes/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$logtime = $this->api_update($request, $id);
		$timecard = Timecard::where('id', $logtime->timecard_id)->with('logtimes')->get()->first();
		$tmpcumul = 0;
		$tmpprocess = 2;
		foreach ($timecard->logtimes as $log){
			$tmpcumul += $log->length;
			if ($tmpprocess && ($log->status_logtime_id == 3 || $log->status_logtime_id == 4 || $log->status_logtime_id == 5 || $log->status_logtime_id == 6 )){
				$tmpprocess = 1;
			}
			if ($log->status_logtime_id == 1 || $log->status_logtime_id == 2){
				$tmpprocess = 0;
			}
		}
		$timecard->cumul = $tmpcumul;
		$timecard->process_step = $tmpprocess;
		$timecard->save();
		return $logtime;
	}

	/*	@route ::/logtimes/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
