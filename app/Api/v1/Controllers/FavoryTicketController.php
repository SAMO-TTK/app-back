<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\FavoryTicket;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Kamion;
use App\Api\v1\Models\Chapter;
use App\Api\v1\Transformers\MetaTransformer;
/*
	@group ::FavoryTickets::
	@model ::FavoryTicket::
	@migration ::create_favory_tickets_table::
 */
class FavoryTicketController extends Controller{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\FavoryTicket';
	protected $index_fields = ['*'];
	/*	@route ::/favory_tickets::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/favory_tickets/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/favory_tickets::
		@write ::store::*/
	public function store(Request $request){
		$test = FavoryTicket::where('user_id', $request->user_id)->where('ticket_id', $request->ticket_id)->get();
		if($test->count() == 0) {
			return $this->api_store($request);
		}
	}

	/*	@route ::/favory_tickets/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@rule ::Delete favory ticket::
	@route ::[DELETE /favory_tickets/{id}/{user_id}]::
	@parameters
	+ id:(integer, required) - id of the favory ticket
	+ user_id:(integer, required) - id of the User
	@endparameters
	@write ::custom::*/
	public function delete($id, $user_id){
		$test = FavoryTicket::where('user_id', $user_id)->where('ticket_id', $id)->get();
		if($test->count() != 0) {
			return $this->api_delete($test[0]->id);
		}
	}
}
