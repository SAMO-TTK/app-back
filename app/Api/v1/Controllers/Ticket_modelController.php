<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Kamion;
use App\Api\v1\Models\Chapter;
use App\Api\v1\Transformers\MetaTransformer;
/*
	@group ::Ticket_models::
	@model ::Ticket_model::
	@migration ::create_ticket_models_table::
 */
class Ticket_modelController extends Controller{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Ticket_model';
	protected $index_fields = ['*'];

	/*	@route ::/ticket_models::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/ticket_models/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/ticket_models::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/ticket_models/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/ticket_models/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
