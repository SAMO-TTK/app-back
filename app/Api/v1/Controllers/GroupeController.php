<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class GroupeController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	// @group ::Groupes::
	// @model ::Groupe::
	// @migration ::create_groupes_table::

	protected $model        = 'App\Api\v1\Models\Groupe';
	protected $index_fields = ['*'];
	private function sync($data, $document){
		if(isset($data['applications'])) {
			foreach ($data['applications'] as $user) {
				$sync[$user] = array(
				);
			}
			$document->applications()->sync($sync);
		}else{
			$document->applications()->sync([]);
		}
	}
	/*
		@route ::/groupes::
		@write ::index::
		*/
	public function index(Request $request){
		return $this->api_index($request);
	}
	/*
		@route ::/groupes/{id}::
		@write ::show::
		*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*
		@route ::/groupes::
		@write ::store::
		*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/groupes/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$document = $this->api_update($request, $id);
		$data = $request->all();
		$this->sync($data, $document);
		return($document);
	}

	/*	@route ::/groupes/{id}::
		@write ::delete:: */
	public function delete($id){
		return $this->api_delete($id);
	}
}
