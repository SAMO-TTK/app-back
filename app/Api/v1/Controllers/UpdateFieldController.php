<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class UpdateFieldController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::UpdateFields::
	//@model ::UpdateField::
	//@migration ::create_update_fields_table::
	protected $model        = 'App\Api\v1\Models\UpdateField';
	protected $index_fields = ['*'];

	/*	@route ::/update_fields::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/update_fields/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/update_fields::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/update_fields/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/update_fields/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
