<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Cell;
use App\Api\v1\Models\Field;
use App\Api\v1\Models\Modell;
use App\Api\v1\Models\Row;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class ModelController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::Categories::
	//@model ::Category::
	//@migration ::create_categories_table::
	protected $model        = 'App\Api\v1\Models\Modell';
	protected $index_fields = ['*'];

	/*	@route ::/categories::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/categories/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/categories::
		@write ::store::*/
	public function store(Request $request){
		$model =  $this->api_store($request);
		$row = new Row();
		$row->model_id = $model->id;
		$row->position = 0;
		$row->save();
		$cell = new Cell();
		$cell->row_id = $row->id;
		$cell->position = 0;
		$cell->save();
		return $model;
	}

	private function duplicate_field($field, $model_id){
		if (!$field)
			return null;
		$new = new Field();
		$new->model_id = $model_id;
		$new->type = $field->type;
		$new->value = $field->value;
		$new->save();
		foreach ($field->rows as $row){
			$newRow = new Row();
			$newRow->field_id = $new->id;
			$newRow->position = $row->position;
			$newRow->content = $row->content;
			$newRow->save();
			foreach ($row->cells as $cell){
				$newCell = new Cell();
				$newCell->row_id = $newRow->id;
				$newCell->position = $cell->position;
				$newCell->field_id = $this->duplicate_field($cell->field, $model_id);
				$newCell->save();
			}
		}
		return $new->id;
	}

	public function duplicate(Request $request, $id){
		$model = Modell::where('id', $id)->with(['rows'])->get()->first();
		$new = new Modell();
		$new->is_active = 0;
		$new->is_model = $request->is_model;
		$new->name = $request->name;
		$new->save();
		foreach ($model->rows as $row){
			$newRow = new Row();
			$newRow->model_id = $new->id;
			$newRow->position = $row->position;
			$newRow->content = $row->content;
			$newRow->save();
			foreach ($row->cells as $cell){
				$newCell = new Cell();
				$newCell->row_id = $newRow->id;
				$newCell->position = $cell->position;
				$newCell->field_id = $this->duplicate_field($cell->field, $new->id);
				$newCell->save();
			}
		}
		return($new);
	}

	/*	@route ::/categories/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/categories/{id}::
		@write ::delete::*/
	public function delete_field($field){
		if (!$field){
			return;
		}
		foreach ($field->rows as $row){
			foreach ($row->cells as $cell){
				$this->delete_field($cell->field);
				$cell->delete();
			}
			$row->delete();
		}
		$field->delete();
		return;
	}
	public function delete($id){
		$model = Modell::where('id', $id)->with(['rows'])->get()->first();
		foreach ($model->rows as $row){
			foreach ($row->cells as $cell){
				$this->delete_field($cell->field);
				$cell->delete();
			}
			$row->delete();
		}
		return $this->api_delete($id);
	}
}
