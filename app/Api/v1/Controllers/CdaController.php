<?php
namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Discussion;
use App\Api\v1\Models\Impact;
use App\Api\v1\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Chapter;
use App\Api\v1\Models\Kamion;
use App\Api\v1\Models\Element;
use App\Api\v1\Models\Quality_control;
use App\Api\v1\Models\Validation_control;
use App\Api\v1\Models\Supplement;
use phpoffice\phpexcel\PHPExcel;

// @group ::Chemin des attentes::
class CdaController extends Controller
{
	private $color = array(
		'ff1abc9c',
		'ffcfe2f3',
		'ffead1dc',
		'fffff2cc'
	);

	private function create_chapter_i($sheet){
		$numbering = array();
		$i = 1;
		while($sheet->getCell('C'.$i) != ""){
			$i++;
			$num = $sheet->getCell('C'.$i)->getValue();
			if (in_array($num, $numbering)){
				return (object) array('error' => (object) array('code' => 1, 'message' => 'numbering: "'.$num.'" is duplicate row '.$i));
			}else{
				$numbering[] = $num;
			}
		}
		$stack = array();
		$i = 2;
		while(1){
//			dump('a');
			$i++;
			$curent = preg_match_all("/\.?[0-9]?[0-9]/", ($num_curent = explode(':', $sheet->getCell('C'.$i)->getValue()))[0]);
			$prev = preg_match_all("/\.?[0-9]?[0-9]/", ($num_prev = explode(':', $cell_prev = $sheet->getCell('C'.($i - 1))->getValue()))[0]);
			$next = preg_match_all("/\.?[0-9]?[0-9]/", ($num_next = explode(':', $cell_next = $sheet->getCell('C'.($i + 1))->getValue()))[0]);

//			dd($next, $num_next, $cell_next, $i);
			if (isset($num_curent[1])){
				$element = preg_match_all("/:?\.?[0-9]?[0-9]/", $num_curent[1]);
			}
			$id = end($stack);
			$id = $id == false ? NULL : $id;
			if(!isset($num_curent[1]) && $curent <= 3 && $num_curent[0] !== $num_prev[0]){
				$model = new Chapter;
				// $model->imported_at = date('Y-m-d H:i:s');
//                dump($sheet->getCell('A'.$i)->getValue());
//                dump($sheet->getCell('C'.$i)->getValue());
				$model->numbering = $sheet->getCell('C'.$i)->getValue()."";
				$model->title = $sheet->getCell('E'.$i)->getValue();
				$model->kamion_id = request()->kamion_id;
				$model->parent_id = $id;
				$model->validate()->save();
				if($curent < $next){
					$stack[] = $model->id;
				}
			}
			if (isset($num_curent[1]) && $element == 1){
				$id = end($stack);
				$elem = new Element;
				// $elem->imported_at = date('Y-m-d H:i:s');
				$elem->numbering = $sheet->getCell('C'.$i)->getValue();
				$elem->references = $sheet->getCell('D'.$i)->getValue();
				$elem->content = $sheet->getCell('E'.$i)->getValue();
				$elem->chapter_id = $model->id;
				$elem->type = $sheet->getCell('A'.$i)->getValue();
				$elem->is_open = 0;
				$elem->is_client_requirement =  -1;
				$elem->validate()->save();
				$supp = new Supplement;
				$supp->element_id = $elem->id;
				$supp->save();
				$val = new Validation_control;
				$val->element_id = $elem->id;
				$val->is_controled = -1;
				$val->save();
				$qual = new Quality_control;
				$qual->element_id = $elem->id;
				$qual->save();
				$imp = new Impact;
				$imp->element_id = $elem->id;
				$imp->save();
				unset($element);
			}
			if($curent > $next){
				$index = count($stack) - $next +1;
				while($index){
					array_pop($stack);
					$index--;
				}
			}
			if($next == 0){
				break;
			}
		}
		return false;
	}

	private function dump_all($all, $sheet, $i = 0){
		foreach ($all as $key => $value) {
			if (is_object($value)){
				// dump($value->childrens);
				$i++;
				$sheet->getRowDimension($i)->setRowHeight(-1);
				$sheet->getCell('A'.$i)->setValue(hash('md5', $value->id.$value->numbering.$value->title));
				$sheet->getCell('B'.$i)->setValue($value->id);
				// $sheet->getStyle('B'.$i)->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_PROTECTED);
				$sheet->getCell('C'.$i)->setValueExplicit($value->numbering, \PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getCell('E'.$i)->setValueExplicit($value->title, \PHPExcel_Cell_DataType::TYPE_STRING);
				$num = preg_match_all("/\.?[0-9]?[0-9]/", (explode(':', $value->numbering))[0]);
				$sheet->getStyle('B'.$i.':E'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
				$sheet->getStyle('B'.$i.':E'.$i)->getFill()->getStartColor()->setARGB($this->color[$num]);
				$sheet->getStyle('B'.$i.':E'.$i)->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
				$i = $this->dump_all($value->childrens, $sheet, $i);
				if (isset($value->elements)){
					foreach ($value->elements as $evalue) {
						$i++;
						$sheet->getStyle('E'.$i)->getAlignment()->setWrapText(true);
						$sheet->getRowDimension($i)->setRowHeight(-1);
						$sheet->getCell('A'.$i)->setValue(hash('md5', $evalue->id.$evalue->numbering.$value->references.$evalue->content));
						$sheet->getCell('B'.$i)->setValue($evalue->id);
						// $sheet->getStyle('B'.$i)->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_PROTECTED);
						$sheet->getCell('C'.$i)->setValueExplicit($evalue->numbering, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('D'.$i)->setValueExplicit($evalue->references, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('E'.$i)->setValueExplicit($evalue->content, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getStyle('B'.$i.':E'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
						$sheet->getStyle('B'.$i.':E'.$i)->getFill()->getStartColor()->setARGB('fff8f8f8');
						$sheet->getStyle('B'.$i.':E'.$i)->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
					}
				}
			}
		}
		return $i;
	}

	private function full_dump_all($all, $kamion, $sheet, $i = 2){
		$sheet->freezePane('B3');
		foreach ($all as $key => $value) {
			if (is_object($value)){
				// dump($value->childrens);
				$i++;
				$sheet->getStyle('A'.$i.':U'.$i)->applyFromArray(array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_HAIR, 'color' => array('argb' => 'ffbfbfbf')))));
				// $sheet->getRowDimension($i)->setRowHeight(13 + (11 * substr_count($value->title, "\n")));
				// $sheet->getStyle('B'.$i)->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_PROTECTED);
				$sheet->getRowDimension($i)->setRowHeight(-1);
				$sheet->getCell('A'.$i)->setValueExplicit($value->numbering, \PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getCell('C'.$i)->setValueExplicit($value->title, \PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getStyle('C'.$i)->getAlignment()->setWrapText(true);
				$num = preg_match_all("/\.?[0-9]?[0-9]/", (explode(':', $value->numbering))[0]);
				$sheet->getStyle('A'.$i.':U'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
				$sheet->getStyle('A'.$i.':U'.$i)->getFill()->getStartColor()->setARGB($this->color[$num]);
				$sheet->getStyle('O'.$i)->getFill()->getStartColor()->setARGB('ffbfbfbf');
				$sheet->getStyle('S'.$i)->getFill()->getStartColor()->setARGB('ffbfbfbf');
				// $sheet->getStyle('A'.$i.':U'.$i)->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
				$i = $this->full_dump_all($value->childrens, $kamion, $sheet, $i);
				if (isset($value->elements)){
					foreach ($value->elements as $element) {
						$i++;
						$sheet->getStyle('A'.$i.':U'.$i)->applyFromArray(array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_HAIR, 'color' => array('argb' => 'ffbfbfbf')))));
						// $sheet->getRowDimension($i)->setRowHeight(13 + (11 * substr_count($element->content, "\n")));
						// $sheet->getStyle('B'.$i)->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_PROTECTED);
						$sheet->getRowDimension($i)->setRowHeight(-1);
						$sheet->getCell('A'.$i)->setValueExplicit($element->numbering, \PHPExcel_Cell_DataType::TYPE_STRING);
						isset($element->references) ? $sheet->getCell('B'.$i)->setValueExplicit($element->references, \PHPExcel_Cell_DataType::TYPE_STRING) : 0;
						$sheet->getCell('C'.$i)->setValueExplicit($element->content, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getStyle('C'.$i)->getAlignment()->setWrapText(true);
						isset($element->supplement->content) ? $sheet->getCell('D'.$i)->setValueExplicit($element->supplement->content, \PHPExcel_Cell_DataType::TYPE_STRING) : 0;
						$sheet->getStyle('D'.$i)->getAlignment()->setWrapText(true);
						$sheet->getCell('E'.$i)->setValueExplicit($element->is_client_requirement == 1 ? $kamion->client : 'TTK', \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('G'.$i)->setValueExplicit($element->is_client_requirement == 1 ? 'O' : 'N', \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('H'.$i)->setValueExplicit($element->is_open == 1 ? 'O' : 'C', \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('I'.$i)->setValueExplicit(date('Y-m-d',strtotime($element->created_at)) , \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('K'.$i)->setValueExplicit(isset($element->impact->delai) ? 'O': 'N', \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('L'.$i)->setValueExplicit(isset($element->impact->amount) ? 'O': 'N', \PHPExcel_Cell_DataType::TYPE_STRING);
						$tmp = '';
						foreach ($element->user_files as $user_file){
							$tmp .= $user_file->title." : ".$user_file->url."\n";
						}

						$sheet->getCell('N'.$i)->setValueExplicit($tmp, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('P'.$i)->setValueExplicit($element->quality_controls->code, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('Q'.$i)->setValueExplicit($element->quality_controls->type, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('R'.$i)->setValueExplicit($element->quality_controls->note, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getStyle('R'.$i)->getAlignment()->setWrapText(true);
						$sheet->getCell('T'.$i)->setValueExplicit($element->validation_controls->is_controled, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getCell('U'.$i)->setValueExplicit($element->validation_controls->note, \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->getStyle('U'.$i)->getAlignment()->setWrapText(true);
						$sheet->getStyle('A'.$i.':U'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
						$sheet->getStyle('A'.$i.':U'.$i)->getFill()->getStartColor()->setARGB('fff8f8f8');
						$sheet->getStyle('O'.$i)->getFill()->getStartColor()->setARGB('ffbfbfbf');
						$sheet->getStyle('S'.$i)->getFill()->getStartColor()->setARGB('ffbfbfbf');
						// $sheet->getStyle('A'.$i.':U'.$i)->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
						$dis_num = "01";
						// dd($element);
						if (isset($element->discussions)){
							foreach ($element->discussions as $discussion) {
								$i++;
								$sheet->getStyle('A'.$i.':U'.$i)->applyFromArray(array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_HAIR, 'color' => array('argb' => 'ffbfbfbf')))));
								// $sheet->getRowDimension($i)->setRowHeight(13 + (11 * substr_count($discussion->content, "\n")));
								$sheet->getRowDimension($i)->setRowHeight(-1);
								$sheet->getCell('A'.$i)->setValueExplicit($element->numbering.'.'.sprintf('%02d', $dis_num), \PHPExcel_Cell_DataType::TYPE_STRING);
								$sheet->getCell('C'.$i)->setValueExplicit($discussion->content, \PHPExcel_Cell_DataType::TYPE_STRING);
								$sheet->getStyle('C'.$i)->getAlignment()->setWrapText(true);
								if($discussion->from_client != 1){
									foreach ($discussion->users as $user) {
										if ($user->pivot->is_author == 1){
											$sheet->getCell('E'.$i)->setValueExplicit($user->username, \PHPExcel_Cell_DataType::TYPE_STRING);
											break;
										}else{
											$sheet->getCell('E'.$i)->setValueExplicit('TTK', \PHPExcel_Cell_DataType::TYPE_STRING);
										}
									}
								}else{
									$sheet->getCell('E'.$i)->setValueExplicit($kamion->client, \PHPExcel_Cell_DataType::TYPE_STRING);
								}
								$sheet->getCell('H'.$i)->setValueExplicit($discussion->is_open == 1 ? 'O' : 'C', \PHPExcel_Cell_DataType::TYPE_STRING);
								$sheet->getCell('I'.$i)->setValueExplicit(date('Y-m-d',strtotime($discussion->created_at)) , \PHPExcel_Cell_DataType::TYPE_STRING);
								$sheet->getCell('J'.$i)->setValueExplicit(date('Y-m-d',strtotime($discussion->deadline)) , \PHPExcel_Cell_DataType::TYPE_STRING);
								$sheet->getStyle('A'.$i.':U'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
								$sheet->getStyle('A'.$i.':U'.$i)->getFill()->getStartColor()->setARGB('ffffe8f8');
								$sheet->getStyle('O'.$i)->getFill()->getStartColor()->setARGB('ffbfbfbf');
								$sheet->getStyle('S'.$i)->getFill()->getStartColor()->setARGB('ffbfbfbf');
								// $sheet->getStyle('A'.$i.':U'.$i)->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
								$mes_num = "01";
								if (isset($discussion->messages)){
									foreach ($discussion->messages as $message ) {
										$i++;
										$sheet->getStyle('A'.$i.':U'.$i)->applyFromArray(array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_HAIR, 'color' => array('argb' => 'ffbfbfbf')))));
										// $sheet->getRowDimension($i)->setRowHeight(13 + (11 * substr_count($message->content, "\n")));
										$sheet->getRowDimension($i)->setRowHeight(-1);
										$sheet->getCell('A'.$i)->setValueExplicit($element->numbering.'.'.sprintf('%02d', $dis_num).'.'.sprintf('%02d', $mes_num), \PHPExcel_Cell_DataType::TYPE_STRING);
										$sheet->getCell('C'.$i)->setValueExplicit($message->content, \PHPExcel_Cell_DataType::TYPE_STRING);
										$sheet->getStyle('C'.$i)->getAlignment()->setWrapText(true);
										$sheet->getCell('E'.$i)->setValueExplicit($message->user->username, \PHPExcel_Cell_DataType::TYPE_STRING);
										$sheet->getCell('I'.$i)->setValueExplicit(date('Y-m-d',strtotime($message->created_at)) , \PHPExcel_Cell_DataType::TYPE_STRING);
										$sheet->getCell('J'.$i)->setValueExplicit(date('Y-m-d',strtotime($discussion->deadline)) , \PHPExcel_Cell_DataType::TYPE_STRING);
										$sheet->getStyle('A'.$i.':U'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
										$sheet->getStyle('A'.$i.':U'.$i)->getFill()->getStartColor()->setARGB('fff8ffe8');
										$sheet->getStyle('O'.$i)->getFill()->getStartColor()->setARGB('ffbfbfbf');
										$sheet->getStyle('S'.$i)->getFill()->getStartColor()->setARGB('ffbfbfbf');
										// $sheet->getStyle('A'.$i.':U'.$i)->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
										$mes_num++;
									}
								}
								$dis_num++;
							}
						}
					}
				}
			}
		}
		return $i;
	}

	private function update_chapter_i($sheet, $kamion_id){
		$i = 0;
		$stack = array();
		$insert = array();
		$update = array();

		while(1){
			$i++;
			$raw_i = $sheet->getCell('B'.$i)->getValue();
			$raw_n = $sheet->getCell('C'.$i)->getValue();
			$raw_r = $sheet->getCell('D'.$i)->getValue();
			$raw_t = $sheet->getCell('E'.$i)->getValue();
			$curent = preg_match_all("/\.?[0-9]?[0-9]/", ($num_curent = explode(':', $raw_n))[0]);
			$prev = preg_match_all("/\.?[0-9]?[0-9]/", ($num_prev = explode(':', $cell_prev = $sheet->getCell('C'.($i - 1))->getValue()))[0]);
			$next = preg_match_all("/\.?[0-9]?[0-9]/", ($num_next = explode(':', $cell_next = $sheet->getCell('C'.($i + 1))->getValue()))[0]);
			if (isset($num_curent[1])){
				$element = preg_match_all("/:?\.?[0-9]?[0-9]/", $num_curent[1]);
			}
			$id = end($stack);
			$id = $id == false ? NULL : $id;
			if(!isset($num_curent[1]) && $curent <= 3 && $num_curent[0] !== $num_prev[0]){
				if ($sheet->getCell('B'.($i))->getValue() != ""){
					if ($sheet->getCell('A'.$i)->getValue() != hash('md5', $raw_i.$raw_n.$raw_t)){
						$model = Chapter::find($raw_i);
						// $model->imported_at = date('Y-m-d H:i:s');
						$model->numbering = $raw_n;
						$model->title = $raw_t;
						$model->parent_id = $id;
						$model->validate()->save();
						$update[] = 'chapter:'.$model->id.", raw:".$i;
					}
				}
				else{
					$model = new Chapter;
					// $model->imported_at = date('Y-m-d H:i:s');
					$model->numbering = $raw_n;
					$model->title = $raw_t;
					$model->kamion_id = $kamion_id;
					$model->parent_id = $id;
					$model->validate()->save();
					$insert[] = 'chapter:'.$model->id.", raw:".$i;
				}
				if($curent < $next){
					$stack[] = isset($model) ? $model->id : $raw_i;
				}
				$elemnt_id = isset($model) ? $model->id : $raw_i;
			}
			if (isset($num_curent[1]) && $element == 1){
				$id = end($stack);
				if ($raw_i != ""){
					if ($sheet->getCell('A'.$i)->getValue() != hash('md5', $raw_i.$raw_n.$raw_r.$raw_t)){
						$elem = Element::find($raw_i);
						// $elem->imported_at = date('Y-m-d H:i:s');
						$elem->numbering = $raw_n;
						$elem->references = $raw_r;
						$elem->content = $raw_t;
						$elem->chapter_id = $elemnt_id;
						$elem->validate()->save();
						$update[] = 'element:'.$elem->id.", raw:".$i;
					}
				}else{
					$elem = new Element;
					// $elem->imported_at = date('Y-m-d H:i:s');
					$elem->numbering = $raw_n;
					$elem->references = $raw_r;
					$elem->content = $raw_t;
					$elem->chapter_id = $elemnt_id;
					$elem->type = "type";
					$elem->is_open = 0;
					$elem->is_client_requirement = -1;
					$elem->validate()->save();
					$supp = new Supplement;
					$supp->element_id = $elem->id;
					$supp->save();
					$val = new Validation_control;
					$val->element_id = $elem->id;
					$val->is_controled = -1;
					$val->save();
					$qual = new Quality_control;
					$qual->element_id = $elem->id;
					$qual->save();
					$imp = new Impact;
					$imp->element_id = $elem->id;
					$imp->save();
					$insert[] = 'element:'.$elem->id.", raw:".$i;
				}
				unset($element);
			}
			if($curent > $next){
				$index = count($stack) - $next +1;
				while($index){
					array_pop($stack);
					$index--;
				}
			}
			if($next == 0){
				break;
			}
		}
		return(object) array('insert' => $insert, 'update' => $update);
	}

	private function validation_update($sheet, $chapters_db, $elements_db){
		$numbering = array();
		$elements_xl = array();
		$chapters_xl = array();
		$i = 1;
		while($sheet->getCell('C'.$i) != ""){
			$num = $sheet->getCell('C'.$i)->getValue();
			if (in_array($num, $numbering)){
				return (object) array('error' => (object) array('code' => 1, 'message' => 'numbering: "'.$num.'" is duplicate row '.$i));
			}else{
				$numbering[] = $num;
			}
			$num_curent = explode(':', $num);
			$id = $sheet->getCell('B'.$i)->getValue();
			if (isset($num_curent[1])){
				if ($id != ""){
					$elements_xl[] = $id;
					if (!in_array($id, $elements_db)){
						return (object) array('error' => (object) array('code' => 2, 'message' => 'element uid:'.$id.' (row '.$i.') not found in database'));
					}
				}
			}else{
				if ($id != ""){
					$chapters_xl[] = $id;
					if (!in_array($id, $chapters_db)){
						return (object) array('error' => (object) array('code' => 3, 'message' => 'chapter uid:'.$id.' (row '.$i.') not found in database'));
					}
				}
			}
			$i++;
			unset($id);
			unset($num);
		}
		foreach ($elements_db as $value) {
			if (!in_array($value, $elements_xl)){
				return (object) array('error' => (object) array('code' => 4, 'message' => 'missing element uid:'.$value));
			}
		}
		foreach ($elements_db as $value) {
			if (!in_array($value, $elements_xl)){
				return (object) array('error' => (object) array('code' => 5, 'message' => 'missing chapter uid:'.$value));
			}
		}
		return false;
	}

	private function dump_bloc_note($kamion, $sheet){

		$sheet->getColumnDimension('A')->setWidth(14);
		$sheet->getColumnDimension('B')->setWidth(14);
		$sheet->getColumnDimension('C')->setWidth(14);
		$sheet->getColumnDimension('D')->setWidth(14);
		$sheet->getColumnDimension('E')->setWidth(14);
		$sheet->getColumnDimension('F')->setWidth(14);
		$sheet->getColumnDimension('G')->setWidth(14);
		$sheet->getColumnDimension('H')->setWidth(14);
		$sheet->getColumnDimension('I')->setWidth(14);
		$sheet->getColumnDimension('J')->setWidth(14);
		$sheet->getColumnDimension('K')->setWidth(14);
		$sheet->getColumnDimension('L')->setWidth(14);
		for ($i=1; $i < 22; $i++) {
			$sheet->getRowDimension($i)->setRowHeight(24);
		}


		$sheet->mergeCells('A1:L1');
		$sheet->mergeCells('A22:L24');
		$sheet->mergeCells('B7:L7');
		$sheet->mergeCells('B11:E11');
		$sheet->mergeCells('B13:L13');
		$sheet->mergeCells('B15:L15');
		$sheet->mergeCells('B19:C19');
		$sheet->mergeCells('G19:H19');
		$sheet->mergeCells('H5:L5');
		$sheet->mergeCells('H11:L11');
		$sheet->mergeCells('K19:L19');

		$sheet->getStyle('A22')->getAlignment()->setWrapText(true);
		$sheet->getStyle('B13')->getAlignment()->setWrapText(true);
		$sheet->getRowDimension('13')->setRowHeight(-1);
		$sheet->getRowDimension('22')->setRowHeight(-1);

		$sheet->getCell('A1')->setValueExplicit('bloc notes', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A3')->setValueExplicit('équipe :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A5')->setValueExplicit('KAMION :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A7')->setValueExplicit('PILOTES :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A9')->setValueExplicit('description :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A11')->setValueExplicit('CLIENT :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A13')->setValueExplicit("Description \nde l\'unité :", \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A15')->setValueExplicit('MODELE(S) :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A17')->setValueExplicit('délais contractuels :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A19')->setValueExplicit("DATE DE\nCOMMANDE :", \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A21')->setValueExplicit('notes', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A22')->setValueExplicit($kamion->note, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('B5')->setValueExplicit($kamion->wana_id, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('B11')->setValueExplicit($kamion->client, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('B13')->setValueExplicit($kamion->description, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('B15')->setValueExplicit($kamion->model, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('B19')->setValueExplicit(date('Y-m-d',strtotime($kamion->ordered_at)), \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('D5')->setValueExplicit('CHEF DE PROJET :', \PHPExcel_Cell_DataType::TYPE_STRING);
		foreach ($kamion->users as $user) {
		}
		$sheet->getCell('F19')->setValueExplicit("DATE LIVRAISON\nCHASSIS PREVISIONNELLE :", \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('G5')->setValueExplicit('CHEF(S) DE KMION :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('G11')->setValueExplicit('PAYS DE DESTINATION :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('G19')->setValueExplicit($kamion->expected_frame_delivery, \PHPExcel_Cell_DataType::TYPE_STRING);
		$pilotes = "";
		foreach ($kamion->users as $user) {
			if ($user->pivot->is_kamion_manager == 1)
				$sheet->getCell('H5')->setValueExplicit($user->username, \PHPExcel_Cell_DataType::TYPE_STRING);
			if ($user->pivot->is_project_manager == 1)
				$sheet->getCell('E5')->setValueExplicit($user->username, \PHPExcel_Cell_DataType::TYPE_STRING);
			if ($user->pivot->is_pilot == 1)
			$pilotes .= $user->username.' ';
		}
		$sheet->getCell('B7')->setValueExplicit($pilotes, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('H11')->setValueExplicit($kamion->destination, \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('J19')->setValueExplicit('DATE DE LIVRAISON :', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('K19')->setValueExplicit($kamion->frame_delivery, \PHPExcel_Cell_DataType::TYPE_STRING);
		for ($i=1; $i < 22; $i++) {
			$sheet->getStyle('A'.$i.':L'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
			$sheet->getStyle('A'.$i.':L'.$i)->getFill()->getStartColor()->setARGB('ffffffff');
		}

		$sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle('D5')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle('G5')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle('G11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle('F19')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle('J19')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle('A3:L3')->getFill()->getStartColor()->setARGB('ffbfbfbf');
		$sheet->getStyle('A9:L9')->getFill()->getStartColor()->setARGB('ffbfbfbf');
		$sheet->getStyle('A17:L17')->getFill()->getStartColor()->setARGB('ffbfbfbf');
		$sheet->getStyle('A21:L21')->getFill()->getStartColor()->setARGB('ffbfbfbf');
		$sheet->getStyle('A3:L3')->getFont()->getColor()->setARGB('ffffffff');
		$sheet->getStyle('A9:L9')->getFont()->getColor()->setARGB('ffffffff');
		$sheet->getStyle('A17:L17')->getFont()->getColor()->setARGB('ffffffff');
		$sheet->getStyle('A21:L21')->getFont()->getColor()->setARGB('ffffffff');
		$sheet->getStyle('A1')->getFont()->getColor()->setARGB('ffc00000');
		$sheet->getStyle('A1')->getFont()->setSize(24);
		$sheet->getStyle('A22')->getFont()->setSize(11);
		$sheet->getStyle('A3:L3')->getFont()->setSize(11);
		$sheet->getStyle('A9:L9')->getFont()->setSize(11);
		$sheet->getStyle('A17:L17')->getFont()->setSize(11);
		$sheet->getStyle('A21:L21')->getFont()->setSize(11);
		$sheet->getStyle('B5:B19')->getFont()->setSize(16);
		$sheet->getStyle('E5')->getFont()->setSize(14);
		$sheet->getStyle('H5')->getFont()->setSize(14);
		$sheet->getStyle('H11')->getFont()->setSize(14);
		$sheet->getStyle('G19')->getFont()->setSize(14);
		$sheet->getStyle('K19')->getFont()->setSize(14);
		$sheet->getStyle('B5')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('E5')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('H5:L5')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('H5:L5')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('H5:L5')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('H11:L11')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('B11:E11')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('B7:L7')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('B13:L13')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('B15:L15')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('B19:C19')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('G19:h19')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
		$sheet->getStyle('K19:L19')->applyFromArray(array('borders' => array('outline' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
	}

	private function dump_header($sheet){
		$sheet->mergeCells('P1:R1');
		$sheet->mergeCells('T1:U1');
		$sheet->getCell('P1')->setValueExplicit('Qualité', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('T1')->setValueExplicit('Conformité', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getCell('A1')->setValueExplicit('Numérotaiton', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('A1:A2');
		$sheet->getStyle('A1')->getAlignment()->setWrapText(true);
		$sheet->getCell('B1')->setValueExplicit('Référence / éxigence client', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('B1:B2');
		$sheet->getStyle('B1')->getAlignment()->setWrapText(true);
		$sheet->getCell('C1')->setValueExplicit('Description technique', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('C1:C2');
		$sheet->getStyle('C1')->getAlignment()->setWrapText(true);
		$sheet->getCell('D1')->setValueExplicit("Compléments - remarques - modifications", \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('D1:D2');
		$sheet->getStyle('D1')->getAlignment()->setWrapText(true);
		$sheet->getCell('E1')->setValueExplicit('Emetteur', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('E1:E2');
		$sheet->getStyle('E1')->getAlignment()->setWrapText(true);
		$sheet->getCell('F1')->setValueExplicit('Responsable de l\'action', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('F1:F2');
		$sheet->getStyle('F1')->getAlignment()->setWrapText(true);
		$sheet->getCell('G1')->setValueExplicit('Exigence Client', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('G1:G2');
		$sheet->getStyle('G1')->getAlignment()->setWrapText(true);
		$sheet->getCell('H1')->setValueExplicit('Statut', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('H1:H2');
		$sheet->getStyle('H1')->getAlignment()->setWrapText(true);
		$sheet->getCell('I1')->setValueExplicit('Date d\'évènement', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('I1:I2');
		$sheet->getStyle('I1')->getAlignment()->setWrapText(true);
		$sheet->getCell('J1')->setValueExplicit('Date butoire de décision', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('J1:J2');
		$sheet->getStyle('J1')->getAlignment()->setWrapText(true);
		$sheet->getCell('K1')->setValueExplicit('Incidence délai', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('K1:K2');
		$sheet->getStyle('K1')->getAlignment()->setWrapText(true);
		$sheet->getCell('L1')->setValueExplicit('Incidence Financière', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('L1:L2');
		$sheet->getStyle('L1')->getAlignment()->setWrapText(true);
		$sheet->getCell('M1')->setValueExplicit('Montant', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('M1:M2');
		$sheet->getStyle('M1')->getAlignment()->setWrapText(true);
		$sheet->getCell('N1')->setValueExplicit('Documents de référence', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->mergeCells('N1:N2');
		$sheet->getStyle('N1')->getAlignment()->setWrapText(true);
		$sheet->getCell('P2')->setValueExplicit('Conformité', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getStyle('P2')->getAlignment()->setWrapText(true);
		$sheet->getCell('Q2')->setValueExplicit('Vérification', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getStyle('Q2')->getAlignment()->setWrapText(true);
		$sheet->getCell('R2')->setValueExplicit('Compléments - justificatifs', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getStyle('R2')->getAlignment()->setWrapText(true);
		$sheet->getCell('T2')->setValueExplicit('Contrôle validation', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getStyle('T2')->getAlignment()->setWrapText(true);
		$sheet->getCell('U2')->setValueExplicit('Remarques', \PHPExcel_Cell_DataType::TYPE_STRING);
		$sheet->getStyle('U2')->getAlignment()->setWrapText(true);

	}

	private function pars_db($db){
		$elements = array();
		$chapters = array();
		foreach ($db as $value) {
			foreach ($value->elements as $evalue) {
				$elements[] = $evalue->id;
			}
			$chapters[] = $value->id;
		}
		return (object) array('elements' => $elements, 'chapters' => $chapters);
	}

	private function delete_discussion($discussion){
		foreach ($discussion->messages as $message){
			$message->delete();
		}
		$discussion->delete();
	}

	private function delete_element($element){
		if ($element->impact){
			$element->impact->delete();
		}
		if ($element->supplement){
			$element->supplement->delete();
		}
		if ($element->quality_controls){
			$element->quality_controls->delete();
		}
		if ($element->validation_controls){
			$element->validation_controls->delete();
		}
		foreach ($element->discussions as $discussion){
			$this->delete_discussion($discussion);
		}
		$element->delete();
	}


	private function delete_chapter($chapter){
		foreach ($chapter->children as $child) {
			$this->delete_chapter($child);
		}
		foreach ($chapter->elements as $element) {
			$this->delete_element($element);
		}
		$chapter->delete();
	}

	/*	@route ::/cda/{id}::
		@write ::delete::*/
	public function delete($id){
		$chapters = Chapter::where('kamion_id', $id)->with([
			'elements.impact',
			'elements.supplement',
			'elements.quality_controls',
			'elements.validation_controls',
			'elements.discussions.messages',
			'children.elements.impact',
			'children.elements.supplement',
			'children.elements.quality_controls',
			'children.elements.validation_controls',
			'children.elements.discussions.messages',
			'children.children.elements.impact',
			'children.children.elements.supplement',
			'children.children.elements.quality_controls',
			'children.children.elements.validation_controls',
			'children.children.elements.discussions.messages',
			'children.children.children.elements.impact',
			'children.children.children.elements.supplement',
			'children.children.children.elements.quality_controls',
			'children.children.children.elements.validation_controls',
			'children.children.children.elements.discussions.messages',
		])->get();
		foreach ($chapters as $chapter){
			$this->delete_chapter($chapter);
		}
		return (1);
	}

	/*	@rule ::NotificationTicket Excel's file of CDA::
	@route ::[POST /cda]::
	@field
	+ FILE(xlsx): `cda`
	@endfield
	@responseheader
			Content-Type : application/json
	@endresponseheader
	@write ::custom::*/
	public function import(Request $request){
		if (!is_object($request->cda)){
			return response(json_encode((object) array('error' => (object) array('code' => 7, 'message' => 'invalid document have been send'))), 422)->header('Content-Type', 'application/json');
		}
		$filename = $request->cda->getPathName();
		$type = \PHPExcel_IOFactory::identify($filename);
		if ($type != "Excel2007"){
			return response(json_encode((object) array('error' => (object) array('code' => 6, 'message' => 'type : '.$type.' is not suported'))), 422)->header('Content-Type', 'application/json');
		}
		$objReader = \PHPExcel_IOFactory::createReader($type);
		$objPHPExcel = $objReader->load($filename);
		if ($err = $this->create_chapter_i($objPHPExcel->getActiveSheet())){
			return response(json_encode($err), 422)->header('Content-Type', 'application/json');
		}
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
		unset($objReader);
		$query = Chapter::select('*');
		$query->where(array(array('kamion_id', '=', $request->kamion_id)));
        $query->with('All_children');
		$query->whereNull('parent_id');
		return response($query->get(), 200);
	}

/*	@rule ::Download Excel's file of CDA::
	@route ::[GET /cda/{id}]::
	@parameters
	+ id:(integer, required) - id of the Kamion
	@endparameters
	@responseheader
			Content-Type : application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
	@endresponseheader
	@write ::custom::*/
	public function export($kamion_id){
		$objPHPExcel = new \PHPExcel();
		$query = Chapter::select('id', 'numbering', 'title');
		$query->where(array(array('kamion_id', '=', $kamion_id)));
        $query->with('childrens', 'elements');
		$query->whereNull('parent_id');
		$all = $query->get();
		$kamion = Kamion::find($kamion_id)->name;
		// dump($all);
		// $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
		// $objPHPExcel->getActiveSheet()->getStyle('all')->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
		$this->dump_all($all, $objPHPExcel->getActiveSheet());
		$objPHPExcel->getProperties()
		    ->setCreator("TTK hub")
		    ->setLastModifiedBy("TTK hub")
		    ->setTitle("TTK CDA generated for update")
		    ->setSubject("TTK CDA generated for update")
		    ->setDescription("Chemain des attentes generated for manual update");
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(70);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename='CDA-Hub-TTK-$kamion-updating".time().".xlsx'");
		header('Cache-Control: max-age=0');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		// $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		// $objWriter->save("../testout.xlsx");
		// `chmod 777 ../testout.xlsx`;
		$objPHPExcel->disconnectWorksheets();
		unset($objWriter);
		unset($objPHPExcel);
	}
	/*	@rule ::Download full Excel's file of CDA::
		@route ::[GET /cda/{id}/full]::
		@parameters
		+ id:(integer, required) - id of the Kamion
		@endparameters
		@responseheader
				Content-Type : application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
		@endresponseheader
		@write ::custom::*/
	public function full_export($kamion_id){
		$objPHPExcel = new \PHPExcel();
		$query = Chapter::select('id', 'numbering', 'title');
		$query->where(array(array('kamion_id', '=', $kamion_id)));
		$query->with('export', 'elements.discussions.messages', 'elements.discussions.messages.user', 'elements.supplement', 'elements.impact', 'elements.quality_controls', 'elements.validation_controls', 'elements.user_files');
		$query->whereNull('parent_id');
		$all = $query->get();
		// return($all);
		$query = Kamion::select();
		$query->where(array(array('id', '=', $kamion_id)));
		$query->with('users');
		$kamion = $query->get();
		$objPHPExcel->createSheet();
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getSheet(0)->getDefaultStyle()->getFont()->setSize(9);
		$objPHPExcel->getSheet(0)->setTitle('Bloc note');
		$objPHPExcel->getSheet(1)->setTitle('Chemin des attentes');
		$this->full_dump_all($all,$kamion[0], $objPHPExcel->getSheet(1));
		$this->dump_bloc_note($kamion[0], $objPHPExcel->getSheet(0));
		$this->dump_header($objPHPExcel->getSheet(1));
		$name = $kamion[0]->name;
		$objPHPExcel->getProperties()
		    ->setCreator("TTK hub")
		    ->setLastModifiedBy("TTK hub")
		    ->setTitle("TTK CDA generated for update")
		    ->setSubject("TTK CDA generated for update")
		    ->setDescription("Chemain des attentes generated for manual update");
		$objPHPExcel->getSheet(1)->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getSheet(1)->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getSheet(1)->getColumnDimension('C')->setWidth(50);
		$objPHPExcel->getSheet(1)->getColumnDimension('D')->setWidth(50);
		$objPHPExcel->getSheet(1)->getColumnDimension('E')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('I')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('J')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('K')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('L')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('M')->setWidth(15);
		$objPHPExcel->getSheet(1)->getColumnDimension('N')->setWidth(50);
		$objPHPExcel->getSheet(1)->getColumnDimension('O')->setWidth(2);
		$objPHPExcel->getSheet(1)->getColumnDimension('P')->setWidth(10);
		$objPHPExcel->getSheet(1)->getColumnDimension('Q')->setAutoSize(true);
		$objPHPExcel->getSheet(1)->getColumnDimension('R')->setWidth(30);
		$objPHPExcel->getSheet(1)->getColumnDimension('S')->setWidth(2);
		$objPHPExcel->getSheet(1)->getColumnDimension('T')->setWidth(10);
		$objPHPExcel->getSheet(1)->getColumnDimension('U')->setWidth(30);
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename='CDA-Hub-TTK-$name".time().".xlsx'");
		header('Cache-Control: max-age=0');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		$objPHPExcel->disconnectWorksheets();
		unset($objWriter);
		unset($objPHPExcel);
	}
/*	@rule ::NotificationTicket Excel's file of CDA::
	@route ::[POST /cda/{id}]::
	@parameters
	+ id:(integer, required) - id of the Kamion
	@endparameters
	@field
	+ FILE(xlsx): `cda`
	@endfield
	@responseheader
			Content-Type : application/json
	@endresponseheader
	@write ::custom::*/
	public function update(Request $request, $kamion_id){
		if (!is_object($request->cda)){
			return response(json_encode((object) array('error' => (object) array('code' => 7, 'message' => 'invalid document have been send'))), 422)->header('Content-Type', 'application/json');
		}
		$filename = $request->cda->getPathName();
		$type = \PHPExcel_IOFactory::identify($filename);
		if ($type != "Excel2007"){
			return response(json_encode((object) array('error' => (object) array('code' => 6, 'message' => 'type : '.$type.' is not suported'))), 422)->header('Content-Type', 'application/json');
		}
		$objReader = \PHPExcel_IOFactory::createReader($type);
		$objPHPExcel = $objReader->load($filename);
		$query = Chapter::select('*');
		$query->where(array(array('kamion_id', '=', $kamion_id)));
        $query->with('elements');
		$db = $query->get();
		$tmp = $this->pars_db($db);
		if ($err = $this->validation_update($objPHPExcel->getActiveSheet(), $tmp->chapters, $tmp->elements)){
			return response(json_encode($err), 422)->header('Content-Type', 'application/json');
		}
		$ret = $this->update_chapter_i($objPHPExcel->getActiveSheet(), $kamion_id);
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExecl);
		unset($objReader);
		$query = Chapter::select('*');
		$query->where(array(array('kamion_id', '=', $kamion_id)));
		$query->with('All_children');
		$query->whereNull('parent_id');
		return response(json_encode((object) array('process' => $ret, 'database' => $query->get())), 200);
	}
}
