<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Validation_controls::
	@model ::Validation_control::
	@migration ::create_validation_controls_table::
 */
class Validation_controlController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\Validation_control';
	protected $index_fields = ['*'];
	private function sync($data, $discussion){
		if (isset($data['documents'])) {
			foreach ($data['documents'] as $document) {
				$sync[$document] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$discussion->documents()->sync($sync);
		}else{
			$discussion->documents()->sync([]);
		}
	}
	/*	@route ::/validation_controls::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/validation_controls/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/validation_controls::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/validation_controls/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$vc = $this->api_update($request, $id);
		$data = $request->all();
		$this->sync($data, $vc);
		return $vc;
	}

	/*	@route ::/validation_controls/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}

}
