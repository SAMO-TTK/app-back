<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use App\Api\v1\Models\Login;

// @group ::Authenfication::

class AuthController extends Controller
{
	/*	@rule ::Retrieve authtoken with username::
		@route ::[POST /login]::
		@responseheader
				Content-Type : application/json
		@endresponseheader
		@field
		+ VARCHAR(255): `username`
		+ VARCHAR(255): `password`
		@endfield
		@write ::custom::*/
	public function authenticate(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('username', 'password');
		try {
			// attempt to verify the credentials and create a token for the user
			if (! $token = \JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'invalid_credentials'], 401);
			}else{

				$user = \Auth::user();
				$user->last_connection = date('Y-m-d H:i:s');
				$user->save();
			}
		} catch (\JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json(['error' => 'could_not_create_token'], 500);
		}
		return response()->json(compact('token'));
	}
	/*	@rule ::Retrieve authtoken with nfc::
		@route ::[POST /nfc]::
		@responseheader
				Content-Type : application/json
		@endresponseheader
		@field
		+ VARCHAR(255): `badge_id`
		@endfield
		@write ::custom::*/
	public function nfc(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('badge_id');
		// dd($credentials);
		try {
			$user = \App\Api\v1\Models\User::where('badge_id', $request->badge_id)->get();
			// dd($user);
			if (isset($user[0])):
				$id = $user[0]->id;
				$t = time();
				// attempt to verify the credentials and create a token for the user
				$str = str_shuffle("qwertyuiopasdfghjklzxcvbnm1234567890poiuytrewqlkjhgfdsamnbvcx098765432");
				$customClaims = ['iss' => "http://ttk.dev/api/jwt", 'iat' => $t, 'exp' => $t + 360000, 'nbf' => $t, 'sub' => $id, 'jti' =>$str];
				$payload = \JWTFactory::make($customClaims);
				$token = \JWTAuth::encode($payload);
				$user[0]->last_connection = date('Y-m-d H:i:s');
				$user[0]->save();
				return response()->json(['token' => "$token"]);
			endif;
		} catch (\JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json(['error' => 'could_not_create_token'], 500);
		}

		return response()->json(['error' => 'invalid_credentials'], 401);
	}
	/*	@rule ::logout::
		@route ::[GET /logout]::
		@responseheader
				Content-Type : application/json
		@endresponseheader
		@field
		+ TEXT: `authToken`
		@endfield
		@write ::custom::*/
	public function logout() {
		\JWTAuth::invalidate(\JWTAuth::getToken());
	}
}
