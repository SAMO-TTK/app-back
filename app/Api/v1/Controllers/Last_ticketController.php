<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class Last_ticketController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::Last_tickets::
	//@model ::Last_ticket::
	//@migration ::create_last_tickets_table::
	protected $model        = 'App\Api\v1\Models\Last_ticket';
	protected $index_fields = ['*'];

	/*	@route ::/last_tickets::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/last_tickets/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/last_tickets::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/last_tickets/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$tmp = $this->model::find($id);
		$tmp->updated_at = date('Y-m-d H:i:s');
		$tmp->save();
		return $this->api_update($request, $id);
	}

	/*	@route ::/last_tickets/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
