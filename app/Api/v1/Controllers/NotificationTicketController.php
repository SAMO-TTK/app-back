<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\UpdateField;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class NotificationTicketController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::NotificationTickets::
	//@model ::NotificationTicket::
	//@migration ::create_notification_tickets_table::
	protected $model        = 'App\Api\v1\Models\NotificationTicket';
	protected $index_fields = ['*'];

	/*	@route ::/updates::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/updates/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/updates::
		@write ::store::*/
	public function store(Request $request){
		$tmp = $this->api_store($request);
		foreach ($request->fields as $field){
			$f = new UpdateField;
			$f->cat_id = $field['cat_id'];
			if(isset($field['sub_cat_id']))
				$f->sub_cat_id = $field['sub_cat_id'];
			$f->update_id = $tmp->id;
			$f->save();
		}
		return $tmp;
	}

	/*	@route ::/updates/{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/updates/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
