<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Instruction;
use App\Api\v1\Models\InstructionUser;
use App\Api\v1\Models\Job;
use App\Api\v1\Models\MaterialResourceUser;
use App\Api\v1\Models\User;
use App\Api\v1\Models\Role;
use Curl\Curl;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
/*
	@group ::Users::
	@model ::User::
	@migration ::create_users_table::
 */
class UserController extends Controller{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	protected $model        = 'App\Api\v1\Models\User';
	protected $index_fields = ['*'];
	/*	@route ::/users::
		@body
		{"data": [{"id": 1,"created_at": "2017-08-28 12:49:51","updated_at": "2017-08-28 12:49:51","deleted_at": null,"last_connection": null,"first_name": "Jaqueline","last_name": "Kozey","username": "manley.brown","email": "towne.brice@wiegand.com","wana_id": "5401652528346846","badge_id": null,"smartphone_id": null,"is_active": 0,"role_id": 7,"manager_id": null},{"id": 2,"created_at": "2017-08-28 12:49:51","updated_at": "2017-08-28 12:49:51","deleted_at": null,"last_connection": null,"first_name": "Alvena","last_name": "Senger","username": "jazlyn.thompson","email": "virginia91@gerhold.com","wana_id": "4485067200105588","badge_id": null,"smartphone_id": null,"is_active": 1,"role_id": 2,"manager_id": null},]}
		@endbody
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/users/{id}::
		@body
		{"data": {"id": 12,"created_at": "2017-08-28 12:49:51","updated_at": "2017-08-28 12:49:51","deleted_at": null,"last_connection": null,"first_name": "Rowland","last_name": "Tillman","username": "laurine.glover","email": "jerde.leslie@lubowitz.net","wana_id": "4024007196971","badge_id": null,"smartphone_id": null,"is_active": 0,"role_id": 5,"manager_id": null}}
		@endbody
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/users::
		@write ::store::*/
	public function store(Request $request){
		$data = $request->all();
		$user =  $this->api_store($request);
		if (null !== $request->file('avatar')){
			$name = $request->file('avatar')->store('avatars');
			$user->avatar = $name;
		}
		$user->save();
		if(isset($data['roles'])){
			$user->roles()->sync($data['roles']);
		}
		if(isset($data['job'])){
			$job = Job::where('label', $data['job'])->get()->first();
			if ($job){
				$user->jobs()->sync([$job->id]);
			}
		}
		return $user;
	}
	/*	@rule ::Save user's avatar::
		@route ::[POST /users/{id}/image]::
		@field
		+ FILE: `file`
		@endfield
		@parameters
		+ id:(integer, required) - the id of the user
		@endparameters
		@write ::custom::*/
	public function image(Request $request, $id){
		$user = $this->model::find($id);
		$name = $request->file('file')->store('avatars');
		$user->avatar = $name;
		$user->save();
		return $name;
	}


	/*	@route ::/users/{id}::
		@body
		{
			"user": {"id": 1,"created_at": "2017-08-28 12:49:51","updated_at": "2017-08-28 13:18:06","deleted_at": null,"last_connection": null,"first_name": "Jaqueline","last_name": "Kozey","username": "manley.brown","email": "towne.brice@wiegand.com","wana_id": "5401652528346846","badge_id": "ser68g5dfg6y8sergt","smartphone_id": null,"is_active": 0,"role_id": 7,"manager_id": null}}
		@endbody
		@write ::update::*/
	public function update(Request $request, $id){
		$data = $request->all();
		$user =  $this->api_update($request, $id);
		$user->save();
		if(isset($data['roles'])){
			$user->roles()->sync($data['roles']);
		}
		return $user;
	}

	/*	@route ::/users/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
	/*	@route ::[PUT /users/wana/{id}]::
		@rule ::sync a wana users::
		@parameters
		+ id: (integer, required) - the wana id of the user
		@endparameters
		@write ::custom::*/
	public function sync($wana_id){
		$curl = new \Curl\Curl();
		$curl->get("http://www.liftingsignalisations.fr/compagnon/$wana_id");
		$compagnon = json_decode($curl->response);
		$user = \App\Api\v1\Models\User::select('users.id')->where('wana_id', $wana_id)->get();
		if (isset($user[0])){
			$user = \App\Api\v1\Models\User::find($user[0]->id);
		}else {
			$user = new \App\Api\v1\models\User;
		}
		$user->wana_id = $wana_id;
		$user->first_name = isset($compagnon->sPrenom) && $compagnon->sPrenom != "" ? $compagnon->sPrenom : "none";
		$user->last_name = isset($compagnon->sNom) && $compagnon->sNom != "" ? $compagnon->sNom : "none";
		$user->username = isset($compagnon->sSurnom) && $compagnon->sSurnom != "" ? $compagnon->sSurnom : "none";
		$user->password = '0000';
		$user->is_active = 1;
		$user->role_id = 1;
		$user->validate()->save();
		return($user);
	}

	/*	@rule ::Last instruction of the user::
		@route ::[GET /users/{id_user}/kamions/{id_kamion}/last_instruction]::
		@parameters
			+ id_user:(integer, required) - the id of the user
			+ id_kamion:(integer, required) - the id of the kamion
		@endparameters
		@write ::custom::*/
	public function last_instruction($id, $idkamion){
		$instructionuser =  InstructionUser::where('user_id', $id)->whereHas('instruction',function($q) use ($idkamion){
			$q->where('instructions.kamion_id', $idkamion)->whereNotNull('wana_id');
		})->orderBy('updated_at', 'desc')->limit(10)->with('instruction.job')->get();
		return $instructionuser->map(function ($item) {
			return $item->instruction;
		});
	}

	/*	@rule ::Insert a new instruction of the last instruction of the current user::
		@route ::[POST /users/instructions/{id}]::
		@parameters
			+ id:(integer, required) - the id of the instruction
		@endparameters
		@write ::custom::*/
	public function insert_last_instruction($id){
		$user_id = \JWTAuth::parseToken()->authenticate()->id;
		$tmp = InstructionUser::where(['user_id'=> $user_id, 'instruction_id' => $id])->get()->first();
		if($tmp){
			$tmp->updated_at = date('Y-m-d H:i:s');
			$tmp->save();
			return $tmp;
		}else{
			$instructionuser = new InstructionUser;
			$instructionuser->user_id = $user_id;
			$instructionuser->instruction_id = $id;
			$instructionuser->save();
			return $instructionuser;
		}
	}

	/*	@rule ::Last instruction of the user::
		@route ::[GET /users/{id_user}/last_instruction]::
		@parameters
			+ id_user:(integer, required) - the id of the user
		@endparameters
		@write ::custom::*/
	public function last_instruction_all($id){
		$instructionuser =  InstructionUser::where('user_id', $id)->whereHas('instruction',function($q){
			$q->whereNotNull('wana_id');
		})->orderBy('updated_at', 'desc')->limit(10)->with('instruction.job')->get();
		return $instructionuser->map(function ($item) {
			return $item->instruction;
		});
	}

	/*	@rule ::Insert a new material resource of the last material resource of the current user::
		@route ::[POST /users/last_materialresource/{id}]::
		@parameters
			+ id:(integer, required) - the id of the instruction
		@endparameters
		@write ::custom::*/
	public function insert_last_material_resource($id){
		$user_id = \JWTAuth::parseToken()->authenticate()->id;
		$tmp = MaterialResourceUser::where(['user_id'=> $user_id, 'material_resource_id' => $id])->get()->first();
		if($tmp){
			$tmp->updated_at = date('Y-m-d H:i:s');
			$tmp->save();
			return $tmp;
		}else{
			$instructionuser = new MaterialResourceUser;
			$instructionuser->user_id = $user_id;
			$instructionuser->material_resource_id = $id;
			$instructionuser->save();
			return $instructionuser;
		}
	}

	/*	@rule ::Last material resource of the user::
		@route ::[GET /users/{id_user}/last_materialresource]::
		@parameters
			+ id_user:(integer, required) - the id of the user
		@endparameters
		@write ::custom::*/
	public function last_material_resource_all($id){
		$instructionuser =  MaterialResourceUser::where('user_id', $id)->whereHas('material_resource',function($q){
			$q->whereNotNull('wana_id');
		})->orderBy('updated_at', 'desc')->limit(5)->with('material_resource')->get();
		return $instructionuser->map(function ($item) {
			return $item->material_resource;
		});
	}
//{
//	"IDCompagnon":1013,
//	"sNom":"CHATAIN",
//	"sPrenom":"LIONEL",
//	"sSurnom":"L.CHAT",
//	"sMetier":"Etude & Services",
//	"sDefinition":"",
//	"dDateEntree":"1996-07-01",
//	"dDatedepart":"1996-08-31",
//	"sRessource":"",
//	"nArchive":1,
//	"sAdresse":"",
//	"sAdresse2":"",
//	"sCodepostal":"",
//	"sVille":"",
//	"sChef":"0",
//	"sTelmobile":"",
//	"bRespQualite":false,
//	"bRespConformite":false,
//	"nCompagnonValidationHeure":0
//}
	public function update_all_user(){
		$curl = new Curl();
		$ret = array();
		$curl->get("http://wkrest.toutenkamion.com:80/compagnon/0");
		$users = json_decode($curl->response);
		foreach ($users as $k => $user){
			$old_user = User::where('wana_id', $user->IDCompagnon)->get()->first();
			if (!$old_user){
				$old_user = new User();
				$old_user->wana_id = $user->IDCompagnon;
			}
			$old_user->last_name = $user->sNom;
			$old_user->first_name = $user->sPrenom;
			$old_user->username = $user->sSurnom;
			$old_user->is_pilot = $user->bPilote == false ? 0 : 1;
			$old_user->job = $user->sMetier;
			if ($user->dDatedepart && $user->dDatedepart != "0000-00-00" && $user->dDatedepart != ""){
				if ($old_user->is_active_on_wana == 1){
					$old_user->is_active = 0;
				}
				$old_user->is_active_on_wana = 0;
			}else{
				if ($old_user->is_active_on_wana == 0){
					$old_user->is_active = 1;
				}
				$old_user->is_active_on_wana = 1;
			}
			$old_user->save();
			if ($user->bChefDeMetier == 'true'){
				$roleId = Role::where('label', 'Chef de Metier')->get()->first()->id;
				$old_user->roles()->detach($roleId);
				$old_user->roles()->attach($roleId);
			}
			$roleId = Role::where('label', 'Compagnon')->get()->first()->id;
			$old_user->roles()->detach($roleId);
			$old_user->roles()->attach($roleId);
			$ret[] = $old_user;
		}
		$curl->close();
		return $ret;
	}
}
