<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;
/*
	@group ::Workgroups::
	@model ::Workgroup::
	@migration ::create_workgroups_table::
 */
class WorkgroupController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}

	private function sync($data, $document){
		if(isset($data['users'])) {
			foreach ($data['users'] as $user) {
				$sync[$user] = array(
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
			}
			$document->users()->sync($sync);
		}
	}
	protected $model        = 'App\Api\v1\Models\Workgroup';
	protected $index_fields = ['*'];
	/*	@route ::/workgroups::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/workgroups/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/workgroups::
		@write ::store::*/
	public function store(Request $request){
		$document = $this->api_store($request);
		$data = $request->all();
		$this->sync($data, $document);
		return($document);
	}

	/*	@route ::/workgroups{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		$document = $this->api_update($request, $id);
		$data = $request->all();
		$this->sync($data, $document);
		return($document);
	}

	/*	@route ::/workgroups/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
