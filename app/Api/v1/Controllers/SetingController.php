<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Models\Seting;
use Illuminate\Http\Request;
use App\Api\v1\Traits\ApiController;
use App\Http\Controllers\Controller;

class SetingController extends Controller
{
	use ApiController {

		index   as protected api_index;
		show    as protected api_show;
		store   as protected api_store;
		update  as protected api_update;
		delete  as protected api_delete;
	}
	//@group ::Settings::
	//@model ::Setting::
	//@migration ::create_setings_table::
	protected $model        = 'App\Api\v1\Models\Seting';
	protected $index_fields = ['*'];
	public function get_id($slug){
		return Seting::where('slug', $slug)->get()->first();
	}
	/*	@route ::/settings::
		@write ::index::*/
	public function index(Request $request){
		return $this->api_index($request);
	}

	/*	@route ::/settings/{id}::
		@write ::show::*/
	public function show(Request $request, $id){
		return $this->api_show($request, $id);
	}

	/*	@route ::/settings::
		@write ::store::*/
	public function store(Request $request){
		return $this->api_store($request);
	}

	/*	@route ::/settings{id}::
		@write ::update::*/
	public function update(Request $request, $id){
		return $this->api_update($request, $id);
	}

	/*	@route ::/settings/{id}::
		@write ::delete::*/
	public function delete($id){
		return $this->api_delete($id);
	}
}
