<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Workgroup extends Model
{
    use ApiModel;
    use SoftDeletes;

    protected $fillable = [
        'target', 'delay', 'hours', 'planning', 'report', 'decision', 'is_visible'
    ];
    protected $rules = [
        'target'	=> 'required|string|max:255',
        'delay'		=> 'nullable|date|date_format:Y-m-d H:i:s',
        'hours'		=> 'nullable|integer',
        'planning'	=> 'nullable|string',
        'report'	=> 'nullable|string',
        'decision'	=> 'nullable|string',
		'is_visible' => 'nullable|integer|max:255'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function users(){
        return $this->belongsToMany('App\Api\v1\Models\User');
    }

    public function tickets(){
        return $this->hasMany('App\Api\v1\Models\Ticket');
    }
}
