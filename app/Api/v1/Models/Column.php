<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Column extends Model
{
    use ApiModel;
    use SoftDeletes;

    protected $fillable = [
        'kamion_id', 'numbering', 'reference', 'content' ,'supplement', 'crecre', 'upupup', 'client_requirement', 'type', 'status'
    ];
    protected $rules = [
        'kamion_id'		    => 'required|integer|exists:kamions,id',
        'numbering'         => 'required|integer|max:255',
        'reference'         => 'required|integer|max:255',
        'content'           => 'required|integer|max:255',
        'supplement'        => 'required|integer|max:255',
        'crecre'           => 'required|integer|max:255',
        'upupup'           => 'required|integer|max:255',
        'client_requirement'=> 'required|integer|max:255',
        'type'              => 'required|integer|max:255',
        'status'            => 'required|integer|max:255',
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function kamion(){
        return $this->belongsTo('App\Api\v1\Models\Kamion');
    }
}
