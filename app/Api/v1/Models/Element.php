<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;
use Sofa\Eloquence\Eloquence;

class Element extends Model
{
	use ApiModel;
	use SoftDeletes;
	use Eloquence;

    protected $fillable = [
        'is_open', 'numbering', 'type', 'content', 'is_client_requirement', 'chapter_id','references'
    ];
    protected $rules = [
		'content'				=> 'required|string',
		'type'					=> 'required|string|max:255',
		'references'			=> 'nullable|string|max:255',
		'numbering'				=> 'required|string|max:255',
		'is_open'				=> 'required|integer|max:255',
		'is_client_requirement'	=> 'required|integer|max:255',
        'chapter_id'			=> 'required|integer|exists:chapters,id'
    ];
	protected $searchableColumns = [
		'content'			=> 8,
		'references'		=> 10,
		'numbering'			=> 5,
		'supplement.content'=> 5,
	];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function chapter(){
        return $this->belongsTo('App\Api\v1\Models\Chapter');
    }

    public function quality_controls(){
        return $this->hasOne('App\Api\v1\Models\Quality_control');
    }

    public function validation_controls(){
        return $this->hasOne('App\Api\v1\Models\Validation_control');
    }

	public function discussions(){
		return $this->hasMany('App\Api\v1\Models\Discussion');
	}

    public function documents(){
        return $this->belongsToMany('App\Api\v1\Models\Document', 'document_element');
    }

	public function supplement(){
		return $this->hasOne('App\Api\v1\Models\Supplement');
	}

	public function impact(){
		return $this->hasOne('App\Api\v1\Models\Impact');
	}

	public function users(){
		return $this->belongsToMany('App\Api\v1\Models\User', 'element_user');
	}

	public function user_files(){
	    return $this->hasMany('App\Api\v1\Models\User_file');
    }
}
