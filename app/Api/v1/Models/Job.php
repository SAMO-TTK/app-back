<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Job extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
        'label', 'wana_id'
    ];

    protected $rules = [
        'label' => 'required|string|max:255',
        'wana_id'  => 'required|string|max:255'
    ];

    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function users(){
        return $this->belongsToMany('App\Api\v1\Models\User', 'job_user')->withPivot('is_manager');
    }
}
