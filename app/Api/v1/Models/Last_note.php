<?php

namespace App\Api\v1\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Last_note extends Model
{
    use ApiModel;
    use SoftDeletes;

    protected $fillable = [
        'note_id', 'user_id'
    ];
    protected $rules = [
        'note_id'	=> 'required|integer|exists:notes,id',
        'user_id'		=> 'required|integer|exists:users,id'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function note(){
        return $this->belongsTo('App\Api\v1\Models\Note');
    }

    public function user(){
        return $this->belongsTo('App\Api\v1\Models\User', 'users');
    }
}
