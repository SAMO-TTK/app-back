<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Cell extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'row_id', 'position', 'field_id'
	];
	protected $rules = [
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function row(){
		return $this->belongsTo('App\Api\v1\Models\Row');
	}

	public function  field(){
		return $this->belongsTo('App\Api\v1\Models\Field')->with(['rows']);
	}
}
