<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Field_value extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'value', 'field_id'
	];
	protected $rules = [
		'value'		=> 'required|string|max:255',
		'field_id'	=> 'required|integer|exists:model_fields,id',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function field(){
		return $this->belongsTo('App\Api\v1\Models\Model_field', 'field_id');
	}

	public function tickets(){
		return $this->belongsToMany('App\Api\v1\Models\Ticket', 'ticket_fields', 'ticket_id', 'field_id');
	}
}
