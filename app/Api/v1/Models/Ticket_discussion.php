<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Ticket_discussion extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'content','is_open','deadline','ticket_id','created_by', 'title', 'is_public'
	];
	protected $rules = [
		'title'			=> 'required|string|max:255',
		'content'		=> 'required|string',
		'is_open'		=> 'required|integer|max:255',
		'is_public'		=> 'nullable|integer|max:255',
		'deadline'		=> 'nullable|date|date_format:Y-m-d H:i:s',
		'ticket_id'		=> 'required|integer|exists:tickets,id',
		'created_by'	=> 'required|integer|exists:users,id',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function ticket(){
		return $this->belongsTo('App\Api\v1\Models\Ticket');
	}
	public function creator(){
		return $this->belongsTo('App\Api\v1\Models\User', 'created_by');
	}
	public function messages(){
		return $this->hasMany('App\Api\v1\Models\Ticket_message', 'discussion_id');
	}
	public function documents(){
		return $this->belongsToMany('App\Api\v1\Models\Document', 'document_ticket_discussion', 'discussion_id');
	}
	public function users(){
		return $this->belongsToMany('App\Api\v1\Models\User', 'ticket_discussion_user', 'discussion_id', 'user_id');
	}

}
