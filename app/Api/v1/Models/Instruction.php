<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

use App\Api\v1\Traits\ApiModel;


class Instruction extends Model
{
	use ApiModel;
	use SoftDeletes;
	use Eloquence;

	protected $fillable = [
		'label','wana_id','kamion_id','wana_number',
	];
	protected $rules = [
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];
	protected $searchableColumns = [
		'label'				=> 8,
		'job.label'			=> 8,
		'wana_number'		=> 10,
	];

	public function kamion(){
		return $this->belongsTo('App\Api\v1\Models\Kamion');
	}

	public function job(){
		return $this->belongsTo('App\Api\v1\Models\Job');
	}
	public function instruction_users(){
		return $this->hasMany('App\Api\v1\Models\InstructionUser');
	}


}
