<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class User_file extends Model
{
    use ApiModel;
    use SoftDeletes;

    protected $fillable = [
        'url', 'title', 'element_id'
    ];
    protected $rules = [
        'url'		    => 'required|string|max:255',
        'title'		    => 'required|string|max:255',
        'element_id'	=> 'required|integer',
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function element(){
        return $this->belongsTo('App\Api\v1\Models\Element');
    }
}
