<?php

namespace App\Api\v1\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Last_ticket extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $table = 'last_ticket';
	protected $fillable = [
		'ticket_id', 'user_id'
	];
	protected $rules = [
		'ticket_id'	=> 'required|integer|exists:tickets,id',
		'user_id'		=> 'required|integer|exists:users,id'
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function tickets(){
		return $this->belongsTo('App\Api\v1\Models\Ticket');
	}

	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User', 'users');
	}
}
