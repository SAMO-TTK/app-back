<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Model_field extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'label','model_id','position','type'
	];
	protected $rules = [
		'label'		=> 'required|string|max:255',
		'type'		=> 'required|string|max:255',
		'position'	=> 'required|integer',
		'depend'	=> 'nullable|integer',
		'model_id'	=> 'required|integer|exists:ticket_models,id',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function model(){
		return $this->belongsTo('App\Api\v1\Models\Ticket_model', 'model_id');
	}

	public function values(){
		return $this->hasMany('App\Api\v1\Models\Field_value', 'field_id');
	}

	public function ticket_fields(){
		return $this->hasMany('App\Api\v1\Models\Ticket_field', 'field_id');
	}

	public function tickets(){
		return $this->belongsToMany('App\Api\v1\Models\Ticket', 'ticket_fields', 'ticket_id', 'field_id')->withPivot('value');
	}


}
