<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Validation_control extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
         'is_controled', 'note', 'element_id', 'user_id'
    ];

    protected $rules = [
        'is_controled'	=> 'nullable|integer|max:255',
		'note'			=> 'nullable|string',
        'element_id'	=> 'required|integer|exists:elements,id',
        'user_id'		=> 'nullable|integer|exists:users,id'
    ];

    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function element(){
        return $this->belongsTo('App\Api\v1\Models\Element');
    }

    public function user(){
        return $this->belongsTo('App\Api\v1\Models\User');
    }

    public function documents(){
        return $this->belongsToMany('\App\Api\v1\Models\Document', 'validation_document', 'control_id');
    }
}
