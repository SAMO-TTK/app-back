<?php

namespace App\Api\v1\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Login extends Model {

    use ApiModel, SoftDeletes;

    protected $dates    = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $guarded = [

    ];

    protected $rules = [
        'username'	=> 'required',
        'IP'		=> 'required'
    ];
}
