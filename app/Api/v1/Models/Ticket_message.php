<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Ticket_message extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'content','is_pinned','user_id','discussion_id'
	];
	protected $rules = [
		'content'		=> 'required|string',
		'is_pinned'		=> 'nullable|integer|max:255',
		'user_id'		=> 'required|integer|exists:users,id',
		'discussion_id'	=> 'required|integer|exists:ticket_discussions,id',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User');
	}
	public function discussion(){
		return $this->belongsTo('App\Api\v1\Models\Ticket_discussion', 'discssion_id');
	}

	public function documents(){
		return $this->belongsToMany('App\Api\v1\Models\Document', 'document_ticket_message', 'message_id');
	}

}
