<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Extension extends Model
{
    use ApiModel;
    use SoftDeletes;

    protected $fillable = [
        'label', 'groupe_id'
    ];
    protected $rules = [
        'label' => 'required|string|max:255',
        'groupe_id'	=> 'nullable|integer|exists:groupes,id'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function groupe(){
        return $this->belongsTo('App\Api\v1\Models\Groupe');
    }
}
