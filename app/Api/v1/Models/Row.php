<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Row extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'model_id', 'field_id', 'position', 'content'
	];
	protected $rules = [
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function cells(){
		return $this->hasMany('App\Api\v1\Models\Cell')->with(['field'])->orderBy('position');
	}

	public function model(){
		return $this->belongsTo('App\Api\v1\Models\Modell', 'model_id');
	}
}
