<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Impact extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
        'amount', 'delai', 'element_id'
    ];

    protected $rules = [
        'amount'		=> 'nullable',
        'delai'			=> 'nullable|date|date_format:Y-m-d H:i:s',
		'element_id'	=> 'required|integer|exists:elements,id'
    ];

    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

	public function element(){
		return $this->BelongsTo('App\Api\v1\Models\Element');
	}
}
