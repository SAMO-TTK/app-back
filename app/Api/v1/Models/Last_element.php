<?php

namespace App\Api\v1\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Last_element extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
        'element_id', 'user_id'
    ];
    protected $rules = [
		'element_id'	=> 'required|integer|exists:elements,id',
        'user_id'		=> 'required|integer|exists:users,id'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

	public function element(){
		return $this->belongsTo('App\Api\v1\Models\Element');
	}

	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User', 'users');
	}
}
