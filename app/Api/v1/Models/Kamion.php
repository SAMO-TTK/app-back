<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;


use App\Api\v1\Traits\ApiModel;

class Kamion extends Model
{
	use ApiModel;
	use Eloquence;
	use SoftDeletes;

    protected $fillable = [
        'wana_id',
		'wana_created_at',
		'wana_updated_at',
        'name',
		'description',
		'image',
        'length',
        'amount',
		'planned_time',
		'worked_time',
		'is_delivered',
		'delivery',
		'frame_delivery',
		'expected_frame_delivery',
		'ordered_at',
		'coeff_mp',
		'hourly_rate',
		'model',
		'client',
		'destination',
		'note',
		'cda_exported_at',
		'is_locked',
		'stage_id',
        'type',
		'category_id'
    ];

    protected $rules = [

        'wana_id'					=> 'required|string|max:255',
		'wana_created_at'			=> 'required|date|date_format:Y-m-d',
		'wana_updated_at'			=> 'nullable|date|date_format:Y-m-d',
        'name'						=> 'required|string|max:255',
		'description'				=> 'nullable|string',
		'image'						=> 'nullable|string|max:255',
		'length'					=> 'nullable|string|max:255',
		'amount'					=> 'nullable|integer',
		'planned_time'				=> 'nullable',
		'worked_time'				=> 'nullable',
		'is_delivered'				=> 'required|integer|max:255',
		'delivery'					=> 'nullable|date|date_format:Y-m-d',
		'frame_delivery'			=> 'nullable|date|date_format:Y-m-d',
		'expected_frame_delivery'	=> 'nullable|date|date_format:Y-m-d H:i:s',
		'ordered_at'				=> 'nullable|date|date_format:Y-m-d',
		'coeff_mp'					=> 'nullable|string|max:255',
		'hourly_rate'				=> 'nullable',
		'model'						=> 'nullable|string|max:255',
		'client'					=> 'nullable|string|max:255',
		'destination'				=> 'nullable|string|max:255',
		'note'						=> 'nullable|string',
		'cda_exported_at'			=> 'nullable|date|date_format:Y-m-d H:i:s',
		'is_locked'					=> 'required|integer|max:255',
		'stage_id'					=> 'required|integer|exists:stages,id',
		'type'						=> 'required',
		'category_id'				=> 'required|integer|exists:categories,id',
    ];
	protected $searchableColumns = [
		'name'			=> 8,
		'wana_id'		=> 10,
		'description'	=> 5,
		'client'		=> 5,
		'note'			=> 3,
	];

    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function users(){
        return $this->belongsToMany('\App\Api\v1\Models\User', 'kamion_user')->withPivot('is_kamion_manager','is_project_manager', 'is_pilot');
    }

    public function chapters(){
        return $this->hasMany('\App\Api\v1\Models\Chapter');
    }

    public function announcements(){
        return $this->hasMany('App\Api\v1\Models\Announcement');
    }
    public function column(){
        return $this->hasOne('App\Api\v1\Models\Column');
    }

	public function notes(){
		return $this->hasMany('App\Api\v1\Models\Note');
	}

	public function tickets(){
		return $this->hasMany('App\Api\v1\Models\Ticket');
	}

	public function stage(){
		return $this->belongsTo('App\Api\v1\Models\Stage');
	}

	public function category(){
		return $this->belongsTo('App\Api\v1\Models\Category');
	}

	public function masters(){
		return $this->belongsToMany('\App\Api\v1\Models\Kamion', 'linked_projects', 'linked_id', 'master_id');
	}

	public function linked(){
		return $this->belongsToMany('\App\Api\v1\Models\Kamion', 'linked_projects', 'master_id', 'linked_id');
	}

	public function last_projects(){
		return $this->belongsToMany('App\Api\v1\Models\User', 'last_projects');
	}

	public function instructions(){
    	return $this->hasMany('App\Api\v1\Models\Instruction');
	}

	public function pilots(){
    	return $this->belongsToMany('App\Api\v1\Models\User')->where('is_active', 1)->wherePivot('is_pilot', 1);
	}

	public function project_managers(){
    	return $this->belongsToMany('App\Api\v1\Models\User')->where('is_active', 1)->wherePivot('is_project_manager', 1);
	}
}
