<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;
use Sofa\Eloquence\Eloquence;



class Ticket extends Model
{
	use ApiModel;
	use SoftDeletes;
	use Eloquence;

	protected $fillable = [
		'number', 'title','is_public','deadline','instruction_id','import','has_workgroup','has_quality_committee','quality_committee','creator_id','manager_id','kamion_id','workgroup_id','status_id','model_id','quality_visible'
	];
	protected $rules = [
		'number'                => 'nullable|string|max:255',
		'title'                 => 'required|string|max:255',
		'is_public'             => 'required|integer|max:255',
		'deadline'              => 'nullable|date|date_format:Y-m-d H:i:s',
		'instruction_id'        => 'nullable|string|max:255',
		'import'                => 'nullable|string',
		'has_workgroup'         => 'nullable|integer|max:255',
		'has_quality_committee' => 'nullable|integer|max:255',
		'quality_committee'     => 'nullable|string',
		'creator_id'            => 'required|integer|exists:users,id',
		'manager_id'            => 'nullable|integer|exists:users,id',
		'kamion_id'             => 'nullable|integer|exists:kamions,id',
		'workgroup_id'          => 'nullable|integer|exists:workgroups,id',
		'status_id'             => 'required|integer|exists:ticket_status,id',
		'model_id'              => 'nullable|integer|exists:models,id',
		'quality_visible'		=> 'nullable|integer|max:255'
	];
	protected $searchableColumns = [
		'title'			=> 8,
		'number'		=> 10,
		'fields.value'	=> 8,
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function instruction(){
		return $this->belongsTo('App\Api\v1\Models\Instruction');
	}

	public function documents(){
		return $this->belongsToMany('App\Api\v1\Models\Document');
	}

	public function kamion(){
		return $this->belongsTo('App\Api\v1\Models\Kamion');
	}

	public function kamions(){
		return $this->belongsToMany('App\Api\v1\Models\Kamion');
	}

	public function ticket(){
		return $this->belongsToMany('App\Api\v1\Models\Ticket', 'linked_tickets', 'ticket_id', 'linked_id');
	}

	public function linked(){
		return $this->belongsToMany('App\Api\v1\Models\Ticket', 'linked_tickets', 'linked_id', 'ticket_id');
	}

	public function model(){
		return $this->belongsTo('App\Api\v1\Models\Ticket_model', 'model_id');
	}

	public function fields(){
		return $this->hasMany('App\Api\v1\Models\Ticket_field');
	}
	public function favory_tickets(){
		return $this->hasMany('App\Api\v1\Models\FavoryTicket');
	}

	public function status(){
		return $this->belongsTo('App\Api\v1\Models\Ticket_status', 'status_id');
	}

	public function categories(){
		return $this->belongsToMany('App\Api\v1\Models\Ticket_category', 'category_ticket', 'ticket_id', 'category_id');
	}

	public function workgroup(){
		return $this->belongsTo('App\Api\v1\Models\Workgroup');
	}

	public function users(){
		return $this->belongsToMany('App\Api\v1\Models\User');
	}

	public function manager(){
		return $this->belongsTo('App\Api\v1\Models\User', 'manager_id');
	}

	public function creator(){
		return $this->belongsTo('App\Api\v1\Models\User', 'creator_id');
	}

	public function discussions(){
		return $this->hasMany('App\Api\v1\Models\Ticket_discussion');
	}

	public function last_tickets(){
		return $this->hasMany('App\Api\v1\Models\Last_ticket');
	}
}
