<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Capability extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
		'label', 'slug'
    ];
    protected $rules = [
		'label'	=> 'required|string|max:255',
		'slug'	=> 'required|string|max:255'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function users(){
        return $this->belongsToMany('App\Api\v1\Models\User', 'capability_user');
    }

    public function roles(){
        return $this->belongsToMany('App\Api\v1\Models\Role', 'capability_role');
    }
}
