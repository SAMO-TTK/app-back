<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Seting extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'label', 'slug', 'value'
	];
	protected $rules = [
		'label'		=> 'nullable|string|max:255',
		'value'		=> 'required|string|max:255',
		'slug'		=> 'required|string|max:255',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

}
