<?php

namespace App\Api\v1\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;


class Logtime extends Model {

	use ApiModel, SoftDeletes, Eloquence;

	protected $dates    = [
		'created_at', 'updated_at', 'deleted_at'
	];


	protected $fillable = [
		'length', 'user_id', 'status_logtime_id', 'kamion_id','instruction_id', 'remark', 'timecard_id'
	];
	protected $rules = [
		'length',
		'user_id',
		'status_logtime_id',
		'kamion_id',
		'instruction_id',
		'timecard_id'
	];

	protected $searchableColumns = [
		'kamion.wana_id'			=> 10,
		'kamion.name'				=> 10,
		'remark'					=> 8,
		'instruction.label'			=> 5,
		'instruction.wana_number'	=> 5,
		'user.first_name'			=> 3,
		'user.last_name'			=> 3
	];

	public function statuslogtime(){
		return $this->belongsTo('App\Api\v1\Models\StatusLogtime', 'status_logtime_id');
	}

	public function histories(){
		return $this->hasMany('App\Api\v1\Models\LogtimeHistory', 'logtime_id');
	}

	public function kamion(){
		return $this->belongsTo('App\Api\v1\Models\Kamion');
	}
	public function instruction(){
		return $this->belongsTo('App\Api\v1\Models\Instruction');
	}

	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User');
	}

	public function timecard(){
		return $this->belongsTo('App\Api\v1\Models\Timecard');
	}
}
