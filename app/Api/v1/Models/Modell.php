<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Modell extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'is_active', 'is_model', 'name'
	];

	protected $table = 'models';
	protected $rules = [
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function fields(){
		return $this->hasMany('App\Api\v1\Models\Field', 'model_id');
	}

	public function rows(){
		return $this->hasMany('App\Api\v1\Models\Row', 'model_id')->with(['cells']);
	}
}
