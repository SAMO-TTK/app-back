<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class MaterialResourceUser extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $table = 'material_resource_user';

	protected $fillable = [
		'material_resource_id', 'user_id'
	];

	protected $rules = [
	];

	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function user(){
		return $this->BelongsTo('App\Api\v1\Models\User');
	}

	public function material_resource(){
		return $this->BelongsTo('App\Api\v1\Models\MaterialResource');
	}
}
