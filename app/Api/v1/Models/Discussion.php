<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Discussion extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
        'content', 'is_open', 'from_client', 'deadline', 'element_id'
    ];
    protected $rules = [
		'content' => 'required|string',
		'is_open' => 'required|integer|max:255',
        'from_client' => 'required|integer|max:255',
		'deadline' => 'nullable|date',
		'element_id' => 'nullable|integer|exists:elements,id'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function element(){
        return $this->belongsTo('App\Api\v1\Models\Element');
    }

    public function messages(){
        return $this->hasMany('App\Api\v1\Models\Message');
    }

    public function users(){
        return $this->belongsToMany('App\Api\v1\Models\User', 'discussion_user')->withPivot('is_author', 'is_concerned');
    }

    public function documents(){
        return $this->belongsToMany('App\Api\v1\Models\Document', 'discussion_document');
    }
}
