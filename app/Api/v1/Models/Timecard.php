<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Timecard extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'date', 'cumul', 'process_step', 'user_id'
	];
	protected $rules = [
		'date'			=> 'required|string|max:255',
		'cumul'			=> 'nullable|string|max:255',
		'process_step'	=> 'nullable|string',
		'user_id'		=> 'required'
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function logtimes(){
		return $this->hasMany('App\Api\v1\Models\Logtime');
	}

	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User');
	}
}
