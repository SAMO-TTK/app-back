<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class UpdateField extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'cat_id', 'sub_cat_id','update_id'
	];
	protected $rules = [
		'cat_id'		=> 'required|integer',
		'sub_cat_id'	=> 'nullable|integer',
		'notification_ticket_id'		=> 'required|integer',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];
}
