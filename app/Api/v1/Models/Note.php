<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;
use Sofa\Eloquence\Eloquence;



class Note extends Model
{
	use ApiModel;
	use SoftDeletes;
	use Eloquence;

    protected $fillable = [
        'content', 'title', 'is_public', 'kamion_id', 'user_id'
    ];
    protected $rules = [
        'title'		=> 'required|string|max:255',
		'content'	=> 'nullable|string',
		'is_public'	=> 'required|integer|max:255',
		'kamion_id'	=> 'nullable|integer|exists:kamions,id',
        'user_id'	=> 'required|integer|exists:users,id'
    ];
	protected $searchableColumns = [
		'title'			=> 8,
		'content'		=> 10,
	];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function kamion(){
        return $this->belongsTo('App\Api\v1\Models\Kamion');
    }

    public function user(){
        return $this->belongsTo('App\Api\v1\Models\User');
    }

    public function documents(){
        return $this->belongsToMany('\App\Api\v1\Models\Document', 'document_note', 'note_id');
    }
    public function shared(){
        return $this->belongsToMany('App\Api\v1\Models\User', 'note_user', 'note_id', 'user_id');
    }

    public function last_notes(){
        return $this->belongsToMany('App\Api\v1\Models\User', 'last_notes')->withPivot('updated_at');
    }
}
