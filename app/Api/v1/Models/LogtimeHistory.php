<?php

namespace App\Api\v1\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class LogtimeHistory extends Model {

	use ApiModel, SoftDeletes;

	protected $dates    = [
		'created_at', 'updated_at', 'deleted_at'
	];


	protected $fillable = [
		'type', 'remark', 'update_name', 'user_id', 'logtime_id'
	];
	protected $rules = [
	];

	public function logtime(){
		return $this->belongsTo('App\Api\v1\Models\Logtime');
	}
	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User');
	}
}
