<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class NotificationTicket extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'ticket_id'
	];
	protected $rules = [
		'ticket_id'		=> 'required|integer',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function ticket(){
		return $this->belongsTo('App\Api\v1\Models\Ticket');
	}

	public function update_fields(){
		return $this->hasMany('App\Api\v1\Models\UpdateField', 'notification_ticket_id');
	}
}
