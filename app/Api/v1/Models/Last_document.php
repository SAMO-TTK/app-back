<?php

namespace App\Api\v1\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Last_document extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
        'document_id', 'user_id'
    ];
    protected $rules = [
		'document_id'	=> 'required|integer|exists:documents,id',
        'user_id'			=> 'required|integer|exists:users,id'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User');
	}
	public function document(){
		return $this->belongsTo('App\Api\v1\Models\Document');
	}
}
