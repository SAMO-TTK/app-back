<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Ticket_type extends Model
{
    use ApiModel;
	use SoftDeletes;

    protected $fillable = [
        'content', 'title', 'label'
    ];
    protected $rules = [
        'label'		=> 'required|string|max:255',
		'title'		=> 'nullable|string|max:255',
		'content'	=> 'nullable|string'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function tickets(){
        return $this->hasMany('App\Api\v1\Models\Ticket');
    }
}
