<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;
use Sofa\Eloquence\Eloquence;



class Notification extends Model
{
	use ApiModel;
	use SoftDeletes;
	use Eloquence;

	protected $fillable = [
		'title', 'icon', 'row1', 'row2', 'row3', 'type'
	];
	protected $rules = [
		'title'		=> 'required|string|max:255',
		'icon'		=> 'required|string|max:255',
		'row1'		=> 'nullable|string|max:255',
		'row2'		=> 'nullable|string|max:255',
		'row3'		=> 'nullable|string|max:255',
		'type'		=> 'required|string|max:255'
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User');
	}

	public function notification_ticket(){
		return $this->hasOne('\App\Api\v1\Models\NotificationTicket');
	}

	public function users(){
		return $this->belongsToMany('App\Api\v1\Models\User', 'notification_user', 'notification_id', 'user_id')->withPivot(['is_read']);
	}

	public function noreadusers(){
		return $this->belongsToMany('App\Api\v1\Models\User', 'notification_user', 'notification_id', 'user_id')->wherePivot('is_read', '=', 0	);
	}

	public function discussions(){
		return $this->belongsToMany('App\Api\v1\Models\Discussion');
	}
}
