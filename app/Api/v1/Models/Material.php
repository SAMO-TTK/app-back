<?php

namespace App\Api\v1\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;


class Material extends Model {

	use ApiModel, Eloquence, SoftDeletes;

	protected $dates    = [
		'created_at', 'updated_at', 'deleted_at'
	];


	protected $fillable = [
		'user_id', 'kamion_id', 'material_resource_id', 'instruction_id','quantity', 'wana_id'
	];
	protected $rules = [
		'user_id',
		'quantity',
		'kamion_id',
		'material_resource_id',
		'instruction_id',
		'wana_id'
	];

	protected $searchableColumns = [
		'wana_id'					=> 10,
		'material_resource.label'	=> 5,
		'material_resource.wana_id'	=> 5,
		'instruction.label'			=> 5,
		'instruction.wana_number'	=> 5,
		'user.first_name'			=> 3,
		'user.last_name'			=> 3
	];

	public function kamion(){
		return $this->belongsTo('App\Api\v1\Models\Kamion');
	}

	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User');
	}

	public function instruction(){
		return $this->belongsTo('App\Api\v1\Models\Instruction');
	}

	public function material_resource(){
		return $this->belongsTo('App\Api\v1\Models\MaterialResource');
	}
}
