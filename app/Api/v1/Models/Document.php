<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;
use Sofa\Eloquence\Eloquence;

class Document extends Model
{
	use ApiModel;
	use Eloquence;
	use SoftDeletes;

    protected $fillable = [
        'title', 'extension', 'mfile_id','author_id','mfile_object_id','mfile_created_at','mfile_updated_at','guid','is_unlinkable'
    ];

    protected $rules = [
        'title' => 'required|string|max:255',
        'extension'  => 'required|string|max:255',
        'mfile_id' => 'required|string|max:255',
        'mfile_object_id' => 'required|string|max:255',
        'mfile_created_at',
        'mfile_updated_at',
        'guid' => 'nullable|string|max:255',
        'author_id' => 'nullable|integer|exists:users,id',
		'is_unlinkable'=>'integer|max:255'
    ];
	protected $searchableColumns = [
		'title'		=> 10,
	];

    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function users(){
        return $this->belongsToMany('App\Api\v1\Models\User', 'document_user');
    }

    public function elements(){
        return $this->belongsToMany('App\Api\v1\Models\Element', 'document_element');
    }

	public function messages(){
		return $this->belongsToMany('App\Api\v1\Models\Message', 'document_message');
	}
    public function quality_controls(){
        return $this->belongsToMany('App\Api\v1\Models\Quality_controls', 'quality_document');
    }

    public function validation_controls(){
        return $this->belongsToMany('App\Api\v1\Models\Validation_controls', 'validation_document');
    }

    public function tickets(){
    	return $this->belongsToMany('App\Api\v1\Models\Ticket', 'document_ticket');
	}

	public function discussions(){
		return $this->belongsToMany('App\Api\v1\Models\Discussion', 'discussion_document');
	}

	public function kamions(){
		return $this->belongsToMany('App\Api\v1\Models\Kamion', 'document_kamion');
	}

	public function author(){
        return $this->belongsTo('App\Api\v1\Models\User', 'author_id');
    }

	public function last_documents(){
	    return $this->belongsToMany('App\Api\v1\Models\User', 'last_documents')->withPivot('updated_at');
    }
}
