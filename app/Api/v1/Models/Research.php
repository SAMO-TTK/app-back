<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Research extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'name','destinater', 'redacter', 'responsable', 'id_kamion', 'model', 'category', 'id_ticket', 'status', 'instruction','deadline','patate'
	];

	protected $rules = [
		'name'			=> 'required|string|max:255',
		'destinater'	=> 'nullable|string|max:255',
		'redacter'		=> 'nullable|string|max:255',
		'responsable'	=> 'nullable|string|max:255',
		'id_kamion'		=> 'nullable|string|max:255',
		'model'			=> 'nullable|string|max:255',
		'category'		=> 'nullable|string|max:255',
		'id_ticket'		=> 'nullable|string|max:255',
		'status'		=> 'nullable|string|max:255',
		'instruction'	=> 'nullable|string|max:255',
		'deadline'		=> 'nullable|integer',
		'patate'		=> 'nullable|integer',
	];

	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function users(){
		return $this->belongsToMany('App\Api\v1\Models\User');
	}
}
