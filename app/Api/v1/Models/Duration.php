<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Duration extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
			'date', 'reason', 'duration'
	];
	protected $rules = [
		'date', 'reason', 'duration'
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

}
