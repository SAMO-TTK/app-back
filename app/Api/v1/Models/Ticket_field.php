<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Ticket_field extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'value','field_value_id','ticket_id','field_id'
	];
	protected $rules = [
		'value'				=> 'nullable|string',
		'field_value_id'	=> 'nullable|integer',
		'ticket_id'			=> 'required|integer',
		'field_id'			=> 'required|integer',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function model(){
		return $this->belongsTo('App\Api\v1\Models\Ticket_model', 'model_id');
	}

	public function values(){
		return $this->belongsTo('App\Api\v1\Models\Field_value');
	}

	public function ticket_fields(){
		return $this->belongsTo('App\Api\v1\Models\Model_field', 'field_id');
	}

	public function tickets(){
		return $this->belongsToMany('App\Api\v1\Models\Ticket', 'ticket_fields', 'ticket_id', 'field_id')->withPivot('value');
	}


}