<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Announcement extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
        'content', 'title', 'kamion_id', 'user_id'
    ];
    protected $rules = [
        'title' => 'required|string|max:255',
        'user_id' => 'required|integer|exists:users,id',
		'kamion_id' => 'nullable|integer|exists:kamions,id'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function kamion(){
        return $this->belongsTo('App\Api\v1\Models\kamion');
    }

    public function user(){
        return $this->belongsTo('App\Api\v1\Models\user');
    }
}
