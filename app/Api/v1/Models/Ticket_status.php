<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Ticket_status extends Model
{
    use ApiModel;
    use SoftDeletes;

	protected $table = 'ticket_status';
    protected $fillable = [
        'label'
    ];
    protected $rules = [
        'label'		=> 'required|string|max:255',
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function tickets(){
        return $this->hasMany('App\Api\v1\Models\Ticket', 'status_id');
    }

}
