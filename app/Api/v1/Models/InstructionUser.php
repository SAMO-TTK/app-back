<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class InstructionUser extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $table = 'instruction_user';

	protected $fillable = [
		'instruction_id', 'user_id'
	];

	protected $rules = [
	];

	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function user(){
		return $this->BelongsTo('App\Api\v1\Models\User');
	}

	public function instruction(){
		return $this->BelongsTo('App\Api\v1\Models\Instruction');
	}
}
