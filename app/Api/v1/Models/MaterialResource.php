<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

use App\Api\v1\Traits\ApiModel;


class MaterialResource extends Model
{
	use ApiModel;
	use SoftDeletes;
	use Eloquence;

	protected $fillable = [
		'label','wana_id',
	];
	protected $rules = [
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];
	protected $searchableColumns = [
		'label'				=> 8,
		'wana_id'			=> 10,
	];


}
