<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class FavoryTicket extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'user_id', 'ticket_id'
	];
	protected $rules = [
		'user_id'		=> 'required|integer|exists:users,id',
		'ticket_id'		=> 'required|integer|exists:tickets,id',
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function ticket(){
		return $this->belongsTo('App\Api\v1\Models\Ticket', 'ticket_id');
	}
	public function user(){
		return $this->belongsTo('App\Api\v1\Models\User', 'user_id');
	}

}
