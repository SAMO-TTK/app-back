<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;

class Supplement extends Model
{
	use ApiModel;
	use SoftDeletes;

    protected $fillable = [
        'content', 'element_id', 'user_id'
    ];

    protected $rules = [
        'content'		=> 'nullable|string',
        'element_id'	=> 'required|integer|exists:elements,id',
		'user_id'		=> 'nullable|integer|exists:users,id'
    ];

    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

	public function user(){
		return $this->BelongsTo('App\Api\v1\Models\User');
	}

	public function element(){
		return $this->BelongsTo('App\Api\v1\Models\Element');
	}
}
