<?php

namespace App\Api\v1\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;
use App\Api\v1\Traits\ApiModel;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use ApiModel;
	use SoftDeletes;
	use Eloquence;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_connection',
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
		'is_active_on_wana',
        'wana_id',
        'badge_id',
        'smartphone_id',
        'is_active',
        'level',
		'manager_id',
		'job',
		'avatar',
		'is_quality_responsable',
		'is_validation_responsable'
	];

	protected $rules = [
		'last_connection'			=> 'nullable|date|date_format:Y-m-d H:i:s',
		'first_name'				=> 'required|string|max:255',
		'last_name'					=> 'required|string|max:255',
		'username'					=> 'required|string|max:255',
		'email'						=> 'nullable|string|max:255',
		'password'					=> 'required|string|max:255',
		'wana_id'					=> 'required|string|max:255',
		'badge_id'					=> 'nullable|string|max:255',
		'smartphone_id'				=> 'nullable|string|max:255',
		'is_active'					=> 'required|integer|max:255',
		'is_active_on_wana'			=> 'required|integer|max:255',
		'manager_id'				=> 'nullable|integer|exists:users,id',
		'job'						=> 'nullable|string|max:255',
		'avatar'					=> 'nullable|string|max:255',
    ];
	protected $searchableColumns = [
		'first_name'			=> 8,
		'last_name'		=> 8,
		'username'	=> 10,
		'email'		=> 5,
		'wana_id'			=> 10,
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function researches(){
    	$this->belongsToMany('App\Api\v1\Models\Research');
	}
	public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

	public function validation_controls(){
        return $this->hasMany('App\Api\v1\Models\Validation_control');
    }

	public function capabilities(){
		return $this->belongsToMany('App\Api\v1\Models\Capability', 'capability_user');
	}

	public function roles(){
		return $this->belongsToMany('App\Api\v1\Models\Role');
	}

	public function tickets(){
		return $this->hasMany('App\Api\v1\Models\Ticket');
	}

	public function notes(){
		return $this->hasMany('App\Api\v1\Models\Note');
	}
	public function favory_tickets(){
		return $this->hasMany('App\Api\v1\Models\FavoryTicket');
	}

	public function announcements(){
		return $this->hasMany('App\Api\v1\Models\Announcement');
	}

    public function quality_controls(){
        return $this->hasMany('App\Api\v1\Models\Quality_control');
    }

    public function resources(){
        return $this->belongsToMany('App\Api\v1\Models\Resource', 'resource_user')->withPivot('is_manager');
    }

    public function discussions(){
        return $this->belongsToMany('App\Api\v1\Models\Discussion', 'discussion_user')->withPivot('is_author', 'is_concerned');
    }

    public function jobs(){
        return $this->belongsToMany('App\Api\v1\Models\Job', 'job_user')->withPivot('is_manager');
    }

    public function kamions(){
        return $this->belongsToMany('App\Api\v1\Models\Kamion', 'kamion_user')->withPivot('is_kamion_manager', 'is_project_manager', 'is_pilot');
    }

    public function piloted(){
    	return $this->belongsToMany('App\Api\v1\Models\Kamion')->where('is_locked', 0)->wherePivot('is_pilot', 1);
	}

    public function documents(){
        return $this->belongsToMany('App\Api\v1\Models\Document', 'document_user');
    }

	public function manager(){
		return $this->belongsTo('App\Api\v1\Models\User', 'manager_id');
	}

	public function managed(){
		return $this->hasMany('App\Api\v1\Models\User', 'manager_id');
	}

	public function last_documents(){
		return $this->hasMany('App\Api\v1\Models\Last_document')->wherehas('document');
	}
	public function last_elements(){
		return $this->hasMany('App\Api\v1\Models\Last_element');
	}

	public function supplements(){
		return $this->hasMany('App\Api\v1\Models\Supplement');
	}

	public function elements(){
		return $this->belongsToMany('App\Api\v1\Models\Element', 'element_user');
	}
}
