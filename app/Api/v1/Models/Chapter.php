<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;
use Sofa\Eloquence\Eloquence;

class Chapter extends Model
{
	use ApiModel;
	use Eloquence;
	use SoftDeletes;

    protected $fillable = [
        'numbering', 'title', 'kamion_id', 'parent_id'
    ];
    protected $rules = [
        // 'imported_at'	=> 'required|date|date_format:Y-m-d H:i:s',
        'title'			=> 'required|string',
		'numbering'		=> 'required|string|max:255',
        'kamion_id'		=> 'required|integer|exists:kamions,id',
		'parent_id'		=> 'nullable|integer|exists:chapters,id'
    ];
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];
	protected $searchableColumns = [
		'title'			=> 8,
		'numbering'		=> 3,
	];

    public function kamion(){
        return $this->belongsTo('App\Api\v1\Models\Kamion');
    }

    public function elements(){
        return $this->hasMany('App\Api\v1\Models\Element');
    }

    public function parent(){
        return $this->belongsTo('App\Api\v1\Models\Chapter', 'parent_id');
    }

    public function children(){
        return $this->hasMany('App\Api\v1\Models\Chapter', 'parent_id');
    }
	public function all_children(){
		return $this->children()->with('all_children');
	}
	public function childrens(){
		return $this->children()->with('childrens', 'elements');
	}
	public function export(){
		return $this->children()->with('export', 'elements.discussions.messages.user', 'elements.discussions.users', 'elements.supplement', 'elements.impact', 'elements.quality_controls', 'elements.validation_controls');
	}
}
