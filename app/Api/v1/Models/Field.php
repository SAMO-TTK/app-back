<?php

namespace App\Api\v1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Api\v1\Traits\ApiModel;


class Field extends Model
{
	use ApiModel;
	use SoftDeletes;

	protected $fillable = [
		'model_id', 'type', 'value'
	];
	protected $rules = [
	];
	protected $date = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function rows(){
		return $this->hasMany('App\Api\v1\Models\Row')->with(['cells'])->orderBy('position');
	}

	public function cell(){
		return $this->hasOne('App\Api\v1\Models\Cell');
	}

	public function model(){
		return $this->belongsTo('App\Api\v1\Models\Modell', 'model_id');
	}
}
