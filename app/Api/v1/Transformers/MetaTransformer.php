<?php

namespace App\Api\v1\Transformers;

use App\Models\Meta;
use App\Models\MetaValue;
use League\Fractal\TransformerAbstract;

class MetaTransformer extends TransformerAbstract {

    public function transform($entity) {

        $return     = $entity->getAttributes();

        foreach($entity->getHidden() as $h) {
            unset($return[$h]);
        };

        $relations  = $entity->getRelations();

        foreach($relations as $k => $v) {

            if($k === 'metas') {
                $metas = [];

                foreach($v as $meta) {

                    $m      = $meta->getAttributes();
                    $pivot  = $meta->pivot->getAttributes();

                    if(is_null($pivot['meta_value_id'])) {
                        $m['value'] = $pivot['value'];

                    } else {
                        if($m['multiple']) {
                            if(isset($metas[$m['slug']])) {
                                $metas[$m['slug']]['value_id'][] = $pivot['meta_value_id'];

                            } else {
                                $m['value_id'][] = $pivot['meta_value_id'];
                            }
                        } else {
                            $m['value_id'] = $pivot['meta_value_id'];
                        }

                        foreach($meta->meta_values as $mv) {
                            $attr = $mv->getAttributes();

                            $m['options'][] = $mv;

                            if($m['multiple']) {

                                if(isset($metas[$m['slug']]) && $attr['id'] === $pivot['meta_value_id']) {
                                    $metas[$m['slug']]['value'][] = $attr['value'];

                                } elseif(isset($m['value_id']) && $attr['id'] === $pivot['meta_value_id']) {
                                    $m['value'][] = $attr['value'];

                                }

                            } else {
                                if($attr['id'] === $m['value_id']) {
                                    $m['value'] = $attr['value'];
                                }
                            }
                        }
                    }

                    if(!isset($metas[$m['slug']])) {
                        $metas[$m['slug']] = $m;
                    }

                }

                $return['metas'] = array_values($metas);

            } else {
                $return[$k] = $v;
            }
        }

        return $return;
    }

}
