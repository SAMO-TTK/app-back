<?php

namespace App\Console;

use App\Api\v1\Controllers\InstructionController;
use App\Api\v1\Controllers\KamionController;
use App\Api\v1\Controllers\UserController;
use App\Api\v1\Models\Notification;
use App\Api\v1\Models\Ticket;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		Commands\dumpToSeed::class
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
//		$schedule->timezone('Europe/Paris');

//		$schedule->call(function () {
//			$ticket = new Ticket;
//			$ticket->title = "CronSchedultYear";
//			$ticket->is_public = 0;
//			$ticket->creator_id = 1;
//			$ticket->status_id = 1;
//			$ticket->model_id = 1;
//			$ticket->number = 'TTK'.date('y').'/0000';
//			$ticket->deleted_at = date('Y-m-d H:i:s');
//			$ticket->save();
//		})->cron('1 0 1 1 *');
//
		//#region - Manage Ticket By Email
//		$schedule->call(function (){
//			new ManageTicketByEmail();
//		})->everyMinute();
		//#endregion

		//#region - Cron for status logtimes
		//wait to controls

		$schedule->call(function (){
			\DateInterval::createFromDateString('72 hours');
			$now = date_create();
			$notifications = Notification::where([
				['updated_at', '<', $now->sub(\DateInterval::createFromDateString('72 hours'))->format('Y-m-d H:i:s')],
				['is_read', 1]
			])->orWhere([
				['updated_at', '<', $now->sub(\DateInterval::createFromDateString('3 weeks'))->format('Y-m-d H:i:s')],
			])->get();
			foreach ($notifications as $notification){
				$notification->delete();
			}
		})->hourly();
	/*	$schedule->call(function (){
			$logtimes = DB::table('logtimes')
					->where('status_logtime_id', 1);
			foreach ($logtimes as $logtime) {
				DB::table('logtime_histories')->insert([
					'logtime_id' => $logtime->id,
					'user_id' => 0,
					'type' => 'submitted',
					'remark' => ''
				]);
			}
			$logtimes->update(['status_logtime_id' => 2]);
		})->dailyAt('18:00');

		*///controls to validations
		$schedule->call(function (){
			$logtimes = DB::table('logtimes')
				->where('status_logtime_id', 2)
				->orWhere('status_logtime_id', 3);

			foreach ($logtimes as $logtime) {
				DB::table('logtime_histories')->insert([
					'logtime_id' => $logtime->id,
					'user_id' => 0,
					'type' => 'controlled',
					'remark' => 'no_ok'
				]);
			}

			$logtimes->update(['status_logtime_id' => 6]);
		})->dailyAt('07:00');

		$schedule->call(function (){
			$logtimes = DB::table('logtimes')
				->where('status_logtime_id', 4);

			foreach ($logtimes as $logtime) {
				DB::table('logtime_histories')->insert([
					'logtime_id' => $logtime->id,
					'user_id' => 0,
					'type' => 'controlled',
					'remark' => 'ok'
				]);
			}

			$logtimes->update(['status_logtime_id' => 5]);
		})->dailyAt('7:00');
		//#endregion
/*
		//#region - Sync with WANA
		$schedule->call(function (){

			$curl = new \Curl\Curl();
			$curl->get('http://wkrest.toutenkamion.com/stock/'.date('Ymd'));
			$data = json_decode($curl->response);
			$count = count($data);
			if ($count) {
				foreach ($data as $i => $row) {
					if (!DB::table('material_resources')
						->where('wana_id', addslashes($row->sIDArticle))
						->get()
						->first()) {
						DB::table('material_resources')
							->insert([
								'label' => addslashes($row->sNomArticle),
								'wana_id' => addslashes($row->sIDArticle),
								'unit_label' => addslashes($row->sUnite),
							]);
					}
				}
			}
		})->hourly();

*/
		$schedule->call(function (){
			$uc = new UserController();
			$uc->update_all_user();
		})->dailyAt('3:00');

		$schedule->call(function (){
			$kc = new KamionController();
			$kc->update_all_kamion();
		})->hourly();
		$schedule->call(function (){
			$ic = new InstructionController();
			$ic->update_all_instruction();
		})->hourly();
		//#endregion
	}

	/**
	 * Register the Closure based commands for the application.
	 *
	 * @return void
	 */
	protected function commands()
	{
		require base_path('routes/console.php');
	}
}
