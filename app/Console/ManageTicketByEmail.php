<?php


namespace App\Console;

use App\Api\v1\Models\Model_field;
use Webklex\IMAP\Facades\Client;
use App\Api\v1\Models\Ticket_model;
use App\Api\v1\Models\Ticket_field;
use App\Api\v1\Models\Ticket_discussion;
use App\Api\v1\Models\Ticket_message;
use App\Api\v1\Models\Ticket;
use App\Api\v1\Models\User;
use App\Api\v1\Models\Kamion;
use App\Api\v1\Models\Permission;

class ManageTicketByEmail {

	private $newTicketClient;
	private $discussionTicketClient;

	/**
	 * The name of the folder contain the new ticket
	 * @var string
	 */
	private $newTicketFolder = 'newTTK';

	/**
	 * The name of the folder contain the ticket for discussions
	 * @var string
	 */
	private $discussionTicketFolder = 'TTK';

	/**
	 * Contains regex to parse BBCode
	 * @var array
	 */
	private $regex = [
		'kamion' => '/(?>\[kamion\]\s*(?P<kamion>[^[]+\S)\s*\[\/kamion\])/',
		'message' => '/(?>\[message\]\s*(?P<message>[^[]+\S)\s*\[\/message\])/',
		'titre' => '/(?>\[titre\]\s*(?P<titre>[^[]+\S)\s*\[\/titre\])/'
	];

	public function __construct() {
		$this->newTickets();
		$this->answerToTickets();
	}

	/**
	 * Check if new emails are available.
	 * If there are new emails, create a new tickets with the object and the content of the email
	 */
	private function newTickets() {
		//connect to the new_ticket imap account
		//see config/imap.php
		$this->newTicketClient = Client::account('new_ticket');
		$this->newTicketClient->connect();

		//get folder
		$folder = $this->newTicketClient->getFolder($this->newTicketFolder);

		//get messages in the selected folder
		$messages = $this->newTicketClient->getUnseenMessages($folder);

		//check if there are messages
		if (count($messages) > 0) {

			//get the id of the model dedicated to ticket created thanks to mail
			$model_id = Ticket_model::where('label', 'Email')->get()->first()->id;

			//read each message
			foreach ($messages as $message) {
				$subject = $message->getSubject();
				$body = $message->getTextBody();

				//get the user send the message and check if have the permission to manage ticket by email
				$user_permissions = [];
				$user = User::where('email', $message->getFrom()[0]->mail)->with(['roles.permissions'])->get()->first();

				if ($user) {
					$permission_tomanageticketbyemail = Permission::where('slug', 'tomanageticketbyemail')->get()->first();
					$permission_createfullticket = Permission::where('slug', 'createfullticket')->get()->first();
					foreach ($user->roles as $role) {
						foreach ($role->permissions as $permission) {
							$user_permissions[] = $permission->id;
						}
					}
				}


				if (!empty($subject) && $user && in_array($permission_tomanageticketbyemail->id, $user_permissions)) {
					$parsedBody = $this->getParsedData($body);//get data in the message

					//check if the information about kamion exist
					if (isset($parsedBody['kamion'])) {
						$kamion = Kamion::where('wana_id', $parsedBody['kamion'])->get()->first();
						$body = "";//set to must use message bbcode
					}

					//check if the information about message exist
					if (isset($parsedBody['message'])) {
						$body = $parsedBody['message'];
					}

					//create a new ticket
					$ticket = new Ticket;
					$ticket->title = $subject;
					$ticket->creator_id = $user->id;
					$ticket->model_id = $model_id;
					$ticket->status_id = in_array($permission_createfullticket->id, $user_permissions) ? 2 : 1;
					if (isset($kamion)) {
						$ticket->kamion_id = $kamion->id;
					}
					$ticket->save();

					//create and fill the ticket field
					if (!empty($body)) {
						$models_field = Model_field::where([
							'model_id' => 10,
							'type' => 'text',
							'label' => 'Message'
						])->get()->first();

						$ticket_field = new Ticket_field;
						$ticket_field->ticket_id = $ticket->id;
						$ticket_field->value = $body;
						$ticket_field->field_id = $models_field->id;
						$ticket_field->save();
					}
				}
			}
		}


	}


	/**
	 * Check if new emails are available
	 * If there are new emails, check if the ticket have already a discussion
	 * If there are already a discussion, answer to the last one created by the same user with the email content
	 * Else create a new discussion with the email content
	 */
	private function answerToTickets() {
		//connect to the ticket imap account
		//see config/imap.php
		$this->discussionTicketClient = Client::account('discussion_ticket');
		$this->discussionTicketClient->connect();

		//get folder
		$folder = $this->discussionTicketClient->getFolder($this->discussionTicketFolder);

		//get messages in the selected folder
		$messages = $this->discussionTicketClient ->getUnseenMessages($folder);

		//read each message
		foreach ($messages as $message) {
			$subject = $message->getSubject();
			$body = $message->getTextBody();
			$ticket_id = 0;

			//get the user send the message and check if have the permission to manage ticket by email
			$user_permissions = [];
			$user = User::where('email', $message->getFrom()[0]->mail)->with(['roles.permissions'])->get()->first();

			if ($user) {
				$permission_tomanageticketbyemail = Permission::where('slug', 'tomanageticketbyemail')->get()->first();
				foreach ($user->roles as $role) {
					foreach ($role->permissions as $permission) {
						$user_permissions[] = $permission->id;
					}
				}
			}

			//get the ticket_id in the subject of the message
			if (!empty($subject)) {
				preg_match('/\[(?P<ticket_id>\d+)]/', $subject, $parsedSubject);
				$ticket_id = $parsedSubject['ticket_id'];
			}

			if (!empty($ticket_id) && !empty($body) && $user && in_array($permission_tomanageticketbyemail->id, $user_permissions)) {
				$parsedBody = $this->getParsedData($body);
				$ticketExist = Ticket::where('id', $ticket_id)->get()->first();

				//check the ticket exist and if the email have a message
				if ($ticketExist && isset($parsedBody['message'])) {

					//get the most recent discussion created by the user
					$ticket_discussion = Ticket_discussion::where([
						'ticket_id' => $ticket_id,
						'created_by' => $user->id
					])->orderBy('updated_at', 'desc')->get()->first();//take the most recent discussion

					//check if the discussion exist and if it is open
					if ($ticket_discussion && $ticket_discussion->is_open) {
						//get message bbcode if it exist
						if (count($parsedBody) > 0 && isset($parsedBody['message'])) {
							$body = $parsedBody['message'];
						}

						//create new ticket message to the discussion
						$ticket_message = new Ticket_message;
						$ticket_message->content = $body;
						$ticket_message->user_id = $user->id;
						$ticket_message->discussion_id = $ticket_discussion->id;
						$ticket_message->save();

					} else {
						$title = "";

						if (isset($parsedBody['titre'])) {
							$title = $parsedBody['titre'];
						}
						if (isset($parsedBody['message'])) {
							$body = $parsedBody['message'];
							if (empty($title)) {
								$title = implode(' ', array_slice(explode(' ', $body), 0, 5));;//take 5 first word of the message
							}
						}

						if (!empty($title)) {
							//create a new ticket discussion
							$ticket_discussion = new Ticket_discussion;
							$ticket_discussion->title = $title;
							$ticket_discussion->content = $body;
							$ticket_discussion->ticket_id = $ticket_id;
							$ticket_discussion->created_by = $user->id;
							$ticket_discussion->is_open = 1;
							$ticket_discussion->save();
						}

					}
				}


			}
		}
	}

	/**
	 * Parse data to return the content of the BBCode values in the string
	 * @param string $data
	 * @return array
	 */
	private function getParsedData($data) {
		$array = [];

		foreach ($this->regex as $key => $value) {
			preg_match($value, $data, $matches);
			if (isset($matches[$key])) {
				$array[$key] = $matches[$key];
			}
		}

		return $array;
	}

}