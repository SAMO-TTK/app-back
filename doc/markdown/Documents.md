# Documents

## Retrieve all documents [GET /]

Default returned fields : `id`, `title`, `extension`, `path`, `mfile_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `users`, `quality_controls`, `elements`, `validation_controls`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"title": "magnam-ut-eos-ab-praesentium-temporibus-nam-aliquid",
            			"extension": "xop",
            			"path": "quas\/dolorem\/nihil\/",
            			"mfile_id": "4024007171818"
            		},
            		{
            			"id": 2,
            			"title": "nihil-voluptas-neque-voluptas",
            			"extension": "swf",
            			"path": "nulla\/nobis\/nihil\/",
            			"mfile_id": "5175104496463900"
            		}
                ]
            }

## Retrieve a document by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the document
    + embed: (array, optional) - Embed linked resources : `users`, `quality_controls`, `elements`, `validation_controls`

+ Response 200 (application/json)
    + Body

            {
            	"data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"title": "magnam-ut-eos-ab-praesentium-temporibus-nam-aliquid",
            		"extension": "xop",
            		"path": "quas\/dolorem\/nihil\/",
            		"mfile_id": "4024007171818"
            	}
            }

## Create a document [POST /]
Required field : `title`, `extension`, `path`, `mfile_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            title=coucou_hiboux&extension=avi&path=lapin%2Fstorage%2Ftest%2F&mfile_id=54416516145

+ Response 200 (application/json)
    + Body

            {
                "document": {
            		"title": "coucou_hiboux",
            		"extension": "avi",
            		"path": "lapin\/storage\/test\/",
            		"mfile_id": "54416516145",
            		"updated_at": "2017-08-08 12:58:06",
            		"created_at": "2017-08-08 12:58:06",
            		"id": 12
            	}
            }

## Update a document [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the document

+ Request (application/x-www-form-urlencoded)
    + Body

            title=maitre%20renard

+ Response 200 (application/json)
    + Body

            {
            	"document": {
            		"id": 12,
            		"created_at": "2017-08-08 12:58:06",
            		"updated_at": "2017-08-08 13:00:32",
            		"deleted_at": null,
            		"title": "maitre renard",
            		"extension": "avi",
            		"path": "lapin\/storage\/test\/",
            		"mfile_id": "54416516145"
            	}
            }

## Delete a document [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the document

+ Response 200 (application/json)
    + Body

            1
