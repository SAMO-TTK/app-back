# Resources

## Retrieve all resources [GET /]

Default returned fields : `id`, `label`, `wana_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `users`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"label": "Sienna",
            			"wana_id": "4485663261946314"
            		},
            		{
            			"id": 2,
            			"label": "MediumOrchid",
            			"wana_id": "4716816449174"
            		}
                ]
            }

## Retrieve a resources by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the resources
    + embed: (array, optional) - Embed linked resources : `users`

+ Response 200 (application/json)
    + Body

            {
                "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"label": "Sienna",
            		"wana_id": "4485663261946314"
            	}
            }

## Create a resources [POST /]
Required field : `label`, `wana_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            label=Sienna&wana_id=4485663261946314

+ Response 200 (application/json)
    + Body

            {
            	"resource": {
            		"label": "Sienna",
            		"wana_id": "4485663261946314",
            		"updated_at": "2017-08-08 14:02:29",
            		"created_at": "2017-08-08 14:02:29",
            		"id": 9
            	}
            }

## Update a resources [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the resources

+ Request (application/x-www-form-urlencoded)
    + Body

            wana_id=0

+ Response 200 (application/json)
    + Body

            {
            	"resource": {
                	"id": 9,
                	"created_at": "2017-08-08 14:02:29",
                	"updated_at": "2017-08-08 14:03:49",
                	"deleted_at": null,
                	"label": "Sienna",
                	"wana_id": "0"
            	}
            }

## Delete a resources [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the resources

+ Response 200 (application/json)
    + Body

            1
