# Chapters

## Retrieve all chapters [GET /]

Default returned fields : `id`, `imported_at`, `title`, `kamion_id`, `parent_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `parent`, `childrens`, `kamion`, `elements`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 1,
                        "imported_at": "2017-07-27 03:40:18",
                        "title": "Neque sunt esse dolore.",
                        "kamion_id": 2,
                        "parent_id": null
                    },
                    {
                        "id": 2,
                        "imported_at": "2017-08-03 22:19:07",
                        "title": "Est voluptatem quisquam eaque.",
                        "kamion_id": 5,
                        "parent_id": null
                    }
                ]
            }

## Retrieve a chapter by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the chapter
    + embed: (array, optional) - Embed linked resources : `parent`, `childrens`, `kamion`, `elements`

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 1,
                    "created_at": "2017-08-08 10:06:52",
                    "updated_at": "2017-08-08 10:06:52",
                    "deleted_at": null,
                    "imported_at": "2017-07-27 03:40:18",
                    "title": "Neque sunt esse dolore.",
                    "kamion_id": 2,
                    "parent_id": null
                }
            }

## Create an chapter [POST /]
Required field : `imported_at`, `title`, `kamion_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            title=douche&kamion_id=4&imported_at=2017-07-29%2004%3A24%3A49

+ Response 200 (application/json)
    + Body

            {
                "chapter": {
                    "title": "douche",
                    "kamion_id": "4",
                    "imported_at": "2017-07-29 04:24:49",
                    "updated_at": "2017-08-08 11:31:43",
                    "created_at": "2017-08-08 11:31:43",
                    "id": 9
                }
            }

## Update a chapter [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the chapter

+ Request (application/x-www-form-urlencoded)
    + Body

            parent_id=1

+ Response 200 (application/json)
    + Body

            {
                "chapter": {
                    "id": 9,
                    "created_at": "2017-08-08 11:31:43",
                    "updated_at": "2017-08-08 11:33:06",
                    "deleted_at": null,
                    "imported_at": "2017-07-29 04:24:49",
                    "title": "douche",
                    "kamion_id": 4,
                    "parent_id": "1"
                }
            }

## Delete a chapter [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the chapter

+ Response 200 (application/json)
    + Body

            1
# Documents

## Retrieve all documents [GET /]

Default returned fields : `id`, `title`, `extension`, `path`, `mfile_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `users`, `quality_controls`, `elements`, `validation_controls`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"title": "magnam-ut-eos-ab-praesentium-temporibus-nam-aliquid",
            			"extension": "xop",
            			"path": "quas\/dolorem\/nihil\/",
            			"mfile_id": "4024007171818"
            		},
            		{
            			"id": 2,
            			"title": "nihil-voluptas-neque-voluptas",
            			"extension": "swf",
            			"path": "nulla\/nobis\/nihil\/",
            			"mfile_id": "5175104496463900"
            		}
                ]
            }

## Retrieve a document by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the document
    + embed: (array, optional) - Embed linked resources : `users`, `quality_controls`, `elements`, `validation_controls`

+ Response 200 (application/json)
    + Body

            {
            	"data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"title": "magnam-ut-eos-ab-praesentium-temporibus-nam-aliquid",
            		"extension": "xop",
            		"path": "quas\/dolorem\/nihil\/",
            		"mfile_id": "4024007171818"
            	}
            }

## Create a document [POST /]
Required field : `title`, `extension`, `path`, `mfile_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            title=coucou_hiboux&extension=avi&path=lapin%2Fstorage%2Ftest%2F&mfile_id=54416516145

+ Response 200 (application/json)
    + Body

            {
                "document": {
            		"title": "coucou_hiboux",
            		"extension": "avi",
            		"path": "lapin\/storage\/test\/",
            		"mfile_id": "54416516145",
            		"updated_at": "2017-08-08 12:58:06",
            		"created_at": "2017-08-08 12:58:06",
            		"id": 12
            	}
            }

## Update a document [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the document

+ Request (application/x-www-form-urlencoded)
    + Body

            title=maitre%20renard

+ Response 200 (application/json)
    + Body

            {
            	"document": {
            		"id": 12,
            		"created_at": "2017-08-08 12:58:06",
            		"updated_at": "2017-08-08 13:00:32",
            		"deleted_at": null,
            		"title": "maitre renard",
            		"extension": "avi",
            		"path": "lapin\/storage\/test\/",
            		"mfile_id": "54416516145"
            	}
            }

## Delete a document [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the document

+ Response 200 (application/json)
    + Body

            1
# Elements

## Retrieve all elements [GET /]

Default returned fields : `id`, `imported_ad`, `title`, `content`, `supplement`, `chapter_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `quality_controls`, `chapter`, `documents`, `validation_controls`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"imported_at": "2017-08-01 02:29:19",
            			"title": "consequuntur",
            			"content": null,
            			"supplement": null,
            			"chapter_id": 8
            		},
            		{
            			"id": 2,
            			"imported_at": "2017-07-21 16:17:48",
            			"title": "consectetur",
            			"content": null,
            			"supplement": null,
            			"chapter_id": 3
            		}
                ]
            }

## Retrieve an element by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the element
    + embed: (array, optional) - Embed linked resources : `quality_controls`, `chapter`, `documents`, `validation_controls`

+ Response 200 (application/json)
    + Body

            {
            	"data": {
            		"id": 5,
            		"created_at": "2017-08-08 10:06:52",
            		"updated_at": "2017-08-08 10:06:52",
            		"deleted_at": null,
            		"imported_at": "2017-08-06 22:25:03",
            		"title": "corrupti",
            		"content": null,
            		"supplement": null,
            		"chapter_id": 1
            	}
            }

## Create an element [POST /]
Required field : `title`, `imported_at`, `chapter_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            title=roux&imported_at=2017-07-29%2004%3A24%3A49&chapiter_id=4

+ Response 200 (application/json)
    + Body

            {
            	"element": {
            		"title": "roux",
            		"chapter_id": "4",
            		"imported_at": "2017-07-29 04:24:49",
            		"updated_at": "2017-08-08 13:11:33",
            		"created_at": "2017-08-08 13:11:33",
            		"id": 13
            	}
            }

## Update an element [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the element

+ Request (application/x-www-form-urlencoded)
    + Body

            content=Je%20me%20souviens%20en%20fait,%20je%20sais%20que,%20gr%C3%A2ce%20%C3%A0%20ma%20propre%20v%C3%A9rit%C3%A9%20le%20cycle%20du%20cosmos%20dans%20la%20vie...%20c'est%20une%20grande%20roue%20parce%20que%20spirituellement,%20on%20est%20tous%20ensemble,%20ok%20%3F%20Et%20tu%20as%20envie%20de%20le%20dire%20au%20monde%20entier,%20including%20yourself.%20

+ Response 200 (application/json)
    + Body

            {
            	"element": {
            		"id": 13,
            		"created_at": "2017-08-08 13:11:33",
            		"updated_at": "2017-08-08 13:15:13",
            		"deleted_at": null,
            		"imported_at": "2017-07-29 04:24:49",
            		"title": "roux",
            		"content": "Je me souviens en fait, je sais que, grâce à ma propre vérité le cycle du cosmos dans la vie... c'est une grande roue parce que spirituellement, on est tous ensemble, ok ? Et tu as envie de le dire au monde entier, including yourself.",
            		"supplement": null,
            		"chapter_id": 4
            	}
            }

## Delete an element [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the element

+ Response 200 (application/json)
    + Body

            1
# Jobs

## Retrieve all jobs [GET /]

Default returned fields : `id`, `label`, `wana_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `users`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"label": "Purchasing Agent",
            			"wana_id": "5459562051949901"
            		},
            		{
            			"id": 2,
            			"label": "Pile-Driver Operator",
            			"wana_id": "5306572426591820"
            		}
                ]
            }

## Retrieve a jobs by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the element
    + embed: (array, optional) - Embed linked resources : `users`

+ Response 200 (application/json)
    + Body

            {
            "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"label": "Purchasing Agent",
            		"wana_id": "5459562051949901"
            	}
            }

## Create a jobs [POST /]
Required field : `label`, `wana_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            label=big-boss&wana_id=46543586853

+ Response 200 (application/json)
    + Body

            {
            	"job": {
            		"label": "big-boss",
            		"wana_id": "46543586853",
            		"updated_at": "2017-08-08 13:22:18",
            		"created_at": "2017-08-08 13:22:18",
            		"id": 8
            	}
            }

## Update a jobs [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the jobs

+ Request (application/x-www-form-urlencoded)
    + Body

            "wana_id": "498321684"

+ Response 200 (application/json)
    + Body

            {
            	"job": {
            		"id": 8,
            		"created_at": "2017-08-08 13:22:18",
            		"updated_at": "2017-08-08 13:23:21",
            		"deleted_at": null,
            		"label": "big-boss",
            		"wana_id": "498321684"
            	}
            }

## Delete a jobs [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the jobs

+ Response 200 (application/json)
    + Body

            1
# Kamions

## Retrieve all kamions [GET /]

Default returned fields : `id`, `wana_id`, `name`, `number`, `length`, `amount`, `category`, `type`, `phase`, `planned_time`, `worked_time`, `is_delivered`, `delivery`, `frame_delivery`, `ordered_at`, `wana_created_at`, `wana_updated_at`, `coeff_mp`, `hourly_rate`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `users`, `chapters`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"wana_id": "5514591826323483",
            			"name": "BlanchedAlmond",
            			"number": 429058686,
            			"length": null,
            			"amount": null,
            			"category": null,
            			"type": null,
            			"phase": null,
            			"planned_time": null,
            			"worked_time": null,
            			"is_delivered": null,
            			"delivery": null,
            			"frame_delivery": null,
            			"ordered_at": null,
            			"wana_created_at": null,
            			"wana_updated_at": null,
            			"coeff_mp": null,
            			"hourly_rate": null
            		},
            		{
            			"id": 2,
            			"wana_id": "4716959563039",
            			"name": "Cornsilk",
            			"number": 993982716,
            			"length": null,
            			"amount": null,
            			"category": null,
            			"type": null,
            			"phase": null,
            			"planned_time": null,
            			"worked_time": null,
            			"is_delivered": null,
            			"delivery": null,
            			"frame_delivery": null,
            			"ordered_at": null,
            			"wana_created_at": null,
            			"wana_updated_at": null,
            			"coeff_mp": null,
            			"hourly_rate": null
            		}
                ]
            }

## Retrieve a kamion by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the kamion
    + embed: (array, optional) - Embed linked resources : `users`, `chapters`

+ Response 200 (application/json)
    + Body

            {
            "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"wana_id": "5514591826323483",
            		"name": "BlanchedAlmond",
            		"number": 429058686,
            		"length": null,
            		"amount": null,
            		"category": null,
            		"type": null,
            		"phase": null,
            		"planned_time": null,
            		"worked_time": null,
            		"is_delivered": null,
            		"delivery": null,
            		"frame_delivery": null,
            		"ordered_at": null,
            		"wana_created_at": null,
            		"wana_updated_at": null,
            		"coeff_mp": null,
            		"hourly_rate": null
            	}
            }

## Create a kamion [POST /]
Required field : `name`, `wana_id`, `number`

+ Request (application/x-www-form-urlencoded)
    + Body

            name=mongrauhquamion&wana_id=5843321684&number=201

+ Response 200 (application/json)
    + Body

            {
            	"kamion": {
            		"name": "mongrauhquamion",
            		"wana_id": "5843321684",
            		"number": "201",
            		"updated_at": "2017-08-08 13:40:04",
            		"created_at": "2017-08-08 13:40:04",
            		"id": 6
            	}
            }

## Update a kamion [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the kamion

+ Request (application/x-www-form-urlencoded)
    + Body

            "type": "metal"

+ Response 200 (application/json)
    + Body

            {
            	"kamion": {
            		"id": 6,
            		"created_at": "2017-08-08 13:40:04",
            		"updated_at": "2017-08-08 13:41:58",
            		"deleted_at": null,
            		"wana_id": "5843321684",
            		"name": "mongrauhquamion",
            		"number": 201,
            		"length": null,
            		"amount": null,
            		"category": null,
            		"type": "metal",
            		"phase": null,
            		"planned_time": null,
            		"worked_time": null,
            		"is_delivered": null,
            		"delivery": null,
            		"frame_delivery": null,
            		"ordered_at": null,
            		"wana_created_at": null,
            		"wana_updated_at": null,
            		"coeff_mp": null,
            		"hourly_rate": null
            	}
            }

## Delete a kamion [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the kamion

+ Response 200 (application/json)
    + Body

            1
# Quality_controls

## Retrieve all quality_control [GET /]

Default returned fields : `id`, `code`, `type`, `note`, `element_id`, `user_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `user`, `element`, `documents`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"code": "AVKFRVVG",
            			"type": "ti_ET",
            			"note": "Et ut expedita et velit dolorem. Illo et eum rerum aut nulla. Adipisci rerum et tempora consequatur impedit in. Sunt id at quam enim.",
            			"element_id": 5,
            			"user_id": 15
            		},
            		{
            			"id": 2,
            			"code": "APPIZW20",
            			"type": "en_CA",
            			"note": "Veritatis deserunt magni corrupti incidunt placeat. Sed repudiandae odit a hic. Nulla natus ducimus velit. Expedita non voluptatem asperiores quas.",
            			"element_id": 9,
            			"user_id": 7
            		}
                ]
            }

## Retrieve a quality_control by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the quality_control
    + embed: (array, optional) - Embed linked resources : `user`, `element`, `documents`

+ Response 200 (application/json)
    + Body

            {
                "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:35",
            		"updated_at": "2017-08-08 11:17:35",
            		"deleted_at": null,
            		"code": "AVKFRVVG",
            		"type": "ti_ET",
            		"note": "Et ut expedita et velit dolorem. Illo et eum rerum aut nulla. Adipisci rerum et tempora consequatur impedit in. Sunt id at quam enim.",
            		"element_id": 5,
            		"user_id": 15
            	}
            }

## Create a quality_control [POST /]
Required field : `code`, `type`, `element_id`, `user_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            code=NC&type=pre-eval&element_id=1&user_id7

+ Response 200 (application/json)
    + Body

            {
            	"quality_control": {
            		"code": "NC",
            		"type": "pre-eval",
            		"element_id": "1",
            		"user_id": "7",
            		"updated_at": "2017-08-08 13:54:58",
            		"created_at": "2017-08-08 13:54:58",
            		"id": 4
            	}
            }

## Update a quality_control [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the quality_control

+ Request (application/x-www-form-urlencoded)
    + Body

            note=Et%20ut%20expedita%20et%20velit%20dolorem.%20Illo%20et%20eum%20rerum%20aut%20nulla.%20Adipisci%20rerum%20et%20tempora%20consequatur%20impedit%20in.%20Sunt%20id%20at%20quam%20enim.

+ Response 200 (application/json)
    + Body

            {
            	"quality_control": {
            		"id": 4,
            		"created_at": "2017-08-08 13:54:58",
            		"updated_at": "2017-08-08 13:56:37",
            		"deleted_at": null,
            		"code": "NC",
            		"type": "pre-eval",
            		"note": "Et ut expedita et velit dolorem. Illo et eum rerum aut nulla. Adipisci rerum et tempora consequatur impedit in. Sunt id at quam enim.",
            		"element_id": 1,
            		"user_id": 7
            	}
            }

## Delete a quality_control [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the quality_control

+ Response 200 (application/json)
    + Body

            1
# Resources

## Retrieve all resources [GET /]

Default returned fields : `id`, `label`, `wana_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `users`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"label": "Sienna",
            			"wana_id": "4485663261946314"
            		},
            		{
            			"id": 2,
            			"label": "MediumOrchid",
            			"wana_id": "4716816449174"
            		}
                ]
            }

## Retrieve a resources by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the resources
    + embed: (array, optional) - Embed linked resources : `users`

+ Response 200 (application/json)
    + Body

            {
                "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"label": "Sienna",
            		"wana_id": "4485663261946314"
            	}
            }

## Create a resources [POST /]
Required field : `label`, `wana_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            label=Sienna&wana_id=4485663261946314

+ Response 200 (application/json)
    + Body

            {
            	"resource": {
            		"label": "Sienna",
            		"wana_id": "4485663261946314",
            		"updated_at": "2017-08-08 14:02:29",
            		"created_at": "2017-08-08 14:02:29",
            		"id": 9
            	}
            }

## Update a resources [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the resources

+ Request (application/x-www-form-urlencoded)
    + Body

            wana_id=0

+ Response 200 (application/json)
    + Body

            {
            	"resource": {
                	"id": 9,
                	"created_at": "2017-08-08 14:02:29",
                	"updated_at": "2017-08-08 14:03:49",
                	"deleted_at": null,
                	"label": "Sienna",
                	"wana_id": "0"
            	}
            }

## Delete a resources [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the resources

+ Response 200 (application/json)
    + Body

            1
# Users

## Retrieve all users [GET /]

Default returned fields : `id`, `last_connection`, `first_name`, `last_name`, `username`, `email`, `wana_id`, `is_job_manager`, `is_kamion_manager`, `his_manager`, `badge_id`, `smartphone_id`, `is_active`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `jobs`, `resources`, `quality_controls`, `validation_controls`, `documents`, `kamions`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"last_connection": null,
            			"first_name": "Michaela",
            			"last_name": "Macejkovic",
            			"username": "annabell.keeling",
            			"email": "predovic.isabel@adams.info",
            			"wana_id": "6011756014952155",
            			"is_job_manager": 3,
            			"is_kamion_manager": 5,
            			"his_manager": "false",
            			"badge_id": null,
            			"smartphone_id": null,
            			"is_active": 4
            		},
            		{
            			"last_connection": null,
            			"first_name": "Robb",
            			"last_name": "Goldner",
            			"username": "garrick22",
            			"email": "janick98@vonrueden.biz",
            			"wana_id": "5228129500200813",
            			"is_job_manager": 0,
            			"is_kamion_manager": 2,
            			"his_manager": "false",
            			"badge_id": null,
            			"smartphone_id": null,
            			"is_active": 4
            		}
                ]
            }

## Retrieve a user by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the user
    + embed: (array, optional) - Embed linked resources : `jobs`, `resources`, `quality_controls`, `validation_controls`, `documents`, `kamions`

+ Response 200 (application/json)
    + Body

            {
                "data": {
            		"id": 2,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"last_connection": null,
            		"first_name": "Robb",
            		"last_name": "Goldner",
            		"username": "garrick22",
            		"email": "janick98@vonrueden.biz",
            		"wana_id": "5228129500200813",
            		"is_job_manager": 0,
            		"is_kamion_manager": 2,
            		"his_manager": "false",
            		"badge_id": null,
            		"smartphone_id": null,
            		"is_active": 4
            	}
            }

## Create an user [POST /]
Required field : `first_name`, `last_name`, `username`, `password`, `wana_id`, `is_job_manager`, `is_kamion_manager`, `his_manager`, `is_active`

+ Request (application/x-www-form-urlencoded)
    + Body

            first_name=jhon&last_name=doe&username=jd5225&password=lapin18&wana_id=16843259756248&is_job_manager=0&is_kamion_manager=0&his_manager=false&is_active=1

+ Response 200 (application/json)
    + Body

            {
                "user": {
            		"first_name": "jhon",
            		"last_name": "doe",
            		"username": "jd5225",
            		"wana_id": "16843259756248",
            		"is_job_manager": "0",
            		"is_kamion_manager": "0",
            		"his_manager": "false",
            		"is_active": "1",
            		"updated_at": "2017-08-08 12:43:44",
            		"created_at": "2017-08-08 12:43:44",
            		"id": 17
            	}
            }

## Update a user [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the user

+ Request (application/x-www-form-urlencoded)
    + Body

            badge_id=4568a7e54

+ Response 200 (application/json)
    + Body

            {
                "user": {
            		"id": 9,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 12:46:19",
            		"deleted_at": null,
            		"last_connection": null,
            		"first_name": "Sigrid",
            		"last_name": "Nitzsche",
            		"username": "brannon.witting",
            		"email": "jeramy13@yahoo.com",
            		"wana_id": "5543745700994885",
            		"is_job_manager": 0,
            		"is_kamion_manager": 2,
            		"his_manager": "true",
            		"badge_id": "4568a7e54",
            		"smartphone_id": null,
            		"is_active": 0
            	}
            }

## Delete a user [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the user

+ Response 200 (application/json)
    + Body

            1
# Validation_controls

## Retrieve all validation_controls [GET /]

Default returned fields : `id`, `is_controled`, `note`, `element_id`, `user_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `user`, `element`, `documents`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"is_controled": 0,
            			"note": "0",
            			"element_id": 7,
            			"user_id": 7
            		},
            		{
            			"id": 2,
            			"is_controled": 0,
            			"note": "2",
            			"element_id": 8,
            			"user_id": 6
            		}
                ]
            }

## Retrieve a validation_control by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the validation_control
    + embed: (array, optional) - Embed linked validation_control : `user`, `element`, `documents`

+ Response 200 (application/json)
    + Body

            {
                "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:35",
            		"updated_at": "2017-08-08 11:17:35",
            		"deleted_at": null,
            		"is_controled": 0,
            		"note": "0",
            		"element_id": 7,
            		"user_id": 7
            	}
            }

## Create a validation_control [POST /]
Required field : `is_controled`, `element_id`, `user_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            is_controled=3&element_id=7&user_id=11

+ Response 200 (application/json)
    + Body

            {
            	"validation_control": {
            		"is_controled": "3",
            		"element_id": "7",
            		"user_id": "11",
            		"updated_at": "2017-08-08 14:12:16",
            		"created_at": "2017-08-08 14:12:16",
            		"id": 11
            	}
            }

## Update a validation_control [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the validation_control

+ Request (application/x-www-form-urlencoded)
    + Body

            note=Tu%20vois,%20m%C3%AAme%20si%20on%20frime%20comme%20on%20appelle%20%C3%A7a%20en%20France...%20le%20cycle%20du%20cosmos%20dans%20la%20vie...%20c'est%20une%20grande%20roue%20et%20je%20ne%20cherche%20pas%20ici%20%C3%A0%20mettre%20un%20point%20!%20Et%20tu%20as%20envie%20de%20le%20dire%20au%20monde%20entier,%20including%20yourself.%20


+ Response 200 (application/json)
    + Body

            {
            	"validation_control": {
            		"id": 11,
            		"created_at": "2017-08-08 14:12:16",
            		"updated_at": "2017-08-08 14:13:46",
            		"deleted_at": null,
            		"is_controled": 3,
            		"note": "Tu vois, même si on frime comme on appelle ça en France... le cycle du cosmos dans la vie... c'est une grande roue et je ne cherche pas ici à mettre un point ! Et tu as envie de le dire au monde entier, including yourself.",
            		"element_id": 7,
            		"user_id": 11
            	}
            }

## Delete a validation_control [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the validation_control

+ Response 200 (application/json)
    + Body

            1
