# Users

## Retrieve all users [GET /]

Default returned fields : `id`, `last_connection`, `first_name`, `last_name`, `username`, `email`, `wana_id`, `is_job_manager`, `is_kamion_manager`, `his_manager`, `badge_id`, `smartphone_id`, `is_active`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `jobs`, `resources`, `quality_controls`, `validation_controls`, `documents`, `kamions`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"last_connection": null,
            			"first_name": "Michaela",
            			"last_name": "Macejkovic",
            			"username": "annabell.keeling",
            			"email": "predovic.isabel@adams.info",
            			"wana_id": "6011756014952155",
            			"is_job_manager": 3,
            			"is_kamion_manager": 5,
            			"his_manager": "false",
            			"badge_id": null,
            			"smartphone_id": null,
            			"is_active": 4
            		},
            		{
            			"last_connection": null,
            			"first_name": "Robb",
            			"last_name": "Goldner",
            			"username": "garrick22",
            			"email": "janick98@vonrueden.biz",
            			"wana_id": "5228129500200813",
            			"is_job_manager": 0,
            			"is_kamion_manager": 2,
            			"his_manager": "false",
            			"badge_id": null,
            			"smartphone_id": null,
            			"is_active": 4
            		}
                ]
            }

## Retrieve a user by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the user
    + embed: (array, optional) - Embed linked resources : `jobs`, `resources`, `quality_controls`, `validation_controls`, `documents`, `kamions`

+ Response 200 (application/json)
    + Body

            {
                "data": {
            		"id": 2,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"last_connection": null,
            		"first_name": "Robb",
            		"last_name": "Goldner",
            		"username": "garrick22",
            		"email": "janick98@vonrueden.biz",
            		"wana_id": "5228129500200813",
            		"is_job_manager": 0,
            		"is_kamion_manager": 2,
            		"his_manager": "false",
            		"badge_id": null,
            		"smartphone_id": null,
            		"is_active": 4
            	}
            }

## Create an user [POST /]
Required field : `first_name`, `last_name`, `username`, `password`, `wana_id`, `is_job_manager`, `is_kamion_manager`, `his_manager`, `is_active`

+ Request (application/x-www-form-urlencoded)
    + Body

            first_name=jhon&last_name=doe&username=jd5225&password=lapin18&wana_id=16843259756248&is_job_manager=0&is_kamion_manager=0&his_manager=false&is_active=1

+ Response 200 (application/json)
    + Body

            {
                "user": {
            		"first_name": "jhon",
            		"last_name": "doe",
            		"username": "jd5225",
            		"wana_id": "16843259756248",
            		"is_job_manager": "0",
            		"is_kamion_manager": "0",
            		"his_manager": "false",
            		"is_active": "1",
            		"updated_at": "2017-08-08 12:43:44",
            		"created_at": "2017-08-08 12:43:44",
            		"id": 17
            	}
            }

## Update a user [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the user

+ Request (application/x-www-form-urlencoded)
    + Body

            badge_id=4568a7e54

+ Response 200 (application/json)
    + Body

            {
                "user": {
            		"id": 9,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 12:46:19",
            		"deleted_at": null,
            		"last_connection": null,
            		"first_name": "Sigrid",
            		"last_name": "Nitzsche",
            		"username": "brannon.witting",
            		"email": "jeramy13@yahoo.com",
            		"wana_id": "5543745700994885",
            		"is_job_manager": 0,
            		"is_kamion_manager": 2,
            		"his_manager": "true",
            		"badge_id": "4568a7e54",
            		"smartphone_id": null,
            		"is_active": 0
            	}
            }

## Delete a user [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the user

+ Response 200 (application/json)
    + Body

            1
