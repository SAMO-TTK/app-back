# Quality_controls

## Retrieve all quality_control [GET /]

Default returned fields : `id`, `code`, `type`, `note`, `element_id`, `user_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `user`, `element`, `documents`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"code": "AVKFRVVG",
            			"type": "ti_ET",
            			"note": "Et ut expedita et velit dolorem. Illo et eum rerum aut nulla. Adipisci rerum et tempora consequatur impedit in. Sunt id at quam enim.",
            			"element_id": 5,
            			"user_id": 15
            		},
            		{
            			"id": 2,
            			"code": "APPIZW20",
            			"type": "en_CA",
            			"note": "Veritatis deserunt magni corrupti incidunt placeat. Sed repudiandae odit a hic. Nulla natus ducimus velit. Expedita non voluptatem asperiores quas.",
            			"element_id": 9,
            			"user_id": 7
            		}
                ]
            }

## Retrieve a quality_control by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the quality_control
    + embed: (array, optional) - Embed linked resources : `user`, `element`, `documents`

+ Response 200 (application/json)
    + Body

            {
                "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:35",
            		"updated_at": "2017-08-08 11:17:35",
            		"deleted_at": null,
            		"code": "AVKFRVVG",
            		"type": "ti_ET",
            		"note": "Et ut expedita et velit dolorem. Illo et eum rerum aut nulla. Adipisci rerum et tempora consequatur impedit in. Sunt id at quam enim.",
            		"element_id": 5,
            		"user_id": 15
            	}
            }

## Create a quality_control [POST /]
Required field : `code`, `type`, `element_id`, `user_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            code=NC&type=pre-eval&element_id=1&user_id7

+ Response 200 (application/json)
    + Body

            {
            	"quality_control": {
            		"code": "NC",
            		"type": "pre-eval",
            		"element_id": "1",
            		"user_id": "7",
            		"updated_at": "2017-08-08 13:54:58",
            		"created_at": "2017-08-08 13:54:58",
            		"id": 4
            	}
            }

## Update a quality_control [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the quality_control

+ Request (application/x-www-form-urlencoded)
    + Body

            note=Et%20ut%20expedita%20et%20velit%20dolorem.%20Illo%20et%20eum%20rerum%20aut%20nulla.%20Adipisci%20rerum%20et%20tempora%20consequatur%20impedit%20in.%20Sunt%20id%20at%20quam%20enim.

+ Response 200 (application/json)
    + Body

            {
            	"quality_control": {
            		"id": 4,
            		"created_at": "2017-08-08 13:54:58",
            		"updated_at": "2017-08-08 13:56:37",
            		"deleted_at": null,
            		"code": "NC",
            		"type": "pre-eval",
            		"note": "Et ut expedita et velit dolorem. Illo et eum rerum aut nulla. Adipisci rerum et tempora consequatur impedit in. Sunt id at quam enim.",
            		"element_id": 1,
            		"user_id": 7
            	}
            }

## Delete a quality_control [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the quality_control

+ Response 200 (application/json)
    + Body

            1
