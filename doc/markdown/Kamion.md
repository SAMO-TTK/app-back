# Kamions

## Retrieve all kamions [GET /]

Default returned fields : `id`, `wana_id`, `name`, `number`, `length`, `amount`, `category`, `type`, `phase`, `planned_time`, `worked_time`, `is_delivered`, `delivery`, `frame_delivery`, `ordered_at`, `wana_created_at`, `wana_updated_at`, `coeff_mp`, `hourly_rate`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `users`, `chapters`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"wana_id": "5514591826323483",
            			"name": "BlanchedAlmond",
            			"number": 429058686,
            			"length": null,
            			"amount": null,
            			"category": null,
            			"type": null,
            			"phase": null,
            			"planned_time": null,
            			"worked_time": null,
            			"is_delivered": null,
            			"delivery": null,
            			"frame_delivery": null,
            			"ordered_at": null,
            			"wana_created_at": null,
            			"wana_updated_at": null,
            			"coeff_mp": null,
            			"hourly_rate": null
            		},
            		{
            			"id": 2,
            			"wana_id": "4716959563039",
            			"name": "Cornsilk",
            			"number": 993982716,
            			"length": null,
            			"amount": null,
            			"category": null,
            			"type": null,
            			"phase": null,
            			"planned_time": null,
            			"worked_time": null,
            			"is_delivered": null,
            			"delivery": null,
            			"frame_delivery": null,
            			"ordered_at": null,
            			"wana_created_at": null,
            			"wana_updated_at": null,
            			"coeff_mp": null,
            			"hourly_rate": null
            		}
                ]
            }

## Retrieve a kamion by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the kamion
    + embed: (array, optional) - Embed linked resources : `users`, `chapters`

+ Response 200 (application/json)
    + Body

            {
            "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"wana_id": "5514591826323483",
            		"name": "BlanchedAlmond",
            		"number": 429058686,
            		"length": null,
            		"amount": null,
            		"category": null,
            		"type": null,
            		"phase": null,
            		"planned_time": null,
            		"worked_time": null,
            		"is_delivered": null,
            		"delivery": null,
            		"frame_delivery": null,
            		"ordered_at": null,
            		"wana_created_at": null,
            		"wana_updated_at": null,
            		"coeff_mp": null,
            		"hourly_rate": null
            	}
            }

## Create a kamion [POST /]
Required field : `name`, `wana_id`, `number`

+ Request (application/x-www-form-urlencoded)
    + Body

            name=mongrauhquamion&wana_id=5843321684&number=201

+ Response 200 (application/json)
    + Body

            {
            	"kamion": {
            		"name": "mongrauhquamion",
            		"wana_id": "5843321684",
            		"number": "201",
            		"updated_at": "2017-08-08 13:40:04",
            		"created_at": "2017-08-08 13:40:04",
            		"id": 6
            	}
            }

## Update a kamion [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the kamion

+ Request (application/x-www-form-urlencoded)
    + Body

            "type": "metal"

+ Response 200 (application/json)
    + Body

            {
            	"kamion": {
            		"id": 6,
            		"created_at": "2017-08-08 13:40:04",
            		"updated_at": "2017-08-08 13:41:58",
            		"deleted_at": null,
            		"wana_id": "5843321684",
            		"name": "mongrauhquamion",
            		"number": 201,
            		"length": null,
            		"amount": null,
            		"category": null,
            		"type": "metal",
            		"phase": null,
            		"planned_time": null,
            		"worked_time": null,
            		"is_delivered": null,
            		"delivery": null,
            		"frame_delivery": null,
            		"ordered_at": null,
            		"wana_created_at": null,
            		"wana_updated_at": null,
            		"coeff_mp": null,
            		"hourly_rate": null
            	}
            }

## Delete a kamion [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the kamion

+ Response 200 (application/json)
    + Body

            1
