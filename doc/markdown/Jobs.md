# Jobs

## Retrieve all jobs [GET /]

Default returned fields : `id`, `label`, `wana_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `users`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"label": "Purchasing Agent",
            			"wana_id": "5459562051949901"
            		},
            		{
            			"id": 2,
            			"label": "Pile-Driver Operator",
            			"wana_id": "5306572426591820"
            		}
                ]
            }

## Retrieve a jobs by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the element
    + embed: (array, optional) - Embed linked resources : `users`

+ Response 200 (application/json)
    + Body

            {
            "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:34",
            		"updated_at": "2017-08-08 11:17:34",
            		"deleted_at": null,
            		"label": "Purchasing Agent",
            		"wana_id": "5459562051949901"
            	}
            }

## Create a jobs [POST /]
Required field : `label`, `wana_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            label=big-boss&wana_id=46543586853

+ Response 200 (application/json)
    + Body

            {
            	"job": {
            		"label": "big-boss",
            		"wana_id": "46543586853",
            		"updated_at": "2017-08-08 13:22:18",
            		"created_at": "2017-08-08 13:22:18",
            		"id": 8
            	}
            }

## Update a jobs [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the jobs

+ Request (application/x-www-form-urlencoded)
    + Body

            "wana_id": "498321684"

+ Response 200 (application/json)
    + Body

            {
            	"job": {
            		"id": 8,
            		"created_at": "2017-08-08 13:22:18",
            		"updated_at": "2017-08-08 13:23:21",
            		"deleted_at": null,
            		"label": "big-boss",
            		"wana_id": "498321684"
            	}
            }

## Delete a jobs [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the jobs

+ Response 200 (application/json)
    + Body

            1
