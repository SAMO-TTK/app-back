# Elements

## Retrieve all elements [GET /]

Default returned fields : `id`, `imported_ad`, `title`, `content`, `supplement`, `chapter_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `quality_controls`, `chapter`, `documents`, `validation_controls`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"imported_at": "2017-08-01 02:29:19",
            			"title": "consequuntur",
            			"content": null,
            			"supplement": null,
            			"chapter_id": 8
            		},
            		{
            			"id": 2,
            			"imported_at": "2017-07-21 16:17:48",
            			"title": "consectetur",
            			"content": null,
            			"supplement": null,
            			"chapter_id": 3
            		}
                ]
            }

## Retrieve an element by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the element
    + embed: (array, optional) - Embed linked resources : `quality_controls`, `chapter`, `documents`, `validation_controls`

+ Response 200 (application/json)
    + Body

            {
            	"data": {
            		"id": 5,
            		"created_at": "2017-08-08 10:06:52",
            		"updated_at": "2017-08-08 10:06:52",
            		"deleted_at": null,
            		"imported_at": "2017-08-06 22:25:03",
            		"title": "corrupti",
            		"content": null,
            		"supplement": null,
            		"chapter_id": 1
            	}
            }

## Create an element [POST /]
Required field : `title`, `imported_at`, `chapter_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            title=roux&imported_at=2017-07-29%2004%3A24%3A49&chapiter_id=4

+ Response 200 (application/json)
    + Body

            {
            	"element": {
            		"title": "roux",
            		"chapter_id": "4",
            		"imported_at": "2017-07-29 04:24:49",
            		"updated_at": "2017-08-08 13:11:33",
            		"created_at": "2017-08-08 13:11:33",
            		"id": 13
            	}
            }

## Update an element [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the element

+ Request (application/x-www-form-urlencoded)
    + Body

            content=Je%20me%20souviens%20en%20fait,%20je%20sais%20que,%20gr%C3%A2ce%20%C3%A0%20ma%20propre%20v%C3%A9rit%C3%A9%20le%20cycle%20du%20cosmos%20dans%20la%20vie...%20c'est%20une%20grande%20roue%20parce%20que%20spirituellement,%20on%20est%20tous%20ensemble,%20ok%20%3F%20Et%20tu%20as%20envie%20de%20le%20dire%20au%20monde%20entier,%20including%20yourself.%20

+ Response 200 (application/json)
    + Body

            {
            	"element": {
            		"id": 13,
            		"created_at": "2017-08-08 13:11:33",
            		"updated_at": "2017-08-08 13:15:13",
            		"deleted_at": null,
            		"imported_at": "2017-07-29 04:24:49",
            		"title": "roux",
            		"content": "Je me souviens en fait, je sais que, grâce à ma propre vérité le cycle du cosmos dans la vie... c'est une grande roue parce que spirituellement, on est tous ensemble, ok ? Et tu as envie de le dire au monde entier, including yourself.",
            		"supplement": null,
            		"chapter_id": 4
            	}
            }

## Delete an element [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the element

+ Response 200 (application/json)
    + Body

            1
