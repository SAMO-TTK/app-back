# Chapters

## Retrieve all chapters [GET /]

Default returned fields : `id`, `imported_at`, `title`, `kamion_id`, `parent_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `parent`, `childrens`, `kamion`, `elements`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 1,
                        "imported_at": "2017-07-27 03:40:18",
                        "title": "Neque sunt esse dolore.",
                        "kamion_id": 2,
                        "parent_id": null
                    },
                    {
                        "id": 2,
                        "imported_at": "2017-08-03 22:19:07",
                        "title": "Est voluptatem quisquam eaque.",
                        "kamion_id": 5,
                        "parent_id": null
                    }
                ]
            }

## Retrieve a chapter by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the chapter
    + embed: (array, optional) - Embed linked resources : `parent`, `childrens`, `kamion`, `elements`

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 1,
                    "created_at": "2017-08-08 10:06:52",
                    "updated_at": "2017-08-08 10:06:52",
                    "deleted_at": null,
                    "imported_at": "2017-07-27 03:40:18",
                    "title": "Neque sunt esse dolore.",
                    "kamion_id": 2,
                    "parent_id": null
                }
            }

## Create an chapter [POST /]
Required field : `imported_at`, `title`, `kamion_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            title=douche&kamion_id=4&imported_at=2017-07-29%2004%3A24%3A49

+ Response 200 (application/json)
    + Body

            {
                "chapter": {
                    "title": "douche",
                    "kamion_id": "4",
                    "imported_at": "2017-07-29 04:24:49",
                    "updated_at": "2017-08-08 11:31:43",
                    "created_at": "2017-08-08 11:31:43",
                    "id": 9
                }
            }

## Update a chapter [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the chapter

+ Request (application/x-www-form-urlencoded)
    + Body

            parent_id=1

+ Response 200 (application/json)
    + Body

            {
                "chapter": {
                    "id": 9,
                    "created_at": "2017-08-08 11:31:43",
                    "updated_at": "2017-08-08 11:33:06",
                    "deleted_at": null,
                    "imported_at": "2017-07-29 04:24:49",
                    "title": "douche",
                    "kamion_id": 4,
                    "parent_id": "1"
                }
            }

## Delete a chapter [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the chapter

+ Response 200 (application/json)
    + Body

            1
