# Validation_controls

## Retrieve all validation_controls [GET /]

Default returned fields : `id`, `is_controled`, `note`, `element_id`, `user_id`

Extra fields : `created_at`, `updated_at`, `deleted_at`

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + embed: (array, optional) - Embed linked resources : `user`, `element`, `documents`
    + filters: (array, optional) - Allow to filter on the resource fields


+ Response 200 (application/json)
    + Body

            {
               "data": [
            		{
            			"id": 1,
            			"is_controled": 0,
            			"note": "0",
            			"element_id": 7,
            			"user_id": 7
            		},
            		{
            			"id": 2,
            			"is_controled": 0,
            			"note": "2",
            			"element_id": 8,
            			"user_id": 6
            		}
                ]
            }

## Retrieve a validation_control by its ID [GET /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the validation_control
    + embed: (array, optional) - Embed linked validation_control : `user`, `element`, `documents`

+ Response 200 (application/json)
    + Body

            {
                "data": {
            		"id": 1,
            		"created_at": "2017-08-08 11:17:35",
            		"updated_at": "2017-08-08 11:17:35",
            		"deleted_at": null,
            		"is_controled": 0,
            		"note": "0",
            		"element_id": 7,
            		"user_id": 7
            	}
            }

## Create a validation_control [POST /]
Required field : `is_controled`, `element_id`, `user_id`

+ Request (application/x-www-form-urlencoded)
    + Body

            is_controled=3&element_id=7&user_id=11

+ Response 200 (application/json)
    + Body

            {
            	"validation_control": {
            		"is_controled": "3",
            		"element_id": "7",
            		"user_id": "11",
            		"updated_at": "2017-08-08 14:12:16",
            		"created_at": "2017-08-08 14:12:16",
            		"id": 11
            	}
            }

## Update a validation_control [PUT /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the validation_control

+ Request (application/x-www-form-urlencoded)
    + Body

            note=Tu%20vois,%20m%C3%AAme%20si%20on%20frime%20comme%20on%20appelle%20%C3%A7a%20en%20France...%20le%20cycle%20du%20cosmos%20dans%20la%20vie...%20c'est%20une%20grande%20roue%20et%20je%20ne%20cherche%20pas%20ici%20%C3%A0%20mettre%20un%20point%20!%20Et%20tu%20as%20envie%20de%20le%20dire%20au%20monde%20entier,%20including%20yourself.%20


+ Response 200 (application/json)
    + Body

            {
            	"validation_control": {
            		"id": 11,
            		"created_at": "2017-08-08 14:12:16",
            		"updated_at": "2017-08-08 14:13:46",
            		"deleted_at": null,
            		"is_controled": 3,
            		"note": "Tu vois, même si on frime comme on appelle ça en France... le cycle du cosmos dans la vie... c'est une grande roue et je ne cherche pas ici à mettre un point ! Et tu as envie de le dire au monde entier, including yourself.",
            		"element_id": 7,
            		"user_id": 11
            	}
            }

## Delete a validation_control [DELETE /{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the validation_control

+ Response 200 (application/json)
    + Body

            1
