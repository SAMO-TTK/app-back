

# Group Announcements


## Retrieve all Announcements [GET /announcements]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamion` `user` 
    + filters: (array, optional) - Allow to filter on the Announcements fields
    + order: (array, optional) - order on the Announcements's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Announcement by its ID [GET /announcements/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Announcement
	+ embed: (array, optional) - Embed linked resources : `kamion` `user` 

+ Response 200 (application/json)

## Create a Announcement [POST /announcements]
+ Required post field : 
	+ VARCHAR(255): `title`
	+ INT: `user_id`

+ Response 200 (application/json)

## NotificationTicket a Announcement [PUT /announcements/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Announcement

+ Response 200 (application/json)

## Delete a Announcement [DELETE /announcements/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Announcement

+ Response 200 (application/json)
    + Body

            1

# Group Applications


## Retrieve all Applications [GET /applications]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `groupes` 
    + filters: (array, optional) - Allow to filter on the Applications fields
    + order: (array, optional) - order on the Applications's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Application by its ID [GET /applications/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Application
	+ embed: (array, optional) - Embed linked resources : `groupes` 

+ Response 200 (application/json)

## Create a Application [POST /applications]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a Application [PUT /applications/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Application

+ Response 200 (application/json)

## Delete a Application [DELETE /applications/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Application

+ Response 200 (application/json)
    + Body

            1

# Group Authenfication


## Retrieve authtoken with username [POST /login]

+ Required fields
	+ VARCHAR(255): `username`
	+ VARCHAR(255): `password`

+ Response 200
	+ Headers
				Content-Type : application/json


## Retrieve authtoken with nfc [POST /nfc]

+ Required fields
	+ VARCHAR(255): `badge_id`

+ Response 200
	+ Headers
				Content-Type : application/json


## logout [GET /logout]

+ Required fields
	+ TEXT: `authToken`

+ Response 200
	+ Headers
				Content-Type : application/json


# Group Categories


## Retrieve all Categories [GET /categories]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamions` 
    + filters: (array, optional) - Allow to filter on the Categories fields
    + order: (array, optional) - order on the Categories's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Category by its ID [GET /categories/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Category
	+ embed: (array, optional) - Embed linked resources : `kamions` 

+ Response 200 (application/json)

## Create a Category [POST /categories]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a Category [PUT /categories/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Category

+ Response 200 (application/json)

## Delete a Category [DELETE /categories/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Category

+ Response 200 (application/json)
    + Body

            1

# Group Chemin des attentes


## Delete a Category [DELETE /cda/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Category

+ Response 200 (application/json)
    + Body

            1

## NotificationTicket Excel's file of CDA [POST /cda]

+ Required fields
	+ FILE(xlsx): `cda`

+ Response 200
	+ Headers
			Content-Type : application/json


## Download Excel's file of CDA [GET /cda/{id}]

+ Parameters
	+ id:(integer, required) - id of the Kamion

+ Response 200
	+ Headers
			Content-Type : application/vnd.openxmlformats-officedocument.spreadsheetml.sheet


## Download full Excel's file of CDA [GET /cda/{id}/full]

+ Parameters
	+ id:(integer, required) - id of the Kamion

+ Response 200
	+ Headers
				Content-Type : application/vnd.openxmlformats-officedocument.spreadsheetml.sheet


## NotificationTicket Excel's file of CDA [POST /cda/{id}]

+ Required fields
	+ FILE(xlsx): `cda`

+ Parameters
	+ id:(integer, required) - id of the Kamion

+ Response 200
	+ Headers
			Content-Type : application/json


# Group Chapters


## Retrieve all Chapters [GET /chapters]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamion` `elements` `parent` `children` `all_children` `childrens` `export` 
    + filters: (array, optional) - Allow to filter on the Chapters fields
    + order: (array, optional) - order on the Chapters's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Chapter by its ID [GET /chapters/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Chapter
	+ embed: (array, optional) - Embed linked resources : `kamion` `elements` `parent` `children` `all_children` `childrens` `export` 

+ Response 200 (application/json)

## Create a Chapter [POST /chapters]
+ Required post field : 
	+ VARCHAR(255): `numbering`
	+ VARCHAR(255): `title`
	+ INT: `kamion_id`

+ Response 200 (application/json)

## NotificationTicket a Chapter [PUT /chapters/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Chapter

+ Response 200 (application/json)

## Delete a Chapter [DELETE /chapters/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Chapter

+ Response 200 (application/json)
    + Body

            1

# Group Columns


## Retrieve all Columns [GET /columns]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamion` 
    + filters: (array, optional) - Allow to filter on the Columns fields
    + order: (array, optional) - order on the Columns's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Column by its ID [GET /columns/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Column
	+ embed: (array, optional) - Embed linked resources : `kamion` 

+ Response 200 (application/json)

## Create a Column [POST /columns]
+ Required post field : 
	+ INT: `kamion_id`
	+ TINYINT: `numbering`
	+ TINYINT: `reference`
	+ TINYINT: `content`
	+ TINYINT: `supplement`
	+ TINYINT: `crecre`
	+ TINYINT: `upupup`
	+ TINYINT: `client_requirement`
	+ TINYINT: `type`
	+ TINYINT: `status`

+ Response 200 (application/json)

## NotificationTicket a Column [PUT /columns/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Column

+ Response 200 (application/json)

## Delete a Column [DELETE /columns/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Column

+ Response 200 (application/json)
    + Body

            1

# Group Discussions


## Retrieve all Discussions [GET /discussions]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `element` `messages` `users` `documents` 
    + filters: (array, optional) - Allow to filter on the Discussions fields
    + order: (array, optional) - order on the Discussions's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Discussion by its ID [GET /discussions/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Discussion
	+ embed: (array, optional) - Embed linked resources : `element` `messages` `users` `documents` 

+ Response 200 (application/json)

## Create a Discussion [POST /discussions]
+ Required post field : 
	+ TEXT: `content`
	+ TINYINT: `is_open`
	+ TINYINT: `from_client`

+ Response 200 (application/json)

## NotificationTicket a Discussion [PUT /discussions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Discussion

+ Response 200 (application/json)

## Delete a Discussion [DELETE /discussions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Discussion

+ Response 200 (application/json)
    + Body

            1

# Group Documents


## Retrieve all Documents [GET /documents]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `users` `elements` `messages` `quality_controls` `validation_controls` `tickets` `discussions` `kamions` `author` `last_documents` 
    + filters: (array, optional) - Allow to filter on the Documents fields
    + order: (array, optional) - order on the Documents's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Document by its ID [GET /documents/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Document
	+ embed: (array, optional) - Embed linked resources : `users` `elements` `messages` `quality_controls` `validation_controls` `tickets` `discussions` `kamions` `author` `last_documents` 

+ Response 200 (application/json)

## Create a Document [POST /documents]
+ Required post field : 
	+ VARCHAR(255): `title`
	+ VARCHAR(255): `extension`
	+ VARCHAR(255): `mfile_id`
	+ VARCHAR(255): `mfile_object_id`

+ Response 200 (application/json)

## NotificationTicket a Document [PUT /documents/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Document

+ Response 200 (application/json)

## Delete a Document [DELETE /documents/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Document

+ Response 200 (application/json)
    + Body

            1

## Get last documents [GET /users/{id_user}/kamions/{id_kamion}/last_document]

+ Parameters
	+ id_user:(integer, required) - id of the User
	+ id_kamion:(integer, required) - id of the Kamion

+ Response 200
	+ Headers
			Content-Type : application/json


## Get last documents [GET /users/{id_user}/last_document]

+ Parameters
	+ id_user:(integer, required) - id of the User

+ Response 200
	+ Headers
			Content-Type : application/json


# Group Durations


## Retrieve all Durations [GET /durations]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + filters: (array, optional) - Allow to filter on the Durations fields
    + order: (array, optional) - order on the Durations's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Duration by its ID [GET /durations/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Duration

+ Response 200 (application/json)

## Create a Duration [POST /durations]
+ Required post field : 
	+ INT: `duration`
	+ TEXT: `reason`

+ Response 200 (application/json)

## NotificationTicket a Duration [PUT /durations/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Duration

+ Response 200 (application/json)

## Delete a Duration [DELETE /durations/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Duration

+ Response 200 (application/json)
    + Body

            1

# Group Elements


## Retrieve all Elements [GET /elements]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `chapter` `quality_controls` `validation_controls` `discussions` `documents` `supplement` `impact` `users` `user_files` 
    + filters: (array, optional) - Allow to filter on the Elements fields
    + order: (array, optional) - order on the Elements's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Element by its ID [GET /elements/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Element
	+ embed: (array, optional) - Embed linked resources : `chapter` `quality_controls` `validation_controls` `discussions` `documents` `supplement` `impact` `users` `user_files` 

+ Response 200 (application/json)

## Create a Element [POST /elements]
+ Required post field : 
	+ TEXT: `content`
	+ VARCHAR(255): `numbering`
	+ VARCHAR(255): `type`
	+ TINYINT: `is_open`
	+ TINYINT: `is_client_requirement`
	+ INT: `chapter_id`

+ Response 200 (application/json)

## NotificationTicket a Element [PUT /elements/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Element

+ Response 200 (application/json)

## Delete a Element [DELETE /elements/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Element

+ Response 200 (application/json)
    + Body

            1

# Group Extensions


## Retrieve all Extensions [GET /extensions]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `groupe` 
    + filters: (array, optional) - Allow to filter on the Extensions fields
    + order: (array, optional) - order on the Extensions's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Extension by its ID [GET /extensions/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Extension
	+ embed: (array, optional) - Embed linked resources : `groupe` 

+ Response 200 (application/json)

## Create a Extension [POST /extensions]

+ Response 200 (application/json)

## NotificationTicket a Extension [PUT /extensions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Extension

+ Response 200 (application/json)

## Delete a Extension [DELETE /extensions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Extension

+ Response 200 (application/json)
    + Body

            1

# Group FavoryTickets


## Retrieve all FavoryTickets [GET /favory_tickets]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `ticket` `user` 
    + filters: (array, optional) - Allow to filter on the FavoryTickets fields
    + order: (array, optional) - order on the FavoryTickets's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a FavoryTicket by its ID [GET /favory_tickets/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the FavoryTicket
	+ embed: (array, optional) - Embed linked resources : `ticket` `user` 

+ Response 200 (application/json)

## Create a FavoryTicket [POST /favory_tickets]
+ Required post field : 
	+ INT: `user_id`
	+ INT: `ticket_id`

+ Response 200 (application/json)

## NotificationTicket a FavoryTicket [PUT /favory_tickets/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the FavoryTicket

+ Response 200 (application/json)

## Delete favory ticket [DELETE /favory_tickets/{id}/{user_id}]

+ Parameters
	+ id:(integer, required) - id of the favory ticket
	+ user_id:(integer, required) - id of the User


# Group Field_Values


## Retrieve all Field_Values [GET /field_values]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `field` `tickets` 
    + filters: (array, optional) - Allow to filter on the Field_Values fields
    + order: (array, optional) - order on the Field_Values's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Field_Value by its ID [GET /field_values/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Field_Value
	+ embed: (array, optional) - Embed linked resources : `field` `tickets` 

+ Response 200 (application/json)

## Create a Field_Value [POST /field_values]
+ Required post field : 
	+ VARCHAR(255): `value`
	+ INT: `field_id`

+ Response 200 (application/json)

## NotificationTicket a Field_Value [PUT /field_values/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Field_Value

+ Response 200 (application/json)

## Delete a Field_Value [DELETE /field_values/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Field_Value

+ Response 200 (application/json)
    + Body

            1

# Group Groupes


## Retrieve all Groupes [GET /groupes]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `applications` `extensions` 
    + filters: (array, optional) - Allow to filter on the Groupes fields
    + order: (array, optional) - order on the Groupes's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Groupe by its ID [GET /groupes/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Groupe
	+ embed: (array, optional) - Embed linked resources : `applications` `extensions` 

+ Response 200 (application/json)

## Create a Groupe [POST /groupes]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a Groupe [PUT /groupes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Groupe

+ Response 200 (application/json)

## Delete a Groupe [DELETE /groupes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Groupe

+ Response 200 (application/json)
    + Body

            1

# Group Impacts


## Retrieve all Impacts [GET /impacts]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `element` 
    + filters: (array, optional) - Allow to filter on the Impacts fields
    + order: (array, optional) - order on the Impacts's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Impact by its ID [GET /impacts/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Impact
	+ embed: (array, optional) - Embed linked resources : `element` 

+ Response 200 (application/json)

## Create a Impact [POST /impacts]
+ Required post field : 
	+ INT: `element_id`

+ Response 200 (application/json)

## NotificationTicket a Impact [PUT /impacts/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Impact

+ Response 200 (application/json)

## Delete a Impact [DELETE /impacts/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Impact

+ Response 200 (application/json)
    + Body

            1

# Group Instructions


## Retrieve all Instructions [GET /instructions]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamion` `job` `instruction_users` 
    + filters: (array, optional) - Allow to filter on the Instructions fields
    + order: (array, optional) - order on the Instructions's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Instruction by its ID [GET /instructions/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Instruction
	+ embed: (array, optional) - Embed linked resources : `kamion` `job` `instruction_users` 

+ Response 200 (application/json)

## Create a Instruction [POST /instructions]
+ Required post field : 
	+ VARCHAR(255): `label`
	+ VARCHAR(255): `wana_id`
	+ INT: `kamion_id`
	+ INT: `instruction_id`
	+ INT: `instruction_id`

+ Response 200 (application/json)

## NotificationTicket a Instruction [PUT /instructions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Instruction

+ Response 200 (application/json)

## Delete a Instruction [DELETE /instructions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Instruction

+ Response 200 (application/json)
    + Body

            1

# Group InstructionUsers


## Retrieve all InstructionUsers [GET /instructionusers]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `user` `instruction` 
    + filters: (array, optional) - Allow to filter on the InstructionUsers fields
    + order: (array, optional) - order on the InstructionUsers's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a InstructionUser by its ID [GET /instructionusers/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the InstructionUser
	+ embed: (array, optional) - Embed linked resources : `user` `instruction` 

+ Response 200 (application/json)

## Create a InstructionUser [POST /instructionusers]
+ Required post field : 
	+ INT: `instruction_id`
	+ INT: `user_id`

+ Response 200 (application/json)

## NotificationTicket a InstructionUser [PUT /instructionusers/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the InstructionUser

+ Response 200 (application/json)

## Delete a InstructionUser [DELETE /instructionusers/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the InstructionUser

+ Response 200 (application/json)
    + Body

            1

# Group Jobs


## Retrieve all Jobs [GET /jobs]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `users` 
    + filters: (array, optional) - Allow to filter on the Jobs fields
    + order: (array, optional) - order on the Jobs's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Job by its ID [GET /jobs/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Job
	+ embed: (array, optional) - Embed linked resources : `users` 

+ Response 200 (application/json)

## Create a Job [POST /jobs]
+ Required post field : 
	+ VARCHAR(255): `label`
	+ VARCHAR(255): `wana_id`

+ Response 200 (application/json)

## NotificationTicket a Job [PUT /jobs/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Job

+ Response 200 (application/json)

## Delete a Job [DELETE /jobs/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Job

+ Response 200 (application/json)
    + Body

            1

# Group Kamions


## Retrieve all Kamions [GET /kamions]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `users` `chapters` `announcements` `column` `notes` `tickets` `stage` `type` `category` `masters` `linked` `last_projects` `instructions` 
    + filters: (array, optional) - Allow to filter on the Kamions fields
    + order: (array, optional) - order on the Kamions's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Kamion by its ID [GET /kamions/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Kamion
	+ embed: (array, optional) - Embed linked resources : `users` `chapters` `announcements` `column` `notes` `tickets` `stage` `type` `category` `masters` `linked` `last_projects` `instructions` 

+ Response 200 (application/json)

## Create a Kamion [POST /kamions]
+ Required post field : 
	+ VARCHAR(255): `wana_id`
	+ VARCHAR(255): `name`
	+ TINYINT: `is_delivered`
	+ TINYINT: `is_locked`
	+ INT: `stage_id`
	+ INT: `category_id`

+ Response 200 (application/json)

## NotificationTicket a Kamion [PUT /kamions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Kamion

+ Response 200 (application/json)

## Delete a Kamion [DELETE /kamions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Kamion

+ Response 200 (application/json)
    + Body

            1

## Save kamion's avatar [POST /kamions/{id}/image]

+ Required fields
	+ FILE: `file`

+ Parameters
	+ id:(integer, required) - the id of the kamion


## Link a Kamion to another kamion [PUT /kamions/{id}/projects]

+ Required fields
	+ ARRAY: `projects`

+ Parameters
	+ id:(integer, required) - the id of the principal kamion


## Link a User to a Kamion [PUT /kamions/{id}/users]

+ Required fields
	+ ARRAY: `users`

+ Parameters
	+ id:(integer, required) - the id of the principal kamion

+ Response 200
	+ Headers
				Content-Type : application/json


## Show all linked project of kamion [GET /kamions/{id}/projects]

+ Parameters
	+ id:(integer, required) - the id of the principal kamion


## Show all cda structure [GET /kamions/{id}/cda]

+ Parameters
	+ id:(integer, required) - the id of the principal kamion


## retrive the 8 resently consulted kamion by curent user [GET /kamions/users]


## retrive the elements linked [GET /kamions/{id}/elements]

+ Parameters
	+ id: (integer,required) - the id of the kamion


# Group Last_documents


## Retrieve all Last_documents [GET /last_documents]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `user` `document` 
    + filters: (array, optional) - Allow to filter on the Last_documents fields
    + order: (array, optional) - order on the Last_documents's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Last_document by its ID [GET /last_documents/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Last_document
	+ embed: (array, optional) - Embed linked resources : `user` `document` 

+ Response 200 (application/json)

## Create a Last_document [POST /last_documents]
+ Required post field : 
	+ INT: `user_id`
	+ INT: `document_id`

+ Response 200 (application/json)

## NotificationTicket a Last_document [PUT /last_documents/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_document

+ Response 200 (application/json)

## Delete a Last_document [DELETE /last_documents/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_document

+ Response 200 (application/json)
    + Body

            1

# Group Last_elements


## Retrieve all Last_elements [GET /last_elements]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `element` `user` 
    + filters: (array, optional) - Allow to filter on the Last_elements fields
    + order: (array, optional) - order on the Last_elements's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Last_element by its ID [GET /last_elements/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Last_element
	+ embed: (array, optional) - Embed linked resources : `element` `user` 

+ Response 200 (application/json)

## Create a Last_element [POST /last_elements]
+ Required post field : 
	+ INT: `user_id`
	+ INT: `element_id`

+ Response 200 (application/json)

## NotificationTicket a Last_element [PUT /last_elements/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_element

+ Response 200 (application/json)

## Delete a Last_element [DELETE /last_elements/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_element

+ Response 200 (application/json)
    + Body

            1

# Group Last_notes


## Retrieve all Last_notes [GET /last_notes]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `note` `user` 
    + filters: (array, optional) - Allow to filter on the Last_notes fields
    + order: (array, optional) - order on the Last_notes's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Last_note by its ID [GET /last_notes/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Last_note
	+ embed: (array, optional) - Embed linked resources : `note` `user` 

+ Response 200 (application/json)

## Create a Last_note [POST /last_notes]
+ Required post field : 
	+ INT: `user_id`
	+ INT: `note_id`

+ Response 200 (application/json)

## NotificationTicket a Last_note [PUT /last_notes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_note

+ Response 200 (application/json)

## Delete a Last_note [DELETE /last_notes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_note

+ Response 200 (application/json)
    + Body

            1

# Group Last_projects


## Retrieve all Last_projects [GET /last_projects]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `element` `user` 
    + filters: (array, optional) - Allow to filter on the Last_projects fields
    + order: (array, optional) - order on the Last_projects's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Last_project by its ID [GET /last_projects/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Last_project
	+ embed: (array, optional) - Embed linked resources : `element` `user` 

+ Response 200 (application/json)

## Create a Last_project [POST /last_projects]
+ Required post field : 
	+ INT: `user_id`
	+ INT: `kamion_id`

+ Response 200 (application/json)

## NotificationTicket a Last_project [PUT /last_projects/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_project

+ Response 200 (application/json)

## Delete a Last_project [DELETE /last_projects/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_project

+ Response 200 (application/json)
    + Body

            1

# Group Last_tickets


## Retrieve all Last_tickets [GET /last_tickets]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `tickets` `user` 
    + filters: (array, optional) - Allow to filter on the Last_tickets fields
    + order: (array, optional) - order on the Last_tickets's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Last_ticket by its ID [GET /last_tickets/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Last_ticket
	+ embed: (array, optional) - Embed linked resources : `tickets` `user` 

+ Response 200 (application/json)

## Create a Last_ticket [POST /last_tickets]

+ Response 200 (application/json)

## NotificationTicket a Last_ticket [PUT /last_tickets/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_ticket

+ Response 200 (application/json)

## Delete a Last_ticket [DELETE /last_tickets/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Last_ticket

+ Response 200 (application/json)
    + Body

            1

# Group Logtimes


## Retrieve all Logtimes [GET /logtimes]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `statuslogtime` `histories` `kamion` `instruction` `user` `timecard` 
    + filters: (array, optional) - Allow to filter on the Logtimes fields
    + order: (array, optional) - order on the Logtimes's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Logtime by its ID [GET /logtimes/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Logtime
	+ embed: (array, optional) - Embed linked resources : `statuslogtime` `histories` `kamion` `instruction` `user` `timecard` 

+ Response 200 (application/json)

## Create a Logtime [POST /logtimes]
+ Required post field : 
	+ FLOAT: `length`
	+ INT: `user_id`
	+ INT: `status_logtime_id`
	+ INT: `kamion_id`

+ Response 200 (application/json)

## NotificationTicket a Logtime [PUT /logtimes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Logtime

+ Response 200 (application/json)

## Delete a Logtime [DELETE /logtimes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Logtime

+ Response 200 (application/json)
    + Body

            1

# Group LogtimeHistories


## Retrieve all LogtimeHistories [GET /logtimehistories]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `logtime` `user` 
    + filters: (array, optional) - Allow to filter on the LogtimeHistories fields
    + order: (array, optional) - order on the LogtimeHistories's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a LogtimeHistory by its ID [GET /logtimehistories/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the LogtimeHistory
	+ embed: (array, optional) - Embed linked resources : `logtime` `user` 

+ Response 200 (application/json)

## Create a LogtimeHistory [POST /logtimehistories]
+ Required post field : 
	+ VARCHAR(255): `type`
	+ VARCHAR(255): `update_name`

+ Response 200 (application/json)

## NotificationTicket a LogtimeHistory [PUT /logtimehistories/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the LogtimeHistory

+ Response 200 (application/json)

## Delete a LogtimeHistory [DELETE /logtimehistories/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the LogtimeHistory

+ Response 200 (application/json)
    + Body

            1

# Group Materials


## Retrieve all Materials [GET /materials]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamion` `user` `instruction` `material_resource` 
    + filters: (array, optional) - Allow to filter on the Materials fields
    + order: (array, optional) - order on the Materials's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Material by its ID [GET /materials/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Material
	+ embed: (array, optional) - Embed linked resources : `kamion` `user` `instruction` `material_resource` 

+ Response 200 (application/json)

## Create a Material [POST /materials]
+ Required post field : 
	+ INT: `user_id`
	+ INT: `kamion_id`

+ Response 200 (application/json)

## NotificationTicket a Material [PUT /materials/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Material

+ Response 200 (application/json)

## Delete a Material [DELETE /materials/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Material

+ Response 200 (application/json)
    + Body

            1

# Group MaterialResources


## Retrieve all MaterialResources [GET /materialresources]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + filters: (array, optional) - Allow to filter on the MaterialResources fields
    + order: (array, optional) - order on the MaterialResources's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a MaterialResource by its ID [GET /materialresources/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the MaterialResource

+ Response 200 (application/json)

## Create a MaterialResource [POST /materialresources]

+ Response 200 (application/json)

## NotificationTicket a MaterialResource [PUT /materialresources/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the MaterialResource

+ Response 200 (application/json)

## Delete a MaterialResource [DELETE /materialresources/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the MaterialResource

+ Response 200 (application/json)
    + Body

            1

# Group Messages


## Retrieve all Messages [GET /messages]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `user` `discussion` `documents` 
    + filters: (array, optional) - Allow to filter on the Messages fields
    + order: (array, optional) - order on the Messages's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Message by its ID [GET /messages/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Message
	+ embed: (array, optional) - Embed linked resources : `user` `discussion` `documents` 

+ Response 200 (application/json)

## Create a Message [POST /messages]
+ Required post field : 
	+ TEXT: `content`
	+ INT: `user_id`
	+ INT: `discussion_id`

+ Response 200 (application/json)

## NotificationTicket a Message [PUT /messages/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Message

+ Response 200 (application/json)

## Delete a Message [DELETE /messages/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Message

+ Response 200 (application/json)
    + Body

            1

# Group Model_fields


## Retrieve all Model_fields [GET /model_fields]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `model` `values` `ticket_fields` `tickets` 
    + filters: (array, optional) - Allow to filter on the Model_fields fields
    + order: (array, optional) - order on the Model_fields's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Model_field by its ID [GET /model_fields/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Model_field
	+ embed: (array, optional) - Embed linked resources : `model` `values` `ticket_fields` `tickets` 

+ Response 200 (application/json)

## Create a Model_field [POST /model_fields]
+ Required post field : 
	+ VARCHAR(255): `label`
	+ VARCHAR(255): `type`
	+ INT: `position`
	+ INT: `model_id`

+ Response 200 (application/json)

## NotificationTicket a Model_field [PUT /model_fields/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Model_field

+ Response 200 (application/json)

## Delete a Model_field [DELETE /model_fields/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Model_field

+ Response 200 (application/json)
    + Body

            1

# Group Notes


## Retrieve all Notes [GET /notes]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamion` `user` `documents` `shared` `last_notes` 
    + filters: (array, optional) - Allow to filter on the Notes fields
    + order: (array, optional) - order on the Notes's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Note by its ID [GET /notes/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Note
	+ embed: (array, optional) - Embed linked resources : `kamion` `user` `documents` `shared` `last_notes` 

+ Response 200 (application/json)

## Create a Note [POST /notes]
+ Required post field : 
	+ VARCHAR(255): `title`
	+ TINYINT: `is_public`
	+ INT: `user_id`

+ Response 200 (application/json)

## NotificationTicket a Note [PUT /notes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Note

+ Response 200 (application/json)

## Delete a Note [DELETE /notes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Note

+ Response 200 (application/json)
    + Body

            1

# Group Notifications


## Retrieve all Notifications [GET /notifications]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `user` `notification_ticket` `users` `noreadusers` `discussions` 
    + filters: (array, optional) - Allow to filter on the Notifications fields
    + order: (array, optional) - order on the Notifications's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Notification by its ID [GET /notifications/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Notification
	+ embed: (array, optional) - Embed linked resources : `user` `notification_ticket` `users` `noreadusers` `discussions` 

+ Response 200 (application/json)

## Create a Notification [POST /notifications]
+ Required post field : 
	+ VARCHAR(255): `title`
	+ VARCHAR(255): `icon`
	+ VARCHAR(255): `type`

+ Response 200 (application/json)

## NotificationTicket a Notification [PUT /notifications/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Notification

+ Response 200 (application/json)

## Delete a Notification [DELETE /notifications/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Notification

+ Response 200 (application/json)
    + Body

            1

## Get not read notifications about a ticket [GET /notifications/tickets/{id}/users/{id_user}]

+ Parameters
	+ id:(integer, required) - id of the Ticket
	+ id_user:(integer, required) - id of the User

+ Response 200
	+ Headers
			Content-Type : application/json


## Mark read all user notifications [PUT /notifications/users/{user_id}]

+ Parameters
	+ user_id:(integer, required) - id of the User

+ Response 200
	+ Headers
			Content-Type : application/json


## Mark read a notification [PUT /notifications/{id}/users/{user_id}]

+ Parameters
	+ id:(integer, required) - id of the Notification
	+ user_id:(integer, required) - id of the User


# Group NotificationTickets


## Retrieve all NotificationTickets [GET /updates]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `ticket` `update_fields` 
    + filters: (array, optional) - Allow to filter on the NotificationTickets fields
    + order: (array, optional) - order on the NotificationTickets's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a NotificationTicket by its ID [GET /updates/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the NotificationTicket
	+ embed: (array, optional) - Embed linked resources : `ticket` `update_fields` 

+ Response 200 (application/json)

## Create a NotificationTicket [POST /updates]
+ Required post field : 
	+ INT: `ticket_id`
	+ INT: `notification_id`

+ Response 200 (application/json)

## NotificationTicket a NotificationTicket [PUT /updates/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the NotificationTicket

+ Response 200 (application/json)

## Delete a NotificationTicket [DELETE /updates/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the NotificationTicket

+ Response 200 (application/json)
    + Body

            1

# Group Permissions


## Retrieve all Permissions [GET /permissions]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `roles` 
    + filters: (array, optional) - Allow to filter on the Permissions fields
    + order: (array, optional) - order on the Permissions's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Permission by its ID [GET /permissions/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Permission
	+ embed: (array, optional) - Embed linked resources : `roles` 

+ Response 200 (application/json)

## Create a Permission [POST /permissions]
+ Required post field : 
	+ VARCHAR(255): `label`
	+ VARCHAR(255): `slug`

+ Response 200 (application/json)

## NotificationTicket a Permission [PUT /permissions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Permission

+ Response 200 (application/json)

## Delete a Permission [DELETE /permissions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Permission

+ Response 200 (application/json)
    + Body

            1

# Group Quality_controls


## Retrieve all Quality_controls [GET /quality_controls]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `element` `user` `documents` 
    + filters: (array, optional) - Allow to filter on the Quality_controls fields
    + order: (array, optional) - order on the Quality_controls's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Quality_control by its ID [GET /quality_controls/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Quality_control
	+ embed: (array, optional) - Embed linked resources : `element` `user` `documents` 

+ Response 200 (application/json)

## Create a Quality_control [POST /quality_controls]
+ Required post field : 
	+ INT: `element_id`

+ Response 200 (application/json)

## NotificationTicket a Quality_control [PUT /quality_controls/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Quality_control

+ Response 200 (application/json)

## Delete a Quality_control [DELETE /quality_controls/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Quality_control

+ Response 200 (application/json)
    + Body

            1

# Group Researches


## Retrieve all Researches [GET /researches]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `users` 
    + filters: (array, optional) - Allow to filter on the Researches fields
    + order: (array, optional) - order on the Researches's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Research by its ID [GET /researches/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Research
	+ embed: (array, optional) - Embed linked resources : `users` 

+ Response 200 (application/json)

## Create a Research [POST /researches]
+ Required post field : 
	+ VARCHAR(255): `name`

+ Response 200 (application/json)

## NotificationTicket a Research [PUT /researches/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Research

+ Response 200 (application/json)

## Delete a Research [DELETE /researches/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Research

+ Response 200 (application/json)
    + Body

            1

# Group Resources


## Retrieve all Resources [GET /resources]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `users` 
    + filters: (array, optional) - Allow to filter on the Resources fields
    + order: (array, optional) - order on the Resources's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Resource by its ID [GET /resources/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Resource
	+ embed: (array, optional) - Embed linked resources : `users` 

+ Response 200 (application/json)

## Create a Resource [POST /resources]
+ Required post field : 
	+ VARCHAR(255): `label`
	+ VARCHAR(255): `wana_id`

+ Response 200 (application/json)

## NotificationTicket a Resource [PUT /resources/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Resource

+ Response 200 (application/json)

## Delete a Resource [DELETE /resources/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Resource

+ Response 200 (application/json)
    + Body

            1

# Group Roles


## Retrieve all Roles [GET /roles]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `capabilities` `users` `permissions` 
    + filters: (array, optional) - Allow to filter on the Roles fields
    + order: (array, optional) - order on the Roles's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Role by its ID [GET /roles/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Role
	+ embed: (array, optional) - Embed linked resources : `capabilities` `users` `permissions` 

+ Response 200 (application/json)

## Create a Role [POST /roles]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a Role [PUT /roles/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Role

+ Response 200 (application/json)

## Delete a Role [DELETE /roles/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Role

+ Response 200 (application/json)
    + Body

            1

# Group Settings


## Retrieve all Settings [GET /settings]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + filters: (array, optional) - Allow to filter on the Settings fields
    + order: (array, optional) - order on the Settings's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Setting by its ID [GET /settings/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Setting

+ Response 200 (application/json)

## Create a Setting [POST /settings]
+ Required post field : 
	+ VARCHAR(255): `slug`
	+ VARCHAR(255): `value`

+ Response 200 (application/json)

## NotificationTicket a Setting [PUT /settings{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Setting

+ Response 200 (application/json)

## Delete a Setting [DELETE /settings/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Setting

+ Response 200 (application/json)
    + Body

            1

# Group Stages


## Retrieve all Stages [GET /stages]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamions` 
    + filters: (array, optional) - Allow to filter on the Stages fields
    + order: (array, optional) - order on the Stages's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Stage by its ID [GET /stages/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Stage
	+ embed: (array, optional) - Embed linked resources : `kamions` 

+ Response 200 (application/json)

## Create a Stage [POST /stages]
+ Required post field : 
	+ VARCHAR(255): `label`
	+ VARCHAR(255): `wana_id`

+ Response 200 (application/json)

## NotificationTicket a Stage [PUT /stages/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Stage

+ Response 200 (application/json)

## Delete a Stage [DELETE /stages/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Stage

+ Response 200 (application/json)
    + Body

            1

# Group StatusLogtimes


## Retrieve all StatusLogtimes [GET /statuslogtimes]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `logtimes` 
    + filters: (array, optional) - Allow to filter on the StatusLogtimes fields
    + order: (array, optional) - order on the StatusLogtimes's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a StatusLogtime by its ID [GET /statuslogtimes/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the StatusLogtime
	+ embed: (array, optional) - Embed linked resources : `logtimes` 

+ Response 200 (application/json)

## Create a StatusLogtime [POST /statuslogtimes]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a StatusLogtime [PUT /statuslogtimes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the StatusLogtime

+ Response 200 (application/json)

## Delete a StatusLogtime [DELETE /statuslogtimes/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the StatusLogtime

+ Response 200 (application/json)
    + Body

            1

# Group Supplements


## Retrieve all Supplements [GET /supplements]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `user` `element` 
    + filters: (array, optional) - Allow to filter on the Supplements fields
    + order: (array, optional) - order on the Supplements's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Supplement by its ID [GET /supplements/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Supplement
	+ embed: (array, optional) - Embed linked resources : `user` `element` 

+ Response 200 (application/json)

## Create a Supplement [POST /supplements]
+ Required post field : 
	+ INT: `element_id`

+ Response 200 (application/json)

## NotificationTicket a Supplement [PUT /supplements/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Supplement

+ Response 200 (application/json)

## Delete a Supplement [DELETE /supplements/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Supplement

+ Response 200 (application/json)
    + Body

            1

# Group Tickets


## Retrieve all Tickets [GET /tickets]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `instruction` `documents` `kamion` `kamions` `ticket` `linked` `model` `fields` `favory_tickets` `status` `categories` `workgroup` `users` `manager` `creator` `discussions` `last_tickets` 
    + filters: (array, optional) - Allow to filter on the Tickets fields
    + order: (array, optional) - order on the Tickets's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Ticket by its ID [GET /tickets/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket
	+ embed: (array, optional) - Embed linked resources : `instruction` `documents` `kamion` `kamions` `ticket` `linked` `model` `fields` `favory_tickets` `status` `categories` `workgroup` `users` `manager` `creator` `discussions` `last_tickets` 

+ Response 200 (application/json)

## Create a Ticket [POST /tickets]
+ Required post field : 
	+ VARCHAR(255): `title`
	+ TINYINT: `is_public`
	+ INT: `creator_id`
	+ INT: `status_id`
	+ INT: `model_id`

+ Response 200 (application/json)

## NotificationTicket a Ticket [PUT /tickets/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket

+ Response 200 (application/json)

## Delete a Ticket [DELETE /tickets/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket

+ Response 200 (application/json)
    + Body

            1

## Searche in kicket with filters [PUT /tickets/search]

+ Required fields
	+ ARRAY: `destinater`
	+ ARRAY: `redacter`
	+ ARRAY: `responsable`
	+ ARRAY: `id_kamion`
	+ ARRAY: `model`
	+ ARRAY: `category`
	+ ARRAY: `id_ticket`
	+ ARRAY: `status`
	+ TIMESTAMP: `deadline`
	+ TIMESTAMP: `created_at`


## Get linked ticket [GET /tickets/{id}/tickets]

+ Parameters
	+ id:(integer, required) - the id of the ticket


## link ticket to another ticket [PUT /tickets/{id}/tickets]

+ Required fields
	+ ARRAY: `projects`

+ Parameters
	+ id:(integer, required) - the id of the ticket


## get the last ticket's number [GET /tickets/last]

+ Response 200
	+ Headers
			Content-Type : application/json


# Group Ticket_categories


## Retrieve all Ticket_categories [GET /ticket_categories]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `tickets` 
    + filters: (array, optional) - Allow to filter on the Ticket_categories fields
    + order: (array, optional) - order on the Ticket_categories's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Ticket_category by its ID [GET /ticket_categories/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_category
	+ embed: (array, optional) - Embed linked resources : `tickets` 

+ Response 200 (application/json)

## Create a Ticket_category [POST /ticket_categories]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a Ticket_category [PUT /ticket_categories/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_category

+ Response 200 (application/json)

## Delete a Ticket_category [DELETE /ticket_categories/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_category

+ Response 200 (application/json)
    + Body

            1

# Group Ticket_discussions


## Retrieve all Ticket_discussions [GET /ticket_discussions]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `ticket` `creator` `messages` `documents` `users` 
    + filters: (array, optional) - Allow to filter on the Ticket_discussions fields
    + order: (array, optional) - order on the Ticket_discussions's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Ticket_discussion by its ID [GET /ticket_discussions/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_discussion
	+ embed: (array, optional) - Embed linked resources : `ticket` `creator` `messages` `documents` `users` 

+ Response 200 (application/json)

## Create a Ticket_discussion [POST /ticket_discussions]
+ Required post field : 
	+ VARCHAR(255): `title`
	+ TEXT: `content`
	+ TINYINT: `is_open`
	+ TINYINT: `is_public`
	+ INT: `ticket_id`
	+ INT: `created_by`

+ Response 200 (application/json)

## NotificationTicket a Ticket_discussion [PUT /ticket_discussions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_discussion

+ Response 200 (application/json)

## Delete a Ticket_discussion [DELETE /ticket_discussions/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_discussion

+ Response 200 (application/json)
    + Body

            1

# Group Ticket_fields


## Retrieve all Ticket_fields [GET /ticket_fields]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `model` `values` `ticket_fields` `tickets` 
    + filters: (array, optional) - Allow to filter on the Ticket_fields fields
    + order: (array, optional) - order on the Ticket_fields's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Ticket_field by its ID [GET /ticket_fields/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_field
	+ embed: (array, optional) - Embed linked resources : `model` `values` `ticket_fields` `tickets` 

+ Response 200 (application/json)

## Create a Ticket_field [POST /ticket_fields]
+ Required post field : 
	+ INT: `ticket_id`
	+ INT: `field_id`

+ Response 200 (application/json)

## NotificationTicket a Ticket_field [PUT /ticket_fields/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_field

+ Response 200 (application/json)

## Delete a Ticket_field [DELETE /ticket_fields/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_field

+ Response 200 (application/json)
    + Body

            1

# Group Ticket_messages


## Retrieve all Ticket_messages [GET /ticket_messages]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `user` `discussion` `documents` 
    + filters: (array, optional) - Allow to filter on the Ticket_messages fields
    + order: (array, optional) - order on the Ticket_messages's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Ticket_message by its ID [GET /ticket_messages/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_message
	+ embed: (array, optional) - Embed linked resources : `user` `discussion` `documents` 

+ Response 200 (application/json)

## Create a Ticket_message [POST /ticket_messages]
+ Required post field : 
	+ TEXT: `content`
	+ INT: `user_id`
	+ INT: `discussion_id`

+ Response 200 (application/json)

## NotificationTicket a Ticket_message [PUT /ticket_messages/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_message

+ Response 200 (application/json)

## Delete a Ticket_message [DELETE /ticket_messages/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_message

+ Response 200 (application/json)
    + Body

            1

# Group Ticket_models


## Retrieve all Ticket_models [GET /ticket_models]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `tickets` `fields` 
    + filters: (array, optional) - Allow to filter on the Ticket_models fields
    + order: (array, optional) - order on the Ticket_models's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Ticket_model by its ID [GET /ticket_models/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_model
	+ embed: (array, optional) - Embed linked resources : `tickets` `fields` 

+ Response 200 (application/json)

## Create a Ticket_model [POST /ticket_models]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a Ticket_model [PUT /ticket_models/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_model

+ Response 200 (application/json)

## Delete a Ticket_model [DELETE /ticket_models/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_model

+ Response 200 (application/json)
    + Body

            1

# Group Ticket_Status


## Retrieve all Ticket_Status [GET /ticket_status]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `tickets` 
    + filters: (array, optional) - Allow to filter on the Ticket_Status fields
    + order: (array, optional) - order on the Ticket_Status's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Ticket_Status by its ID [GET /ticket_status/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_Status
	+ embed: (array, optional) - Embed linked resources : `tickets` 

+ Response 200 (application/json)

## Create a Ticket_Status [POST /ticket_status]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a Ticket_Status [PUT /ticket_status/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_Status

+ Response 200 (application/json)

## Delete a Ticket_Status [DELETE /ticket_status/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_Status

+ Response 200 (application/json)
    + Body

            1

# Group Ticket_types


## Retrieve all Ticket_types [GET /ticket_types]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `tickets` 
    + filters: (array, optional) - Allow to filter on the Ticket_types fields
    + order: (array, optional) - order on the Ticket_types's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Ticket_type by its ID [GET /ticket_types/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_type
	+ embed: (array, optional) - Embed linked resources : `tickets` 

+ Response 200 (application/json)

## Create a Ticket_type [POST /ticket_types]

+ Response 200 (application/json)

## NotificationTicket a Ticket_type [PUT /ticket_types/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_type

+ Response 200 (application/json)

## Delete a Ticket_type [DELETE /ticket_types/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Ticket_type

+ Response 200 (application/json)
    + Body

            1

## Export ticket to excel forma [GET /tickets/export]

+ Required fields
	+ ARRAY: `tickets`

+ Response 200
	+ Headers
			Content-Type : application/json


# Group TimeCards


## Retrieve all TimeCards [GET /timecards]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `logtimes` `user` 
    + filters: (array, optional) - Allow to filter on the TimeCards fields
    + order: (array, optional) - order on the TimeCards's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a TimeCard by its ID [GET /timecards/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the TimeCard
	+ embed: (array, optional) - Embed linked resources : `logtimes` `user` 

+ Response 200 (application/json)

## Create a TimeCard [POST /timecards]
+ Required post field : 
	+ INT: `cumul`
	+ TINYINT: `process_step`
	+ INT: `user_id`

+ Response 200 (application/json)

## NotificationTicket a TimeCard [PUT /timecards/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the TimeCard

+ Response 200 (application/json)

## Delete a TimeCard [DELETE /timecards/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the TimeCard

+ Response 200 (application/json)
    + Body

            1

# Group Types


## Retrieve all Types [GET /types]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `kamions` 
    + filters: (array, optional) - Allow to filter on the Types fields
    + order: (array, optional) - order on the Types's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Type by its ID [GET /types/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Type
	+ embed: (array, optional) - Embed linked resources : `kamions` 

+ Response 200 (application/json)

## Create a Type [POST /types]
+ Required post field : 
	+ VARCHAR(255): `label`

+ Response 200 (application/json)

## NotificationTicket a Type [PUT /types/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Type

+ Response 200 (application/json)

## Delete a Type [DELETE /types/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Type

+ Response 200 (application/json)
    + Body

            1

# Group UpdateFields


## Retrieve all UpdateFields [GET /update_fields]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
    + filters: (array, optional) - Allow to filter on the UpdateFields fields
    + order: (array, optional) - order on the UpdateFields's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a UpdateField by its ID [GET /update_fields/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the UpdateField

+ Response 200 (application/json)

## Create a UpdateField [POST /update_fields]
+ Required post field : 
	+ INT: `cat_id`
	+ INT: `notification_ticket_id`

+ Response 200 (application/json)

## NotificationTicket a UpdateField [PUT /update_fields/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the UpdateField

+ Response 200 (application/json)

## Delete a UpdateField [DELETE /update_fields/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the UpdateField

+ Response 200 (application/json)
    + Body

            1

# Group Users


## Retrieve all Users [GET /users]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `researches` `validation_controls` `capabilities` `roles` `tickets` `notes` `favory_tickets` `announcements` `quality_controls` `resources` `discussions` `jobs` `kamions` `documents` `manager` `managed` `last_documents` `last_elements` `supplements` `elements` 
    + filters: (array, optional) - Allow to filter on the Users fields
    + order: (array, optional) - order on the Users's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)
	+ Body
		{
			"data": [
				{
					"id": 1,
					"created_at": "2017-08-28 12:49:51",
					"updated_at": "2017-08-28 12:49:51",
					"deleted_at": null,
					"last_connection": null,
					"first_name": "Jaqueline",
					"last_name": "Kozey",
					"username": "manley.brown",
					"email": "towne.brice@wiegand.com",
					"wana_id": "5401652528346846",
					"badge_id": null,
					"smartphone_id": null,
					"is_active": 0,
					"role_id": 7,
					"manager_id": null
				},
				{
					"id": 2,
					"created_at": "2017-08-28 12:49:51",
					"updated_at": "2017-08-28 12:49:51",
					"deleted_at": null,
					"last_connection": null,
					"first_name": "Alvena",
					"last_name": "Senger",
					"username": "jazlyn.thompson",
					"email": "virginia91@gerhold.com",
					"wana_id": "4485067200105588",
					"badge_id": null,
					"smartphone_id": null,
					"is_active": 1,
					"role_id": 2,
					"manager_id": null
				},
				
			]
		}

## Retrieve a User by its ID [GET /users/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the User
	+ embed: (array, optional) - Embed linked resources : `researches` `validation_controls` `capabilities` `roles` `tickets` `notes` `favory_tickets` `announcements` `quality_controls` `resources` `discussions` `jobs` `kamions` `documents` `manager` `managed` `last_documents` `last_elements` `supplements` `elements` 

+ Response 200 (application/json)
	+ Body
		{
			"data": {
				"id": 12,
				"created_at": "2017-08-28 12:49:51",
				"updated_at": "2017-08-28 12:49:51",
				"deleted_at": null,
				"last_connection": null,
				"first_name": "Rowland",
				"last_name": "Tillman",
				"username": "laurine.glover",
				"email": "jerde.leslie@lubowitz.net",
				"wana_id": "4024007196971",
				"badge_id": null,
				"smartphone_id": null,
				"is_active": 0,
				"role_id": 5,
				"manager_id": null
			}
		}

## Create a User [POST /users]
+ Required post field : 
	+ VARCHAR(255): `first_name`
	+ VARCHAR(255): `last_name`
	+ VARCHAR(255): `username`
	+ VARCHAR(255): `password`
	+ VARCHAR(255): `wana_id`
	+ TINYINT: `is_active`
	+ INT: `level`

+ Response 200 (application/json)

## Save user's avatar [POST /users/{id}/image]

+ Required fields
	+ FILE: `file`

+ Parameters
	+ id:(integer, required) - the id of the user


## NotificationTicket a User [PUT /users/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the User

+ Response 200 (application/json)
	+ Body
		{
			"user": {
				"id": 1,
				"created_at": "2017-08-28 12:49:51",
				"updated_at": "2017-08-28 13:18:06",
				"deleted_at": null,
				"last_connection": null,
				"first_name": "Jaqueline",
				"last_name": "Kozey",
				"username": "manley.brown",
				"email": "towne.brice@wiegand.com",
				"wana_id": "5401652528346846",
				"badge_id": "ser68g5dfg6y8sergt",
				"smartphone_id": null,
				"is_active": 0,
				"role_id": 7,
				"manager_id": null
			}
		}

## Delete a User [DELETE /users/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the User

+ Response 200 (application/json)
    + Body

            1

## sync a wana users [PUT /users/wana/{id}]

+ Parameters
	+ id: (integer, required) - the wana id of the user


## Last instruction of the user [GET /users/{id_user}/kamions/{id_kamion}/last_instruction]

+ Parameters
	+ id_user:(integer, required) - the id of the user
	+ id_kamion:(integer, required) - the id of the kamion


## Insert a new instruction of the last instruction of the current user [POST /users/instructions/{id}]

+ Parameters
	+ id:(integer, required) - the id of the instruction


## Last instruction of the user [GET /users/{id_user}/last_instruction]

+ Parameters
	+ id_user:(integer, required) - the id of the user


## Insert a new material resource of the last material resource of the current user [POST /users/last_materialresource/{id}]

+ Parameters
	+ id:(integer, required) - the id of the instruction


## Last material resource of the user [GET /users/{id_user}/last_materialresource]

+ Parameters
	+ id_user:(integer, required) - the id of the user


# Group User_file


## Retrieve all User_file [GET /user_files]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `element` 
    + filters: (array, optional) - Allow to filter on the User_file fields
    + order: (array, optional) - order on the User_file's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a User_file by its ID [GET /user_files/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the User_file
	+ embed: (array, optional) - Embed linked resources : `element` 

+ Response 200 (application/json)

## Create a User_file [POST /user_files]
+ Required post field : 
	+ VARCHAR(255): `url`
	+ VARCHAR(255): `title`
	+ INT: `element_id`

+ Response 200 (application/json)

## NotificationTicket a User_file [PUT /user_files/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the User_file

+ Response 200 (application/json)

## Delete a User_file [DELETE /user_files/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the User_file

+ Response 200 (application/json)
    + Body

            1

# Group Validation_controls


## Retrieve all Validation_controls [GET /validation_controls]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `element` `user` `documents` 
    + filters: (array, optional) - Allow to filter on the Validation_controls fields
    + order: (array, optional) - order on the Validation_controls's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Validation_control by its ID [GET /validation_controls/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Validation_control
	+ embed: (array, optional) - Embed linked resources : `element` `user` `documents` 

+ Response 200 (application/json)

## Create a Validation_control [POST /validation_controls]
+ Required post field : 
	+ INT: `element_id`

+ Response 200 (application/json)

## NotificationTicket a Validation_control [PUT /validation_controls/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Validation_control

+ Response 200 (application/json)

## Delete a Validation_control [DELETE /validation_controls/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Validation_control

+ Response 200 (application/json)
    + Body

            1

# Group Workgroups


## Retrieve all Workgroups [GET /workgroups]

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`
	+ embed: (array, optional) - Embed linked resources : `users` `tickets` 
    + filters: (array, optional) - Allow to filter on the Workgroups fields
    + order: (array, optional) - order on the Workgroups's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)

## Retrieve a Workgroup by its ID [GET /workgroups/{id}]

+ Parameters
    + id: (integer, required) - Path variable, id of the Workgroup
	+ embed: (array, optional) - Embed linked resources : `users` `tickets` 

+ Response 200 (application/json)

## Create a Workgroup [POST /workgroups]
+ Required post field : 
	+ VARCHAR(255): `target`

+ Response 200 (application/json)

## NotificationTicket a Workgroup [PUT /workgroups{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Workgroup

+ Response 200 (application/json)

## Delete a Workgroup [DELETE /workgroups/{id}]


+ Parameters
    + id: (integer, required) - Path variable, id of the Workgroup

+ Response 200 (application/json)
    + Body

            1