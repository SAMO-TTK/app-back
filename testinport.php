<?php


require_once './vendor/autoload.php';


function progressBar($done, $total, $name) {
	$perc = floor(($done / $total) * 100);
	$alf = intval($perc/2);
	$left = 50 - $alf;
	$write = sprintf("\033[0G\033[2K┃\e[0m[%'={$alf}s>%-{$left}s] - $perc%% - $done/$total  =>  $name\t\e[1;33m┃", "", "");
	fwrite(STDERR, $write);
}

echo "NANY !\n";
$EX_DBNAME = 'hubttk';
$EX_HOST = '127.0.0.1';
$EX_USERNAME = 'hubttk';
$EX_PASSWD = 'R[S<qwer';



$export = new PDO("mysql:dbname=$EX_DBNAME;host=$EX_HOST", $EX_USERNAME, $EX_PASSWD);
#region - matiere

$curl = new \Curl\Curl();
$curl->get('http://wkrest.toutenkamion.com/stock/');
$data = json_decode($curl->response);
$count = count($data);
echo "\e[1;36mMatiere\n\e[1;33m┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n";
foreach ($data as $i => $row){

	$sql = "INSERT INTO material_resources (label, wana_id, unit_label) VALUE ('" . addslashes($row->sNomArticle) . "' , '". addslashes($row->sIDArticle). "' , '". addslashes($row->sUnite) ."')";
	$rep = $export->query($sql);
	if ($rep === false){
		echo "\n".$sql."\n";
	}
	progressBar($i + 1, $count, "import Matiere");
//	dump($rep);
}
echo "\n\e[1;33m┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\e[0m\n\n";

#endregion

#region - jobs

$curl = new \Curl\Curl();
$curl->get('http://wkrest.toutenkamion.com/metier/0');
$data = json_decode($curl->response);
$count = count($data);
echo "\e[1;36mJobs\n\e[1;33m┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n";
foreach ($data as $i => $row){

	$sql = "INSERT INTO jobs (label, wana_id) VALUE ('" . addslashes($row->sLibelle) . "' , '". addslashes($row->IDMetier) ."')";
	$rep = $export->query($sql);
	if ($rep === false){
		echo "\n".$sql."\n";
	}
	progressBar($i + 1, $count, "import Jobs ");
//	dump($rep);
}
echo "\n\e[1;33m┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\e[0m\n\n";

#endregion

#region - instruction

$curl = new \Curl\Curl();
$curl->get('http://wkrest.toutenkamion.com/instructions');
$data = json_decode($curl->response);
$count = count($data);
echo "\e[1;36mInstruction\n\e[1;33m┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n";
foreach ($data as $i => $row){

	$sql = "SELECT id FROM instructions WHERE wana_id = ".$row->nIDInstruction;
	$rep = $export->query($sql)->fetch();
	if($rep === false){

		$sql = "SELECT id FROM jobs WHERE wana_id = ".$row->nAtelier;
		$rep = $export->query($sql)->fetch();
		if($rep !== false){
			$sql = "INSERT INTO instructions (label, wana_id, wana_number, job_id) VALUE ('".addslashes($row->sDescInstruction)."', '".addslashes($row->nIDInstruction)."', '".addslashes($row->nInstruction)."', '".$rep['id']."')";
			$rep = $export->query($sql);
			if ($rep === false){
				echo "\n".$sql."\n";
			}
		}
	}
	progressBar($i + 1, $count, "import Inst.");
}
echo "\n\e[1;33m┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\e[0m\n\n";

#endregion

#region - instruction->kamion_id

$sql = "SELECT * FROM kamions WHERE is_locked = 0";
$kamions = $export->query($sql)->fetchAll();
foreach ($kamions as $kamion){
	$curl = new \Curl\Curl();
	$curl->get('http://wkrest.toutenkamion.com/instruction/kamion/'.$kamion['wana_id']);
	$data = json_decode($curl->response);
	$count = count($data);
	if ($count) {
		echo "\e[1;36mKamion " . $kamion['wana_id'] . " instruction\n\e[1;33m┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n";
		foreach ($data as $i => $row) {
			$sql = "SELECT id FROM instructions WHERE wana_number = '" . $row->nInstruction ."'";
			$rep = $export->query($sql)->fetch();
			if ($rep !== false) {
				$sql = "UPDATE instructions SET kamion_id=" . $kamion['id'] . " WHERE id = " . $rep['id'];
				$rep = $export->query($sql);
				if ($rep === false) {
					echo "\n" . $sql . "\n";
				}
			}else{

				echo "\n" . $sql . "\n";
			}
			progressBar($i + 1, $count, "import Kam. Inst.");

		}
		echo "\n\e[1;33m┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\e[0m\n\n";
	}
}

#endregion