<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// $users = factory(App\Api\v1\Models\User::class, 15)->create();
        DB::table('users')->insert([
            'first_name' => 'Gael',
            'last_name' => 'Mallet',
            'username' => 'admin',
            'password' => bcrypt('admin'),
			'badge_id' => ('7c82323e'),
            'wana_id' => '123456789987654321',
            'is_active' => 1,
            'level' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'first_name' => 'Frédéric',
            'last_name' => 'Glaume',
            'username' => 'frgl',
            'password' => bcrypt('frgl'),
			'badge_id' => ('fgsdgs3e'),
            'wana_id' => 'rtgswergse4321',
            'is_active' => 1,
			'is_quality_responsable' => 1,
			'is_validation_responsable' => 1,
            'level' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
			'manager_id' => 1
        ]);
		DB::table('users')->insert([
			'first_name' => 'Franck',
			'last_name' => 'Neveu',
			'username' => 'frne',
			'password' => bcrypt('frne'),
			'badge_id' => ('7c82hde'),
			'wana_id' => '12345d6gh415sdfg1',
			'is_active' => 1,
			'is_quality_responsable' => 0,
			'level' => 4,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
			'manager_id' => 1
		]);
		DB::table('users')->insert([
			'first_name' => 'Bruno',
			'last_name' => 'Pebelier',
			'username' => 'brpe',
			'password' => bcrypt('brpe'),
			'badge_id' => ('7c82hde'),
			'wana_id' => '12345d6gh415sdfg1',
			'is_active' => 1,
			'is_quality_responsable' => 0,
			'level' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
			'manager_id' => 1
		]);
		DB::table('users')->insert([
			'first_name' => 'TTK',
			'last_name' => 'APP',
			'username' => 'ttk_app',
			'password' => bcrypt('ttk-dvi-33'),
			'badge_id' => ('7c82hde'),
			'wana_id' => '12345d6gh415sdfg1',
			'is_active' => 1,
			'is_quality_responsable' => 0,
			'level' => 4,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
			'manager_id' => 1
		]);
    }
}
