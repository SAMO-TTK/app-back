<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Api\v1\Models\Kamion::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'wana_id' => $faker->creditCardNumber,
		'wana_created_at' => date('Y-m-d H:i:s'),
		'name' => $faker->colorName,
		'is_delivered' => rand(0,1),
		'is_locked' => rand(0,1),
		'stage_id' => rand(1, 3),
		'type_id' => rand(1, 17),
		'category_id' => rand(1, 6),
    ];
});
