<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Api\v1\Models\Element::class, function (Faker\Generator $faker) {
    static $password;

    return [
        // 'imported_at' => $faker->dateTimeThisMonth,
        'title' => $faker->word,
        'chapter_id' => rand(1, 8)
    ];
});
