<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Api\v1\Models\Last_document::class, function (Faker\Generator $faker) {
    return [
        'document_mfile_id' => $faker->creditCardNumber,
        'user_id' => rand(1, 16),
    ];
});
