<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Illuminate\Support\Facades\Hash;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Api\v1\Models\Impact::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'amount' => $faker->randomFloat(2, 15000.0, 150000.0),
		'delai' => date( 'Y-m-d H:i:s', $faker->unixTime(315529200) + time()),
		'element_id' => rand(1, 1500)
    ];
});
