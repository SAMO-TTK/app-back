<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Illuminate\Support\Facades\Hash;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Api\v1\Models\Announcement::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'title' => $faker->colorName,
        'content' => $faker->text,
        'user_id' => rand(1, 16),
		'kamion_id'=>rand(1, 16)
    ];
});
