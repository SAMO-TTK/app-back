<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Api\v1\Models\Document::class, function (Faker\Generator $faker) {
    static $password;
    $i = rand(1, 6);
    $word = "";
    while ($i > 0){
        $word .= $faker->word . '/';
        $i--;
    }
    return [
        'title' => $faker->slug,
        'extension' => $faker->fileExtension,
        'path' => $word,
        'mfile_id' => $faker->creditCardNumber,
    ];
});
