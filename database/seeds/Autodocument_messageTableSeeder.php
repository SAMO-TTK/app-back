
<?php

use Illuminate\Database\Seeder;

class Autodocument_messageTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('document_message')->insert([
			'created_at' => '2018-06-05 12:26:54',
			'updated_at' => '2018-06-05 12:26:54',
			'document_id' => '186',
			'message_id' => '1',
		]);
		DB::table('document_message')->insert([
			'created_at' => '2018-06-05 12:26:54',
			'updated_at' => '2018-06-05 12:26:54',
			'document_id' => '187',
			'message_id' => '1',
		]);
		DB::table('document_message')->insert([
			'created_at' => '2018-06-05 12:26:54',
			'updated_at' => '2018-06-05 12:26:54',
			'document_id' => '198',
			'message_id' => '1',
		]);
    }
}