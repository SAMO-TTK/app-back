
<?php

use Illuminate\Database\Seeder;

class Autorole_userTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('role_user')->insert([
			'created_at' => '0000-00-00 00:00:00',
			'updated_at' => '0000-00-00 00:00:00',
			'user_id' => '3',
			'role_id' => '1',
		]);
		DB::table('role_user')->insert([
			'user_id' => '2',
			'role_id' => '3',
		]);
		DB::table('role_user')->insert([
			'user_id' => '2',
			'role_id' => '2',
		]);
		DB::table('role_user')->insert([
			'user_id' => '2',
			'role_id' => '7',
		]);
		DB::table('role_user')->insert([
			'user_id' => '1',
			'role_id' => '8',
		]);
		DB::table('role_user')->insert([
			'user_id' => '4',
			'role_id' => '1',
		]);
    }
}