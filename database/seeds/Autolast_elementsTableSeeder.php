
<?php

use Illuminate\Database\Seeder;

class Autolast_elementsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('last_elements')->insert([
			'created_at' => '2018-05-15 13:20:07',
			'updated_at' => '2018-05-15 13:20:07',
			'user_id' => '1',
			'element_id' => '1',
		]);
		DB::table('last_elements')->insert([
			'created_at' => '2018-06-04 07:39:26',
			'updated_at' => '2018-06-04 07:39:26',
			'user_id' => '1',
			'element_id' => '920',
		]);
		DB::table('last_elements')->insert([
			'created_at' => '2018-06-04 07:44:00',
			'updated_at' => '2018-06-04 07:44:00',
			'user_id' => '1',
			'element_id' => '972',
		]);
		DB::table('last_elements')->insert([
			'created_at' => '2018-06-04 08:44:23',
			'updated_at' => '2018-06-05 20:45:38',
			'user_id' => '1',
			'element_id' => '916',
		]);
		DB::table('last_elements')->insert([
			'created_at' => '2018-06-05 12:18:31',
			'updated_at' => '2018-06-05 12:27:50',
			'user_id' => '1',
			'element_id' => '925',
		]);
		DB::table('last_elements')->insert([
			'created_at' => '2018-06-05 12:18:46',
			'updated_at' => '2018-06-05 12:18:46',
			'user_id' => '1',
			'element_id' => '962',
		]);
		DB::table('last_elements')->insert([
			'created_at' => '2018-06-05 12:18:54',
			'updated_at' => '2018-06-05 12:19:15',
			'user_id' => '1',
			'element_id' => '956',
		]);
		DB::table('last_elements')->insert([
			'created_at' => '2018-06-05 12:46:07',
			'updated_at' => '2018-06-05 12:46:07',
			'user_id' => '1',
			'element_id' => '917',
		]);
		DB::table('last_elements')->insert([
			'created_at' => '2018-06-05 13:33:50',
			'updated_at' => '2018-06-05 13:33:50',
			'user_id' => '4',
			'element_id' => '916',
		]);
    }
}