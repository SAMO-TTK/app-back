<?php

use Illuminate\Database\Seeder;

class QualityDocumentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 5; $i++) :
        $ra = rand(1,3);
        $rb = rand(1,11);
            if ($ra != $rb):
                DB::table('quality_document')->insert([
                    'control_id' => $ra, 'document_id' => $rb, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
                ]);
            endif;
        endfor;
    }
}
