
<?php

use Illuminate\Database\Seeder;

class AutogroupesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('groupes')->insert([
			'created_at' => '2018-05-15 13:08:57',
			'updated_at' => '2018-05-15 13:08:57',
			'label' => 'png',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'DWG',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'pdf',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'log',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'err',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'dxf',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-05-17 12:11:28',
			'updated_at' => '2018-05-17 12:11:28',
			'label' => 'JPG',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-05-17 12:11:29',
			'updated_at' => '2018-05-17 12:11:29',
			'label' => 'zip',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-06-05 12:29:15',
			'updated_at' => '2018-06-05 12:29:15',
			'label' => 'doc',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-06-05 12:29:16',
			'updated_at' => '2018-06-05 12:29:16',
			'label' => 'xlsx',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-06-05 12:29:16',
			'updated_at' => '2018-06-05 12:29:16',
			'label' => 'msg',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-06-05 13:36:18',
			'updated_at' => '2018-06-05 13:36:18',
			'label' => 'svg',
		]);
		DB::table('groupes')->insert([
			'created_at' => '2018-06-05 13:36:18',
			'updated_at' => '2018-06-05 13:36:18',
			'label' => 'xlsm',
		]);
    }
}