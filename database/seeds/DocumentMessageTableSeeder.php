<?php

use Illuminate\Database\Seeder;

class DocumentMessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) :
		$ra = rand(1,11);
		$rb = rand(1,150);
			if ($ra != $rb):
		        DB::table('document_message')->insert([
		            'document_id' => $ra, 'message_id' => $rb, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
		        ]);
			endif;
		endfor;
    }
}
