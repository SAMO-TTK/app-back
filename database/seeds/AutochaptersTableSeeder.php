
<?php

use Illuminate\Database\Seeder;

class AutochaptersTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00',
			'title' => 'CONDITIONS CONTRACTUELLES & ORGANISATION',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.01',
			'title' => 'DATE DE COMMANDE : 13/09/2017',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.02',
			'title' => 'DELAI : 14/04/2018',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.03',
			'title' => 'LC : NON',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.04',
			'title' => 'PASSAGE AUX MINES : OUI',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.05',
			'title' => 'CONDITIONS DE REGLEMENT :
xxx',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.06',
			'title' => 'INCOTERM : LIVRAISON EN Belgique SUR SITE CLIENT',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.07',
			'title' => 'Date réunion de conception provisoire : JJ/MM/AAAA',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.08',
			'title' => 'REFERENCE PIGE:',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.09',
			'title' => 'Date fin de pige : JJ/MM/AAAA',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:44:49',
			'deleted_at' => '2018-05-16 09:44:49',
			'numbering' => '00.10',
			'title' => 'REFERENCE OFFRE TECHNIQUE : 37249-02-a',
			'kamion_id' => '2',
			'parent_id' => '1',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'numbering' => '01',
			'title' => '- AMENAGEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'numbering' => '01.01',
			'title' => 'Local vestiaires (a)',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'numbering' => '01.02',
			'title' => 'Local WC (a)',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'numbering' => '01.03',
			'title' => 'Salle de formation – zone froide (b) – Equipements fixes',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'numbering' => '01.04',
			'title' => 'Salle de formation – zone froide (b) – Equipements fixes',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'numbering' => '01.05',
			'title' => 'Salle de formation – zone froide (b) – Equipements mobiles',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'numbering' => '01.06',
			'title' => 'Chambre froide positive 1°C – 6°C (20)',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'numbering' => '01.07',
			'title' => 'Salle de formation – zone chaude (c)',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'numbering' => '01.08',
			'title' => 'Salle de formation – zone chaude (c)',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'numbering' => '01.09',
			'title' => 'Local plonge (d)',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'numbering' => '01.10',
			'title' => 'Zone de stockage (e)',
			'kamion_id' => '2',
			'parent_id' => '12',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '02',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '02.01',
			'title' => 'CHÂSSIS SEMI-REMORQUE – FOURNITURE TOUTENKAMION',
			'kamion_id' => '2',
			'parent_id' => '23',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '02.02',
			'title' => 'TRACTEUR – FOURNITURE CLIENT',
			'kamion_id' => '2',
			'parent_id' => '23',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.01',
			'title' => 'LE FAUX CHASSIS ET SES ACCESSOIRES',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '03.02',
			'title' => 'Structure du faux-châssis',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '03.03',
			'title' => 'Escalier',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '03.04',
			'title' => 'Stabilisation',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '03.05',
			'title' => 'Cales',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '03.06',
			'title' => 'LA CARROSSERIE',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '03.07',
			'title' => 'Dimensions',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '03.08',
			'title' => 'Longueur',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'numbering' => '03.09',
			'title' => 'Largeur ',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.10',
			'title' => 'Hauteur',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.11',
			'title' => 'Assemblage des panneaux de la carrosserie',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.12',
			'title' => 'Finition de la carrosserie',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.13',
			'title' => 'Profilé bas de caisse ',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.14',
			'title' => 'Cloisons et portes entre locaux',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.15',
			'title' => 'Extensions 	 ',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.16',
			'title' => 'Décor extérieur personnalisé',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.17',
			'title' => 'LES PORTES, ACCES TECHNIQUES ET BAIES VITREES',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.18',
			'title' => 'Portes d’accès extérieurs',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.19',
			'title' => 'Accès techniques',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.20',
			'title' => 'Baies vitrées',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'numbering' => '03.21',
			'title' => 'Deux baies fixes à double vitrage et leurs stores intégrés dans l’extension arrière droite de la salle de formation – zone froide._x000D_',
			'kamion_id' => '2',
			'parent_id' => '26',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'numbering' => '04',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'numbering' => '04.01',
			'title' => 'Bourreuse verticale (16)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'numbering' => '04.02',
			'title' => 'Hachoir réfrigéré (17)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'numbering' => '04.03',
			'title' => 'Sous-videuse (18)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'numbering' => '04.04',
			'title' => 'Désinsectiseur (22)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'numbering' => '04.05',
			'title' => 'Ensemble de lavage avec réservoir de produit (45)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'numbering' => '04.06',
			'title' => 'Fourneau 4 plaques électriques (26)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'numbering' => '04.07',
			'title' => 'Soubassement réfrigéré (28)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'numbering' => '04.08',
			'title' => 'Four de cuisson à vapeur injectée (29)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'numbering' => '04.09',
			'title' => 'Support ouvert pour four (30)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'numbering' => '04.10',
			'title' => 'Hotte (34)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'numbering' => '04.11',
			'title' => 'Désinsectiseur (36)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'numbering' => '04.12',
			'title' => 'Stérilisateur à couteaux à eau (43)',
			'kamion_id' => '2',
			'parent_id' => '48',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'numbering' => '05',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'numbering' => '05.01',
			'title' => 'Mélangeur 30kg (48)',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'numbering' => '05.02',
			'title' => 'Operculeuse (49)',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'numbering' => '05.03',
			'title' => 'Trancheuse (50)',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'numbering' => '05.04',
			'title' => 'Batteur mélangeur 5 litres (52)',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'numbering' => '05.05',
			'title' => 'Mixer 15 litres (53)',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.06',
			'title' => 'Mixer 30 litres (54)',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.07',
			'title' => 'Balance 60kg – 2g (55)',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.08',
			'title' => 'Balance 3kg – 0,1g (56)',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.09',
			'title' => 'LES REVETEMENTS',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.10',
			'title' => 'Revêtement de sol',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.11',
			'title' => 'Revêtement des parois',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.12',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.13',
			'title' => 'Eclairage extérieur',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.14',
			'title' => 'Eclairage de coffre',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'numbering' => '05.15',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.16',
			'title' => 'Basse tension',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.17',
			'title' => 'Distribution interne',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.18',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.19',
			'title' => 'Alimentation en eau propre',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.20',
			'title' => 'Equipements raccordés en eau propre',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.21',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.22',
			'title' => 'Evacuation des eaux usées',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.23',
			'title' => 'LA CLIMATISATION ET LE CHAUFFAGE',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.24',
			'title' => 'L’unité est équipée d’un système de climatisation réversible',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.25',
			'title' => 'La salle de formation – zone froide est équipée avec un ensemble groupe froid',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.26',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.27',
			'title' => 'Détecteur de fumée',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.28',
			'title' => 'Extincteur',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.29',
			'title' => 'Bloc autonome d’éclairage de sécurité',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.30',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.31',
			'title' => 'Système de ventilation',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.32',
			'title' => 'LES CONTROLES DE CONFORMITE',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.33',
			'title' => 'Installation électrique',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'numbering' => '05.34',
			'title' => 'Conformité code de la route',
			'kamion_id' => '2',
			'parent_id' => '61',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00',
			'title' => 'CONDITIONS CONTRACTUELLES & ORGANISATION',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.01',
			'title' => 'DATE DE COMMANDE : 13/09/2017',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.02',
			'title' => 'DELAI : 14/04/2018',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.03',
			'title' => 'LC : NON',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.04',
			'title' => 'PASSAGE AUX MINES : OUI',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.05',
			'title' => 'CONDITIONS DE REGLEMENT :
xxx',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.06',
			'title' => 'INCOTERM : LIVRAISON EN Belgique SUR SITE CLIENT',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.07',
			'title' => 'Date réunion de conception provisoire : JJ/MM/AAAA',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.08',
			'title' => 'REFERENCE PIGE:',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.09',
			'title' => 'Date fin de pige : JJ/MM/AAAA',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:15',
			'deleted_at' => '2018-05-16 09:55:15',
			'numbering' => '00.10',
			'title' => 'REFERENCE OFFRE TECHNIQUE : 37249-02-a',
			'kamion_id' => '2',
			'parent_id' => '96',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:17',
			'deleted_at' => '2018-05-16 09:55:17',
			'numbering' => '01',
			'title' => '- AMENAGEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'numbering' => '01.01',
			'title' => 'Local vestiaires (a)',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'numbering' => '01.02',
			'title' => 'Local WC (a)',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'numbering' => '01.03',
			'title' => 'Salle de formation – zone froide (b) – Equipements fixes',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'numbering' => '01.04',
			'title' => 'Salle de formation – zone froide (b) – Equipements fixes',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'numbering' => '01.05',
			'title' => 'Salle de formation – zone froide (b) – Equipements mobiles',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'numbering' => '01.06',
			'title' => 'Chambre froide positive 1°C – 6°C (20)',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'numbering' => '01.07',
			'title' => 'Salle de formation – zone chaude (c)',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'numbering' => '01.08',
			'title' => 'Salle de formation – zone chaude (c)',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'numbering' => '01.09',
			'title' => 'Local plonge (d)',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'numbering' => '01.10',
			'title' => 'Zone de stockage (e)',
			'kamion_id' => '2',
			'parent_id' => '107',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'numbering' => '02',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'numbering' => '02.01',
			'title' => 'CHÂSSIS SEMI-REMORQUE – FOURNITURE TOUTENKAMION',
			'kamion_id' => '2',
			'parent_id' => '118',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'numbering' => '02.02',
			'title' => 'TRACTEUR – FOURNITURE CLIENT',
			'kamion_id' => '2',
			'parent_id' => '118',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:22',
			'deleted_at' => '2018-05-16 09:55:22',
			'numbering' => '03',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:22',
			'deleted_at' => '2018-05-16 09:55:22',
			'numbering' => '03.01',
			'title' => 'LE FAUX CHASSIS ET SES ACCESSOIRES',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.02',
			'title' => 'Structure du faux-châssis',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.03',
			'title' => 'Escalier',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.04',
			'title' => 'Stabilisation',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.05',
			'title' => 'Cales',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.06',
			'title' => 'LA CARROSSERIE',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.07',
			'title' => 'Dimensions',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.08',
			'title' => 'Longueur',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.09',
			'title' => 'Largeur ',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.10',
			'title' => 'Hauteur',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.11',
			'title' => 'Assemblage des panneaux de la carrosserie',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.12',
			'title' => 'Finition de la carrosserie',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.13',
			'title' => 'Profilé bas de caisse ',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'numbering' => '03.14',
			'title' => 'Cloisons et portes entre locaux',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'numbering' => '03.15',
			'title' => 'Extensions 	 ',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'numbering' => '03.16',
			'title' => 'Décor extérieur personnalisé',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'numbering' => '03.17',
			'title' => 'LES PORTES, ACCES TECHNIQUES ET BAIES VITREES',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'numbering' => '03.18',
			'title' => 'Portes d’accès extérieurs',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'numbering' => '03.19',
			'title' => 'Accès techniques',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'numbering' => '03.20',
			'title' => 'Baies vitrées',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'numbering' => '03.21',
			'title' => 'Deux baies fixes à double vitrage et leurs stores intégrés dans l’extension arrière droite de la salle de formation – zone froide._x000D_',
			'kamion_id' => '2',
			'parent_id' => '121',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:26',
			'deleted_at' => '2018-05-16 09:55:26',
			'numbering' => '04',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:26',
			'deleted_at' => '2018-05-16 09:55:26',
			'numbering' => '04.01',
			'title' => 'Bourreuse verticale (16)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'numbering' => '04.02',
			'title' => 'Hachoir réfrigéré (17)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'numbering' => '04.03',
			'title' => 'Sous-videuse (18)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'numbering' => '04.04',
			'title' => 'Désinsectiseur (22)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'numbering' => '04.05',
			'title' => 'Ensemble de lavage avec réservoir de produit (45)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'numbering' => '04.06',
			'title' => 'Fourneau 4 plaques électriques (26)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'numbering' => '04.07',
			'title' => 'Soubassement réfrigéré (28)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'numbering' => '04.08',
			'title' => 'Four de cuisson à vapeur injectée (29)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'numbering' => '04.09',
			'title' => 'Support ouvert pour four (30)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'numbering' => '04.10',
			'title' => 'Hotte (34)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'numbering' => '04.11',
			'title' => 'Désinsectiseur (36)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:29',
			'deleted_at' => '2018-05-16 09:55:29',
			'numbering' => '04.12',
			'title' => 'Stérilisateur à couteaux à eau (43)',
			'kamion_id' => '2',
			'parent_id' => '143',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'numbering' => '05',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'numbering' => '05.01',
			'title' => 'Mélangeur 30kg (48)',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'numbering' => '05.02',
			'title' => 'Operculeuse (49)',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'numbering' => '05.03',
			'title' => 'Trancheuse (50)',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'numbering' => '05.04',
			'title' => 'Batteur mélangeur 5 litres (52)',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.05',
			'title' => 'Mixer 15 litres (53)',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.06',
			'title' => 'Mixer 30 litres (54)',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.07',
			'title' => 'Balance 60kg – 2g (55)',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.08',
			'title' => 'Balance 3kg – 0,1g (56)',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.09',
			'title' => 'LES REVETEMENTS',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.10',
			'title' => 'Revêtement de sol',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.11',
			'title' => 'Revêtement des parois',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.12',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'numbering' => '05.13',
			'title' => 'Eclairage extérieur',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.14',
			'title' => 'Eclairage de coffre',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.15',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.16',
			'title' => 'Basse tension',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.17',
			'title' => 'Distribution interne',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.18',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.19',
			'title' => 'Alimentation en eau propre',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.20',
			'title' => 'Equipements raccordés en eau propre',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.21',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.22',
			'title' => 'Evacuation des eaux usées',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.23',
			'title' => 'LA CLIMATISATION ET LE CHAUFFAGE',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.24',
			'title' => 'L’unité est équipée d’un système de climatisation réversible',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.25',
			'title' => 'La salle de formation – zone froide est équipée avec un ensemble groupe froid',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.26',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.27',
			'title' => 'Détecteur de fumée',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.28',
			'title' => 'Extincteur',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.29',
			'title' => 'Bloc autonome d’éclairage de sécurité',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.30',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.31',
			'title' => 'Système de ventilation',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.32',
			'title' => 'LES CONTROLES DE CONFORMITE',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.33',
			'title' => 'Installation électrique',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'numbering' => '05.34',
			'title' => 'Conformité code de la route',
			'kamion_id' => '2',
			'parent_id' => '156',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00',
			'title' => 'CONDITIONS CONTRACTUELLES & ORGANISATION',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.01',
			'title' => 'DATE DE COMMANDE : 13/09/2017',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.02',
			'title' => 'DELAI : 14/04/2018',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.03',
			'title' => 'LC : NON',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.04',
			'title' => 'PASSAGE AUX MINES : OUI',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.05',
			'title' => 'CONDITIONS DE REGLEMENT :
xxx',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.06',
			'title' => 'INCOTERM : LIVRAISON EN Belgique SUR SITE CLIENT',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.07',
			'title' => 'Date réunion de conception provisoire : JJ/MM/AAAA',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.08',
			'title' => 'REFERENCE PIGE:',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.09',
			'title' => 'Date fin de pige : JJ/MM/AAAA',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:48',
			'deleted_at' => '2018-05-16 11:31:48',
			'numbering' => '00.10',
			'title' => 'REFERENCE OFFRE TECHNIQUE : 37249-02-a',
			'kamion_id' => '2',
			'parent_id' => '191',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:50',
			'deleted_at' => '2018-05-16 11:31:50',
			'numbering' => '01',
			'title' => '- AMENAGEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'numbering' => '01.01',
			'title' => 'Local vestiaires (a)',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'numbering' => '01.02',
			'title' => 'Local WC (a)',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'numbering' => '01.03',
			'title' => 'Salle de formation – zone froide (b) – Equipements fixes',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'numbering' => '01.04',
			'title' => 'Salle de formation – zone froide (b) – Equipements fixes',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'numbering' => '01.05',
			'title' => 'Salle de formation – zone froide (b) – Equipements mobiles',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'numbering' => '01.06',
			'title' => 'Chambre froide positive 1°C – 6°C (20)',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'numbering' => '01.07',
			'title' => 'Salle de formation – zone chaude (c)',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'numbering' => '01.08',
			'title' => 'Salle de formation – zone chaude (c)',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'numbering' => '01.09',
			'title' => 'Local plonge (d)',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'numbering' => '01.10',
			'title' => 'Zone de stockage (e)',
			'kamion_id' => '2',
			'parent_id' => '202',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'numbering' => '02',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:54',
			'deleted_at' => '2018-05-16 11:31:54',
			'numbering' => '02.01',
			'title' => 'CHÂSSIS SEMI-REMORQUE – FOURNITURE TOUTENKAMION',
			'kamion_id' => '2',
			'parent_id' => '213',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:54',
			'deleted_at' => '2018-05-16 11:31:54',
			'numbering' => '02.02',
			'title' => 'TRACTEUR – FOURNITURE CLIENT',
			'kamion_id' => '2',
			'parent_id' => '213',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'numbering' => '03',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'numbering' => '03.01',
			'title' => 'LE FAUX CHASSIS ET SES ACCESSOIRES',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'numbering' => '03.02',
			'title' => 'Structure du faux-châssis',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'numbering' => '03.03',
			'title' => 'Escalier',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'numbering' => '03.04',
			'title' => 'Stabilisation',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'numbering' => '03.05',
			'title' => 'Cales',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'numbering' => '03.06',
			'title' => 'LA CARROSSERIE',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'numbering' => '03.07',
			'title' => 'Dimensions',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.08',
			'title' => 'Longueur',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.09',
			'title' => 'Largeur ',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.10',
			'title' => 'Hauteur',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.11',
			'title' => 'Assemblage des panneaux de la carrosserie',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.12',
			'title' => 'Finition de la carrosserie',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.13',
			'title' => 'Profilé bas de caisse ',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.14',
			'title' => 'Cloisons et portes entre locaux',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.15',
			'title' => 'Extensions 	 ',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.16',
			'title' => 'Décor extérieur personnalisé',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.17',
			'title' => 'LES PORTES, ACCES TECHNIQUES ET BAIES VITREES',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.18',
			'title' => 'Portes d’accès extérieurs',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'numbering' => '03.19',
			'title' => 'Accès techniques',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:57',
			'deleted_at' => '2018-05-16 11:31:57',
			'numbering' => '03.20',
			'title' => 'Baies vitrées',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:57',
			'deleted_at' => '2018-05-16 11:31:57',
			'numbering' => '03.21',
			'title' => 'Deux baies fixes à double vitrage et leurs stores intégrés dans l’extension arrière droite de la salle de formation – zone froide._x000D_',
			'kamion_id' => '2',
			'parent_id' => '216',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'numbering' => '04',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'numbering' => '04.01',
			'title' => 'Bourreuse verticale (16)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'numbering' => '04.02',
			'title' => 'Hachoir réfrigéré (17)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'numbering' => '04.03',
			'title' => 'Sous-videuse (18)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'numbering' => '04.04',
			'title' => 'Désinsectiseur (22)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'numbering' => '04.05',
			'title' => 'Ensemble de lavage avec réservoir de produit (45)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'numbering' => '04.06',
			'title' => 'Fourneau 4 plaques électriques (26)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'numbering' => '04.07',
			'title' => 'Soubassement réfrigéré (28)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'numbering' => '04.08',
			'title' => 'Four de cuisson à vapeur injectée (29)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'numbering' => '04.09',
			'title' => 'Support ouvert pour four (30)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'numbering' => '04.10',
			'title' => 'Hotte (34)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'numbering' => '04.11',
			'title' => 'Désinsectiseur (36)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'numbering' => '04.12',
			'title' => 'Stérilisateur à couteaux à eau (43)',
			'kamion_id' => '2',
			'parent_id' => '238',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'numbering' => '05',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'numbering' => '05.01',
			'title' => 'Mélangeur 30kg (48)',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'numbering' => '05.02',
			'title' => 'Operculeuse (49)',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'numbering' => '05.03',
			'title' => 'Trancheuse (50)',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'numbering' => '05.04',
			'title' => 'Batteur mélangeur 5 litres (52)',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'numbering' => '05.05',
			'title' => 'Mixer 15 litres (53)',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'numbering' => '05.06',
			'title' => 'Mixer 30 litres (54)',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'numbering' => '05.07',
			'title' => 'Balance 60kg – 2g (55)',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'numbering' => '05.08',
			'title' => 'Balance 3kg – 0,1g (56)',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'numbering' => '05.09',
			'title' => 'LES REVETEMENTS',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'numbering' => '05.10',
			'title' => 'Revêtement de sol',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'numbering' => '05.11',
			'title' => 'Revêtement des parois',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'numbering' => '05.12',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.13',
			'title' => 'Eclairage extérieur',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.14',
			'title' => 'Eclairage de coffre',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.15',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.16',
			'title' => 'Basse tension',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.17',
			'title' => 'Distribution interne',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.18',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.19',
			'title' => 'Alimentation en eau propre',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.20',
			'title' => 'Equipements raccordés en eau propre',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.21',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.22',
			'title' => 'Evacuation des eaux usées',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.23',
			'title' => 'LA CLIMATISATION ET LE CHAUFFAGE',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.24',
			'title' => 'L’unité est équipée d’un système de climatisation réversible',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.25',
			'title' => 'La salle de formation – zone froide est équipée avec un ensemble groupe froid',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.26',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.27',
			'title' => 'Détecteur de fumée',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'numbering' => '05.28',
			'title' => 'Extincteur',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'numbering' => '05.29',
			'title' => 'Bloc autonome d’éclairage de sécurité',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'numbering' => '05.30',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'numbering' => '05.31',
			'title' => 'Système de ventilation',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'numbering' => '05.32',
			'title' => 'LES CONTROLES DE CONFORMITE',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'numbering' => '05.33',
			'title' => 'Installation électrique',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-16 11:31:37',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'numbering' => '05.34',
			'title' => 'Conformité code de la route',
			'kamion_id' => '2',
			'parent_id' => '251',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00',
			'title' => 'CONDITIONS CONTRACTUELLES & ORGANISATION',
			'kamion_id' => '18',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.01',
			'title' => 'DATE DE COMMANDE : JJ/MM/AAAA',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.02',
			'title' => 'DELAI : 15/12/2018',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.03',
			'title' => 'LC : NON',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.04',
			'title' => 'PASSAGE AUX MINES : OUI',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.05',
			'title' => 'CONDITIONS DE REGLEMENT :
30% d\'acompte du montant total de la commande à la confirmation de commande.
30% d\'acompte du montant total de la commande à l’assemblage de la caisse sur le châssis.
40% à la mise à disposition.',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.06',
			'title' => 'INCOTERM : EX WORKS - LADON',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.07',
			'title' => 'Date réunion de conception provisoire : JJ/MM/AAAA',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.08',
			'title' => 'REFERENCE PIGE:',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.09',
			'title' => 'Date fin de pige : JJ/MM/AAAA',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '00.10',
			'title' => 'REFERENCE OFFRE TECHNIQUE :',
			'kamion_id' => '18',
			'parent_id' => '286',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '01',
			'title' => '- AMENAGEMENTS -',
			'kamion_id' => '18',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '01.01',
			'title' => 'Salle d’attente',
			'kamion_id' => '18',
			'parent_id' => '297',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'numbering' => '01.02',
			'title' => 'Secrétariat',
			'kamion_id' => '18',
			'parent_id' => '297',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'numbering' => '01.03',
			'title' => 'Local WC',
			'kamion_id' => '18',
			'parent_id' => '297',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'numbering' => '01.04',
			'title' => 'Cabinet médical',
			'kamion_id' => '18',
			'parent_id' => '297',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'numbering' => '02',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '18',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'numbering' => '02.01',
			'title' => 'Châssis cabine',
			'kamion_id' => '18',
			'parent_id' => '302',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'numbering' => '03',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '18',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'numbering' => '03.01',
			'title' => 'Equipements complémentaires réalisés par toutenkamion dans la cabine de conduite		',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'numbering' => '03.02',
			'title' => 'Structure du faux-châssis',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'numbering' => '03.03',
			'title' => 'Escalier',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'numbering' => '03.04',
			'title' => 'Stabilisation',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'numbering' => '03.05',
			'title' => 'Cales',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.06',
			'title' => 'LA CARROSSERIE',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.07',
			'title' => 'Dimensions',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.08',
			'title' => 'Assemblage des panneaux de la carrosserie',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.09',
			'title' => 'Finition de la carrosserie',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.10',
			'title' => 'Décor extérieur personnalisé',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.11',
			'title' => 'LES PORTES, ACCES TECHNIQUES ET BAIES VITREES',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.12',
			'title' => 'Portes d’accès extérieurs',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.13',
			'title' => 'Accès techniques',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'numbering' => '03.14',
			'title' => 'Baies vitrées',
			'kamion_id' => '18',
			'parent_id' => '304',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'numbering' => '04',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '18',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'numbering' => '04.01',
			'title' => 'EQUIPEMENTS',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'numbering' => '04.02',
			'title' => 'Ensemble ordinateur secrétariat',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'numbering' => '04.03',
			'title' => 'Ensemble ordinateur cabinet médical',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'numbering' => '04.04',
			'title' => 'Imprimante multifonction et son câble USB',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'numbering' => '04.05',
			'title' => 'ECG',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'numbering' => '04.06',
			'title' => 'EFR',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'numbering' => '04.07',
			'title' => 'Instruments médicaux',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'numbering' => '04.08',
			'title' => 'Tensiomètre',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'numbering' => '04.09',
			'title' => 'Distributeur savon liquide',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'numbering' => '04.10',
			'title' => 'Distributeur serviettes papier',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'numbering' => '04.11',
			'title' => 'Cales de roues',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'numbering' => '04.12',
			'title' => 'Cônes',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'numbering' => '04.13',
			'title' => 'Four micro-ondes',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'numbering' => '04.14',
			'title' => 'Châssis porteur',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'numbering' => '04.15',
			'title' => 'Revêtement des parois',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.16',
			'title' => 'Pavillon',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.17',
			'title' => 'LE MOBILIER',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.18',
			'title' => 'Caractéristiques du mobilier',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.19',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.20',
			'title' => 'Alimentation électrique secteur',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.21',
			'title' => 'Eclairage extérieur',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.22',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.23',
			'title' => 'Très Basse Tension (TBT)',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.24',
			'title' => 'Distribution interne',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.25',
			'title' => 'RESEAU INFORMATIQUE & COMMUNICATIONS',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.26',
			'title' => 'Réseau informatique',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.27',
			'title' => 'Communications',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.28',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'numbering' => '04.29',
			'title' => 'LA CLIMATISATION ET LE CHAUFFAGE',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.30',
			'title' => 'L’unité est équipée de convecteurs électriques',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.31',
			'title' => 'L’unité est équipée d’un plancher à isolation renforcée',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.32',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.33',
			'title' => 'Détecteur de fumée',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.34',
			'title' => 'Extincteur',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.35',
			'title' => 'Bloc autonome d’éclairage de sécurité',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.36',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.37',
			'title' => 'Système de ventilation',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.38',
			'title' => 'LES CONTROLES DE CONFORMITE',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.39',
			'title' => 'Installation électrique',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.40',
			'title' => 'Conformité code de la route',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '04.41',
			'title' => 'Réseau informatique',
			'kamion_id' => '18',
			'parent_id' => '319',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '05',
			'title' => '- DIMENSIONS -',
			'kamion_id' => '18',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '05.01',
			'title' => 'LIMITE',
			'kamion_id' => '18',
			'parent_id' => '361',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'numbering' => '06',
			'title' => '- LIMITE -',
			'kamion_id' => '18',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00',
			'title' => 'CONDITIONS CONTRACTUELLES & ORGANISATION',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.01',
			'title' => 'DATE DE COMMANDE : 13/09/2017',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.02',
			'title' => 'DELAI : 14/04/2018',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.03',
			'title' => 'LC : NON',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.04',
			'title' => 'PASSAGE AUX MINES : OUI',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.05',
			'title' => 'CONDITIONS DE REGLEMENT :
xxx',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.06',
			'title' => 'INCOTERM : LIVRAISON EN Belgique SUR SITE CLIENT',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.07',
			'title' => 'Date réunion de conception provisoire : JJ/MM/AAAA',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.08',
			'title' => 'REFERENCE PIGE:',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.09',
			'title' => 'Date fin de pige : JJ/MM/AAAA',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '00.10',
			'title' => 'REFERENCE OFFRE TECHNIQUE : 37249-02-a',
			'kamion_id' => '2',
			'parent_id' => '364',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '01',
			'title' => '- AMENAGEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'numbering' => '01.01',
			'title' => 'Local vestiaires (a)',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'numbering' => '01.02',
			'title' => 'Local WC (a)',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'numbering' => '01.03',
			'title' => 'Salle de formation – zone froide (b) – Equipements fixes',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'numbering' => '01.04',
			'title' => 'Salle de formation – zone froide (b) – Equipements fixes',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'numbering' => '01.05',
			'title' => 'Salle de formation – zone froide (b) – Equipements mobiles',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'numbering' => '01.06',
			'title' => 'Chambre froide positive 1°C – 6°C (20)',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'numbering' => '01.07',
			'title' => 'Salle de formation – zone chaude (c)',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'numbering' => '01.08',
			'title' => 'Salle de formation – zone chaude (c)',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '01.09',
			'title' => 'Local plonge (d)',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '01.10',
			'title' => 'Zone de stockage (e)',
			'kamion_id' => '2',
			'parent_id' => '375',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '02',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '02.01',
			'title' => 'CHÂSSIS SEMI-REMORQUE – FOURNITURE TOUTENKAMION',
			'kamion_id' => '2',
			'parent_id' => '386',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '02.02',
			'title' => 'TRACTEUR – FOURNITURE CLIENT',
			'kamion_id' => '2',
			'parent_id' => '386',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '03',
			'title' => '- CONSTRUCTION -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '03.01',
			'title' => 'LE FAUX CHASSIS ET SES ACCESSOIRES',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '03.02',
			'title' => 'Structure du faux-châssis',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '03.03',
			'title' => 'Escalier',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '03.04',
			'title' => 'Stabilisation',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'numbering' => '03.05',
			'title' => 'Cales',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.06',
			'title' => 'LA CARROSSERIE',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.07',
			'title' => 'Dimensions',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.08',
			'title' => 'Longueur',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.09',
			'title' => 'Largeur ',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.10',
			'title' => 'Hauteur',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.11',
			'title' => 'Assemblage des panneaux de la carrosserie',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.12',
			'title' => 'Finition de la carrosserie',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.13',
			'title' => 'Profilé bas de caisse ',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.14',
			'title' => 'Cloisons et portes entre locaux',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'numbering' => '03.15',
			'title' => 'Extensions 	 ',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '03.16',
			'title' => 'Décor extérieur personnalisé',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '03.17',
			'title' => 'LES PORTES, ACCES TECHNIQUES ET BAIES VITREES',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '03.18',
			'title' => 'Portes d’accès extérieurs',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '03.19',
			'title' => 'Accès techniques',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '03.20',
			'title' => 'Baies vitrées',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '03.21',
			'title' => 'Deux baies fixes à double vitrage et leurs stores intégrés dans l’extension arrière droite de la salle de formation – zone froide._x000D_',
			'kamion_id' => '2',
			'parent_id' => '389',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '04',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '04.01',
			'title' => 'Bourreuse verticale (16)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '04.02',
			'title' => 'Hachoir réfrigéré (17)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '04.03',
			'title' => 'Sous-videuse (18)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'numbering' => '04.04',
			'title' => 'Désinsectiseur (22)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'numbering' => '04.05',
			'title' => 'Ensemble de lavage avec réservoir de produit (45)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'numbering' => '04.06',
			'title' => 'Fourneau 4 plaques électriques (26)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'numbering' => '04.07',
			'title' => 'Soubassement réfrigéré (28)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'numbering' => '04.08',
			'title' => 'Four de cuisson à vapeur injectée (29)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'numbering' => '04.09',
			'title' => 'Support ouvert pour four (30)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'numbering' => '04.10',
			'title' => 'Hotte (34)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'numbering' => '04.11',
			'title' => 'Désinsectiseur (36)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'numbering' => '04.12',
			'title' => 'Stérilisateur à couteaux à eau (43)',
			'kamion_id' => '2',
			'parent_id' => '411',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'numbering' => '05',
			'title' => '- EQUIPEMENTS -',
			'kamion_id' => '2',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'numbering' => '05.01',
			'title' => 'Mélangeur 30kg (48)',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'numbering' => '05.02',
			'title' => 'Operculeuse (49)',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'numbering' => '05.03',
			'title' => 'Trancheuse (50)',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'numbering' => '05.04',
			'title' => 'Batteur mélangeur 5 litres (52)',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'numbering' => '05.05',
			'title' => 'Mixer 15 litres (53)',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'numbering' => '05.06',
			'title' => 'Mixer 30 litres (54)',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'numbering' => '05.07',
			'title' => 'Balance 60kg – 2g (55)',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.08',
			'title' => 'Balance 3kg – 0,1g (56)',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.09',
			'title' => 'LES REVETEMENTS',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.10',
			'title' => 'Revêtement de sol',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.11',
			'title' => 'Revêtement des parois',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.12',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.13',
			'title' => 'Eclairage extérieur',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.14',
			'title' => 'Eclairage de coffre',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.15',
			'title' => 'L’ALIMENTATION ELECTRIQUE',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.16',
			'title' => 'Basse tension',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.17',
			'title' => 'Distribution interne',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.18',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.19',
			'title' => 'Alimentation en eau propre',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'numbering' => '05.20',
			'title' => 'Equipements raccordés en eau propre',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.21',
			'title' => 'ALIMENTATION EN EAU',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.22',
			'title' => 'Evacuation des eaux usées',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.23',
			'title' => 'LA CLIMATISATION ET LE CHAUFFAGE',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.24',
			'title' => 'L’unité est équipée d’un système de climatisation réversible',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.25',
			'title' => 'La salle de formation – zone froide est équipée avec un ensemble groupe froid',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.26',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.27',
			'title' => 'Détecteur de fumée',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.28',
			'title' => 'Extincteur',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.29',
			'title' => 'Bloc autonome d’éclairage de sécurité',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.30',
			'title' => 'LES EQUIPEMENTS DE SECURITE',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.31',
			'title' => 'Système de ventilation',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.32',
			'title' => 'LES CONTROLES DE CONFORMITE',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.33',
			'title' => 'Installation électrique',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
		DB::table('chapters')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'numbering' => '05.34',
			'title' => 'Conformité code de la route',
			'kamion_id' => '2',
			'parent_id' => '424',
		]);
    }
}