
<?php

use Illuminate\Database\Seeder;

class Autonotification_userTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('notification_user')->insert([
			'is_read' => '1',
			'user_id' => '1',
			'notification_id' => '1',
		]);
		DB::table('notification_user')->insert([
			'is_read' => '0',
			'user_id' => '3',
			'notification_id' => '2',
		]);
		DB::table('notification_user')->insert([
			'is_read' => '1',
			'user_id' => '1',
			'notification_id' => '3',
		]);
		DB::table('notification_user')->insert([
			'is_read' => '1',
			'user_id' => '1',
			'notification_id' => '5',
		]);
		DB::table('notification_user')->insert([
			'is_read' => '0',
			'user_id' => '3',
			'notification_id' => '6',
		]);
    }
}