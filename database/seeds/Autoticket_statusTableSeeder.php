
<?php

use Illuminate\Database\Seeder;

class Autoticket_statusTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('ticket_status')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'label' => 'Brouillon',
		]);
		DB::table('ticket_status')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'label' => 'Relecture',
		]);
		DB::table('ticket_status')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'label' => 'Ouvert',
		]);
		DB::table('ticket_status')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Archivé',
		]);
    }
}