
<?php

use Illuminate\Database\Seeder;

class AutopermissionsTableSeeder extends Seeder
{
    public function run()
    {
		DB::table('permissions')->insert([
			'label' => 'Administration - Accès au paneau d\'administration',
			'slug' => 'adminpanel',
		]);
		DB::table('permissions')->insert([
			'label' => 'Sorties Temps - Validation de temps final',
			'slug' => 'secondvalidtime',
		]);
		DB::table('permissions')->insert([
			'label' => 'Sorties Temps - Contrôle de temps intermédiaire',
			'slug' => 'firstvalidtime',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Consulter',
			'slug' => 'readmycda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Créer un débat',
			'slug' => 'createdebatcda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Répondre à un débat',
			'slug' => 'writedebatcda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Lier des documents au débat',
			'slug' => 'adddoccreatedebatcda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Lier des documents au réponse de débat',
			'slug' => 'adddocwritedebatcda',
		]);
		DB::table('permissions')->insert([
			'label' => 'Documents - Ajouter un document',
			'slug' => 'adddoc',
		]);
		DB::table('permissions')->insert([
			'label' => 'Administration - Modifier compagnons',
			'slug' => 'adminaccesuser',
		]);
		DB::table('permissions')->insert([
			'label' => 'Administration - Modifier kamions',
			'slug' => 'adminacceskamion',
		]);
		DB::table('permissions')->insert([
			'label' => 'Administration - Modifier rôles',
			'slug' => 'adminaccesrole',
		]);
		DB::table('permissions')->insert([
			'label' => 'Administration - Modifier applications',
			'slug' => 'adminaccesapplication',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Créer un ticket',
			'slug' => 'createticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Création complète',
			'slug' => 'createfullticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Modifier un ticket',
			'slug' => 'updateticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Modification complète',
			'slug' => 'updatefullticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Envoyer un ticket',
			'slug' => 'sendticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Annuler un ticket',
			'slug' => 'cancelticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Exporter version client',
			'slug' => 'fullexportcda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Exporter pour modification',
			'slug' => 'exportcda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Importer nouveau CDA',
			'slug' => 'newimportcda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Importer pour modification',
			'slug' => 'importcda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Supprimer un CDA',
			'slug' => 'deletecda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Accès mode qualitée',
			'slug' => 'qualitymodecda',
		]);
		DB::table('permissions')->insert([
			'label' => 'CDA - Accès mode validation',
			'slug' => 'validationmodecda',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Notifier des compagnons',
			'slug' => 'notifyticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Ouvrir et clore un complément',
			'slug' => 'closecomplementticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Rendre public et privé un complément',
			'slug' => 'publiccomplementticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Modifier contenu du model après soumition',
			'slug' => 'updatemodelticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Lier des documents',
			'slug' => 'linckdocticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Lier des ticket',
			'slug' => 'linckticketticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Exporter',
			'slug' => 'exportticket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Traiter les tickets',
			'slug' => 'tomaketicket',
		]);
		DB::table('permissions')->insert([
			'label' => 'Ticket - Traiter les tickets par email',
			'slug' => 'tomanageticketbyemail',
		]);
    }
}