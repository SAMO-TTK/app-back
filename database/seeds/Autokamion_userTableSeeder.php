
<?php

use Illuminate\Database\Seeder;

class Autokamion_userTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '1',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '3',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '3',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '3',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '3',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '4',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '4',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '4',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '4',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '5',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '5',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '5',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '5',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '6',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '6',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '6',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '6',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '7',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '7',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '7',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '7',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '8',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '8',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '8',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '8',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '9',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '9',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '9',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '9',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '10',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '10',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '10',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '10',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '11',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '11',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '11',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '11',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '12',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '12',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '12',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '12',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '13',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '13',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '13',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '13',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '14',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '14',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '14',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '14',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '15',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '15',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '15',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '15',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '16',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '16',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '16',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '16',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '17',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '17',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '17',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '17',
			'user_id' => '3',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 11:52:32',
			'updated_at' => '2018-05-15 11:52:32',
			'is_kamion_manager' => '0',
			'is_project_manager' => '1',
			'is_pilot' => '0',
			'kamion_id' => '2',
			'user_id' => '4',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 11:52:32',
			'updated_at' => '2018-05-15 11:52:32',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '2',
			'user_id' => '1',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 11:52:32',
			'updated_at' => '2018-05-15 11:52:32',
			'is_kamion_manager' => '1',
			'is_project_manager' => '0',
			'is_pilot' => '0',
			'kamion_id' => '2',
			'user_id' => '2',
		]);
		DB::table('kamion_user')->insert([
			'created_at' => '2018-05-15 11:52:32',
			'updated_at' => '2018-05-15 11:52:32',
			'is_kamion_manager' => '0',
			'is_project_manager' => '0',
			'is_pilot' => '1',
			'kamion_id' => '2',
			'user_id' => '3',
		]);
    }
}