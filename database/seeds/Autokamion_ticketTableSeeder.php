
<?php

use Illuminate\Database\Seeder;

class Autokamion_ticketTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('kamion_ticket')->insert([
			'created_at' => '2018-06-04 12:21:11',
			'updated_at' => '2018-06-04 12:21:11',
			'kamion_id' => '3',
			'ticket_id' => '22099',
		]);
		DB::table('kamion_ticket')->insert([
			'created_at' => '2018-06-04 12:21:11',
			'updated_at' => '2018-06-04 12:21:11',
			'kamion_id' => '4',
			'ticket_id' => '22099',
		]);
		DB::table('kamion_ticket')->insert([
			'created_at' => '2018-06-04 12:21:11',
			'updated_at' => '2018-06-04 12:21:11',
			'kamion_id' => '2',
			'ticket_id' => '22099',
		]);
		DB::table('kamion_ticket')->insert([
			'created_at' => '2018-06-05 13:45:19',
			'updated_at' => '2018-06-05 13:45:19',
			'kamion_id' => '5',
			'ticket_id' => '22100',
		]);
    }
}