
<?php

use Illuminate\Database\Seeder;

class Autostatus_logtimesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('status_logtimes')->insert([
			'label' => 'En cours',
		]);
		DB::table('status_logtimes')->insert([
			'label' => 'En attente (Métier)',
		]);
		DB::table('status_logtimes')->insert([
			'label' => 'En attente (Qualité) ',
		]);
		DB::table('status_logtimes')->insert([
			'label' => 'Non validé (Métier)',
		]);
		DB::table('status_logtimes')->insert([
			'label' => 'Non validé (Qualité)',
		]);
		DB::table('status_logtimes')->insert([
			'label' => 'Validé',
		]);
    }
}