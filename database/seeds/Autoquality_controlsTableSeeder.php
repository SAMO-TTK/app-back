
<?php

use Illuminate\Database\Seeder;

class Autoquality_controlsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:45:37',
			'deleted_at' => '2018-05-16 09:45:37',
			'element_id' => '1',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:16',
			'updated_at' => '2018-05-16 09:45:37',
			'deleted_at' => '2018-05-16 09:45:37',
			'element_id' => '2',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:45:37',
			'deleted_at' => '2018-05-16 09:45:37',
			'element_id' => '3',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:45:37',
			'deleted_at' => '2018-05-16 09:45:37',
			'element_id' => '4',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '5',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '6',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '7',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '8',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '9',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '10',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '11',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '12',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '13',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '14',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '15',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '16',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '17',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:35',
			'deleted_at' => '2018-05-16 09:46:35',
			'element_id' => '18',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '19',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '20',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '21',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '22',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '23',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '24',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '25',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '26',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:17',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '27',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '28',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '29',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '30',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '31',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '32',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '33',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '34',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '35',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '36',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '37',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '38',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '39',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '40',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '41',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:36',
			'deleted_at' => '2018-05-16 09:46:36',
			'element_id' => '42',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '43',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '44',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '45',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '46',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '47',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '48',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '49',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '50',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '51',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:18',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '52',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '53',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '54',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '55',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '56',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '57',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '58',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:46:37',
			'deleted_at' => '2018-05-16 09:46:37',
			'element_id' => '59',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '60',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '61',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '62',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '63',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '64',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '65',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '66',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '67',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '68',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '69',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '70',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '71',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:47:20',
			'deleted_at' => '2018-05-16 09:47:20',
			'element_id' => '72',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '73',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '74',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '75',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '76',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '77',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:19',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '78',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '79',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '80',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '81',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '82',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '83',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '84',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:45',
			'deleted_at' => '2018-05-16 09:50:45',
			'element_id' => '85',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '86',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '87',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '88',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '89',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '90',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '91',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '92',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '93',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '94',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '95',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '96',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '97',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '98',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '99',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '100',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:20',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '101',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '102',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '103',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '104',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '105',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:50:46',
			'deleted_at' => '2018-05-16 09:50:46',
			'element_id' => '106',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '107',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '108',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '109',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '110',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '111',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '112',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '113',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '114',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '115',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '116',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '117',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '118',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '119',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '120',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '121',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '122',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '123',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '124',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:21',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '125',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '126',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '127',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '128',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:51',
			'deleted_at' => '2018-05-16 09:51:51',
			'element_id' => '129',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '130',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '131',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '132',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '133',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '134',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '135',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '136',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '137',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '138',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '139',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '140',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '141',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '142',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '143',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '144',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '145',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '146',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '147',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '148',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '149',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '150',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:22',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '151',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '152',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:52',
			'deleted_at' => '2018-05-16 09:51:52',
			'element_id' => '153',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '154',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '155',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '156',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '157',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '158',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '159',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '160',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '161',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '162',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '163',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '164',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '165',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:51:53',
			'deleted_at' => '2018-05-16 09:51:53',
			'element_id' => '166',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '167',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '168',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '169',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '170',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '171',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '172',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '173',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '174',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:23',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '175',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '176',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '177',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '178',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '179',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '180',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '181',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '182',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '183',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '184',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '185',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '186',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '187',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '188',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '189',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '190',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '191',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:49',
			'deleted_at' => '2018-05-16 09:52:49',
			'element_id' => '192',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '193',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '194',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '195',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '196',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '197',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '198',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '199',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:24',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '200',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '201',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '202',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '203',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '204',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '205',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '206',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '207',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '208',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '209',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '210',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '211',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '212',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '213',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '214',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '215',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:50',
			'deleted_at' => '2018-05-16 09:52:50',
			'element_id' => '216',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '217',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '218',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '219',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '220',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '221',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:25',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '222',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '223',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '224',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '225',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '226',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '227',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '228',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '229',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '230',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '231',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '232',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '233',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '234',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '235',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '236',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '237',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '238',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-15 13:19:26',
			'updated_at' => '2018-05-16 09:52:51',
			'deleted_at' => '2018-05-16 09:52:51',
			'element_id' => '239',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '240',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '241',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:57',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '242',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '243',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '244',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '245',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '246',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '247',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '248',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '249',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '250',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '251',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '252',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '253',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '254',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '255',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '256',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '257',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '258',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '259',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '260',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '261',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '262',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '263',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:18',
			'deleted_at' => '2018-05-16 09:55:18',
			'element_id' => '264',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '265',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '266',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:58',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '267',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '268',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '269',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '270',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '271',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '272',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '273',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '274',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '275',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '276',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '277',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '278',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '279',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '280',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '281',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '282',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '283',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '284',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '285',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '286',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '287',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:19',
			'deleted_at' => '2018-05-16 09:55:19',
			'element_id' => '288',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '289',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '290',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '291',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:54:59',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '292',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '293',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '294',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '295',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '296',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '297',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '298',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:20',
			'deleted_at' => '2018-05-16 09:55:20',
			'element_id' => '299',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '300',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '301',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '302',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '303',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '304',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '305',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '306',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '307',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '308',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '309',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '310',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:21',
			'deleted_at' => '2018-05-16 09:55:21',
			'element_id' => '311',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '312',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '313',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '314',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:00',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '315',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '316',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '317',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '318',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '319',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '320',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '321',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '322',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '323',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '324',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '325',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '326',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '327',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '328',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '329',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '330',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '331',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '332',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '333',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '334',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:23',
			'deleted_at' => '2018-05-16 09:55:23',
			'element_id' => '335',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '336',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '337',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '338',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:01',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '339',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '340',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '341',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '342',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '343',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '344',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:24',
			'deleted_at' => '2018-05-16 09:55:24',
			'element_id' => '345',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:26',
			'deleted_at' => '2018-05-16 09:55:26',
			'element_id' => '346',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:26',
			'deleted_at' => '2018-05-16 09:55:26',
			'element_id' => '347',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:26',
			'deleted_at' => '2018-05-16 09:55:26',
			'element_id' => '348',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:26',
			'deleted_at' => '2018-05-16 09:55:26',
			'element_id' => '349',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:26',
			'deleted_at' => '2018-05-16 09:55:26',
			'element_id' => '350',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:26',
			'deleted_at' => '2018-05-16 09:55:26',
			'element_id' => '351',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '352',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '353',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '354',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '355',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '356',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '357',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '358',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '359',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '360',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '361',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:02',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '362',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '363',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '364',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '365',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '366',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '367',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '368',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '369',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '370',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '371',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '372',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '373',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '374',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '375',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '376',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '377',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:27',
			'deleted_at' => '2018-05-16 09:55:27',
			'element_id' => '378',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '379',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '380',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '381',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '382',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '383',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '384',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '385',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:03',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '386',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '387',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '388',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '389',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '390',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '391',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '392',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '393',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '394',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '395',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '396',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '397',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '398',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '399',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '400',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '401',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '402',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '403',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:28',
			'deleted_at' => '2018-05-16 09:55:28',
			'element_id' => '404',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:29',
			'deleted_at' => '2018-05-16 09:55:29',
			'element_id' => '405',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '406',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '407',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '408',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '409',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '410',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:04',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '411',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '412',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '413',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '414',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '415',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '416',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '417',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '418',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '419',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '420',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '421',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '422',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '423',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '424',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '425',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '426',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:32',
			'deleted_at' => '2018-05-16 09:55:32',
			'element_id' => '427',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '428',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '429',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '430',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '431',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '432',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '433',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '434',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:05',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '435',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '436',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '437',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '438',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '439',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '440',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '441',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '442',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '443',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '444',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '445',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '446',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '447',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '448',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '449',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '450',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '451',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '452',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:33',
			'deleted_at' => '2018-05-16 09:55:33',
			'element_id' => '453',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '454',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '455',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '456',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '457',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '458',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:06',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '459',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '460',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '461',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '462',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '463',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '464',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '465',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '466',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '467',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '468',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '469',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '470',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '471',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '472',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '473',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '474',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '475',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '476',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '477',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 09:55:07',
			'updated_at' => '2018-05-16 09:55:34',
			'deleted_at' => '2018-05-16 09:55:34',
			'element_id' => '478',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:50',
			'deleted_at' => '2018-05-16 11:31:50',
			'element_id' => '479',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:50',
			'deleted_at' => '2018-05-16 11:31:50',
			'element_id' => '480',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:50',
			'deleted_at' => '2018-05-16 11:31:50',
			'element_id' => '481',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '482',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '483',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '484',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '485',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '486',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '487',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '488',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '489',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '490',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '491',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '492',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '493',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '494',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '495',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '496',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '497',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '498',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '499',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:27',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '500',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '501',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '502',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '503',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '504',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '505',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '506',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:51',
			'deleted_at' => '2018-05-16 11:31:51',
			'element_id' => '507',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '508',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '509',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '510',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '511',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '512',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '513',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '514',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '515',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '516',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '517',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '518',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '519',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '520',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '521',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '522',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '523',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '524',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '525',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:28',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '526',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '527',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '528',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '529',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '530',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '531',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:52',
			'deleted_at' => '2018-05-16 11:31:52',
			'element_id' => '532',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '533',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '534',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '535',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '536',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '537',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '538',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '539',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '540',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '541',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '542',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '543',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:53',
			'deleted_at' => '2018-05-16 11:31:53',
			'element_id' => '544',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:54',
			'deleted_at' => '2018-05-16 11:31:54',
			'element_id' => '545',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:54',
			'deleted_at' => '2018-05-16 11:31:54',
			'element_id' => '546',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:54',
			'deleted_at' => '2018-05-16 11:31:54',
			'element_id' => '547',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:54',
			'deleted_at' => '2018-05-16 11:31:54',
			'element_id' => '548',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:54',
			'deleted_at' => '2018-05-16 11:31:54',
			'element_id' => '549',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:29',
			'updated_at' => '2018-05-16 11:31:54',
			'deleted_at' => '2018-05-16 11:31:54',
			'element_id' => '550',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'element_id' => '551',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'element_id' => '552',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'element_id' => '553',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'element_id' => '554',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'element_id' => '555',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'element_id' => '556',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'element_id' => '557',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:55',
			'deleted_at' => '2018-05-16 11:31:55',
			'element_id' => '558',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '559',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '560',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '561',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '562',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '563',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '564',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '565',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '566',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '567',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '568',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '569',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '570',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '571',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '572',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '573',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:30',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '574',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '575',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '576',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '577',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '578',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '579',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '580',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '581',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '582',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:56',
			'deleted_at' => '2018-05-16 11:31:56',
			'element_id' => '583',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:57',
			'deleted_at' => '2018-05-16 11:31:57',
			'element_id' => '584',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '585',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '586',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '587',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '588',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '589',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '590',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '591',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '592',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '593',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '594',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '595',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:31:59',
			'deleted_at' => '2018-05-16 11:31:59',
			'element_id' => '596',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '597',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:31',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '598',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '599',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '600',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '601',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '602',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '603',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '604',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '605',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '606',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '607',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '608',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '609',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '610',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '611',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '612',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '613',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '614',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '615',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '616',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '617',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '618',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '619',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:00',
			'deleted_at' => '2018-05-16 11:32:00',
			'element_id' => '620',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '621',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:32',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '622',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '623',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '624',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '625',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '626',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '627',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '628',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '629',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '630',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '631',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '632',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '633',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '634',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '635',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '636',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '637',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '638',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '639',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '640',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '641',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '642',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '643',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:01',
			'deleted_at' => '2018-05-16 11:32:01',
			'element_id' => '644',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '645',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:33',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '646',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '647',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '648',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '649',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '650',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '651',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '652',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '653',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '654',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '655',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '656',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '657',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '658',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '659',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '660',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '661',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '662',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '663',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '664',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '665',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '666',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '667',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:05',
			'deleted_at' => '2018-05-16 11:32:05',
			'element_id' => '668',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '669',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:34',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '670',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '671',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '672',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '673',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '674',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '675',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '676',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '677',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '678',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '679',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '680',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '681',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '682',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '683',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '684',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '685',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '686',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '687',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '688',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '689',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '690',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:06',
			'deleted_at' => '2018-05-16 11:32:06',
			'element_id' => '691',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '692',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '693',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:35',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '694',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '695',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '696',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '697',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '698',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '699',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '700',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '701',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '702',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '703',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '704',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '705',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '706',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '707',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '708',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '709',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '710',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '711',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '712',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '713',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:07',
			'deleted_at' => '2018-05-16 11:32:07',
			'element_id' => '714',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:36',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'element_id' => '715',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:37',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'element_id' => '716',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-16 11:31:37',
			'updated_at' => '2018-05-16 11:32:08',
			'deleted_at' => '2018-05-16 11:32:08',
			'element_id' => '717',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '718',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '719',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '720',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '721',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '722',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '723',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '724',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '725',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '726',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '727',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '728',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '729',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '730',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '731',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '732',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '733',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '734',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '735',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '736',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '737',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:02',
			'updated_at' => '2018-05-31 12:12:02',
			'element_id' => '738',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '739',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '740',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '741',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '742',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '743',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '744',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '745',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '746',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '747',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '748',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '749',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '750',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '751',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '752',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '753',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '754',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '755',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '756',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '757',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '758',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '759',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '760',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '761',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '762',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:03',
			'updated_at' => '2018-05-31 12:12:03',
			'element_id' => '763',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '764',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '765',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '766',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '767',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '768',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '769',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '770',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '771',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '772',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '773',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '774',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '775',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '776',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '777',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '778',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '779',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '780',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '781',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '782',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '783',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '784',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '785',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '786',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:04',
			'updated_at' => '2018-05-31 12:12:04',
			'element_id' => '787',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '788',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '789',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '790',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '791',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '792',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '793',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '794',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '795',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '796',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '797',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '798',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '799',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '800',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '801',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '802',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '803',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '804',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '805',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '806',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '807',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '808',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:05',
			'updated_at' => '2018-05-31 12:12:05',
			'element_id' => '809',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '810',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '811',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '812',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '813',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '814',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '815',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '816',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '817',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '818',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '819',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '820',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '821',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '822',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '823',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '824',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '825',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '826',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '827',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '828',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '829',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '830',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '831',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '832',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:06',
			'updated_at' => '2018-05-31 12:12:06',
			'element_id' => '833',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '834',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '835',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '836',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '837',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '838',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '839',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '840',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '841',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '842',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '843',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '844',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '845',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '846',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '847',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '848',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '849',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '850',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '851',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '852',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '853',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '854',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '855',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:07',
			'updated_at' => '2018-05-31 12:12:07',
			'element_id' => '856',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '857',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '858',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '859',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '860',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '861',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '862',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '863',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '864',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '865',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '866',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '867',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '868',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '869',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '870',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '871',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '872',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '873',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '874',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '875',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '876',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '877',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '878',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '879',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '880',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:08',
			'updated_at' => '2018-05-31 12:12:08',
			'element_id' => '881',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '882',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '883',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '884',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '885',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '886',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '887',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '888',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '889',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '890',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '891',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '892',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '893',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '894',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '895',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '896',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '897',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '898',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '899',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '900',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '901',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '902',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:09',
			'updated_at' => '2018-05-31 12:12:09',
			'element_id' => '903',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '904',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '905',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '906',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '907',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '908',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '909',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '910',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '911',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '912',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '913',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '914',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-05-31 12:12:10',
			'updated_at' => '2018-05-31 12:12:10',
			'element_id' => '915',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-05 20:46:58',
			'element_id' => '916',
			'user_id' => '1',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'element_id' => '917',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:28',
			'updated_at' => '2018-06-04 07:38:28',
			'element_id' => '918',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '919',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '920',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '921',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '922',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '923',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '924',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-05 12:29:27',
			'element_id' => '925',
			'user_id' => '1',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-05 12:32:56',
			'code' => 'C',
			'type' => 'inspection',
			'note' => 'dzadza zae zaeza',
			'element_id' => '926',
			'user_id' => '1',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '927',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '928',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '929',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '930',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '931',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '932',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '933',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '934',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '935',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '936',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '937',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '938',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '939',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '940',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '941',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '942',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:29',
			'updated_at' => '2018-06-04 07:38:29',
			'element_id' => '943',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '944',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '945',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '946',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '947',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '948',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '949',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '950',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '951',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '952',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '953',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '954',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '955',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '956',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '957',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '958',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '959',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '960',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '961',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '962',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '963',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '964',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '965',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '966',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '967',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:30',
			'updated_at' => '2018-06-04 07:38:30',
			'element_id' => '968',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '969',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '970',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '971',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '972',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '973',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '974',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '975',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '976',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '977',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '978',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '979',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '980',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '981',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '982',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '983',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '984',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '985',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '986',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '987',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '988',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '989',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '990',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '991',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '992',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:31',
			'updated_at' => '2018-06-04 07:38:31',
			'element_id' => '993',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '994',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '995',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '996',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '997',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '998',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '999',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1000',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1001',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1002',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1003',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1004',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1005',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1006',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1007',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1008',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1009',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1010',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1011',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1012',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1013',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:32',
			'updated_at' => '2018-06-04 07:38:32',
			'element_id' => '1014',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1015',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1016',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1017',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1018',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1019',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1020',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1021',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1022',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1023',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1024',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1025',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1026',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1027',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1028',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1029',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1030',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1031',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1032',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1033',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1034',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1035',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:33',
			'updated_at' => '2018-06-04 07:38:33',
			'element_id' => '1036',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1037',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1038',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1039',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1040',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1041',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1042',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1043',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1044',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1045',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1046',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1047',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1048',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1049',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1050',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1051',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1052',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1053',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1054',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1055',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1056',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1057',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1058',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1059',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1060',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:34',
			'updated_at' => '2018-06-04 07:38:34',
			'element_id' => '1061',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1062',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1063',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1064',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1065',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1066',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1067',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1068',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1069',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1070',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1071',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1072',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1073',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1074',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1075',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1076',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1077',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1078',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1079',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1080',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1081',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1082',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1083',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1084',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1085',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:35',
			'updated_at' => '2018-06-04 07:38:35',
			'element_id' => '1086',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1087',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1088',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1089',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1090',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1091',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1092',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1093',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1094',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1095',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1096',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1097',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1098',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1099',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1100',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1101',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1102',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1103',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1104',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1105',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1106',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1107',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1108',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1109',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1110',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1111',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:36',
			'updated_at' => '2018-06-04 07:38:36',
			'element_id' => '1112',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1113',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1114',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1115',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1116',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1117',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1118',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1119',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1120',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1121',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1122',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1123',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1124',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1125',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1126',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1127',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1128',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1129',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1130',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1131',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1132',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1133',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1134',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1135',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:37',
			'updated_at' => '2018-06-04 07:38:37',
			'element_id' => '1136',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1137',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1138',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1139',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1140',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1141',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1142',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1143',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1144',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1145',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1146',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1147',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1148',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1149',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1150',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1151',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1152',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1153',
		]);
		DB::table('quality_controls')->insert([
			'created_at' => '2018-06-04 07:38:38',
			'updated_at' => '2018-06-04 07:38:38',
			'element_id' => '1154',
		]);
    }
}