
<?php

use Illuminate\Database\Seeder;

class AutonotesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('notes')->insert([
			'created_at' => '2018-06-05 13:51:48',
			'updated_at' => '2018-06-05 13:51:48',
			'title' => 'test de note que pour BRPE',
			'content' => '<p>RERREZREZREZRZE</p><p>RE</p><p>R</p><p>EZ</p><p>RZE</p><p>R</p><p>ZER</p><p>ZE</p><p>RZE </p>',
			'is_public' => '0',
			'kamion_id' => '2',
			'user_id' => '4',
		]);
		DB::table('notes')->insert([
			'created_at' => '2018-06-05 13:53:12',
			'updated_at' => '2018-06-05 13:53:54',
			'title' => 'test d\'une note public',
			'content' => '<p>rzere rez</p><p>ez</p><p>zre</p><p>t</p><p>tr</p><p>`er</p><p>`z e</p>',
			'is_public' => '1',
			'kamion_id' => '2',
			'user_id' => '4',
		]);
		DB::table('notes')->insert([
			'created_at' => '2018-06-05 13:56:46',
			'updated_at' => '2018-06-05 13:56:46',
			'title' => 'reezr',
			'content' => '<p>ezrezrezrezrze</p>',
			'is_public' => '0',
			'kamion_id' => '2',
			'user_id' => '1',
		]);
		DB::table('notes')->insert([
			'created_at' => '2018-06-05 19:30:27',
			'updated_at' => '2018-06-05 19:30:27',
			'title' => 'test open',
			'content' => '<p>dfsdfsdf</p>',
			'is_public' => '0',
			'kamion_id' => '2',
			'user_id' => '3',
		]);
		DB::table('notes')->insert([
			'created_at' => '2018-06-05 19:56:18',
			'updated_at' => '2018-06-05 19:56:18',
			'title' => 'asdasd',
			'content' => '<p>asdasd</p>',
			'is_public' => '1',
			'kamion_id' => '2',
			'user_id' => '1',
		]);
		DB::table('notes')->insert([
			'created_at' => '2018-06-05 19:56:52',
			'updated_at' => '2018-06-05 19:56:52',
			'title' => 'asdasd',
			'content' => '<p>asdasd</p>',
			'is_public' => '1',
			'kamion_id' => '2',
			'user_id' => '1',
		]);
    }
}