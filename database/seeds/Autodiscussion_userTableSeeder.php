
<?php

use Illuminate\Database\Seeder;

class Autodiscussion_userTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('discussion_user')->insert([
			'created_at' => '2018-06-04 08:50:47',
			'updated_at' => '2018-06-04 08:50:47',
			'is_author' => '1',
			'is_concerned' => '0',
			'discussion_id' => '1',
			'user_id' => '1',
		]);
		DB::table('discussion_user')->insert([
			'created_at' => '2018-06-04 08:50:47',
			'updated_at' => '2018-06-04 08:50:47',
			'is_author' => '0',
			'is_concerned' => '1',
			'discussion_id' => '1',
			'user_id' => '3',
		]);
		DB::table('discussion_user')->insert([
			'created_at' => '2018-06-05 12:26:17',
			'updated_at' => '2018-06-05 12:26:17',
			'is_author' => '1',
			'is_concerned' => '0',
			'discussion_id' => '2',
			'user_id' => '1',
		]);
		DB::table('discussion_user')->insert([
			'created_at' => '2018-06-05 12:26:17',
			'updated_at' => '2018-06-05 12:26:17',
			'is_author' => '0',
			'is_concerned' => '1',
			'discussion_id' => '2',
			'user_id' => '3',
		]);
    }
}