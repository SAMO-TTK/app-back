
<?php

use Illuminate\Database\Seeder;

class Autolast_notesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('last_notes')->insert([
			'created_at' => '2018-06-05 13:52:24',
			'updated_at' => '2018-06-05 13:53:14',
			'user_id' => '4',
			'note_id' => '1',
		]);
		DB::table('last_notes')->insert([
			'created_at' => '2018-06-05 13:53:20',
			'updated_at' => '2018-06-05 13:54:18',
			'user_id' => '4',
			'note_id' => '2',
		]);
		DB::table('last_notes')->insert([
			'created_at' => '2018-06-05 13:56:51',
			'updated_at' => '2018-06-05 19:29:31',
			'user_id' => '1',
			'note_id' => '1',
		]);
		DB::table('last_notes')->insert([
			'created_at' => '2018-06-05 19:30:52',
			'updated_at' => '2018-06-05 19:30:52',
			'user_id' => '1',
			'note_id' => '4',
		]);
		DB::table('last_notes')->insert([
			'created_at' => '2018-06-05 19:56:31',
			'updated_at' => '2018-06-05 19:56:31',
			'user_id' => '1',
			'note_id' => '3',
		]);
		DB::table('last_notes')->insert([
			'created_at' => '2018-06-05 19:56:38',
			'updated_at' => '2018-06-05 19:56:38',
			'user_id' => '1',
			'note_id' => '5',
		]);
		DB::table('last_notes')->insert([
			'created_at' => '2018-06-05 19:56:55',
			'updated_at' => '2018-06-05 19:57:23',
			'user_id' => '1',
			'note_id' => '2',
		]);
    }
}