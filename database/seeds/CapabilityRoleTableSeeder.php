<?php

use Illuminate\Database\Seeder;

class CapabilityRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 18; $i++) :
		$ra = rand(1,8);
		$rb = rand(1,3);
			if ($ra != $rb):
		        DB::table('capability_role')->insert([
		            'capability_id' => $ra, 'role_id' => $rb, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
		        ]);
			endif;
		endfor;
    }
}
