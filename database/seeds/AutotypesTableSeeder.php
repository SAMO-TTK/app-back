
<?php

use Illuminate\Database\Seeder;

class AutotypesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('types')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'type label',
		]);
    }
}