
<?php

use Illuminate\Database\Seeder;

class AutonotificationsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('notifications')->insert([
			'created_at' => '2018-05-15 13:20:45',
			'updated_at' => '2018-05-15 13:20:45',
			'title' => 'vous avez ete notifier sur un ticket',
			'icon' => 'tickets',
			'row1' => 'ticket - NNC6/1407',
			'row2' => 'NNC6/1407 - AUDIT  &  INVENTAIRE de DECEMBRE 2016 : ALU, FERRAILLE et JOINT - Rédac : FRGL (Frédéric GLAUME)',
			'row3' => '2018-05-15 03:20:44',
			'type' => 'TICKET',
		]);
		DB::table('notifications')->insert([
			'created_at' => '2018-06-04 08:50:47',
			'updated_at' => '2018-06-04 08:50:47',
			'title' => 'Vous etes convié à participer à un debat',
			'icon' => 'debats',
			'row1' => 'UNITE TRANSFO-VIANDE- (27249)',
			'row3' => '04/06/2018 08:50:47',
		]);
		DB::table('notifications')->insert([
			'created_at' => '2018-06-04 11:47:31',
			'updated_at' => '2018-06-04 11:47:31',
			'title' => 'vous avez ete notifier sur un ticket',
			'icon' => 'tickets',
			'row1' => 'ticket - NNC6/1407',
			'row2' => 'NNC6/1407 - AUDIT  &  INVENTAIRE de DECEMBRE 2016 : ALU, FERRAILLE et JOINT - Rédac : FRGL (Frédéric GLAUME)',
			'row3' => '2018-06-04 01:47:31',
			'type' => 'TICKET',
		]);
		DB::table('notifications')->insert([
			'created_at' => '2018-06-04 13:39:44',
			'updated_at' => '2018-06-04 13:39:44',
			'title' => 'Vous venez d\'être affecté a un ticket',
			'icon' => 'tickets',
			'row1' => 'TTK18/1983',
			'row2' => 'testounet',
			'row3' => '04/06/2018 13:39:44',
			'type' => 'TICKET',
		]);
		DB::table('notifications')->insert([
			'created_at' => '2018-06-04 13:40:56',
			'updated_at' => '2018-06-04 13:40:56',
			'title' => 'vous avez ete notifier sur un ticket',
			'icon' => 'tickets',
			'row1' => 'ticket - TTK18/1983',
			'row2' => 'testounet',
			'row3' => '2018-06-04 03:40:56',
			'type' => 'TICKET',
		]);
		DB::table('notifications')->insert([
			'created_at' => '2018-06-05 12:26:17',
			'updated_at' => '2018-06-05 12:26:17',
			'title' => 'Vous etes convié à participer à un debat',
			'icon' => 'debats',
			'row1' => 'UNITE TRANSFO-VIANDE- (27249)',
			'row3' => '05/06/2018 12:26:17',
		]);
		DB::table('notifications')->insert([
			'created_at' => '2018-06-05 13:46:27',
			'updated_at' => '2018-06-05 13:46:27',
			'title' => 'Vous venez d\'être affecté a un ticket',
			'icon' => 'tickets',
			'row1' => 'TTK18/1984',
			'row2' => 'eezaeazeza',
			'row3' => '05/06/2018 13:46:27',
			'type' => 'TICKET',
		]);
    }
}