
<?php

use Illuminate\Database\Seeder;

class AutoinstructionsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('instructions')->insert([
			'label' => 'test1',
			'wana_id' => '45123',
			'kamion_id' => '2',
		]);
		DB::table('instructions')->insert([
			'label' => 'test2',
			'wana_id' => '45223',
			'kamion_id' => '2',
		]);
		DB::table('instructions')->insert([
			'label' => 'test3',
			'wana_id' => '45323',
			'kamion_id' => '2',
		]);
		DB::table('instructions')->insert([
			'label' => 'test4',
			'wana_id' => '45423',
			'kamion_id' => '2',
		]);
		DB::table('instructions')->insert([
			'label' => 'test5',
			'wana_id' => '45523',
			'kamion_id' => '2',
		]);
		DB::table('instructions')->insert([
			'label' => 'test6',
			'wana_id' => '45623',
			'kamion_id' => '2',
		]);
		DB::table('instructions')->insert([
			'label' => 'test7',
			'wana_id' => '45723',
			'kamion_id' => '2',
		]);
    }
}