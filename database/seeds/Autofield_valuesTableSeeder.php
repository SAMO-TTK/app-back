
<?php

use Illuminate\Database\Seeder;

class Autofield_valuesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('field_values')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'value' => 'AVANTAGES',
			'field_id' => '7',
		]);
		DB::table('field_values')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'value' => 'INCONVENIENTS',
			'field_id' => '7',
		]);
		DB::table('field_values')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'value' => 'Date controle',
			'field_id' => '36',
		]);
		DB::table('field_values')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'value' => 'Nom chassis',
			'field_id' => '36',
		]);
		DB::table('field_values')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'value' => 'Marque et Type châssis',
			'field_id' => '36',
		]);
		DB::table('field_values')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'value' => 'Effectué par',
			'field_id' => '36',
		]);
		DB::table('field_values')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'value' => 'Constats',
			'field_id' => '36',
		]);
    }
}