<?php

use Illuminate\Database\Seeder;

class LinkedProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		for ($i=1; $i < 17; $i++) :
			$rand = rand(1,3);
			for ($rand; $rand > 0; $rand--) {
				$ra = rand(1,16);
	            if ($ra != $i):
	                DB::table('linked_projects')->insert([
	                    'master_id' => $ra, 'linked_id' => $i, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
	                ]);
	            endif;
			}
        endfor;
    }
}
