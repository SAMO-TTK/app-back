<?php

use Illuminate\Database\Seeder;
use App\Api\v1\Models\Stage;

class StagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		/*
		2:lancement
		3:fabrication
		4:reception
		*/
		$stage = new Stage;
		$stage->label = "2:lancement";
		$stage->wana_id = "sd6fg58hd35hg";
		$stage->save();
		$stage = new Stage;
		$stage->label = "3:fabrication";
		$stage->wana_id = "sd56fg8h1s6g";
		$stage->save();
		$stage = new Stage;
		$stage->label = "4:reception";
		$stage->wana_id = "6584dfth5d6gh6";
		$stage->save();
    }
}
