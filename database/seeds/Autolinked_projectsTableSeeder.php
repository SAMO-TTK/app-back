
<?php

use Illuminate\Database\Seeder;

class Autolinked_projectsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '7',
			'linked_id' => '1',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '13',
			'linked_id' => '1',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '9',
			'linked_id' => '2',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '14',
			'linked_id' => '2',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '6',
			'linked_id' => '2',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '6',
			'linked_id' => '3',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '8',
			'linked_id' => '3',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '13',
			'linked_id' => '3',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '9',
			'linked_id' => '4',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '13',
			'linked_id' => '4',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '8',
			'linked_id' => '5',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '7',
			'linked_id' => '5',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '10',
			'linked_id' => '6',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '4',
			'linked_id' => '7',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '2',
			'linked_id' => '7',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '13',
			'linked_id' => '8',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '15',
			'linked_id' => '8',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '14',
			'linked_id' => '9',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '4',
			'linked_id' => '10',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '5',
			'linked_id' => '10',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '6',
			'linked_id' => '10',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '2',
			'linked_id' => '11',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '2',
			'linked_id' => '11',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '15',
			'linked_id' => '12',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '2',
			'linked_id' => '13',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '5',
			'linked_id' => '13',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '16',
			'linked_id' => '14',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '7',
			'linked_id' => '14',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '6',
			'linked_id' => '15',
		]);
		DB::table('linked_projects')->insert([
			'created_at' => '2018-05-15 10:19:43',
			'updated_at' => '2018-05-15 10:19:43',
			'master_id' => '10',
			'linked_id' => '16',
		]);
    }
}