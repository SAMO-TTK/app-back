
<?php

use Illuminate\Database\Seeder;

class Autonotification_ticketsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('notification_tickets')->insert([
			'created_at' => '2018-05-15 13:20:45',
			'updated_at' => '2018-05-15 13:20:45',
			'ticket_id' => '3',
			'notification_id' => '1',
		]);
		DB::table('notification_tickets')->insert([
			'created_at' => '2018-06-04 11:47:31',
			'updated_at' => '2018-06-04 11:47:31',
			'ticket_id' => '3',
			'notification_id' => '3',
		]);
		DB::table('notification_tickets')->insert([
			'created_at' => '2018-06-04 13:40:56',
			'updated_at' => '2018-06-04 13:40:56',
			'ticket_id' => '22099',
			'notification_id' => '5',
		]);
    }
}