<?php

use Illuminate\Database\Seeder;

class ResourceUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 11; $i++) :
		$ra = rand(1,8);
		$rb = rand(1,16);
			if ($ra != $rb):
		        DB::table('resource_user')->insert([
		            'resource_id' => $ra, 'user_id' => $rb, 'is_manager' => $ra % 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
		        ]);
			endif;
		endfor;
    }
}
