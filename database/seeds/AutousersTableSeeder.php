
<?php

use Illuminate\Database\Seeder;

class AutousersTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('users')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-06-06 06:43:12',
			'last_connection' => '2018-06-06 06:43:12',
			'first_name' => 'Gael',
			'last_name' => 'Mallet',
			'username' => 'admin',
			'password' => '$2y$10$V6Cs4.uGDX1no6v.z5fqDeQ2bMBSHexZNhOmH6iGtSbifUUAa/OGG',
			'wana_id' => '123456789987654321',
			'is_active' => '1',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-06-04 14:52:41',
			'last_connection' => '2018-06-04 14:52:41',
			'first_name' => 'Frédéric',
			'last_name' => 'Glaume',
			'username' => 'frgl',
			'password' => '$2y$10$IeiojLXaQC6/jardQjKSYecbAqQuRmwqWgruEDMPjqb1NRzGWBND.',
			'wana_id' => 'rtgswergse4321',
			'is_active' => '1',
			'manager_id' => '1',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-06-05 20:20:23',
			'last_connection' => '2018-06-05 20:20:23',
			'first_name' => 'Franck',
			'last_name' => 'Neveu',
			'username' => 'frne',
			'password' => '$2y$10$uPIWoSHRNNqqVcE655ORDunUD29.85QwRu9a8S33Nsr4J55Yqyu/e',
			'wana_id' => '12345d6gh415sdfg1',
			'is_active' => '1',
			'manager_id' => '1',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-06-05 13:59:09',
			'last_connection' => '2018-06-05 13:59:09',
			'first_name' => 'Bruno',
			'last_name' => 'Pebelier',
			'username' => 'brpe',
			'password' => '$2y$10$pBWRTbLUvX35hsCVvSRiTeKbzuz..V/ZpbHmRmdceQC/yYdiAVLiS',
			'wana_id' => '12345d6gh415sdfg1',
			'is_active' => '1',
			'manager_id' => '1',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'first_name' => 'TTK',
			'last_name' => 'APP',
			'username' => 'ttk_app',
			'password' => '$2y$10$deBql8hDrcP/2ZI0QapUTOnlvBEs8uUNnGzTRHzi8WMlxsA9ftKUa',
			'wana_id' => '12345d6gh415sdfg1',
			'badge_id' => '7c82hde',
			'is_active' => '1',
			'manager_id' => '1',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'LIONEL',
			'last_name' => 'CHATAIN',
			'username' => 'L.CHAT',
			'password' => '8B0O1I2Z',
			'wana_id' => '1013',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'STEPHANE',
			'last_name' => 'GEZU',
			'username' => 'S.GEZU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1014',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'SYLVIE',
			'last_name' => 'HARTMANNE',
			'username' => 'S.HART',
			'password' => '8B0O1I2Z',
			'wana_id' => '1015',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FRANCK',
			'last_name' => 'DEGOUVE',
			'username' => 'F.DEGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1012',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Jean-André',
			'last_name' => 'VALENTINI',
			'username' => 'JA.VALE',
			'password' => '8B0O1I2Z',
			'wana_id' => '598',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'DAVID',
			'last_name' => 'TROLET',
			'username' => 'D.TROL',
			'password' => '8B0O1I2Z',
			'wana_id' => '586',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Donald',
			'last_name' => 'HEARN',
			'username' => 'DOHE',
			'password' => '8B0O1I2Z',
			'wana_id' => '391',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Catherine',
			'last_name' => 'BONNAIRE',
			'username' => 'CABO',
			'password' => '8B0O1I2Z',
			'wana_id' => '212',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FABIEN',
			'last_name' => 'PESTY',
			'username' => 'F.PEST',
			'password' => '8B0O1I2Z',
			'wana_id' => '1009',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'SEBASTIEN',
			'last_name' => 'DURAND',
			'username' => 'S.DURA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1011',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'CHRISTOPHE',
			'last_name' => 'VAJOU',
			'username' => 'C.VAJO',
			'password' => '8B0O1I2Z',
			'wana_id' => '595',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Maurice',
			'last_name' => 'THIERRY',
			'username' => 'M.THIE',
			'password' => '8B0O1I2Z',
			'wana_id' => '560',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Thierry',
			'last_name' => 'BOUDEAU',
			'username' => 'THBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '221',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'CHRISTOPHE',
			'last_name' => 'VRIGNAUD',
			'username' => 'C.VRIG',
			'password' => '8B0O1I2Z',
			'wana_id' => '605',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'DRICID',
			'last_name' => 'BOUDINOT',
			'username' => 'D.BOUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '225',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'PETER',
			'last_name' => 'BÖRGEL',
			'username' => 'P.BÖRG',
			'password' => '8B0O1I2Z',
			'wana_id' => '994',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'OLIVIER',
			'last_name' => 'BOUILLOUX',
			'username' => 'O.BOUI',
			'password' => '8B0O1I2Z',
			'wana_id' => '227',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JEAN MICHEL',
			'last_name' => 'BOULET',
			'username' => 'J.BOUL',
			'password' => '8B0O1I2Z',
			'wana_id' => '229',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Michel',
			'last_name' => 'BOUQUET',
			'username' => 'MIBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '240',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'PATRICE',
			'last_name' => 'BLANDIN',
			'username' => 'P.BLAN',
			'password' => '8B0O1I2Z',
			'wana_id' => '210',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'OLIVIER',
			'last_name' => 'BOUQUET',
			'username' => 'O.BOUQ',
			'password' => '8B0O1I2Z',
			'wana_id' => '241',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'YVES',
			'last_name' => 'BRAGUE',
			'username' => 'Y.BRAG',
			'password' => '8B0O1I2Z',
			'wana_id' => '250',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'STEPHANE',
			'last_name' => 'BARBONNAIS',
			'username' => 'S.BARB',
			'password' => '8B0O1I2Z',
			'wana_id' => '195',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'THIERRY',
			'last_name' => 'BRAGUE',
			'username' => 'T.BRAG',
			'password' => '8B0O1I2Z',
			'wana_id' => '245',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FRANCOIS',
			'last_name' => 'BRETON',
			'username' => 'F.BRET',
			'password' => '8B0O1I2Z',
			'wana_id' => '255',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FREDERIC',
			'last_name' => 'BUCHERON',
			'username' => 'F.BUCH',
			'password' => '8B0O1I2Z',
			'wana_id' => '257',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'YOANN',
			'last_name' => 'BURONFOSSE',
			'username' => 'Y.BURO',
			'password' => '8B0O1I2Z',
			'wana_id' => '258',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Yves',
			'last_name' => 'CARIOU',
			'username' => 'YVCA',
			'password' => '8B0O1I2Z',
			'wana_id' => '259',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JOAQUIM',
			'last_name' => 'CARRICO',
			'username' => 'J.CARR',
			'password' => '8B0O1I2Z',
			'wana_id' => '260',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'SEVERINE',
			'last_name' => 'CAURE',
			'username' => 'S.CAUR',
			'password' => '8B0O1I2Z',
			'wana_id' => '262',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JEAN LUC',
			'last_name' => 'CHAGOURIN',
			'username' => 'J.CHAG',
			'password' => '8B0O1I2Z',
			'wana_id' => '274',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'NORBERT',
			'last_name' => 'CHAILLY',
			'username' => 'N.CHAI',
			'password' => '8B0O1I2Z',
			'wana_id' => '382',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FRANCK',
			'last_name' => 'CHAPLOTEAU',
			'username' => 'F.CHAP',
			'password' => '8B0O1I2Z',
			'wana_id' => '275',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Joël',
			'last_name' => 'BONGIBAULT',
			'username' => 'J.BONG',
			'password' => '8B0O1I2Z',
			'wana_id' => '214',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'CORINNE',
			'last_name' => 'CHAUSSY',
			'username' => 'C.CHAU',
			'password' => '8B0O1I2Z',
			'wana_id' => '284',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Vincent',
			'last_name' => 'CHARPENTIER',
			'username' => 'VICH',
			'password' => '8B0O1I2Z',
			'wana_id' => '280',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'SYLVAIN',
			'last_name' => 'CHAVET',
			'username' => 'S.CHAV',
			'password' => '8B0O1I2Z',
			'wana_id' => '285',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'DOMINIQUE',
			'last_name' => 'CHENEAU',
			'username' => 'D.CHEN',
			'password' => '8B0O1I2Z',
			'wana_id' => '289',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Eric',
			'last_name' => 'ASSELIN',
			'username' => 'ERAS',
			'password' => '8B0O1I2Z',
			'wana_id' => '169',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'EMMANUELLE',
			'last_name' => 'CHAVET',
			'username' => 'E.CHAV',
			'password' => '8B0O1I2Z',
			'wana_id' => '286',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'PATRICK',
			'last_name' => 'CHESTIER',
			'username' => 'P.CHES',
			'password' => '8B0O1I2Z',
			'wana_id' => '290',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Dominique',
			'last_name' => 'CLAIR',
			'username' => 'D.CLAI',
			'password' => '8B0O1I2Z',
			'wana_id' => '300',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'HUGUES',
			'last_name' => 'COLLE',
			'username' => 'H.COLL',
			'password' => '8B0O1I2Z',
			'wana_id' => '301',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'ROBERT',
			'last_name' => 'ARLICOT',
			'username' => 'R.ARLI',
			'password' => '8B0O1I2Z',
			'wana_id' => '160',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'STEPHANE',
			'last_name' => 'CUZOR',
			'username' => 'S.CUZO',
			'password' => '8B0O1I2Z',
			'wana_id' => '305',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'CAROLE',
			'last_name' => 'COUTURIER',
			'username' => 'C.COUT',
			'password' => '8B0O1I2Z',
			'wana_id' => '303',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JOEL',
			'last_name' => 'COME',
			'username' => 'J.COME',
			'password' => '8B0O1I2Z',
			'wana_id' => '302',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'ROBERT',
			'last_name' => 'CZAPLE',
			'username' => 'R.CZAP',
			'password' => '8B0O1I2Z',
			'wana_id' => '308',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Carole',
			'last_name' => 'CZAPLEJEWICZ',
			'username' => 'CACZ',
			'password' => '8B0O1I2Z',
			'wana_id' => '256',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Cyrille',
			'last_name' => 'DALAIGRE',
			'username' => 'CYDA',
			'password' => '8B0O1I2Z',
			'wana_id' => '311',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'MARIE FRANCE',
			'last_name' => 'TERRE',
			'username' => 'M.TER',
			'password' => '8B0O1I2Z',
			'wana_id' => '318',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'MICHEL',
			'last_name' => 'DAREAU',
			'username' => 'M.DARE',
			'password' => '8B0O1I2Z',
			'wana_id' => '310',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'David',
			'last_name' => 'DELAVEAU',
			'username' => 'DADE',
			'password' => '8B0O1I2Z',
			'wana_id' => '370',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'RICHARD',
			'last_name' => 'DELAVEAU',
			'username' => 'R.DELA',
			'password' => '8B0O1I2Z',
			'wana_id' => '315',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Jean-Loup',
			'last_name' => 'DEGOUVE',
			'username' => 'JLDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '312',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'STEPHANE',
			'last_name' => 'DEVILLIERS',
			'username' => 'S.DEVI',
			'password' => '8B0O1I2Z',
			'wana_id' => '316',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JEAN CHARLES',
			'last_name' => 'BISSONNET',
			'username' => 'J.BISS',
			'password' => '8B0O1I2Z',
			'wana_id' => '998',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Patrick',
			'last_name' => 'DUMONT',
			'username' => 'PADU',
			'password' => '8B0O1I2Z',
			'wana_id' => '330',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'NATHALIE',
			'last_name' => 'DUMEZ',
			'username' => 'N.DUME',
			'password' => '8B0O1I2Z',
			'wana_id' => '327',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'MARIE CHRISTINE',
			'last_name' => 'DUFAY',
			'username' => 'M.DUFA',
			'password' => '8B0O1I2Z',
			'wana_id' => '323',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'ERIC',
			'last_name' => 'DUBOSQ',
			'username' => 'E.DUBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '320',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'MARIE JEANNE',
			'last_name' => 'THILLOU',
			'username' => 'M.THIL',
			'password' => '8B0O1I2Z',
			'wana_id' => '562',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JEAN PIERRE',
			'last_name' => 'DUFOUR',
			'username' => 'J.DUFO',
			'password' => '8B0O1I2Z',
			'wana_id' => '325',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FABRICE',
			'last_name' => 'DOIZON',
			'username' => 'F.DOIZ',
			'password' => '8B0O1I2Z',
			'wana_id' => '321',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'DANIEL',
			'last_name' => 'DUPUIS',
			'username' => 'D.DUPU',
			'password' => '8B0O1I2Z',
			'wana_id' => '332',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'DANIEL',
			'last_name' => 'DURAND',
			'username' => 'D.DURA',
			'password' => '8B0O1I2Z',
			'wana_id' => '340',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JULIEN',
			'last_name' => 'TRIMOUILLE',
			'username' => 'J.TRIM',
			'password' => '8B0O1I2Z',
			'wana_id' => '990',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Francis',
			'last_name' => 'DURAND',
			'username' => 'FRDU',
			'password' => '8B0O1I2Z',
			'wana_id' => '350',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JEAN CLAUDE',
			'last_name' => 'PANOSSIAN',
			'username' => 'J.PANO',
			'password' => '8B0O1I2Z',
			'wana_id' => '500',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'PHILIPPE',
			'last_name' => 'THOMAS',
			'username' => 'P.THOM',
			'password' => '8B0O1I2Z',
			'wana_id' => '564',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FREDERIC',
			'last_name' => 'DURAND',
			'username' => 'F.DURA',
			'password' => '8B0O1I2Z',
			'wana_id' => '351',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Jean-Claude',
			'last_name' => 'DURAND',
			'username' => 'JC.DURA',
			'password' => '8B0O1I2Z',
			'wana_id' => '355',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'RENEE',
			'last_name' => 'DURAND',
			'username' => 'R.DURA',
			'password' => '8B0O1I2Z',
			'wana_id' => '360',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'HERVE',
			'last_name' => 'DURAND',
			'username' => 'H.DURA',
			'password' => '8B0O1I2Z',
			'wana_id' => '356',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'LAURENT',
			'last_name' => 'BORDES',
			'username' => 'L.BORD',
			'password' => '8B0O1I2Z',
			'wana_id' => '215',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FRANCK',
			'last_name' => 'ERNULT',
			'username' => 'F.ERNU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1001',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JEAN NOEL',
			'last_name' => 'VALENTINI',
			'username' => 'J.VALE',
			'password' => '8B0O1I2Z',
			'wana_id' => '986',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'NICOLE',
			'last_name' => 'DURAND',
			'username' => 'N.DURA',
			'password' => '8B0O1I2Z',
			'wana_id' => '365',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'HAKIM',
			'last_name' => 'EL HAKIMI',
			'username' => 'H.EL H',
			'password' => '8B0O1I2Z',
			'wana_id' => '357',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FREDERIC',
			'last_name' => 'FAUQUETTE',
			'username' => 'F.FAUQ',
			'password' => '8B0O1I2Z',
			'wana_id' => '1007',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Daniel',
			'last_name' => 'BOUDEAU',
			'username' => 'D.BOUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '220',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'MICHEL',
			'last_name' => 'FOURNIER',
			'username' => 'M.FOUR',
			'password' => '8B0O1I2Z',
			'wana_id' => '362',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'ALAIN',
			'last_name' => 'BENNET',
			'username' => 'A.BENN',
			'password' => '8B0O1I2Z',
			'wana_id' => '199',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'PASCAL',
			'last_name' => 'GALLIEN',
			'username' => 'P.GALL',
			'password' => '8B0O1I2Z',
			'wana_id' => '376',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'ALAIN',
			'last_name' => 'BILLARD',
			'username' => 'A.BILL',
			'password' => '8B0O1I2Z',
			'wana_id' => '205',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JACKY',
			'last_name' => 'BESOZZI',
			'username' => 'J.BESO',
			'password' => '8B0O1I2Z',
			'wana_id' => '203',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'DANIEL',
			'last_name' => 'FERLAT',
			'username' => 'D.FERL',
			'password' => '8B0O1I2Z',
			'wana_id' => '371',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Jacques',
			'last_name' => 'FOUCHER',
			'username' => 'J.FOUC',
			'password' => '8B0O1I2Z',
			'wana_id' => '361',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'CHRISTOPHE',
			'last_name' => 'GAMET',
			'username' => 'C.GAME',
			'password' => '8B0O1I2Z',
			'wana_id' => '372',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FRANCOIS',
			'last_name' => 'GARCIA',
			'username' => 'F.GARC',
			'password' => '8B0O1I2Z',
			'wana_id' => '364',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'PHILIPPE',
			'last_name' => 'GAURAT',
			'username' => 'P.GAUR',
			'password' => '8B0O1I2Z',
			'wana_id' => '387',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Philippe',
			'last_name' => 'GARNIER',
			'username' => 'P.GARN',
			'password' => '8B0O1I2Z',
			'wana_id' => '374',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'CHRISTELLE',
			'last_name' => 'GAUTHIER',
			'username' => 'C.GAUT',
			'password' => '8B0O1I2Z',
			'wana_id' => '381',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Arnold',
			'last_name' => 'GAUTHIER',
			'username' => 'A.GAUT',
			'password' => '8B0O1I2Z',
			'wana_id' => '375',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'PHILIPPE',
			'last_name' => 'BORDES',
			'username' => 'P.BORD',
			'password' => '8B0O1I2Z',
			'wana_id' => '216',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Claudie',
			'last_name' => 'GERVAIS',
			'username' => 'CLGE',
			'password' => '8B0O1I2Z',
			'wana_id' => '377',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'ROGER',
			'last_name' => 'ASSELIN',
			'username' => 'R.ASSE',
			'password' => '8B0O1I2Z',
			'wana_id' => '170',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'AURORE',
			'last_name' => 'GERVAIS',
			'username' => 'A.GERV',
			'password' => '8B0O1I2Z',
			'wana_id' => '1003',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'WILFRID',
			'last_name' => 'GILLES',
			'username' => 'W.GILL',
			'password' => '8B0O1I2Z',
			'wana_id' => '383',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Jacqueline',
			'last_name' => 'GIRERD',
			'username' => 'JAGI',
			'password' => '8B0O1I2Z',
			'wana_id' => '366',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'DIDIER',
			'last_name' => 'ANDOS',
			'username' => 'D.ANDO',
			'password' => '8B0O1I2Z',
			'wana_id' => '151',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Sylvie',
			'last_name' => 'HERMAND',
			'username' => 'S.HERMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '152',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'MARC',
			'last_name' => 'GIRERD',
			'username' => 'M.GIRE',
			'password' => '8B0O1I2Z',
			'wana_id' => '367',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'SYLVIE',
			'last_name' => 'DURAND',
			'username' => 'E.GRAA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1010',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'NICOLAS',
			'last_name' => 'GORGUET',
			'username' => 'N.GORG',
			'password' => '8B0O1I2Z',
			'wana_id' => '373',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'David',
			'last_name' => 'HAMEAU',
			'username' => 'D.HAME',
			'password' => '8B0O1I2Z',
			'wana_id' => '386',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Fabrice',
			'last_name' => 'GRANGER',
			'username' => 'F.GRAN',
			'password' => '8B0O1I2Z',
			'wana_id' => '384',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Vincent',
			'last_name' => 'HERMAND',
			'username' => 'VIHE',
			'password' => '8B0O1I2Z',
			'wana_id' => '395',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JACQUES',
			'last_name' => 'GROS',
			'username' => 'J.GROS',
			'password' => '8B0O1I2Z',
			'wana_id' => '379',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Francis',
			'last_name' => 'ANCIAUX',
			'username' => 'F.ANCI',
			'password' => '8B0O1I2Z',
			'wana_id' => '150',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'SEBASTIEN',
			'last_name' => 'IMBAULT',
			'username' => 'S.IMBA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1006',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FLORENCE',
			'last_name' => 'JOBET',
			'username' => 'F.JOBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1004',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'OLIVIER',
			'last_name' => 'LACROIX',
			'username' => 'O.LACR',
			'password' => '8B0O1I2Z',
			'wana_id' => '407',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FRANCK',
			'last_name' => 'LE BONTE',
			'username' => 'F.LE B',
			'password' => '8B0O1I2Z',
			'wana_id' => '415',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'BRUNO',
			'last_name' => 'HALLOUIN',
			'username' => 'B.HALL',
			'password' => '8B0O1I2Z',
			'wana_id' => '385',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'STAGIAIRE VACAN',
			'last_name' => 'ASSELIN',
			'username' => 'S.ASSE',
			'password' => '8B0O1I2Z',
			'wana_id' => '988',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'VINCENT',
			'last_name' => 'JACQMIN',
			'username' => 'V.JACQ',
			'password' => '8B0O1I2Z',
			'wana_id' => '1008',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Alain',
			'last_name' => 'HAMEAU',
			'username' => 'ALHA',
			'password' => '8B0O1I2Z',
			'wana_id' => '390',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Jean-Claude',
			'last_name' => 'JOBET',
			'username' => 'JCJO',
			'password' => '8B0O1I2Z',
			'wana_id' => '400',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'DANIEL',
			'last_name' => 'GOUDESENNE',
			'username' => 'D.GOUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '378',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'LAURENT',
			'last_name' => 'JOBET',
			'username' => 'L.JOBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '401',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'ALAIN',
			'last_name' => 'BEAULIER',
			'username' => 'A.BEAU',
			'password' => '8B0O1I2Z',
			'wana_id' => '196',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Pascal',
			'last_name' => 'LEBRUN',
			'username' => 'P.LEBR',
			'password' => '8B0O1I2Z',
			'wana_id' => '420',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Jean-Pierre',
			'last_name' => 'JOUDIOU',
			'username' => 'J.JOUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '406',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'PASCAL',
			'last_name' => 'JOBERT',
			'username' => 'P.JOBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '399',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Patrick',
			'last_name' => 'LEBRUN',
			'username' => 'PALE',
			'password' => '8B0O1I2Z',
			'wana_id' => '430',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FRANCOIS',
			'last_name' => 'LEROY',
			'username' => 'FRLE',
			'password' => '8B0O1I2Z',
			'wana_id' => '445',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'LAURENT',
			'last_name' => 'JULIEN',
			'username' => 'L.JULI',
			'password' => '8B0O1I2Z',
			'wana_id' => '987',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'CENTRE USINAGE',
			'last_name' => 'NUM_ANDOS',
			'username' => 'C.ANDO',
			'password' => '8B0O1I2Z',
			'wana_id' => '153',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Joël',
			'last_name' => 'BAILLARD',
			'username' => 'JOBA',
			'password' => '8B0O1I2Z',
			'wana_id' => '190',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'NATHALIE',
			'last_name' => 'LEROY',
			'username' => 'N.LERO',
			'password' => '8B0O1I2Z',
			'wana_id' => '993',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FABIEN',
			'last_name' => 'LOYEN',
			'username' => 'F.LOYE',
			'password' => '8B0O1I2Z',
			'wana_id' => '992',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'Michel',
			'last_name' => 'LEBERT',
			'username' => 'MILE',
			'password' => '8B0O1I2Z',
			'wana_id' => '410',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'JEAN FRANÇOIS',
			'last_name' => 'LEROY',
			'username' => 'J.LERO',
			'password' => '8B0O1I2Z',
			'wana_id' => '446',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'GWENDOLINE',
			'last_name' => 'MAGGIAR',
			'username' => 'G.MAGG',
			'password' => '8B0O1I2Z',
			'wana_id' => '443',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'FRANCK',
			'last_name' => 'MAISON',
			'username' => 'F.MAIS',
			'password' => '8B0O1I2Z',
			'wana_id' => '997',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:35',
			'updated_at' => '2018-05-15 13:03:35',
			'first_name' => 'CHRISTOPHE',
			'last_name' => 'MARGEZ',
			'username' => 'C.MARG',
			'password' => '8B0O1I2Z',
			'wana_id' => '444',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'COMPAGNONS',
			'last_name' => 'MECAMION',
			'username' => 'C.MECA',
			'password' => '8B0O1I2Z',
			'wana_id' => '671',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEAN',
			'last_name' => 'MELLOT',
			'username' => 'J.MELL',
			'password' => '8B0O1I2Z',
			'wana_id' => '447',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PATRICK',
			'last_name' => 'MENIN',
			'username' => 'P.MENI',
			'password' => '8B0O1I2Z',
			'wana_id' => '449',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'last_name' => 'VENDUVENT',
			'password' => '8B0O1I2Z',
			'wana_id' => '1',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'SEBASTIEN',
			'last_name' => 'MERCIER',
			'username' => 'S.MERC',
			'password' => '8B0O1I2Z',
			'wana_id' => '452',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'FRANCK',
			'last_name' => 'MONTER',
			'username' => 'F.MONT',
			'password' => '8B0O1I2Z',
			'wana_id' => '448',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JACQUES',
			'last_name' => 'NAUD',
			'username' => 'J.NAUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '450',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Régis',
			'last_name' => 'NIGON',
			'username' => 'R.NIGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '460',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Pascal',
			'last_name' => 'NAUDON',
			'username' => 'P.NAUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '455',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JACQUELINE',
			'last_name' => 'NIGON',
			'username' => 'J.NIGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '465',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CENTRE USIN.',
			'last_name' => 'NUM_BERTHIER',
			'username' => 'CENU',
			'password' => '8B0O1I2Z',
			'wana_id' => '201',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ANGELIQUE',
			'last_name' => 'GERMAIN',
			'username' => 'A.GERM',
			'password' => '8B0O1I2Z',
			'wana_id' => '388',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Sylvette',
			'last_name' => 'BRAGUE',
			'username' => 'SYBR',
			'password' => '8B0O1I2Z',
			'wana_id' => '408',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CENTRE USIN.',
			'last_name' => 'NUM_DALAIGRE',
			'username' => 'C.NUMD',
			'password' => '8B0O1I2Z',
			'wana_id' => '309',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Claude',
			'last_name' => 'OUVRELLE',
			'username' => 'C.OUVR',
			'password' => '8B0O1I2Z',
			'wana_id' => '470',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'RENE',
			'last_name' => 'PAILLOUX',
			'username' => 'R.PAIL',
			'password' => '8B0O1I2Z',
			'wana_id' => '480',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEAN',
			'last_name' => 'PACAULT',
			'username' => 'J.PACA',
			'password' => '8B0O1I2Z',
			'wana_id' => '475',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEROME',
			'last_name' => 'POUSSINEAU',
			'username' => 'J.POUS',
			'password' => '8B0O1I2Z',
			'wana_id' => '517',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'THIERRY',
			'last_name' => 'PATIENT',
			'username' => 'T.PATI',
			'password' => '8B0O1I2Z',
			'wana_id' => '497',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CAMILLE',
			'last_name' => 'PELLARD',
			'username' => 'C.PELL',
			'password' => '8B0O1I2Z',
			'wana_id' => '499',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'LYDIE',
			'last_name' => 'PELLARD',
			'username' => 'L.PELL',
			'password' => '8B0O1I2Z',
			'wana_id' => '498',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'FRANCK',
			'last_name' => 'PELLETIER',
			'username' => 'F.PELL',
			'password' => '8B0O1I2Z',
			'wana_id' => '496',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ANDRE',
			'last_name' => 'PENOT',
			'username' => 'A.PENO',
			'password' => '8B0O1I2Z',
			'wana_id' => '490',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PASCAL',
			'last_name' => 'PERON',
			'username' => 'P.PERO',
			'password' => '8B0O1I2Z',
			'wana_id' => '991',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'FRANCK',
			'last_name' => 'PETIPAS',
			'username' => 'F.PETI',
			'password' => '8B0O1I2Z',
			'wana_id' => '502',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'YVES',
			'last_name' => 'PETIT',
			'username' => 'Y.PETI',
			'password' => '8B0O1I2Z',
			'wana_id' => '505',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PHILIPPE',
			'last_name' => 'PETIT',
			'username' => 'P.PETI',
			'password' => '8B0O1I2Z',
			'wana_id' => '504',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'BERNARD',
			'last_name' => 'PETIT',
			'username' => 'B.PETI',
			'password' => '8B0O1I2Z',
			'wana_id' => '503',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Patrice',
			'last_name' => 'PETITIMBERT',
			'username' => 'PAPE',
			'password' => '8B0O1I2Z',
			'wana_id' => '507',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'MYRIAM',
			'last_name' => 'PETITPAS',
			'username' => 'M.PETI',
			'password' => '8B0O1I2Z',
			'wana_id' => '501',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'LAURENT',
			'last_name' => 'PINON',
			'username' => 'L.PINO',
			'password' => '8B0O1I2Z',
			'wana_id' => '508',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'AIME',
			'last_name' => 'HAMEAU',
			'username' => 'A.HAME',
			'password' => '8B0O1I2Z',
			'wana_id' => '380',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ALEXANDRE',
			'last_name' => 'PIOUX',
			'username' => 'A.PIOU',
			'password' => '8B0O1I2Z',
			'wana_id' => '509',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEAN PIERRE',
			'last_name' => 'POISSON',
			'username' => 'J.POIS',
			'password' => '8B0O1I2Z',
			'wana_id' => '510',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'STEPHANE',
			'last_name' => 'PONCET',
			'username' => 'S.PONC',
			'password' => '8B0O1I2Z',
			'wana_id' => '515',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'EMMANUEL',
			'last_name' => 'PONCET',
			'username' => 'E.PONC',
			'password' => '8B0O1I2Z',
			'wana_id' => '514',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ARTURO',
			'last_name' => 'PONS',
			'username' => 'A.PONS',
			'password' => '8B0O1I2Z',
			'wana_id' => '512',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEROME',
			'last_name' => 'POUSSINEAU',
			'username' => 'J.POUS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1005',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Patrick',
			'last_name' => 'PROCHASSON',
			'username' => 'PAPR',
			'password' => '8B0O1I2Z',
			'wana_id' => '520',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'XAVIER',
			'last_name' => 'ROUAUD',
			'username' => 'X.ROUA',
			'password' => '8B0O1I2Z',
			'wana_id' => '528',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Anny',
			'last_name' => 'RICHER',
			'username' => 'A.RICH',
			'password' => '8B0O1I2Z',
			'wana_id' => '525',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CHRISTOPHE',
			'last_name' => 'ROY',
			'username' => 'C.ROY',
			'password' => '8B0O1I2Z',
			'wana_id' => '1002',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'RICHARD',
			'last_name' => 'SABLON',
			'username' => 'R.SABL',
			'password' => '8B0O1I2Z',
			'wana_id' => '527',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'VALERIE',
			'last_name' => 'SALMON',
			'username' => 'V.SALM',
			'password' => '8B0O1I2Z',
			'wana_id' => '529',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEAN PIERRE',
			'last_name' => 'SIBOURD',
			'username' => 'J.SIBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '531',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Stéphane',
			'last_name' => 'SIMON',
			'username' => 'STSI',
			'password' => '8B0O1I2Z',
			'wana_id' => '530',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'FREDERIC',
			'last_name' => 'TARDY',
			'username' => 'F.TARD',
			'password' => '8B0O1I2Z',
			'wana_id' => '989',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Christophe',
			'last_name' => 'TARDY',
			'username' => 'C.TARD',
			'password' => '8B0O1I2Z',
			'wana_id' => '540',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Jean-Pierre',
			'last_name' => 'BERTHIER',
			'username' => 'JPBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '200',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Franck',
			'last_name' => 'TARTINVILLE',
			'username' => 'FRTA',
			'password' => '8B0O1I2Z',
			'wana_id' => '545',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Jean-Claude',
			'last_name' => 'TEMPLIER',
			'username' => 'JC.TEMP',
			'password' => '8B0O1I2Z',
			'wana_id' => '550',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Cyril',
			'last_name' => 'BONNAIRE',
			'username' => 'CYBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '213',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'XAVIER',
			'last_name' => 'TOUDOIRE',
			'username' => 'X.TOUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '581',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Joël',
			'last_name' => 'THOMAS',
			'username' => 'JOTH',
			'password' => '8B0O1I2Z',
			'wana_id' => '563',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'GILLES',
			'last_name' => 'VANDEPUTTE',
			'username' => 'G.VAND',
			'password' => '8B0O1I2Z',
			'wana_id' => '600',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'last_name' => 'TOUS COMPAGNONS',
			'username' => '.TOUS',
			'password' => '8B0O1I2Z',
			'wana_id' => '999',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Cathy',
			'last_name' => 'TRIMOUILLE',
			'username' => 'CATR',
			'password' => '8B0O1I2Z',
			'wana_id' => '585',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'SEBASTIEN',
			'last_name' => 'BERNARD',
			'username' => 'S.BERN',
			'password' => '8B0O1I2Z',
			'wana_id' => '995',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CHRISTIANE',
			'last_name' => 'TOUDOIRE',
			'username' => 'C.TOUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '580',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PASCAL',
			'last_name' => 'THIBAULT',
			'username' => 'P.THIB',
			'password' => '8B0O1I2Z',
			'wana_id' => '558',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'NICOLAS',
			'last_name' => 'TREZARIEU',
			'username' => 'N.TREZ',
			'password' => '8B0O1I2Z',
			'wana_id' => '583',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEROME',
			'last_name' => 'TRIMOUILLE',
			'username' => 'J.TRIM',
			'password' => '8B0O1I2Z',
			'wana_id' => '1016',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Stéphane',
			'last_name' => 'GIRERD',
			'username' => 'STGI',
			'password' => '8B0O1I2Z',
			'wana_id' => '368',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CEDRIC',
			'last_name' => 'MANGON',
			'username' => 'C.MANG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1017',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'STEPHANIE',
			'last_name' => 'THIERRY',
			'username' => 'S.THIE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1018',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Christine',
			'last_name' => 'CAILLARD',
			'username' => 'C.CAIL',
			'password' => '8B0O1I2Z',
			'wana_id' => '317',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'LIONEL',
			'last_name' => 'CHATAIN',
			'username' => 'L.CHAT',
			'password' => '8B0O1I2Z',
			'wana_id' => '1019',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'KAREN',
			'last_name' => 'MARTIN',
			'username' => 'K.MART',
			'password' => '8B0O1I2Z',
			'wana_id' => '442',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'LAURENT',
			'last_name' => 'RONDEAU',
			'username' => 'L.ROND',
			'password' => '8B0O1I2Z',
			'wana_id' => '1020',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CHRISTELLE',
			'last_name' => 'GREGOIRE',
			'username' => 'C.GREG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1021',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ANGELIQUE',
			'last_name' => 'GERVAIS',
			'username' => 'ANGE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1022',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'DAVY',
			'last_name' => 'GARRE',
			'username' => 'D.GARR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1023',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Michel',
			'last_name' => 'BRUNEL',
			'username' => 'M.BRUN',
			'password' => '8B0O1I2Z',
			'wana_id' => '253',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Bruno',
			'last_name' => 'PEBELIER',
			'username' => 'B.PEBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '488',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Thierry',
			'last_name' => 'SAUVEGRAIN',
			'username' => 'T.SAUV',
			'password' => '8B0O1I2Z',
			'wana_id' => '532',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'DENIS',
			'last_name' => 'MELET',
			'username' => 'D.MELE',
			'password' => '8B0O1I2Z',
			'wana_id' => '441',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'THIERRY',
			'last_name' => 'BOUCHET',
			'username' => 'T.BOUC',
			'password' => '8B0O1I2Z',
			'wana_id' => '219',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PHILIPPE',
			'last_name' => 'PAROU',
			'username' => 'P.PARO',
			'password' => '8B0O1I2Z',
			'wana_id' => '485',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PIERRE',
			'last_name' => 'LEMAIRE',
			'username' => 'P.LEMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1024',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Franck',
			'last_name' => 'NEVEU',
			'username' => 'FRNE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1025',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'BRUNO',
			'last_name' => 'AYOUL',
			'username' => 'B.AYOU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1026',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'OLIVIER',
			'last_name' => 'QUEHEN',
			'username' => 'O.QUEH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1027',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CENTRE USIN.',
			'last_name' => 'NUM_SAUVEGRAIN',
			'username' => 'CENU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1028',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ARNAUD',
			'last_name' => 'DEGOUVE',
			'username' => 'A.DEGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1029',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'LUDOVIC',
			'last_name' => 'ANDRE',
			'username' => 'L.ANDR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1030',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ROMUALD',
			'last_name' => 'LEBERT',
			'username' => 'R.LEBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1031',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'VINCENT',
			'last_name' => 'JOBET',
			'username' => 'V.JOBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1032',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CEDRIC',
			'last_name' => 'RENAUDAT',
			'username' => 'C.RENA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1033',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'STEPHANIE',
			'last_name' => 'THIERRY',
			'username' => 'S.THIE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1034',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CELINE',
			'last_name' => 'ARNOULD',
			'username' => 'C.ARNO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1035',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'THIERRY',
			'last_name' => 'DELARUE',
			'username' => 'T.DELA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1036',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEAN-PIERRE',
			'last_name' => 'GOUILLOU',
			'username' => 'J.GOUI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1037',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'SEBASTIEN',
			'last_name' => 'MAY',
			'username' => 'S.MAY',
			'password' => '8B0O1I2Z',
			'wana_id' => '1038',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PASCAL',
			'last_name' => 'CRISPINO',
			'username' => 'P.CRIS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1039',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CYRIL',
			'last_name' => 'FAURE',
			'username' => 'C.FAUR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1040',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'DIDIER',
			'last_name' => 'BRUYERE',
			'username' => 'D.BRUY',
			'password' => '8B0O1I2Z',
			'wana_id' => '1041',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ALEXANDRA',
			'last_name' => 'FOUCHER',
			'username' => 'A.FOUC',
			'password' => '8B0O1I2Z',
			'wana_id' => '1042',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => '-',
			'last_name' => 'RIBEIRO',
			'username' => '-.RIBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1043',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CLHE',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'CLCO',
			'password' => '8B0O1I2Z',
			'wana_id' => '239',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PAPR',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.PAPR',
			'password' => '8B0O1I2Z',
			'wana_id' => '389',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Grégory',
			'last_name' => 'MHUN',
			'username' => 'G.MHUN',
			'password' => '8B0O1I2Z',
			'wana_id' => '1379',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'THBO',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.THBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '222',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Peint/Finition',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.PEIN',
			'password' => '8B0O1I2Z',
			'wana_id' => '396',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'FRDU',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.FRDU',
			'password' => '8B0O1I2Z',
			'wana_id' => '349',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PADU',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.PADU',
			'password' => '8B0O1I2Z',
			'wana_id' => '411',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'FRTA',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.FRTA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1378',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'STGI',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'STGI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1377',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PALE',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.PALE',
			'password' => '8B0O1I2Z',
			'wana_id' => '431',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JOEL',
			'last_name' => 'MICHELET',
			'username' => 'J.MICH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1380',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEAN FRANCOIS',
			'last_name' => 'HOSTE',
			'username' => 'J.HOST',
			'password' => '8B0O1I2Z',
			'wana_id' => '1381',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'SERGE',
			'last_name' => 'BONNEMAIN',
			'username' => 'S.BONN',
			'password' => '8B0O1I2Z',
			'wana_id' => '1382',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Christophe',
			'last_name' => 'SAULNIER',
			'username' => 'C.SAUL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1383',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'BRICE',
			'last_name' => 'NIGON',
			'username' => 'B.NIGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1384',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'FLORENT',
			'last_name' => 'LEBRUN',
			'username' => 'F.LEBR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1385',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'FREDERIC',
			'last_name' => 'BRUAND',
			'username' => 'F.BRUA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1386',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'BORIS',
			'last_name' => 'BEAUVAIS',
			'username' => 'B.BEAU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1387',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'MATHIEU',
			'last_name' => 'ABASSIN',
			'username' => 'M.ABAS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1388',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'VIVIEN',
			'last_name' => 'IMBAULT',
			'username' => 'V.IMBA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1389',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CEDRIC',
			'last_name' => 'THIEVENT',
			'username' => 'C.THIE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1390',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ARNAUD',
			'last_name' => 'AUBRY',
			'username' => 'A.AUBR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1391',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Christophe',
			'last_name' => 'JAWDOSZYN',
			'username' => 'C.JAWD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1392',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'OLIVIER',
			'last_name' => 'CAILLARD',
			'username' => 'O.CAIL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1393',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'CHRISTIAN',
			'last_name' => 'RANGE',
			'username' => 'C.RANG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1394',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Denis',
			'last_name' => 'ROUSSEAU',
			'username' => 'D.ROUS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1395',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'PATRICK',
			'last_name' => 'LEMARCHAND',
			'username' => 'P.LEMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1396',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'ALBAN',
			'last_name' => 'GRONDARD',
			'username' => 'A.GRON',
			'password' => '8B0O1I2Z',
			'wana_id' => '1397',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'JEAN-MICHEL',
			'last_name' => 'GREGOIRE',
			'username' => 'J.GREG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1398',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'BRUNO',
			'last_name' => 'DEFAULT',
			'username' => 'B.DEFA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1399',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'RICHARD',
			'last_name' => 'FAERBER',
			'username' => 'R.FAER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1400',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'HELENE',
			'last_name' => 'BOUGUERRA',
			'username' => 'H.BOUG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1401',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:36',
			'updated_at' => '2018-05-15 13:03:36',
			'first_name' => 'Arnaud',
			'last_name' => 'ROTHENFLUE',
			'username' => 'ARRO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1402',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'PIERRE',
			'last_name' => 'LAMBERT',
			'username' => 'P.LAMB',
			'password' => '8B0O1I2Z',
			'wana_id' => '1403',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'DIDIER',
			'last_name' => 'LOPEZ',
			'username' => 'D.LOPE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1404',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'ALBAN',
			'last_name' => 'ROTHENFLUE',
			'username' => 'A.ROTH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1405',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'YOHANN',
			'last_name' => 'MULLER',
			'username' => 'Y.MULL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1406',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Maryline',
			'last_name' => 'DEFAULT',
			'username' => 'M.DEFA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1407',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'CYRIL',
			'last_name' => 'CHARLOT',
			'username' => 'C.CHAR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1408',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'SYLVIE',
			'last_name' => 'MONAI',
			'username' => 'S.MONA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1409',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'PATRICK',
			'last_name' => 'ARNAUD',
			'username' => 'P.ARNA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1410',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Alexandre',
			'last_name' => 'CHANCEAU',
			'username' => 'A.CHAN',
			'password' => '8B0O1I2Z',
			'wana_id' => '1411',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'MATHIEU',
			'last_name' => 'WALOCQ',
			'username' => 'M.WALO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1412',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'ALEXANDRE',
			'last_name' => 'BOIN',
			'username' => 'A.BOIN',
			'password' => '8B0O1I2Z',
			'wana_id' => '1413',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Richard',
			'last_name' => 'FAERBER',
			'username' => 'R.FAER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1414',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'MICKAEL',
			'last_name' => 'COCHARD',
			'username' => 'M.COCH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1415',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'STEPHANE',
			'last_name' => 'BOIZIER',
			'username' => 'S.BOIZ',
			'password' => '8B0O1I2Z',
			'wana_id' => '1416',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'WILLIAM',
			'last_name' => 'METINGER',
			'username' => 'W.METI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1417',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Christine',
			'last_name' => 'DURAND',
			'username' => 'C.DURA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1418',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'THOMAS',
			'last_name' => 'PINTO',
			'username' => 'T.PINT',
			'password' => '8B0O1I2Z',
			'wana_id' => '1419',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'ALEXANDRE',
			'last_name' => 'JOSSET',
			'username' => 'A.JOSS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1420',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'NICOLAS',
			'last_name' => 'PAGLIARI',
			'username' => 'N.PAGL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1421',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'ABDEL',
			'last_name' => 'ELBOUSSIKHANI',
			'username' => 'A.ELBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1422',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'FRANCK',
			'last_name' => 'FLAO',
			'username' => 'F.FLAO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1423',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'CEDRIC',
			'last_name' => 'LAMERA',
			'username' => 'C.LAME',
			'password' => '8B0O1I2Z',
			'wana_id' => '1424',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'BENOIT',
			'last_name' => 'CARON',
			'username' => 'B.CARO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1425',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Teddy',
			'last_name' => 'ORRIGER',
			'username' => 'T.ORRI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1426',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Jonathan',
			'last_name' => 'VENISSE',
			'username' => 'J.VENI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1427',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Joël',
			'last_name' => 'LEMOAL',
			'username' => 'JOLE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1429',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Eric',
			'last_name' => 'DE TOLLENAERE',
			'username' => 'ERDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1430',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Antoine',
			'last_name' => 'SAUZAY',
			'username' => 'ANSA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1431',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'AMAURY',
			'last_name' => 'DEGOUVE',
			'username' => 'AMDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1432',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Boris',
			'last_name' => 'DUMOULIN',
			'username' => 'B.DUMO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1433',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'JOBA',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.JOBA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1434',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Lionel',
			'last_name' => 'CRAVO',
			'username' => 'LICR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1435',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Emmanuel',
			'last_name' => 'PETITJEAN',
			'username' => 'E.PETI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1436',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Daniel',
			'last_name' => 'GAUDIN',
			'username' => 'D.GAUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1437',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'TEDDY',
			'last_name' => 'HAUTIN',
			'username' => 'TEHA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1438',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Nicolas',
			'last_name' => 'DURAND',
			'username' => 'NIDU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1440',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'JEROME',
			'last_name' => 'LEBERT',
			'username' => 'JELE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1441',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Laurent',
			'last_name' => 'DELAHAIE',
			'username' => 'LAHA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1442',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Christophe',
			'last_name' => 'AQUEVILLO',
			'username' => 'C.ACQU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1444',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Arnaud',
			'last_name' => 'DEGOUVE',
			'username' => 'A.DEGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1445',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'David',
			'last_name' => 'GUILLET',
			'username' => 'D.GUIL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1446',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'SOPHIE',
			'last_name' => 'ACERRA',
			'username' => 'S.ACER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1447',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'PAPE',
			'last_name' => 'COMP TEMPS D\'ECHANGE',
			'username' => 'TE.PAPE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1448',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Christian',
			'last_name' => 'LIEZ',
			'username' => 'CHLI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1449',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Frédéric',
			'last_name' => 'FERET',
			'username' => 'FRFE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1450',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Jean Jacques',
			'last_name' => 'DELAVEAU',
			'username' => 'JJ.DELA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1451',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Alexandre',
			'last_name' => 'MATAROZZO',
			'username' => 'ALMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1452',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Cyril',
			'last_name' => 'CLEUET',
			'username' => 'CYCL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1453',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Vincent',
			'last_name' => 'DELAIDDE',
			'username' => 'VIDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1454',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Fabien',
			'last_name' => 'TEGELBECKERS',
			'username' => 'FATE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1455',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Denis',
			'last_name' => 'COTTET',
			'username' => 'DECO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1456',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Christophe',
			'last_name' => 'CHAPUIS',
			'username' => 'CHCH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1457',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Sébastien',
			'last_name' => 'DUPONT',
			'username' => 'S.DUPO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1458',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Eric',
			'last_name' => 'NGOME',
			'username' => 'ERNG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1459',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Alexis',
			'last_name' => 'VAURY',
			'username' => 'A.VAUR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1460',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Teddy',
			'last_name' => 'FAUTER',
			'username' => 'TEFA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1461',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Johnny',
			'last_name' => 'PIRIOU',
			'username' => 'J.PIRI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1462',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Jérémy',
			'last_name' => 'BRISSOT',
			'username' => 'J.BRIS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1463',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Jérémy',
			'last_name' => 'MICHEL',
			'username' => 'JEMI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1464',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Sébastien',
			'last_name' => 'LEMAIRE',
			'username' => 'SELE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1465',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Nicolas',
			'last_name' => 'AUGUSTE-SAGET',
			'username' => 'NIAU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1466',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Marie',
			'last_name' => 'PELLETIER',
			'username' => 'MAPE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1467',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Antoine',
			'last_name' => 'SIMON',
			'username' => 'A.SIMO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1469',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Cyril',
			'last_name' => 'PETITIMBERT',
			'username' => 'CYPE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1470',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Damien',
			'last_name' => 'THIERRY',
			'username' => 'DATH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1471',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Vincent',
			'last_name' => 'SIMON',
			'username' => 'V.SIMO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1472',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Thomas',
			'last_name' => 'STRASMAN',
			'username' => 'THST',
			'password' => '8B0O1I2Z',
			'wana_id' => '1473',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Jonathan',
			'last_name' => 'ERNULT',
			'username' => 'JOER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1474',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Nicolas',
			'last_name' => 'SAULNIER',
			'username' => 'N.SAUL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1475',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Cédric',
			'last_name' => 'DUENA',
			'username' => 'CEDU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1476',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Mathieu',
			'last_name' => 'JOBET',
			'username' => 'M.JOBET',
			'password' => '8B0O1I2Z',
			'wana_id' => '1477',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Jérome',
			'last_name' => 'BABAK',
			'username' => 'J.BABA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1478',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Nicolas',
			'last_name' => 'PICARD',
			'username' => 'N.PICA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1479',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Anthony',
			'last_name' => 'BIKIALO',
			'username' => 'ANBI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1480',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Maxime',
			'last_name' => 'DEVOUCOUX',
			'username' => 'M.DEVO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1481',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Karen',
			'last_name' => 'BERTHIER',
			'username' => 'K.BERT',
			'password' => '8B0O1I2Z',
			'wana_id' => '1482',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Christelle',
			'last_name' => 'JESUS',
			'username' => 'C.JESU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1483',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Pascal',
			'last_name' => 'VINADELLE',
			'username' => 'P.VINA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1484',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Patrick',
			'last_name' => 'PICARD',
			'username' => 'P.PICA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1485',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Jonathan',
			'last_name' => 'DUPONT',
			'username' => 'J.DUPO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1486',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Kevin',
			'last_name' => 'DEBUSSCHERE',
			'username' => 'K.DEBU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1487',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Frédéric',
			'last_name' => 'GLAUME',
			'username' => 'F.GLAU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1488',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Adrien',
			'last_name' => 'ROQUES',
			'username' => 'A.ROQU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1489',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Frédéric',
			'last_name' => 'LARUE',
			'username' => 'F.LARU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1490',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Sylvain',
			'last_name' => 'BRANCHU',
			'username' => 'S.BRAN',
			'password' => '8B0O1I2Z',
			'wana_id' => '1491',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'David',
			'last_name' => 'SOUVILLE',
			'username' => 'D.SOUV',
			'password' => '8B0O1I2Z',
			'wana_id' => '1492',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Robert',
			'last_name' => 'PHILIPPEAU',
			'username' => 'R.PHIL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1493',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Grégory',
			'last_name' => 'ROBINET',
			'username' => 'G.ROBI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1494',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Patrick',
			'last_name' => 'DOLO',
			'username' => 'P.DOLO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1495',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Perrine',
			'last_name' => 'LEVESQUE',
			'username' => 'P.LEVE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1496',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Didier',
			'last_name' => 'PIVOTEAU',
			'username' => 'D.PIVO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1497',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Jérémy',
			'last_name' => 'HODNIK',
			'username' => 'J.HODN',
			'password' => '8B0O1I2Z',
			'wana_id' => '1498',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Farid',
			'last_name' => 'EL GUANDAOUI',
			'username' => 'F.ELG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1499',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Pierre',
			'last_name' => 'SIMON',
			'username' => 'P.SIMO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1500',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Sylvain',
			'last_name' => 'NAUDON',
			'username' => 'SYNA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1501',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Rachid',
			'last_name' => 'ELHANI',
			'username' => 'R.ELH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1502',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Andy',
			'last_name' => 'COADOU',
			'username' => 'A.COAD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1503',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Fabien',
			'last_name' => 'ARLICOT',
			'username' => 'F.ARLI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1504',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Julien',
			'last_name' => 'VENISSE',
			'username' => 'J.VENI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1505',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Eddy',
			'last_name' => 'LEMAIRE',
			'username' => 'E.LEMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1506',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Nenad',
			'last_name' => 'AVRAMOVIC',
			'username' => 'N.AVRAM',
			'password' => '8B0O1I2Z',
			'wana_id' => '1507',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Mickaël',
			'last_name' => 'BALLET',
			'username' => 'M.BAILL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1508',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Nicolas',
			'last_name' => 'DUDKA',
			'username' => 'N.DUDK',
			'password' => '8B0O1I2Z',
			'wana_id' => '1509',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Allan',
			'last_name' => 'COADOU',
			'username' => 'A.COAD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1510',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Elisabeth',
			'last_name' => 'BEZARD',
			'username' => 'E.BESAR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1511',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Philippe',
			'last_name' => 'COMTE',
			'username' => 'P.COMT',
			'password' => '8B0O1I2Z',
			'wana_id' => '1512',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Nicolas',
			'last_name' => 'BARRIERE',
			'username' => 'N.BARRI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1513',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'first_name' => 'Aliksan',
			'last_name' => 'CAVUSOGLU',
			'username' => 'A.CAVUS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1514',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'username' => 'PR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1515',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'username' => 'ME',
			'password' => '8B0O1I2Z',
			'wana_id' => '1516',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1517',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1518',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1519',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1520',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1521',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1522',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1524',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1525',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1526',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1527',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1528',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1529',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:37',
			'updated_at' => '2018-05-15 13:03:37',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1530',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1531',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1532',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1533',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1534',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1535',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1536',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1537',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1538',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1539',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1540',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1541',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1542',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1543',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1544',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1545',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'METIER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1546',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Jérome',
			'last_name' => 'LESAGE',
			'username' => 'JE.LESA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1547',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Fabrice',
			'last_name' => 'BAUDIN',
			'username' => 'FA.BAUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1548',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Aurélien',
			'last_name' => 'BERTHEAULT',
			'username' => 'A.BERTH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1549',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Isabelle',
			'last_name' => 'GIRERD',
			'username' => 'ISGIRER',
			'password' => '8B0O1I2Z',
			'wana_id' => '1550',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Annabelle',
			'last_name' => 'POITOU',
			'username' => 'ANPO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1551',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Yoann',
			'last_name' => 'JOUY CURIEUX',
			'username' => 'Y.JOUY',
			'password' => '8B0O1I2Z',
			'wana_id' => '1552',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Authindra',
			'last_name' => 'PAWAR',
			'username' => 'AUPA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1554',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Alex',
			'last_name' => 'ROGER',
			'username' => 'A.ROGE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1555',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Audrey',
			'last_name' => 'BOUDEAU',
			'username' => 'AUBOUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1556',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Jerome',
			'last_name' => 'LE DORZE',
			'username' => 'JELE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1558',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Gregory',
			'last_name' => 'BOUDEAU',
			'username' => 'GRBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1559',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Emilie',
			'last_name' => 'DUMONT',
			'username' => 'EMDU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1560',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Magali',
			'last_name' => 'THILLOU',
			'username' => 'MATH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1561',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Nicolas (stag.)',
			'last_name' => 'AUBIN',
			'username' => 'NIAU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1562',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Valentin (stag.',
			'last_name' => 'GAILLARD',
			'username' => 'VAGA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1563',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Sébastien (stag',
			'last_name' => 'BOURGINE',
			'username' => 'SEBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1564',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Guillaume',
			'last_name' => 'CHAUVEAU',
			'username' => 'G.CHAU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1565',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Rémy (stag.)',
			'last_name' => 'LAMBERT',
			'username' => 'RELA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1566',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Léo',
			'last_name' => 'NIGON',
			'username' => 'LEO.NIG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1567',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Eric',
			'last_name' => 'THOMAS',
			'username' => 'ERTH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1568',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Bruno',
			'last_name' => 'BESSON',
			'username' => 'BRBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1569',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Gabriel',
			'last_name' => 'AUDINET',
			'username' => 'GAAU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1570',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Ludovic',
			'last_name' => 'FROLO',
			'username' => 'LUFR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1571',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Yann',
			'last_name' => 'DUBRESSON',
			'username' => 'YADU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1572',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Julien',
			'last_name' => 'TAILLANDIER',
			'username' => 'JU.TAIL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1573',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'GABRIEL',
			'last_name' => 'GONCALVES',
			'username' => 'GAGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1574',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'VALENTIN',
			'last_name' => 'DELAPLANCHE',
			'username' => 'VADE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1575',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'LOIC',
			'last_name' => 'PELLETIER',
			'username' => 'LOPE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1576',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Pascal',
			'last_name' => 'BOCQUET',
			'username' => 'PABO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1577',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Antony',
			'last_name' => 'HARRAULT',
			'username' => 'AN.HARR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1578',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Amélie',
			'last_name' => 'DELANNOY',
			'username' => 'AMDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1579',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Baptiste',
			'last_name' => 'PROCHASSON',
			'username' => 'B.PROC',
			'password' => '8B0O1I2Z',
			'wana_id' => '1580',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Audrey',
			'last_name' => 'GROENEWEG',
			'username' => 'AUGR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1581',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'William',
			'last_name' => 'CATHELAIN',
			'username' => 'W.CATH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1583',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Abdullah',
			'last_name' => 'CAVUSOGLU',
			'username' => 'ABCA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1584',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Halit',
			'last_name' => 'SUMAR',
			'username' => 'HASU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1585',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Dorian',
			'last_name' => 'SALIN',
			'username' => 'DO.SALI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1586',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Stéphane',
			'last_name' => 'RENOUD',
			'username' => 'S.RENO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1587',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'William',
			'last_name' => 'FESSARD',
			'username' => 'W.FESS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1588',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Jérémy',
			'last_name' => 'RAGU',
			'username' => 'J.RAGU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1589',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Hervé',
			'last_name' => 'HAUGUEL',
			'username' => 'H.HAUG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1590',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Franck',
			'last_name' => 'PELLETIER',
			'username' => 'F.PELLE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1592',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Damien',
			'last_name' => 'GLAUME',
			'username' => 'D.GLAUM',
			'password' => '8B0O1I2Z',
			'wana_id' => '1593',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Vanessa',
			'last_name' => 'CHAPLOTEAU',
			'username' => 'V.CHA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1595',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'ARNAUD',
			'last_name' => 'PELLAT',
			'username' => 'ARPE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1596',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'ARLINDO',
			'last_name' => 'DE ABREU',
			'username' => 'ARDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1597',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Matthieu',
			'last_name' => 'PLESSY',
			'username' => 'MAPL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1598',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Abrahim',
			'last_name' => 'EL MOUSSAOUI',
			'username' => 'ABEL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1599',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Alain',
			'last_name' => 'BOGOSSIAN',
			'username' => 'AL.BOGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1600',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'René',
			'last_name' => 'NEMASSOA',
			'username' => 'RE.NEMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1601',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Vivien',
			'last_name' => 'RUSE',
			'username' => 'VI.RUSE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1602',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Maurane',
			'last_name' => 'BOUDEAU',
			'username' => 'MA.BOUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1603',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Damien',
			'last_name' => 'NAUDON',
			'username' => 'DA.NAUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1604',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Vincent',
			'last_name' => 'FUARD',
			'username' => 'VIFU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1605',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Dimitri',
			'last_name' => 'VELASCO',
			'username' => 'DIVE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1606',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Aymerick',
			'last_name' => 'DAUDIER',
			'username' => 'AYDA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1607',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Frédéric',
			'last_name' => 'SIBOUT',
			'username' => 'FRSI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1608',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Patrick',
			'last_name' => 'DOLIVET',
			'username' => 'PADO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1609',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Philippe',
			'last_name' => 'RENARD',
			'username' => 'PHRE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1610',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Alain',
			'last_name' => 'SANGUINETTI',
			'username' => 'ALSA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1611',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'LUCILE',
			'last_name' => 'BONLIEU',
			'username' => 'LUBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1612',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'DADE',
			'last_name' => 'COMP TEMPS ECHANGE',
			'username' => 'TE.DADE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1613',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Julien',
			'last_name' => 'BOULAS',
			'username' => 'JU.BOUL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1614',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Mickaël',
			'last_name' => 'BONTEMPS',
			'username' => 'MIKA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1615',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'FRDU/JOBA',
			'username' => 'DU/BA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1616',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Eric',
			'last_name' => 'HUREAU',
			'username' => 'ERHU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1617',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Danny',
			'last_name' => 'MORENO',
			'username' => 'DAMO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1618',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Arnaud',
			'last_name' => 'DUPUIS',
			'username' => 'AR.DUPU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1619',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Cédric',
			'last_name' => 'SGITCOVICH',
			'username' => 'CESG',
			'password' => '8B0O1I2Z',
			'wana_id' => '1620',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Carlos',
			'last_name' => 'GRILO',
			'username' => 'CA.GRIL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1621',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Franky',
			'last_name' => 'JOURDAIN',
			'username' => 'FR.JOUR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1622',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'José',
			'last_name' => 'BISSONNET',
			'username' => 'JO.BISS',
			'password' => '8B0O1I2Z',
			'wana_id' => '1623',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Sylvain',
			'last_name' => 'CHANDELLIER',
			'username' => 'SYCH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1624',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Maxime',
			'last_name' => 'CLOUSEAU',
			'username' => 'MA.CLOU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1625',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Etienne',
			'last_name' => 'PRETRE',
			'username' => 'ET.PRET',
			'password' => '8B0O1I2Z',
			'wana_id' => '1626',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Fabien',
			'last_name' => 'ROBET',
			'username' => 'FA.ROBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1627',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Yann',
			'last_name' => 'DIRRINGER',
			'username' => 'YA.DIRR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1628',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Bruno',
			'last_name' => 'NAUDIN',
			'username' => 'BR.NAUD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1629',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Geoffrey',
			'last_name' => 'GOUJON',
			'username' => 'GE.GOUJ',
			'password' => '8B0O1I2Z',
			'wana_id' => '1630',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'David',
			'last_name' => 'VIOLLAND',
			'username' => 'DA.VIOL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1631',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Alex',
			'last_name' => 'DARDELET',
			'username' => 'AL.DARD',
			'password' => '8B0O1I2Z',
			'wana_id' => '1632',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Rudy',
			'last_name' => 'CORJON',
			'username' => 'RU.CORJ',
			'password' => '8B0O1I2Z',
			'wana_id' => '1633',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Patrice',
			'last_name' => 'GILLET',
			'username' => 'PA.GILL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1634',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Florence',
			'last_name' => 'MARUITTE',
			'username' => 'FL.MARU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1635',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Eddy',
			'last_name' => 'DI CICCO',
			'username' => 'ED.DICI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1636',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Sullivan',
			'last_name' => 'MADELENAT',
			'username' => 'SU.MADE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1637',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Romain',
			'last_name' => 'SIMOND',
			'username' => 'RO.SIMO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1638',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Arnaud',
			'last_name' => 'COLIN',
			'username' => 'AR.COLI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1639',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Mélanie',
			'last_name' => 'ASSELIN HARVEAU',
			'username' => 'ME.ASSE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1640',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'PALE/FRDU',
			'username' => 'PA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1641',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'THBO/DADE',
			'username' => 'BO/DE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1642',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'JOBA/CYDA',
			'username' => 'BA/DA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1643',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'FRDU/DADE',
			'username' => 'DU/DE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1644',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'PADU/FRDU',
			'username' => 'DU/DU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1645',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Thierry',
			'last_name' => 'GERARD',
			'username' => 'THGE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1646',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Guillaume',
			'last_name' => 'MEILLANT',
			'username' => 'GUME',
			'password' => '8B0O1I2Z',
			'wana_id' => '1647',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'JOBA/FRDU',
			'username' => 'BA/DU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1648',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Adrien',
			'last_name' => 'PINGOT',
			'username' => 'ADPI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1649',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Alexandre',
			'last_name' => 'BAUDOUIN',
			'username' => 'ALBA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1650',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'DADE/PADU',
			'username' => 'DE/DU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1651',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Anthony',
			'last_name' => 'PASDELOUP',
			'username' => 'ANPA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1652',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Pierre',
			'last_name' => 'MARTIN',
			'username' => 'PIMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1653',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Jean Michel',
			'last_name' => 'HAMAN',
			'username' => 'JEHA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1654',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Miguel',
			'last_name' => 'GERIN',
			'username' => 'MIGE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1655',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Fabrice',
			'last_name' => 'BAIN',
			'username' => 'FABA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1656',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Arthur',
			'last_name' => 'LACK',
			'username' => 'ARLA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1657',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'last_name' => 'PADU/DADE',
			'username' => 'DU/DE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1658',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Tristan',
			'last_name' => 'LAFARGUE',
			'username' => 'TRLA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1659',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Stéphane',
			'last_name' => 'BRULE',
			'username' => 'STBR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1660',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:38',
			'updated_at' => '2018-05-15 13:03:38',
			'first_name' => 'Najiha',
			'last_name' => 'EL MOUSSAOUI',
			'username' => 'NAEL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1661',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Nicolas',
			'last_name' => 'RIOS',
			'username' => 'NIRI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1662',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Maxime',
			'last_name' => 'SAUVEGRAIN',
			'username' => 'MASA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1663',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Gérard',
			'last_name' => 'PORTAL',
			'username' => 'GEPO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1664',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Stéphane',
			'last_name' => 'COSTA',
			'username' => 'STCO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1665',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Thomas',
			'last_name' => 'CORBET',
			'username' => 'THCO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1666',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Julie',
			'last_name' => 'HOUY',
			'username' => 'JUHO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1667',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Karelle',
			'last_name' => 'DELAMARRE',
			'username' => 'KADE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1668',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Baptiste',
			'last_name' => 'HERIN',
			'username' => 'BAHE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1669',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Cédric',
			'last_name' => 'GOUDOU',
			'username' => 'CEGO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1670',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Jérémy',
			'last_name' => 'DEVIN',
			'username' => 'JEDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1671',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Alexandre',
			'last_name' => 'LETURC',
			'username' => 'ALLE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1672',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Cédric',
			'last_name' => 'AYROLE',
			'username' => 'CEAY',
			'password' => '8B0O1I2Z',
			'wana_id' => '1673',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Loïc',
			'last_name' => 'PIERRE',
			'username' => 'LOPI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1674',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Nicolas',
			'last_name' => 'CORFMAT',
			'username' => 'NICO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1675',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Hugo',
			'last_name' => 'COLOMBANI',
			'username' => 'HUCO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1676',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Jordan',
			'last_name' => 'JEANNY-EVARISTE',
			'username' => 'JOJE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1677',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Romain',
			'last_name' => 'DEBREUVE',
			'username' => 'RODE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1678',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Sébastien',
			'last_name' => 'MAILLARD',
			'username' => 'SEMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1679',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Valérie',
			'last_name' => 'LECOINTE',
			'username' => 'VALE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1680',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Romain',
			'last_name' => 'BERMUDEZ',
			'username' => 'ROBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1681',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Quentin',
			'last_name' => 'LEROY',
			'username' => 'QULE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1682',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Julien',
			'last_name' => 'BODIN',
			'username' => 'JUBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1683',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Jessica',
			'last_name' => 'HOUY',
			'username' => 'JEHO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1684',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Yann',
			'last_name' => 'JOSSET',
			'username' => 'YAJO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1685',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Mathieu',
			'last_name' => 'ANDRE',
			'username' => 'MAAN',
			'password' => '8B0O1I2Z',
			'wana_id' => '1686',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Bryan',
			'last_name' => 'DULCHE',
			'username' => 'BRDU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1687',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Jérémy',
			'last_name' => 'JABLONSKI',
			'username' => 'JEJA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1688',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'MIBO/DADE',
			'username' => 'BO/DE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1689',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Charly',
			'last_name' => 'DELACROIX',
			'username' => 'CHDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1690',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Michel',
			'last_name' => 'LIM',
			'username' => 'MILI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1691',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Tarek',
			'last_name' => 'WUHRMANN',
			'username' => 'TAWU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1692',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Félicien',
			'last_name' => 'VIDAL',
			'username' => 'FEVI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1693',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Axel',
			'last_name' => 'MACAIGNE',
			'username' => 'AXMA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1694',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Jérome',
			'last_name' => 'GEZELLE',
			'username' => 'JEGE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1695',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Ludovic',
			'last_name' => 'GIRALDON',
			'username' => 'LUGI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1696',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Grégory',
			'last_name' => 'SBOUI',
			'username' => 'GRSB',
			'password' => '8B0O1I2Z',
			'wana_id' => '1697',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Cyril',
			'last_name' => 'NOE',
			'username' => 'CYNO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1698',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Cyril',
			'last_name' => 'TORTEVOIX',
			'username' => 'CYTO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1699',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Clément',
			'last_name' => 'HENRY',
			'username' => 'CLHE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1702',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Sébastien',
			'last_name' => 'RENONCOURT',
			'username' => 'SERE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1703',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Antonin',
			'last_name' => 'LETURC',
			'username' => 'ANLE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1704',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Antony',
			'last_name' => 'LENOTTE',
			'username' => 'ANLE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1705',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Jean-Baptiste',
			'last_name' => 'DEUTSCH',
			'username' => 'JEDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1706',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Margot',
			'last_name' => 'BONNAIRE',
			'username' => 'MABO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1707',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Lucie',
			'last_name' => 'COTE',
			'username' => 'LUCO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1708',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Eric',
			'last_name' => 'BURNAY',
			'username' => 'ERBU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1709',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Smaïn',
			'last_name' => 'DJELLOUL',
			'username' => 'SMDJ',
			'password' => '8B0O1I2Z',
			'wana_id' => '1710',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Julien',
			'last_name' => 'OTT',
			'username' => 'JUOT',
			'password' => '8B0O1I2Z',
			'wana_id' => '1711',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Clémentine',
			'last_name' => 'LEMOINE',
			'username' => 'CLLE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1712',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Alexis',
			'last_name' => 'GASTELLIER',
			'username' => 'ALGA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1713',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Barry',
			'last_name' => 'THIEN',
			'username' => 'BATH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1714',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Drice',
			'last_name' => 'BERAZA',
			'username' => 'DRBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1715',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Kilian',
			'last_name' => 'LE ROC\'H',
			'username' => 'KILE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1716',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Vincent',
			'last_name' => 'CHAVANNEAU',
			'username' => 'VICH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1717',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'CLHE/JOBA',
			'username' => 'HE/BA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1718',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Paul',
			'last_name' => 'BERTON',
			'username' => 'PABE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1719',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Maxime',
			'last_name' => 'RAFFARD',
			'username' => 'MARA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1720',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'REGISSEUR CINE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1721',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Geoffrey',
			'last_name' => 'BEIGNET',
			'username' => 'GEBE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1722',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'CLHE/PAPR',
			'username' => 'HE/PR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1724',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Thomas',
			'last_name' => 'GIRERD',
			'username' => 'THGI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1725',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Charles',
			'last_name' => 'LEROY',
			'username' => 'CHLE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1726',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Adrien',
			'last_name' => 'BONNEAU',
			'username' => 'ADBO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1727',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Barbara',
			'last_name' => 'LABOUS',
			'username' => 'BALA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1728',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Rekha',
			'last_name' => 'CHAKOORY',
			'username' => 'RECH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1729',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'DAVI/CYDA',
			'username' => 'VI/DA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1730',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Christophe',
			'last_name' => 'TRUPIN',
			'username' => 'CHTR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1731',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'CLHE/DADE',
			'username' => 'HE/DE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1732',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'CYDA/DADE',
			'username' => 'DE/DA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1733',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Julien',
			'last_name' => 'PALLU',
			'username' => 'JUPA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1734',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Léo',
			'last_name' => 'THEVENOT',
			'username' => 'LETH',
			'password' => '8B0O1I2Z',
			'wana_id' => '1735',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Joël',
			'last_name' => 'BRUN',
			'username' => 'JOBR',
			'password' => '8B0O1I2Z',
			'wana_id' => '1736',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'LAURA',
			'last_name' => 'DAUNAY',
			'username' => 'LADA',
			'password' => '8B0O1I2Z',
			'wana_id' => '1737',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Donovan',
			'last_name' => 'IBANEZ',
			'username' => 'DOIB',
			'password' => '8B0O1I2Z',
			'wana_id' => '1738',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Pierrick',
			'last_name' => 'ROBERT',
			'username' => 'PIRO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1739',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Samir',
			'last_name' => 'DJELLOUL',
			'username' => 'SADJ',
			'password' => '8B0O1I2Z',
			'wana_id' => '1740',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Dylan',
			'last_name' => 'DIAS',
			'username' => 'DYDI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1741',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Elodie',
			'last_name' => 'HOUDAYER',
			'username' => 'ELHO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1742',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Frédéric',
			'last_name' => 'ROUSSEAU',
			'username' => 'FRRO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1743',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Maxime',
			'last_name' => 'DUROSSET',
			'username' => 'MADU',
			'password' => '8B0O1I2Z',
			'wana_id' => '1744',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Alexandre',
			'last_name' => 'SICARD',
			'username' => 'ALSI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1745',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Christophe',
			'last_name' => 'DELACOUR',
			'username' => 'CHDE',
			'password' => '8B0O1I2Z',
			'wana_id' => '1746',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Bertrand',
			'last_name' => 'PLOUVIEZ',
			'username' => 'BEPL',
			'password' => '8B0O1I2Z',
			'wana_id' => '1747',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'DADE/DAVI',
			'username' => 'DE/VI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1748',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'last_name' => 'JOBA/DAVI',
			'username' => 'JO/VI',
			'password' => '8B0O1I2Z',
			'wana_id' => '1749',
			'is_active' => '0',
		]);
		DB::table('users')->insert([
			'created_at' => '2018-05-15 13:03:39',
			'updated_at' => '2018-05-15 13:03:39',
			'first_name' => 'Samuel',
			'last_name' => 'MORIN e',
			'username' => 'SAMO',
			'password' => '8B0O1I2Z',
			'wana_id' => '1750',
			'is_active' => '0',
		]);
    }
}