<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$role = new App\Api\v1\Models\Role;
		$role->label = "Compagnon"; // 1
		$role->save();
		$role = new App\Api\v1\Models\Role;
		$role->label = "Chef de Kamion"; // 2
		$role->save();
		$role = new App\Api\v1\Models\Role;
		$role->label = "Chef de Projet"; // 3
		$role->save();
		$role = new App\Api\v1\Models\Role;
		$role->label = "Responsable"; // 4
		$role->save();
		$role = new App\Api\v1\Models\Role;
		$role->label = "Responsable Qualité";
		$role->save();
		$role = new App\Api\v1\Models\Role;
		$role->label = "Responsable Conformité";
		$role->save();
		$role = new App\Api\v1\Models\Role;
		$role->label = "Admin"; // 5
		$role->save();
    }
}
