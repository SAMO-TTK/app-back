<?php

use Illuminate\Database\Seeder;

class ImpactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(App\Api\v1\Models\Impact::class, 700)->create();
    }
}
