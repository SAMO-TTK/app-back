
<?php

use Illuminate\Database\Seeder;

class AutorolesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('roles')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'Compagnon',
		]);
		DB::table('roles')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'Chef de Kamion',
		]);
		DB::table('roles')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'Chef de Projet',
		]);
		DB::table('roles')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'Responsable',
		]);
		DB::table('roles')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'Responsable Qualité',
		]);
		DB::table('roles')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'Responsable Conformité',
		]);
		DB::table('roles')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'Admin',
		]);
		DB::table('roles')->insert([
			'label' => 'Super Admin',
		]);
    }
}