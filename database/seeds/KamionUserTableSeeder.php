<?php

use Illuminate\Database\Seeder;

class KamionUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kamion_user')->insert([
            'kamion_id' => 1, 'user_id' => 4, 'is_kamion_manager' => 1, 'is_project_manager' => 1, 'is_pilot' => 0, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
        ]);
		for ($i = 2; $i < 18; $i++) :
	        DB::table('kamion_user')->insert([
				'kamion_id' => $i, 'user_id' => 4, 'is_kamion_manager' => 0, 'is_project_manager' => 1, 'is_pilot' => 0, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
	        ]);
	        DB::table('kamion_user')->insert([
				'kamion_id' => $i, 'user_id' => 1, 'is_kamion_manager' => 0, 'is_project_manager' => 0, 'is_pilot' => 0, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
	        ]);
	        DB::table('kamion_user')->insert([
				'kamion_id' => $i, 'user_id' => 2, 'is_kamion_manager' => 1, 'is_project_manager' => 0, 'is_pilot' => 0, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
	        ]);
	        DB::table('kamion_user')->insert([
				'kamion_id' => $i, 'user_id' => 3, 'is_kamion_manager' => 0, 'is_project_manager' => 0, 'is_pilot' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
	        ]);
		endfor;
    }
}
