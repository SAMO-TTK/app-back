
<?php

use Illuminate\Database\Seeder;

class Autoticket_categoriesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Achats',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Action Corrective',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Action Préventive',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Amélioration Continue',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Aménagements',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Analyse des Risques et Opportunités',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Assurances',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Audit Procédures (PR/INST)',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Audit Processus',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Divers',
		]);
		DB::table('ticket_categories')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Commercial',
		]);
    }
}