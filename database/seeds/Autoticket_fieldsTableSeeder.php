
<?php

use Illuminate\Database\Seeder;

class Autoticket_fieldsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-04 12:21:12',
			'updated_at' => '2018-06-04 12:21:12',
			'value' => '0',
			'ticket_id' => '22099',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-04 12:21:13',
			'updated_at' => '2018-06-04 12:21:13',
			'value' => '0',
			'ticket_id' => '22099',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-04 12:44:34',
			'updated_at' => '2018-06-04 12:44:34',
			'value' => '0',
			'ticket_id' => '22099',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-04 12:47:52',
			'updated_at' => '2018-06-04 12:47:52',
			'value' => '0',
			'ticket_id' => '22099',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-04 13:39:42',
			'updated_at' => '2018-06-04 13:39:42',
			'value' => '0',
			'ticket_id' => '22099',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-04 13:39:43',
			'updated_at' => '2018-06-04 13:39:43',
			'value' => '0',
			'ticket_id' => '22099',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-04 14:40:03',
			'updated_at' => '2018-06-04 14:40:03',
			'value' => '0',
			'ticket_id' => '22099',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-04 14:40:03',
			'updated_at' => '2018-06-04 14:40:03',
			'value' => '0',
			'ticket_id' => '22099',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-05 13:45:20',
			'updated_at' => '2018-06-05 13:45:20',
			'value' => '0',
			'ticket_id' => '22100',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-05 13:45:35',
			'updated_at' => '2018-06-05 13:45:38',
			'value' => '1',
			'ticket_id' => '22100',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-05 20:12:52',
			'updated_at' => '2018-06-05 20:18:54',
			'value' => '1',
			'ticket_id' => '22101',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-05 20:19:46',
			'updated_at' => '2018-06-05 20:20:52',
			'value' => '1',
			'ticket_id' => '22102',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-05 20:19:49',
			'updated_at' => '2018-06-05 20:19:58',
			'value' => '1',
			'ticket_id' => '22102',
			'field_id' => '4',
		]);
		DB::table('ticket_fields')->insert([
			'created_at' => '2018-06-05 20:21:00',
			'updated_at' => '2018-06-05 20:21:00',
			'ticket_id' => '22102',
			'field_id' => '5',
		]);
    }
}