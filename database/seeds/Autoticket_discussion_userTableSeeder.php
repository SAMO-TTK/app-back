
<?php

use Illuminate\Database\Seeder;

class Autoticket_discussion_userTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('ticket_discussion_user')->insert([
			'created_at' => '2018-05-15 12:37:09',
			'updated_at' => '2018-05-15 12:37:09',
			'user_id' => '1',
			'discussion_id' => '1',
		]);
		DB::table('ticket_discussion_user')->insert([
			'created_at' => '2018-06-04 11:56:07',
			'updated_at' => '2018-06-04 11:56:07',
			'user_id' => '1',
			'discussion_id' => '2',
		]);
		DB::table('ticket_discussion_user')->insert([
			'created_at' => '2018-06-04 12:35:01',
			'updated_at' => '2018-06-04 12:35:01',
			'user_id' => '3',
			'discussion_id' => '5',
		]);
		DB::table('ticket_discussion_user')->insert([
			'created_at' => '2018-06-04 12:37:44',
			'updated_at' => '2018-06-04 12:37:44',
			'user_id' => '3',
			'discussion_id' => '7',
		]);
		DB::table('ticket_discussion_user')->insert([
			'created_at' => '2018-06-05 11:04:08',
			'updated_at' => '2018-06-05 11:04:08',
			'user_id' => '2',
			'discussion_id' => '8',
		]);
		DB::table('ticket_discussion_user')->insert([
			'created_at' => '2018-06-05 13:48:21',
			'updated_at' => '2018-06-05 13:48:21',
			'user_id' => '1',
			'discussion_id' => '9',
		]);
		DB::table('ticket_discussion_user')->insert([
			'created_at' => '2018-06-05 13:48:21',
			'updated_at' => '2018-06-05 13:48:21',
			'user_id' => '1',
			'discussion_id' => '10',
		]);
    }
}