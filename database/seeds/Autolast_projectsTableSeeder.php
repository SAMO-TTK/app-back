
<?php

use Illuminate\Database\Seeder;

class Autolast_projectsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 20:46:58',
			'updated_at' => '2018-06-05 20:46:58',
			'user_id' => '1',
			'kamion_id' => '2',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-05-15 11:52:55',
			'updated_at' => '2018-05-15 11:52:55',
			'user_id' => '1',
			'kamion_id' => '3',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 20:20:27',
			'updated_at' => '2018-06-05 20:20:27',
			'user_id' => '3',
			'kamion_id' => '2',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-04 13:47:15',
			'updated_at' => '2018-06-04 13:47:15',
			'user_id' => '1',
			'kamion_id' => '18',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 09:42:38',
			'updated_at' => '2018-06-05 09:42:38',
			'user_id' => '1',
			'kamion_id' => '7',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 14:51:03',
			'updated_at' => '2018-06-05 14:51:03',
			'user_id' => '1',
			'kamion_id' => '4',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 13:19:57',
			'updated_at' => '2018-06-05 13:19:57',
			'user_id' => '1',
			'kamion_id' => '1',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 12:55:50',
			'updated_at' => '2018-06-05 12:55:50',
			'user_id' => '1',
			'kamion_id' => '5',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 13:20:29',
			'updated_at' => '2018-06-05 13:20:29',
			'user_id' => '1',
			'kamion_id' => '17',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 13:59:12',
			'updated_at' => '2018-06-05 13:59:12',
			'user_id' => '4',
			'kamion_id' => '2',
		]);
		DB::table('last_projects')->insert([
			'created_at' => '2018-06-05 13:36:01',
			'updated_at' => '2018-06-05 13:36:01',
			'user_id' => '4',
			'kamion_id' => '11',
		]);
    }
}