<?php

use Illuminate\Database\Seeder;
use App\Api\v1\Models\Chapter;
use App\Api\v1\Models\Element;
use App\Api\v1\Models\Impact;
use App\Api\v1\Models\Quality_control;
use App\Api\v1\Models\Validation_control;
use App\Api\v1\Models\Supplement;

class ChaptersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
		for ($i = 2; $i <= 5; $i++):
			$r = rand(10,20);
			for ($j=1; $j < $r; $j++) {
				$chapitre = new Chapter();
				$chapitre->numbering = $j < 10 ? '0'.$j : "".$j;
				$chapitre->title = $faker->word;
				$chapitre->kamion_id = $i;
				// $chapitre->imported_at = date('Y-m-d H:i:s');
				$chapitre->save();
				$s = rand(1,7);
				for ($k=1; $k < $s; $k++) {
					$section = new Chapter();
					$section->numbering = $k < 10 ? $chapitre->numbering.'.0'.$k : "".$chapitre->numbering.'.'.$k;
					$section->title = $faker->word;
					$section->kamion_id = $i;
					$section->parent_id = $chapitre->id;
					// $section->imported_at = date('Y-m-d H:i:s');
					$section->save();
					$t = rand(1,7);
					for ($l=1; $l < $t; $l++) {
						$ssection = new Chapter();
						$ssection->numbering = $l < 10 ? $section->numbering.'.0'.$l : $section->numbering.'.'.$l;
						$ssection->title = $faker->word;
						$ssection->kamion_id = $i;
						$ssection->parent_id = $section->id;
						// $ssection->imported_at = date('Y-m-d H:i:s');
						$ssection->save();
						$u = rand(0,10);
						for ($m=1; $m < $u; $m++) {
							$element = new Element();
							$element->numbering = $m < 10 ? $ssection->numbering.':0'.$m : $ssection->numbering.':'.$m;
							$element->content = $faker->text;
							$element->type = $faker->word;
							$element->chapter_id = $ssection->id;
							$element->is_open = rand(1, 0);
							$element->is_client_requirement = rand(1, 0);
							$element->save();
							$v = rand(0,1);
							$w = rand(0,1);
							for ($n=0; $n < $v; $n++) {
								$impact = new Impact();
								$impact->amount = $faker->randomFloat(2, 15000.0, 150000.0);
								$impact->delai = date( 'Y-m-d H:i:s', $faker->unixTime(315529200) + time());
								$impact->element_id = $element->id;
								$impact->save();
							}
							$supplement = new Supplement();
							for ($o=0; $o < $w; $o++) {
								$supplement->content = $faker->text;
								$supplement->user_id = rand(1,16);
							}
							$supplement->element_id = $element->id;
							$supplement->save();
							$val = new Validation_control;
							$val->element_id = $element->id;
							$val->is_controled = 0;
							$val->save();
							$qual = new Quality_control;
							$qual->element_id = $element->id;
							$qual->save();
						}
					}
				}
			}
		endfor;
    }
}
