
<?php

use Illuminate\Database\Seeder;

class Autolast_ticketTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('last_ticket')->insert([
			'created_at' => '2018-05-15 12:10:21',
			'updated_at' => '2018-06-05 13:58:13',
			'user_id' => '1',
			'ticket_id' => '3',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-05-24 09:47:45',
			'updated_at' => '2018-05-24 09:47:45',
			'user_id' => '1',
			'ticket_id' => '250',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-05-24 09:54:33',
			'updated_at' => '2018-05-24 09:54:33',
			'user_id' => '1',
			'ticket_id' => '22',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-05-24 09:54:33',
			'updated_at' => '2018-05-24 09:54:33',
			'user_id' => '1',
			'ticket_id' => '22',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-05-24 09:54:49',
			'updated_at' => '2018-05-24 09:54:50',
			'user_id' => '1',
			'ticket_id' => '8',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-05-31 12:14:03',
			'updated_at' => '2018-05-31 12:14:04',
			'user_id' => '3',
			'ticket_id' => '35',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-06-04 12:21:11',
			'updated_at' => '2018-06-05 20:12:28',
			'user_id' => '1',
			'ticket_id' => '22099',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-06-04 12:37:24',
			'updated_at' => '2018-06-04 12:37:24',
			'user_id' => '1',
			'ticket_id' => '7',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-06-05 13:45:19',
			'updated_at' => '2018-06-05 14:00:31',
			'user_id' => '4',
			'ticket_id' => '22100',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-06-05 13:58:33',
			'updated_at' => '2018-06-05 13:58:33',
			'user_id' => '1',
			'ticket_id' => '24',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-06-05 20:12:51',
			'updated_at' => '2018-06-05 20:18:50',
			'user_id' => '1',
			'ticket_id' => '22101',
		]);
		DB::table('last_ticket')->insert([
			'created_at' => '2018-06-05 20:19:45',
			'updated_at' => '2018-06-05 20:20:46',
			'user_id' => '3',
			'ticket_id' => '22102',
		]);
    }
}