
<?php

use Illuminate\Database\Seeder;

class Autoticket_modelsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Import',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Décideuse',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Comité Qualité',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'CONTROLE DE CONFORMITE INITIAL',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Passages aux Mines',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'CONTROLE RECEPTION CHASSIS',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'CR Réunion de lancement',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'INFORMATIONS',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'STANDARD',
		]);
		DB::table('ticket_models')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Email',
		]);
    }
}