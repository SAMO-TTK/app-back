<?php

use Illuminate\Database\Seeder;

class KamionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run(Faker\Generator $faker)
    {
        $kamion = new App\Api\v1\Models\Kamion;
        $kamion->image = 'demo.jpg';
        $kamion->category_id = 1;
        $kamion->type = 1;
        $kamion->stage_id = rand(2,3);
        $kamion->is_locked = 0;
        $kamion->is_delivered = 0;
        $kamion->name = 'Hors Kamions';
        $kamion->description = 'Hors Kamions';
        $kamion->wana_id = '00001';
        $kamion->wana_created_at = date('Y-m-d');
        $kamion->length = rand(10, 25).'m';
        $kamion->coeff_mp = rand(90, 115) / 100;
        $kamion->amount = 1;
        $kamion->model = '64047; 64552';
        $kamion->planned_time = rand(100, 200) * 10;
        $kamion->worked_time = rand(10, 2000) % $kamion->planned_time;
        $kamion->delivery = '2018-04-14';
        $kamion->ordered_at = '2017-09-13';
        $kamion->hourly_rate = 59;
        $kamion->client = "EPICURIS";
        $kamion->destination = 'Belgique';
        $kamion->save();
        $column = new App\Api\v1\Models\Column;
        $column->kamion_id = $kamion->id;
        $column->numbering = 1;
        $column->reference = 0;
        $column->content = 1;
        $column->supplement = 1;
        $column->crecre = 0;
        $column->upupup = 1;
        $column->client_requirement = 1;
        $column->type = 0;
        $column->status = 1;
        $column->save();

        $kamion = new App\Api\v1\Models\Kamion;
        $kamion->image = 'demo.jpg';
        $kamion->category_id = 1;
        $kamion->type = 1;
        $kamion->stage_id = rand(2,3);
        $kamion->is_locked = 0;
        $kamion->is_delivered = 0;
        $kamion->name = 'UNITE TRANSFO-VIANDE';
        $kamion->description = 'UNITE TRANSFO-VIANDE';
        $kamion->wana_id = '27249';
        $kamion->wana_created_at = date('Y-m-d');
        $kamion->length = rand(10, 25).'m';
        $kamion->coeff_mp = rand(90, 115) / 100;
        $kamion->amount = 1;
        $kamion->model = '64047; 64552';
        $kamion->planned_time = rand(100, 200) * 10;
        $kamion->worked_time = rand(10, 2000) % $kamion->planned_time;
        $kamion->delivery = '2018-04-14';
        $kamion->ordered_at = '2017-09-13';
        $kamion->hourly_rate = 59;
        $kamion->client = "EPICURIS";
        $kamion->destination = 'Belgique';
        $kamion->save();
        $column = new App\Api\v1\Models\Column;
        $column->kamion_id = $kamion->id;
        $column->numbering = 1;
        $column->reference = 0;
        $column->content = 1;
        $column->supplement = 1;
        $column->crecre = 0;
        $column->upupup = 1;
        $column->client_requirement = 1;
        $column->type = 0;
        $column->status = 1;
        $column->save();
		$names = array(
			"HERMEX - 20 PANNEAUX MOUSSE PU",
			"LBT - CEPHEID - SHOW ROOM    (FRNE)",
			"DBT - ACMS UNITE SANTE AU TRAVAIL (FRNE)",
			"DBT - EPICURIS FORMATION BOULANGERIE (FRNE)",
			"THALES ALENIA SPACE - STATION MOBILE X 2 (ERAS)",
			"CHASE CINEMOBILE THAILANDE   (ERAS)",
			"DBT - MOBILE RESTAURANT UP   (ERAS)",
			"EPSILON CLINO MOBILE X 2",
			"REP SDPV - GRS BORDEAU",
			"HORIZON SANTE TRAVAIL EX SMIROP PC 5 1",
			"REP CIAMT 3T5 - 175 RDW 75",
			"CMIE - 554 QBD 75 REP CABLE COUPE",
			"BTC CLINIQUE DAMMAN (YOJO)",
			"AIRTEL - PORTEUR ROAD-SHOW (RDC)",
			"REP SDPV - GRS COFAT",
			"BTC DON DU SANG 3 LITS JEDDAH (BRDU)"
		);
		$wana_id = array(
			"27267",
			"47363",
			"37267",
			"37281",
			"26006",
			"24356",
			"22254",
			"22254",
			"17552",
			"47346",
			"65468",
			"78449",
			"78450",
			"78455",
			"78492",
			"78476"
		);
		$description = array(
			"Ce restaurant mobile est composé d’un ensemble de deux semi-remorques pouvant recevoir et offrir un service
de restauration complet de grande qualité à 200 personnes simultanément dans un environnement haut de gamme
particulièrement confortable répondant aux règles d’hygiène et de sécurité les plus draconiennes",
			"Cette unité mobile showroom a été spécialement étudiée et réalisée pour notre
client MOBITEC, leader de la fabrication de mobilier haut de gamme. Ce showroom a pour
vocation d’aller au-devant des clients et prospects de MOBITEC pour promouvoir les nouvelles gammes.",
			"Elaborée pour SMTVO (Santé et Médecine du Travail de la Vallée de l’Oise) à Compiègne,
elle sillonne les routes du département de l’Oise pour aller vers les salariés des Entreprises.",
			"Cette unité de formation à la Boulangerie, livrée en 2014 à la société FRANCE FORMATIONS,
dispose d’un aménagement optimisé aux dimensions compactes pour permettre une circulation
aisée et un stationnement facile sur les sites de formation.",
			"Dernier né de la flotte des cars régie HD d’EUROMEDIA, livré en 2015, ce car de taille intermédiaire a été conçu
conjointement avec les équipes d’EUROMEDIA France et de Project Builders pour assurer des dispositifs
HF mais aussi des prestations multi-caméras classiques.",
			"Semi-remorque inclinée avec trois essieux, empattement 7,13 m - 30 Tonnes.
La carrosserie est aménagée de façon à intégrer 2 extensions hydrauliques latérales.",
			"Ce restaurant mobile est composé d’un ensemble de deux semi-remorques pouvant recevoir et offrir un service
de restauration complet de grande qualité à 200 personnes simultanément dans un environnement haut de gamme
particulièrement confortable répondant aux règles d’hygiène et de sécurité les plus draconiennes",
			"Ces quatre unités dentaires sur châssis poids-lourd de PTAC 17 tonnes, totalement autonomes,
ont été réalisées pour le compte du ministère de la santé des Emirats Arabes Unis pour aller
au-devant de la population pour lui prodiguer tous les soins dentaires de premier niveau nécessaires.",
			"Ce Motor-home à double étages a été conçu et réalisé en moins de 8 mois par nos équipes pour le compte de Renault F1.
Son système de déploiement vertical est unique et sa simplicité de fonctionnement permet une mise en œuvre particulièrement fiable en quelques minutes seulement.
Sa coque supérieure monobloc en matériaux composites assure un niveau de confort inégalé aux ingénieurs qui opèrent depuis le 1er étage.",
			"Elle a été spécialement étudiée et réalisée pour le Ministère de la Santé du Mali.
Elle est conçue pour des campagnes intenses de Dépistage mais aussi de Prévention.",
			"Cette unité mobile showroom a été spécialement étudiée et réalisée pour notre
client MOBITEC, leader de la fabrication de mobilier haut de gamme. Ce showroom a pour
vocation d’aller au-devant des clients et prospects de MOBITEC pour promouvoir les nouvelles gammes.",
			"Elaborée pour SMTVO (Santé et Médecine du Travail de la Vallée de l’Oise) à Compiègne,
elle sillonne les routes du département de l’Oise pour aller vers les salariés des Entreprises.",
			"Cette unité de formation à la Boulangerie, livrée en 2014 à la société FRANCE FORMATIONS,
dispose d’un aménagement optimisé aux dimensions compactes pour permettre une circulation
aisée et un stationnement facile sur les sites de formation.",
			"Dernier né de la flotte des cars régie HD d’EUROMEDIA, livré en 2015, ce car de taille intermédiaire a été conçu
conjointement avec les équipes d’EUROMEDIA France et de Project Builders pour assurer des dispositifs
HF mais aussi des prestations multi-caméras classiques.",
			"Semi-remorque inclinée avec trois essieux, empattement 7,13 m - 30 Tonnes.
La carrosserie est aménagée de façon à intégrer 2 extensions hydrauliques latérales.",
			"Ce restaurant mobile est composé d’un ensemble de deux semi-remorques pouvant recevoir et offrir un service
de restauration complet de grande qualité à 200 personnes simultanément dans un environnement haut de gamme
particulièrement confortable répondant aux règles d’hygiène et de sécurité les plus draconiennes"
		);
		for ($i=1; $i < 17; $i++) {
			$kamion = new App\Api\v1\Models\Kamion;
			$kamion->image = $i.'.jpg';
			$kamion->category_id = 1;
			$kamion->type = 1;
			$kamion->stage_id = rand(2,3);
			$kamion->is_locked = 0;
			$kamion->is_delivered = 0;
			if (isset($names[$i - 1])){
				$kamion->name = $names[$i - 1];
			}else{
				$kamion->name = 'kamion name';
			}
			if (isset($description[$i - 1])){
				$kamion->description = $description[$i - 1];
			}else{
				$kamion->description = 'kamion description';
			}
			if (isset($wana_id[$i - 1])){
				$kamion->wana_id = $wana_id[$i - 1];
			}else{
				$kamion->wana_id = 'wana id';
			}
			$kamion->wana_created_at = date('Y-m-d');
			$kamion->length = rand(10, 25).'m';
			$kamion->coeff_mp = rand(90, 115) / 100;
			$kamion->amount = rand(1, 5);
			$kamion->planned_time = rand(100, 200) * 10;
			$kamion->worked_time = rand(10, 2000) % $kamion->planned_time;
			$kamion->delivery = date('Y-m-d', $faker->unixTime(157762800) + time());
			$kamion->ordered_at = date('Y-m-d', time() - $faker->unixTime(31532400));
			$kamion->hourly_rate = 59;
			$kamion->client = "Burger King";
			$kamion->destination = 'france';
			$kamion->save();
            $column = new App\Api\v1\Models\Column;
            $column->kamion_id = $kamion->id;
            $column->numbering = 0;
            $column->reference = 0;
            $column->content = 1;
            $column->supplement = 1;
            $column->crecre = 0;
            $column->upupup = 1;
            $column->client_requirement = 1;
            $column->type = 0;
            $column->status = 1;
            $column->save();
		}
    }
}
