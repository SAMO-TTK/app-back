
<?php

use Illuminate\Database\Seeder;

class Autolast_documentsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('last_documents')->insert([
			'created_at' => '2018-06-05 12:36:41',
			'updated_at' => '2018-06-05 12:36:41',
			'user_id' => '1',
			'document_id' => '256',
		]);
		DB::table('last_documents')->insert([
			'created_at' => '2018-06-05 13:36:20',
			'updated_at' => '2018-06-05 13:36:20',
			'user_id' => '4',
			'document_id' => '264',
		]);
		DB::table('last_documents')->insert([
			'created_at' => '2018-06-05 13:36:46',
			'updated_at' => '2018-06-05 13:36:46',
			'user_id' => '4',
			'document_id' => '271',
		]);
    }
}