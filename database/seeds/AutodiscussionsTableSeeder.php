
<?php

use Illuminate\Database\Seeder;

class AutodiscussionsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('discussions')->insert([
			'created_at' => '2018-06-04 08:50:47',
			'updated_at' => '2018-06-05 12:33:39',
			'content' => 'lapinmalin',
			'deadline' => '2018-06-19 00:00:00',
			'is_open' => '0',
			'from_client' => '0',
			'element_id' => '916',
		]);
		DB::table('discussions')->insert([
			'created_at' => '2018-06-05 12:26:17',
			'updated_at' => '2018-06-05 12:27:22',
			'content' => 'dsqdazezaeza',
			'deadline' => '2018-06-13 00:00:00',
			'is_open' => '0',
			'from_client' => '1',
			'element_id' => '925',
		]);
    }
}