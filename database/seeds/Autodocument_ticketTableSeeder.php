
<?php

use Illuminate\Database\Seeder;

class Autodocument_ticketTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '1',
			'ticket_id' => '20023',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '2',
			'ticket_id' => '20042',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '3',
			'ticket_id' => '20042',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '4',
			'ticket_id' => '20044',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '5',
			'ticket_id' => '20044',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '6',
			'ticket_id' => '20044',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '7',
			'ticket_id' => '20044',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '8',
			'ticket_id' => '20044',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '9',
			'ticket_id' => '20062',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '10',
			'ticket_id' => '20062',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '11',
			'ticket_id' => '20062',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '12',
			'ticket_id' => '20062',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '13',
			'ticket_id' => '20062',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '14',
			'ticket_id' => '20062',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '15',
			'ticket_id' => '20096',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '16',
			'ticket_id' => '20107',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '17',
			'ticket_id' => '20107',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '18',
			'ticket_id' => '20112',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '19',
			'ticket_id' => '20119',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '20',
			'ticket_id' => '20119',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '21',
			'ticket_id' => '20161',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '22',
			'ticket_id' => '20161',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '23',
			'ticket_id' => '20161',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '24',
			'ticket_id' => '20175',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '25',
			'ticket_id' => '20184',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '26',
			'ticket_id' => '20195',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '27',
			'ticket_id' => '20208',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '28',
			'ticket_id' => '20208',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '29',
			'ticket_id' => '20208',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '30',
			'ticket_id' => '20217',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '31',
			'ticket_id' => '20217',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '32',
			'ticket_id' => '20217',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '33',
			'ticket_id' => '20241',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '34',
			'ticket_id' => '20241',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '35',
			'ticket_id' => '20241',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '36',
			'ticket_id' => '20241',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '37',
			'ticket_id' => '20271',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '38',
			'ticket_id' => '20279',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '39',
			'ticket_id' => '20288',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '40',
			'ticket_id' => '20288',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '41',
			'ticket_id' => '20288',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '42',
			'ticket_id' => '20288',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '43',
			'ticket_id' => '20288',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '44',
			'ticket_id' => '20293',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '45',
			'ticket_id' => '20293',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '46',
			'ticket_id' => '20310',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '47',
			'ticket_id' => '20310',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '48',
			'ticket_id' => '20319',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '49',
			'ticket_id' => '20420',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '50',
			'ticket_id' => '20442',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:46',
			'updated_at' => '2018-05-15 13:17:46',
			'document_id' => '51',
			'ticket_id' => '20442',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '52',
			'ticket_id' => '20442',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '53',
			'ticket_id' => '20469',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '54',
			'ticket_id' => '20469',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '55',
			'ticket_id' => '20476',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '56',
			'ticket_id' => '20539',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '57',
			'ticket_id' => '20552',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '58',
			'ticket_id' => '20655',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '59',
			'ticket_id' => '20655',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '60',
			'ticket_id' => '20662',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '61',
			'ticket_id' => '20667',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '62',
			'ticket_id' => '20717',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '63',
			'ticket_id' => '20771',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '64',
			'ticket_id' => '20798',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '65',
			'ticket_id' => '20902',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '66',
			'ticket_id' => '20910',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '67',
			'ticket_id' => '20912',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '68',
			'ticket_id' => '20912',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '69',
			'ticket_id' => '20912',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '70',
			'ticket_id' => '20912',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '71',
			'ticket_id' => '20912',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '72',
			'ticket_id' => '20912',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '73',
			'ticket_id' => '20920',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '74',
			'ticket_id' => '20920',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '75',
			'ticket_id' => '20922',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '76',
			'ticket_id' => '20958',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '77',
			'ticket_id' => '20958',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '78',
			'ticket_id' => '20958',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '79',
			'ticket_id' => '21104',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '80',
			'ticket_id' => '21104',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '81',
			'ticket_id' => '21104',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '82',
			'ticket_id' => '21132',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '83',
			'ticket_id' => '21132',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '84',
			'ticket_id' => '21137',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '85',
			'ticket_id' => '21144',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '86',
			'ticket_id' => '21146',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '87',
			'ticket_id' => '21146',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '88',
			'ticket_id' => '21146',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '89',
			'ticket_id' => '21147',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '90',
			'ticket_id' => '21168',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '91',
			'ticket_id' => '21191',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '92',
			'ticket_id' => '21191',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '93',
			'ticket_id' => '21191',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '94',
			'ticket_id' => '21231',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '95',
			'ticket_id' => '21252',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '96',
			'ticket_id' => '21283',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '97',
			'ticket_id' => '21283',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '98',
			'ticket_id' => '21283',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '99',
			'ticket_id' => '21283',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '100',
			'ticket_id' => '21283',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '101',
			'ticket_id' => '21284',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '102',
			'ticket_id' => '21307',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '103',
			'ticket_id' => '21316',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '104',
			'ticket_id' => '21347',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '105',
			'ticket_id' => '21430',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '106',
			'ticket_id' => '21431',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '107',
			'ticket_id' => '21451',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '108',
			'ticket_id' => '21459',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '109',
			'ticket_id' => '21466',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '110',
			'ticket_id' => '21483',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '111',
			'ticket_id' => '21483',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '112',
			'ticket_id' => '21483',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '113',
			'ticket_id' => '21486',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '114',
			'ticket_id' => '21497',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '115',
			'ticket_id' => '21524',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '116',
			'ticket_id' => '21524',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '117',
			'ticket_id' => '21524',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '118',
			'ticket_id' => '21545',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '119',
			'ticket_id' => '21547',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '120',
			'ticket_id' => '21547',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '121',
			'ticket_id' => '21547',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '122',
			'ticket_id' => '21547',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '123',
			'ticket_id' => '21556',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '124',
			'ticket_id' => '21603',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '125',
			'ticket_id' => '21638',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '126',
			'ticket_id' => '21638',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '127',
			'ticket_id' => '21692',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '128',
			'ticket_id' => '21704',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '129',
			'ticket_id' => '21704',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '130',
			'ticket_id' => '21704',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '131',
			'ticket_id' => '21706',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '132',
			'ticket_id' => '21706',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '133',
			'ticket_id' => '21773',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:47',
			'updated_at' => '2018-05-15 13:17:47',
			'document_id' => '134',
			'ticket_id' => '21783',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '135',
			'ticket_id' => '21792',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '136',
			'ticket_id' => '21795',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '137',
			'ticket_id' => '21806',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '138',
			'ticket_id' => '21806',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '139',
			'ticket_id' => '21809',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '140',
			'ticket_id' => '21809',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '141',
			'ticket_id' => '21810',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '142',
			'ticket_id' => '21819',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '143',
			'ticket_id' => '21819',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '144',
			'ticket_id' => '21819',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '145',
			'ticket_id' => '21867',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '146',
			'ticket_id' => '21885',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '147',
			'ticket_id' => '21896',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '148',
			'ticket_id' => '21896',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '149',
			'ticket_id' => '21896',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '150',
			'ticket_id' => '21896',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '151',
			'ticket_id' => '21896',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '152',
			'ticket_id' => '21896',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '153',
			'ticket_id' => '21896',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '154',
			'ticket_id' => '21962',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '155',
			'ticket_id' => '21967',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '156',
			'ticket_id' => '21988',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '157',
			'ticket_id' => '21988',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '158',
			'ticket_id' => '21988',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '159',
			'ticket_id' => '21988',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '160',
			'ticket_id' => '21988',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '161',
			'ticket_id' => '21988',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '162',
			'ticket_id' => '22008',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '163',
			'ticket_id' => '22008',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '164',
			'ticket_id' => '22029',
		]);
		DB::table('document_ticket')->insert([
			'created_at' => '2018-05-15 13:17:48',
			'updated_at' => '2018-05-15 13:17:48',
			'document_id' => '165',
			'ticket_id' => '22091',
		]);
    }
}