
<?php

use Illuminate\Database\Seeder;

class Automodel_fieldsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Informations générales',
			'type' => 'title',
			'position' => '1',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Question de départ',
			'type' => 'text',
			'position' => '2',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Vrai question',
			'type' => 'string',
			'position' => '3',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Enquête complète',
			'type' => 'bool',
			'position' => '4',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Qui doit apporter son point de vue ?',
			'type' => 'selectN',
			'position' => '5',
			'depend' => '4',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Avantages inconvénients',
			'type' => 'title',
			'position' => '6',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Ava inc',
			'type' => 'tableR',
			'position' => '7',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Décision',
			'type' => 'title',
			'position' => '8',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Contenu de la décision',
			'type' => 'text',
			'position' => '9',
			'model_id' => '2',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Ordre du jour',
			'type' => 'string',
			'position' => '1',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Liste des membres',
			'type' => 'selectN',
			'position' => '2',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Point Commercial',
			'type' => 'text',
			'position' => '3',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Prix de revient',
			'type' => 'string',
			'position' => '4',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'TTK Client',
			'type' => 'text',
			'position' => '5',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'TTK revue de Direction',
			'type' => 'text',
			'position' => '6',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Nouveau produit / nouveau fournisseur',
			'type' => 'string',
			'position' => '7',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'TTK environnementales',
			'type' => 'text',
			'position' => '8',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'TTK comportmentales ',
			'type' => 'text',
			'position' => '9',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'TTK Sécurité ',
			'type' => 'text',
			'position' => '10',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Divers',
			'type' => 'text',
			'position' => '11',
			'model_id' => '3',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Numéro camion',
			'type' => 'string',
			'position' => '1',
			'model_id' => '4',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Nom du camion',
			'type' => 'string',
			'position' => '2',
			'model_id' => '4',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Date',
			'type' => 'date',
			'position' => '3',
			'model_id' => '4',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Heure',
			'type' => 'string',
			'position' => '4',
			'model_id' => '4',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Informer',
			'type' => 'text',
			'position' => '5',
			'model_id' => '4',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Observations',
			'type' => 'text',
			'position' => '6',
			'model_id' => '4',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Documentation',
			'type' => 'text',
			'position' => '7',
			'model_id' => '4',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Numéro camion',
			'type' => 'string',
			'position' => '1',
			'model_id' => '5',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Nom du camion',
			'type' => 'string',
			'position' => '2',
			'model_id' => '5',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Date',
			'type' => 'date',
			'position' => '3',
			'model_id' => '5',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Heure',
			'type' => 'string',
			'position' => '4',
			'model_id' => '5',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Informer',
			'type' => 'text',
			'position' => '5',
			'model_id' => '5',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Observations',
			'type' => 'text',
			'position' => '6',
			'model_id' => '5',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Documentation',
			'type' => 'text',
			'position' => '7',
			'model_id' => '5',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Liste des contrôles',
			'type' => 'title',
			'position' => '1',
			'model_id' => '6',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Ava inc',
			'type' => 'tableR',
			'position' => '2',
			'model_id' => '6',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Action de réparation',
			'type' => 'text',
			'position' => '3',
			'model_id' => '6',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Relance à',
			'type' => 'string',
			'position' => '4',
			'model_id' => '6',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Vu au Comité Qualité du',
			'type' => 'date',
			'position' => '5',
			'model_id' => '6',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Réunion le',
			'type' => 'date',
			'position' => '1',
			'model_id' => '7',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Heure et lieu',
			'type' => 'string',
			'position' => '2',
			'model_id' => '7',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Participants',
			'type' => 'selectN',
			'position' => '3',
			'model_id' => '7',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Compte rendu',
			'type' => 'text',
			'position' => '4',
			'model_id' => '7',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Relance à',
			'type' => 'string',
			'position' => '5',
			'model_id' => '7',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Vu au Comité Qualité du',
			'type' => 'date',
			'position' => '6',
			'model_id' => '7',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Information NCSE',
			'type' => 'text',
			'position' => '1',
			'model_id' => '8',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Constat',
			'type' => 'string',
			'position' => '1',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Analyse',
			'type' => 'string',
			'position' => '2',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Action Réparation',
			'type' => 'string',
			'position' => '3',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Cout MO',
			'type' => 'string',
			'position' => '4',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Cout MP',
			'type' => 'string',
			'position' => '5',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Risques et opportunités',
			'type' => 'text',
			'position' => '6',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Action Corrective',
			'type' => 'text',
			'position' => '7',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Mesure efficacité',
			'type' => 'text',
			'position' => '8',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Relance',
			'type' => 'string',
			'position' => '9',
			'model_id' => '9',
		]);
		DB::table('model_fields')->insert([
			'created_at' => '2018-05-15 10:19:44',
			'updated_at' => '2018-05-15 10:19:44',
			'label' => 'Vu au Comité Qualité du',
			'type' => 'date',
			'position' => '10',
			'model_id' => '9',
		]);
    }
}