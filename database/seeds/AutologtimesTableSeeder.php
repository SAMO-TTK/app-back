
<?php

use Illuminate\Database\Seeder;

class AutologtimesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('logtimes')->insert([
			'created_at' => '2018-05-29 10:10:10',
			'date' => '2018-05-18',
			'length' => '5.00',
			'user_id' => '1',
			'status_logtime_id' => '2',
			'kamion_id' => '2',
			'instruction_id' => '2',
		]);
		DB::table('logtimes')->insert([
			'created_at' => '2018-06-05 09:50:00',
			'updated_at' => '2018-06-05 09:50:00',
			'date' => '2018-06-05',
			'length' => '4.00',
			'user_id' => '1',
			'status_logtime_id' => '3',
			'kamion_id' => '2',
			'instruction_id' => '3',
		]);
		DB::table('logtimes')->insert([
			'created_at' => '2018-06-05 09:52:47',
			'updated_at' => '2018-06-05 09:52:47',
			'date' => '2018-06-05',
			'length' => '3.00',
			'user_id' => '1',
			'status_logtime_id' => '4',
			'kamion_id' => '2',
			'instruction_id' => '4',
		]);
		DB::table('logtimes')->insert([
			'created_at' => '2018-06-05 09:54:33',
			'updated_at' => '2018-06-05 09:54:33',
			'date' => '2018-06-05',
			'length' => '3.00',
			'user_id' => '1',
			'status_logtime_id' => '5',
			'kamion_id' => '2',
			'instruction_id' => '5',
		]);
		DB::table('logtimes')->insert([
			'created_at' => '2018-06-05 09:58:22',
			'updated_at' => '2018-06-05 09:58:22',
			'date' => '2018-06-12',
			'length' => '4.00',
			'user_id' => '1',
			'status_logtime_id' => '6',
			'kamion_id' => '2',
			'instruction_id' => '6',
		]);
		DB::table('logtimes')->insert([
			'created_at' => '2018-06-05 09:59:11',
			'updated_at' => '2018-06-05 09:59:11',
			'date' => '2018-06-05',
			'length' => '2.00',
			'user_id' => '1',
			'status_logtime_id' => '6',
			'kamion_id' => '2',
			'instruction_id' => '6',
		]);
		DB::table('logtimes')->insert([
			'created_at' => '2018-06-05 10:02:16',
			'updated_at' => '2018-06-05 10:02:16',
			'date' => '2018-06-05',
			'length' => '21.00',
			'user_id' => '1',
			'status_logtime_id' => '1',
			'kamion_id' => '2',
			'instruction_id' => '1',
		]);
		DB::table('logtimes')->insert([
			'created_at' => '2018-06-05 10:04:46',
			'updated_at' => '2018-06-05 10:04:46',
			'date' => '2018-06-05',
			'length' => '18.00',
			'user_id' => '1',
			'status_logtime_id' => '1',
			'kamion_id' => '2',
			'instruction_id' => '1',
		]);
		DB::table('logtimes')->insert([
			'created_at' => '2018-06-05 10:06:43',
			'updated_at' => '2018-06-05 10:06:43',
			'date' => '2018-06-05',
			'length' => '40.00',
			'user_id' => '1',
			'status_logtime_id' => '1',
			'kamion_id' => '2',
			'instruction_id' => '1',
		]);
    }
}