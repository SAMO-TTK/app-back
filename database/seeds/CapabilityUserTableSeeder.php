<?php

use Illuminate\Database\Seeder;

class CapabilityUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 18; $i++) :
		$ra = rand(1,8);
		$rb = rand(1,7);
			if ($ra != $rb):
		        DB::table('capability_user')->insert([
		            'capability_id' => $ra, 'user_id' => $rb, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
		        ]);
			endif;
		endfor;
    }
}
