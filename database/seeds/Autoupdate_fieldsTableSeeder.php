
<?php

use Illuminate\Database\Seeder;

class Autoupdate_fieldsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('update_fields')->insert([
			'created_at' => '2018-05-15 13:20:45',
			'updated_at' => '2018-05-15 13:20:45',
			'cat_id' => '11',
			'notification_ticket_id' => '5',
		]);
		DB::table('update_fields')->insert([
			'created_at' => '2018-06-04 11:47:31',
			'updated_at' => '2018-06-04 11:47:31',
			'cat_id' => '11',
			'notification_ticket_id' => '6',
		]);
		DB::table('update_fields')->insert([
			'created_at' => '2018-06-04 13:40:56',
			'updated_at' => '2018-06-04 13:40:56',
			'cat_id' => '5',
			'notification_ticket_id' => '7',
		]);
    }
}