<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(AutorolesTableSeeder::Class);
		$this->call(AutousersTableSeeder::Class);
		$this->call(AutopermissionsTableSeeder::Class);
		$this->call(Autopermission_roleTableSeeder::Class);
		$this->call(Autorole_userTableSeeder::Class);
		$this->call(AutostagesTableSeeder::Class);

		$this->call(AutocategoriesTableSeeder::Class);
		$this->call(AutokamionsTableSeeder::Class);
		$this->call(Autolinked_projectsTableSeeder::Class);
		$this->call(Autokamion_userTableSeeder::Class);
		$this->call(Autoticket_modelsTableSeeder::Class);
		$this->call(Automodel_fieldsTableSeeder::Class);
		$this->call(Autoticket_categoriesTableSeeder::Class);
		$this->call(AutocolumnsTableSeeder::Class);
		$this->call(Autoticket_statusTableSeeder::Class);

//		$this->call(TicketsTableSeeder::Class);
//		$this->call(AutoTableSeeder::class);
	}
}
