<?php

use Illuminate\Database\Seeder;

class ValidationDocumentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 5; $i++) :
        $ra = rand(1,10);
        $rb = rand(1,11);
            if ($ra != $rb):
                DB::table('validation_document')->insert([
                    'control_id' => $ra, 'document_id' => $rb, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
                ]);
            endif;
        endfor;
    }
}
