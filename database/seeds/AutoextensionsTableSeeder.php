
<?php

use Illuminate\Database\Seeder;

class AutoextensionsTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('extensions')->insert([
			'created_at' => '2018-05-15 13:08:57',
			'updated_at' => '2018-05-15 13:08:57',
			'label' => 'png',
			'groupe_id' => '1',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'DWG',
			'groupe_id' => '2',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'pdf',
			'groupe_id' => '3',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'err',
			'groupe_id' => '5',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'log',
			'groupe_id' => '4',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-05-17 12:11:27',
			'updated_at' => '2018-05-17 12:11:27',
			'label' => 'dxf',
			'groupe_id' => '6',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-05-17 12:11:28',
			'updated_at' => '2018-05-17 12:11:28',
			'label' => 'JPG',
			'groupe_id' => '7',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-05-17 12:11:29',
			'updated_at' => '2018-05-17 12:11:29',
			'label' => 'zip',
			'groupe_id' => '8',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-06-05 12:29:15',
			'updated_at' => '2018-06-05 12:29:15',
			'label' => 'doc',
			'groupe_id' => '9',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-06-05 12:29:16',
			'updated_at' => '2018-06-05 12:29:16',
			'label' => 'xlsx',
			'groupe_id' => '10',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-06-05 12:29:16',
			'updated_at' => '2018-06-05 12:29:16',
			'label' => 'msg',
			'groupe_id' => '11',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-06-05 13:36:18',
			'updated_at' => '2018-06-05 13:36:18',
			'label' => 'svg',
			'groupe_id' => '12',
		]);
		DB::table('extensions')->insert([
			'created_at' => '2018-06-05 13:36:18',
			'updated_at' => '2018-06-05 13:36:18',
			'label' => 'xlsm',
			'groupe_id' => '13',
		]);
    }
}