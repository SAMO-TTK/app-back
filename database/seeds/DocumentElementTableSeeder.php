<?php

use Illuminate\Database\Seeder;

class DocumentElementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 8; $i++) :
		$ra = rand(1,11);
		$rb = rand(1,12);
			if ($ra != $rb):
		        DB::table('document_element')->insert([
		            'document_id' => $ra, 'element_id' => $rb, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
		        ]);
			endif;
		endfor;
    }
}
