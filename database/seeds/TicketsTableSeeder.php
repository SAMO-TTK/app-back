<?php

use Illuminate\Database\Seeder;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$status = new App\Api\v1\Models\Ticket_status;
		$status->label = 'Brouillon';
		$status->save();
		$status = new App\Api\v1\Models\Ticket_status;
		$status->label = 'Relecture';
		$status->save();
		$status = new App\Api\v1\Models\Ticket_status;
		$status->label = 'Ouvert';
		$status->save();
		$status = new App\Api\v1\Models\Ticket_status;
		$status->label = 'Archiver';
		$status->save();

//		$status = new App\Api\v1\Models\Ticket_status;
//		$status->label = 'Annuler';
//		$status->save();



		$category = new App\Api\v1\Models\Ticket_category();
		$category->label = 'Achats';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Action Corrective';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Action Préventive';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Amélioration Continue';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Aménagements';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Analyse des Risques et Opportunités';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Assurances';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Audit Procédures (PR/INST)';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Audit Processus';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Divers';
		$category->save();
		$category = new App\Api\v1\Models\Ticket_category;
		$category->label = 'Commercial';
		$category->save();


		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'Import';
		$model->save();

		#region - Décideuse

		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'Décideuse';
		$model->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Informations générales';
		$field->type = 'title';
		$field->position = 1;
		$field->model_id = 2;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Question de départ';
		$field->type = 'text';
		$field->position = 2;
		$field->model_id = 2;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Vrai question';
		$field->type = 'string';
		$field->position = 3;
		$field->model_id = 2;
		$field->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Enquête complète';
		$field->type = 'bool';
		$field->position = 4;
		$field->model_id = 2;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Qui doit apporter son point de vue ?';
		$field->type = 'selectN';
		$field->position = 5;
		$field->depend = 4;
		$field->model_id = 2;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Avantages inconvénients';
		$field->type = 'title';
		$field->position = 6;
		$field->model_id = 2;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Ava inc';
		$field->type = 'tableR';
		$field->position = 7;
		$field->model_id = 2;
		$field->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "AVANTAGES";
		$value->field_id = $field->id;
		$value->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "INCONVENIENTS";
		$value->field_id = $field->id;
		$value->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Décision';
		$field->type = 'title';
		$field->position = 8;
		$field->model_id = 2;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Contenu de la décision';
		$field->type = 'text';
		$field->position = 9;
		$field->model_id = 2;
		$field->save();
		#endregion

		#region - Comité Qualité

		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'Comité Qualité';
		$model->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Ordre du jour';
		$field->type = 'string';
		$field->position = 1;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Liste des membres';
		$field->type = 'selectN';
		$field->position = 2;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Point Commercial';
		$field->type = 'text';
		$field->position = 3;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Prix de revient';
		$field->type = 'string';
		$field->position = 4;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'TTK Client';
		$field->type = 'text';
		$field->position = 5;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'TTK revue de Direction';
		$field->type = 'text';
		$field->position = 6;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Nouveau produit / nouveau fournisseur';
		$field->type = 'string';
		$field->position = 7;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'TTK environnementales';
		$field->type = 'text';
		$field->position = 8;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'TTK comportmentales ';
		$field->type = 'text';
		$field->position = 9;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'TTK Sécurité ';
		$field->type = 'text';
		$field->position = 10;
		$field->model_id = 3;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Divers';
		$field->type = 'text';
		$field->position = 11;
		$field->model_id = 3;
		$field->save();

		#endregion

		#region - CONTROLE DE CONFORMITE INITIAL

		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'CONTROLE DE CONFORMITE INITIAL';
		$model->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Numéro camion';
		$field->type = 'string';
		$field->position = 1;
		$field->model_id = 4;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Nom du camion';
		$field->type = 'string';
		$field->position = 2;
		$field->model_id = 4;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Date';
		$field->type = 'date';
		$field->position = 3;
		$field->model_id = 4;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Heure';
		$field->type = 'string';
		$field->position = 4;
		$field->model_id = 4;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Informer';
		$field->type = 'text';
		$field->position = 5;
		$field->model_id = 4;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Observations';
		$field->type = 'text';
		$field->position = 6;
		$field->model_id = 4;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Documentation';
		$field->type = 'text';
		$field->position = 7;
		$field->model_id = 4;
		$field->save();

		#endregion

		#region - Passages aux Mines

		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'Passages aux Mines';
		$model->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Numéro camion';
		$field->type = 'string';
		$field->position = 1;
		$field->model_id = 5;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Nom du camion';
		$field->type = 'string';
		$field->position = 2;
		$field->model_id = 5;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Date';
		$field->type = 'date';
		$field->position = 3;
		$field->model_id = 5;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Heure';
		$field->type = 'string';
		$field->position = 4;
		$field->model_id = 5;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Informer';
		$field->type = 'text';
		$field->position = 5;
		$field->model_id = 5;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Observations';
		$field->type = 'text';
		$field->position = 6;
		$field->model_id = 5;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Documentation';
		$field->type = 'text';
		$field->position = 7;
		$field->model_id = 5;
		$field->save();

		#endregion

		#region - CONTROLE RECEPTION CHASSIS

		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'CONTROLE RECEPTION CHASSIS';
		$model->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Liste des contrôles';
		$field->type = 'title';
		$field->position = 1;
		$field->model_id = 6;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Ava inc';
		$field->type = 'tableR';
		$field->position = 2;
		$field->model_id = 6;
		$field->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "Date controle";
		$value->field_id = $field->id;
		$value->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "Nom chassis";
		$value->field_id = $field->id;
		$value->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "Marque et Type châssis";
		$value->field_id = $field->id;
		$value->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "Effectué par";
		$value->field_id = $field->id;
		$value->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "Constats";
		$value->field_id = $field->id;
		$value->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Action de réparation';
		$field->type = 'text';
		$field->position = 3;
		$field->model_id = 6;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Relance à';
		$field->type = 'string';
		$field->position = 4;
		$field->model_id = 6;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Vu au Comité Qualité du';
		$field->type = 'date';
		$field->position = 5;
		$field->model_id = 6;
		$field->save();


		#endregion

		#region - CR Réunion de lancement

		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'CR Réunion de lancement';
		$model->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Réunion le';
		$field->type = 'date';
		$field->position = 1;
		$field->model_id = 7;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Heure et lieu';
		$field->type = 'string';
		$field->position = 2;
		$field->model_id = 7;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Participants';
		$field->type = 'selectN';
		$field->position = 3;
		$field->model_id = 7;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Compte rendu';
		$field->type = 'text';
		$field->position = 4;
		$field->model_id = 7;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Relance à';
		$field->type = 'string';
		$field->position = 5;
		$field->model_id = 7;
		$field->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Vu au Comité Qualité du';
		$field->type = 'date';
		$field->position = 6;
		$field->model_id = 7;
		$field->save();

		#endregion

		#region - INFORMATIONS

		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'INFORMATIONS';
		$model->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Information NCSE';
		$field->type = 'text';
		$field->position = 1;
		$field->model_id = 8;
		$field->save();

		#endregion

		#region - STANDARD

		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'STANDARD';
		$model->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Constat';
		$field->type = 'string';
		$field->position = 1;
		$field->model_id = 9;
		$field->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Analyse';
		$field->type = 'text';
		$field->position = 2;
		$field->model_id = 9;
		$field->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Action Réparation';
		$field->type = 'text';
		$field->position = 3;
		$field->model_id = 9;
		$field->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Cout MO';
		$field->type = 'string';
		$field->position = 4;
		$field->model_id = 9;
		$field->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Cout MP';
		$field->type = 'string';
		$field->position = 5;
		$field->model_id = 9;
		$field->save();



		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Risques et opportunités';
		$field->type = 'tableR';
		$field->position = 6;
		$field->model_id = 9;
		$field->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "Risques";
		$value->field_id = $field->id;
		$value->save();

		$value = new App\Api\v1\Models\Field_value;
		$value->value = "opportunités";
		$value->field_id = $field->id;
		$value->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Action Corrective';
		$field->type = 'text';
		$field->position = 7;
		$field->model_id = 9;
		$field->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Mesure efficacité';
		$field->type = 'text';
		$field->position = 8;
		$field->model_id = 9;
		$field->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Relance';
		$field->type = 'string';
		$field->position = 9;
		$field->model_id = 9;
		$field->save();


		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Vu au Comité Qualité du';
		$field->type = 'date';
		$field->position = 10;
		$field->model_id = 9;
		$field->save();
		#endregion

		#region - Email
		$model = new App\Api\v1\Models\Ticket_model;
		$model->label = 'Email';
		$model->save();

		$field = new App\Api\v1\Models\Model_field;
		$field->label = 'Message';
		$field->type = 'text';
		$field->position = 1;
		$field->model_id = 10;
		$field->save();
		#endregion
    }
}
