
<?php

use Illuminate\Database\Seeder;

class AutocategoriesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('categories')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => 'Category label',
		]);
    }
}