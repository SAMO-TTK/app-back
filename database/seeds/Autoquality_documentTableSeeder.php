
<?php

use Illuminate\Database\Seeder;

class Autoquality_documentTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('quality_document')->insert([
			'created_at' => '2018-06-05 12:29:27',
			'updated_at' => '2018-06-05 12:29:27',
			'control_id' => '925',
			'document_id' => '252',
		]);
		DB::table('quality_document')->insert([
			'created_at' => '2018-06-05 20:46:58',
			'updated_at' => '2018-06-05 20:46:58',
			'control_id' => '916',
			'document_id' => '187',
		]);
		DB::table('quality_document')->insert([
			'created_at' => '2018-06-05 20:46:58',
			'updated_at' => '2018-06-05 20:46:58',
			'control_id' => '916',
			'document_id' => '198',
		]);
    }
}