<?php

use Illuminate\Database\Seeder;

class DiscussionUserTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::table('discussion_user')->insert([
			'discussion_id' => $ra, 'user_id' => $rb, 'is_author' => rand(0,1), 'is_concerned' => rand(0,1), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
		]);

	}
}
