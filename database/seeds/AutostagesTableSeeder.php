
<?php

use Illuminate\Database\Seeder;

class AutostagesTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('stages')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => '2:lancement',
			'wana_id' => 'sd6fg58hd35hg',
		]);
		DB::table('stages')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => '3:fabrication',
			'wana_id' => 'sd56fg8h1s6g',
		]);
		DB::table('stages')->insert([
			'created_at' => '2018-05-15 10:19:42',
			'updated_at' => '2018-05-15 10:19:42',
			'label' => '4:reception',
			'wana_id' => '6584dfth5d6gh6',
		]);
    }
}