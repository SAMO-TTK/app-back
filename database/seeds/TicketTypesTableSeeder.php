<?php

use Illuminate\Database\Seeder;

class TicketTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		for ($i=1; $i <= 30; $i++) :
			$rb = rand(1,2);
			DB::table('ticket_user')->insert([
				'ticket_id' => $i, 'user_id' => $rb, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
			]);
		endfor;
    }
}
