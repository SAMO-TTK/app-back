
<?php

use Illuminate\Database\Seeder;

class Autodocument_elementTableSeeder extends Seeder
{
    public function run()
    {

		DB::table('document_element')->insert([
			'created_at' => '2018-06-05 12:27:38',
			'updated_at' => '2018-06-05 12:27:38',
			'document_id' => '187',
			'element_id' => '925',
		]);
		DB::table('document_element')->insert([
			'created_at' => '2018-06-05 12:27:39',
			'updated_at' => '2018-06-05 12:27:39',
			'document_id' => '198',
			'element_id' => '925',
		]);
    }
}