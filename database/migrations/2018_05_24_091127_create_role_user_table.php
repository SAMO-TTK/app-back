<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_user', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            if(Schema::hasTable('users')){
            	$table->integer('user_id')->unsigned();
            	$table->foreign('user_id')->references('id')->on('users');
			}
            if(Schema::hasTable('roles')){
            	$table->integer('role_id')->unsigned();
            	$table->foreign('role_id')->references('id')->on('roles');
			}
        });
        Schema::table('users', function (Blueprint $table){
			$table->dropColumn('is_quality_responsable');
			$table->dropColumn('is_validation_responsable');
			$table->dropColumn('level');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_user');
    }
}
