<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_fields', function (Blueprint $table){
        	$table->increments('id');
        	$table->timestamps();
        	$table->softDeletes();
        	$table->text('value')->nullable();
        	if (Schema::hasTable('field_values')){
        		$table->integer('field_value_id')->unsigned()->nullable();
        		$table->foreign('field_value_id')->references('id')->on('field_values');
			}
        	if (Schema::hasTable('tickets')){
        		$table->integer('ticket_id')->unsigned();
        		$table->foreign('ticket_id')->references('id')->on('tickets');
			}
        	if (Schema::hasTable('model_fields')){
        		$table->integer('field_id')->unsigned();
        		$table->foreign('field_id')->references('id')->on('model_fields');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_fields');
    }
}
