<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update18062018Bis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logtimes', function (Blueprint $table){
        	$table->integer('kamion_id')->unsigned()->nullable()->change();
		});
        Schema::table('material_resources', function (Blueprint $table){
        	$table->string('unit_label');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
