<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussions', function (Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->text('content');
			$table->dateTime('deadline')->nullable();
			$table->tinyInteger('is_open');
			$table->tinyInteger('from_client');
            if(Schema::hasTable('elements')){
                $table->integer('element_id')->unsigned()->nullable();
                $table->foreign('element_id')->references('id')->on('elements');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussions');
    }
}
