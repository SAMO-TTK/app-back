<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('cat_id');
            $table->integer('sub_cat_id')->nullable();
            if (Schema::hasTable('notification_tickets')){
            	$table->integer('notification_ticket_id')->unsigned();
            	$table->foreign('notification_ticket_id')->references('id')->on('notification_tickets');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_fields');
    }
}
