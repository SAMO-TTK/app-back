<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussionDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussion_document', function (Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			if (Schema::hasTable('discussions')){
				$table->integer('discussion_id')->unsigned();
				$table->foreign('discussion_id')->references('id')->on('discussions');
			}
			if (Schema::hasTable('documents')){
				$table->integer('document_id')->unsigned();
				$table->foreign('document_id')->references('id')->on('documents');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussion_document');
    }
}
