<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->text('content');
            if(Schema::hasTable('users')){
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
            }
            if(Schema::hasTable('discussions')){
                $table->integer('discussion_id')->unsigned();
                $table->foreign('discussion_id')->references('id')->on('discussions');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
