<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->timestamp('last_connection')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username');
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('wana_id');
            $table->string('badge_id')->nullable();
            $table->string('smartphone_id')->nullable();
            $table->string('avatar')->nullable();
            $table->tinyInteger('is_active');
            $table->integer('level');
            $table->string('job')->nullable();
			$table->tinyInteger('is_quality_responsable')->nullable();
			$table->tinyInteger('is_validation_responsable')->nullable();
			$table->integer('manager_id')->unsigned()->nullable();
			$table->foreign('manager_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
