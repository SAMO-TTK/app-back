<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update14062018 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instructions', function (Blueprint $table){
        	if(Schema::hasTable('jobs')){
        		$table->integer('job_id')->unsigned();
        		$table->foreign('job_id')->references('id')->on('jobs');
			}
		});
        Schema::table('logtimes', function(Blueprint $table){
        	$table->text('remark')->nullable();
		});
        Schema::table('logtime_histories', function (Blueprint $table){
        	$table->string('update_name')->nullable()->change();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
