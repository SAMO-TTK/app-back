<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkedProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linked_projects', function(Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			$table->SoftDeletes();
			if(Schema::hasTable('kamions')){
				$table->integer('master_id')->unsigned();
				$table->foreign('master_id')->references('id')->on('kamions');
				$table->integer('linked_id')->unsigned();
				$table->foreign('linked_id')->references('id')->on('kamions');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linked_projects');
    }
}
