<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialResourceUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_resource_user', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			if(Schema::hasTable('material_resources')){
				$table->integer('material_resource_id')->unsigned();
				$table->foreign('material_resource_id')->references('id')->on('material_resources');
			}
			if(Schema::hasTable('users')){
				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_resource_user');
    }
}
