<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('ticket_messages', function (Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->text('content');
			$table->tinyInteger('is_pinned')->nullable();
			if (Schema::hasTable('users')){
				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
			}
			if (Schema::hasTable('ticket_discussions')){
				$table->integer('discussion_id')->unsigned();
				$table->foreign('discussion_id')->references('id')->on('ticket_discussions');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('ticket_messages');
    }
}
