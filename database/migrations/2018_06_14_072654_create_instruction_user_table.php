<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instruction_user', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            if(Schema::hasTable('instructions')){
            	$table->integer('instruction_id')->unsigned();
            	$table->foreign('instruction_id')->references('id')->on('instructions');
			}
            if(Schema::hasTable('users')){
            	$table->integer('user_id')->unsigned();
            	$table->foreign('user_id')->references('id')->on('users');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instruction_user');
    }
}
