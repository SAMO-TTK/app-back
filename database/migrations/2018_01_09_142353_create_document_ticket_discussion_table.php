<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTicketDiscussionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('document_ticket_discussion', function (Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			if (Schema::hasTable('documents')){
				$table->integer('document_id')->unsigned();
				$table->foreign('document_id')->references('id')->on('documents');
			}
			if (Schema::hasTable('ticket_discussions')){
				$table->integer('discussion_id')->unsigned();
				$table->foreign('discussion_id')->references('id')->on('ticket_discussions');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('document_ticket_discussion');
    }
}
