<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_notes', function (Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            if (Schema::hasTable('users')){
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
            }
            if (Schema::hasTable('notes')){
                $table->integer('note_id')->unsigned();
                $table->foreign('note_id')->references('id')->on('notes');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_notes');
    }
}
