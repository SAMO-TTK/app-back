<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('number')->nullable();
            $table->string('title');
            $table->tinyInteger('is_public');
            $table->dateTime('deadline')->nullable();
            $table->string('instruction_id')->nullable();
            $table->longText('import')->nullable();
            $table->tinyInteger('has_workgroup')->nullable();
            $table->tinyInteger('has_quality_committee')->nullable();
            $table->tinyInteger('quality_visible')->nullable();
            $table->text('quality_committee')->nullable();
            if (Schema::hasTable('users')){
                $table->integer('creator_id')->unsigned();
                $table->foreign('creator_id')->references('id')->on('users');
                $table->integer('manager_id')->unsigned()->nullable();
                $table->foreign('manager_id')->references('id')->on('users');
            }
            if (Schema::hasTable('kamions')){
                $table->integer('kamion_id')->unsigned()->nullable();
                $table->foreign('kamion_id')->references('id')->on('kamions');
            }
            if (Schema::hasTable('workgroups')){
                $table->integer('workgroup_id')->unsigned()->nullable();
                $table->foreign('workgroup_id')->references('id')->on('workgroups');
            }
            if (Schema::hasTable('ticket_status')){
				$table->integer('status_id')->unsigned();
				$table->foreign('status_id')->references('id')->on('ticket_status');
			}
			if (Schema::hasTable('ticket_models')){
				$table->integer('model_id')->unsigned();
				$table->foreign('model_id')->references('id')->on('ticket_models');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
