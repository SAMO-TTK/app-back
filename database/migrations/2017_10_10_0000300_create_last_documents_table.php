<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_documents', function (Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            if (Schema::hasTable('users')){
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
            }
            if (Schema::hasTable('documents')){
                $table->integer('document_id')->unsigned();
                $table->foreign('document_id')->references('id')->on('documents');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_documents');
    }
}
