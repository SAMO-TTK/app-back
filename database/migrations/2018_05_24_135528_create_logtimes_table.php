<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogtimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logtimes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->date('date');
            $table->float('length');
            if(Schema::hasTable("users")){
            	$table->integer('user_id')->unsigned();
            	$table->foreign('user_id')->references('id')->on('users');
			}
            if(Schema::hasTable("status_logtimes")){
            	$table->integer('status_logtime_id')->unsigned();
            	$table->foreign('status_logtime_id')->references('id')->on('status_logtimes');
			}
            if(Schema::hasTable("kamions")){
            	$table->integer('kamion_id')->unsigned();
            	$table->foreign('kamion_id')->references('id')->on('kamions');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logtimes');
    }
}
