<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapabilityUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capability_user', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            if(Schema::hasTable('capabilities')){
                $table->integer('capability_id')->unsigned();
                $table->foreign('capability_id')->references('id')->on('capabilities');
            }
            if(Schema::hasTable('users')){
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capability_user');
    }
}
