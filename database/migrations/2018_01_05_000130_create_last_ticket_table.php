<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('last_ticket', function (Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			if (Schema::hasTable('users')){
				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
			}
			if (Schema::hasTable('tickets')){
				$table->integer('ticket_id')->unsigned();
				$table->foreign('ticket_id')->references('id')->on('tickets');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_ticket');
    }
}
