<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_note', function (Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            if(Schema::hasTable('documents')){
                $table->integer('document_id')->unsigned();
                $table->foreign('document_id')->references('id')->on('documents');
            }
            if(Schema::hasTable('notes')){
                $table->integer('note_id')->unsigned();
                $table->foreign('note_id')->references('id')->on('notes');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_note');
    }
}
