<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimecardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timecards', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->date('date');
            $table->integer('cumul')->default(0);
            $table->tinyInteger('process_step')->default(0);
            if(Schema::hasTable('users')){
            	$table->integer('user_id')->unsigned();
            	$table->foreign('user_id')->references('id')->on('users');
			}
        });
        Schema::table('logtimes', function (Blueprint $table) {
        	$table->dropColumn('date');
        	$table->integer('timecard_id')->unsigned()->nullable();
        	$table->foreign('timecard_id')->references('id')->on('timecards');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('logtimes', function (Blueprint $table){
    		$table->dropForeign('logtime_timecard_id_foreign');
    		$table->dropColumn('timecard_id');
		});
        Schema::dropIfExists('timecards');
    }
}
