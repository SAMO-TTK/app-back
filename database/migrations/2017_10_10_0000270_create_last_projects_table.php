<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_projects', function(Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			if(Schema::hasTable('users')){
				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			}
			if(Schema::hasTable('kamions')){
				$table->integer('kamion_id')->unsigned();
				$table->foreign('kamion_id')->references('id')->on('kamions')->onDelete('cascade');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_projects');
    }
}
