<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussion_user', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
			$table->tinyInteger('is_author');
			$table->tinyInteger('is_concerned');
            if (Schema::hasTable('discussions')){
                $table->integer('discussion_id')->unsigned();
                $table->foreign('discussion_id')->references('id')->on('discussions');
            }
            if(Schema::hasTable('users')){
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussion_user');
    }
}
