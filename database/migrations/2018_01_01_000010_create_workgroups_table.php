<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('workgroups', function (Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('target');
			$table->dateTime('delay')->nullable();
			$table->integer('hours')->nullable();
			$table->text('planning')->nullable();
			$table->text('report')->nullable();
			$table->text('decision')->nullable();
			$table->tinyInteger('is_visible')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workgroups');
    }
}
