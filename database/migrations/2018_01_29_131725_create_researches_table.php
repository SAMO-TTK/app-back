<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('researches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name');
            $table->string('destinater')->nullable();
            $table->string('redacter')->nullable();
			$table->string('responsable')->nullable();
			$table->string('id_kamion')->nullable();
			$table->string('model')->nullable();
			$table->string('category')->nullable();
			$table->string('id_ticket')->nullable();
			$table->string('status')->nullable();
			$table->string('instruction')->nullable();
			$table->integer('deadline')->unsigned()->nullable();
			$table->integer('patate')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('researches');
    }
}
