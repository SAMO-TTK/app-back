<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
			$table->text('content');
			$table->string('references')->nullable();
			$table->string('numbering');
			$table->string('type');
			$table->tinyInteger('is_open');
            $table->tinyInteger('is_client_requirement');
            if(Schema::hasTable('chapters')){
				$table->integer('chapter_id')->unsigned();
				$table->foreign('chapter_id')->references('id')->on('chapters');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elements');
    }
}
