<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentKamionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_kamion', function (Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			$table->SoftDeletes();
			if(Schema::hasTable('documents')){
				$table->integer('document_id')->unsigned();
				$table->foreign('document_id')->references('id')->on('documents');
			}
			if(Schema::hasTable('kamions')){
				$table->integer('kamion_id')->unsigned();
				$table->foreign('kamion_id')->references('id')->on('kamions');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_kamion');
    }
}
