<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKamionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('wana_id');
            $table->date('wana_created_at');
            $table->date('wana_updated_at')->nullable();
            $table->string('name');
			$table->text('description')->nullable();
			$table->string('image')->nullable();
            $table->string('length')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('planned_time')->nullable();
            $table->integer('worked_time')->nullable();
            $table->tinyInteger('is_delivered');
            $table->date('delivery')->nullable();
			$table->date('frame_delivery')->nullable();
            $table->dateTime('expected_frame_delivery')->nullable();
            $table->date('ordered_at')->nullable();
			$table->string('coeff_mp')->nullable();
			$table->float('hourly_rate')->nullable();
			$table->string('model')->nullable();
            $table->string('client')->nullable();
            $table->string('destination')->nullable();
			$table->text('note')->nullable();
			$table->dateTime('cda_exported_at')->nullable();
			$table->tinyInteger('is_locked');
			$table->string('type')->nullable();
			if(Schema::hasTable('stages')){
				$table->integer('stage_id')->unsigned();
				$table->foreign('stage_id')->references('id')->on('stages');
			}
			if(Schema::hasTable('categories')){
				$table->integer('category_id')->unsigned();
				$table->foreign('category_id')->references('id')->on('categories');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamions');
    }
}
