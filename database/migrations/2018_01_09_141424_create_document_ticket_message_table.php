<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTicketMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('document_ticket_message', function (Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			if (Schema::hasTable('documents')){
				$table->integer('document_id')->unsigned();
				$table->foreign('document_id')->references('id')->on('documents');
			}
			if (Schema::hasTable('ticket_messages')){
				$table->integer('message_id')->unsigned();
				$table->foreign('message_id')->references('id')->on('ticket_messages');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_ticket_message');
    }
}
