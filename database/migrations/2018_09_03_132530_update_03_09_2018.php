<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update03092018 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kamions', function(Blueprint $table){
        	$table->tinyInteger('is_visible')->default(0);
        	$table->tinyInteger('open_phase_2')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kamions', function (Blueprint $table){
        	$table->dropColumn('is_visible');
		});
    }
}
