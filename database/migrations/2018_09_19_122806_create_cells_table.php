<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cells', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('position');
            if (Schema::hasTable('rows')){
            	$table->integer('row_id')->unsigned();
            	$table->foreign('row_id')->references('id')->on('rows');
			}
			if(Schema::hasTable('fields')){
            	$table->integer('field_id')->unsigned()->nullable();
            	$table->foreign('field_id')->references('id')->on('fields');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cells');
    }
}
