<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            // $table->datetime('imported_at');
			$table->string('numbering');
            $table->string('title');
            if(Schema::hasTable('kamions')){
				$table->integer('kamion_id')->unsigned();
				$table->foreign('kamion_id')->references('id')->on('kamions');
            }
			$table->integer('parent_id')->unsigned()->nullable();
			$table->foreign('parent_id')->references('id')->on('chapters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapters');
    }
}
