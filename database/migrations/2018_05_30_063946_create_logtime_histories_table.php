<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogtimeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logtime_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('type');
            $table->text('remark')->nullable();
            $table->string('update_name');
			if (Schema::hasTable('users')){
				$table->integer('user_id')->unsigned()->nullable();
				$table->foreign('user_id')->references('id')->on('users');
			}
			if (Schema::hasTable('logtimes')){
				$table->integer('logtime_id')->unsigned()->nullable();
				$table->foreign('logtime_id')->references('id')->on('logtimes');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logtime_histories');
    }
}
