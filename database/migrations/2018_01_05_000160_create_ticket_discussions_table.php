<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_discussions', function (Blueprint $table){
        	$table->increments('id');
        	$table->timestamps();
        	$table->softDeletes();
			$table->string('title');
			$table->text('content');
			$table->tinyInteger('is_open');
			$table->tinyInteger('is_public')->default(0);
        	$table->dateTime('deadline')->nullable();
			if (Schema::hasTable('tickets')){
				$table->integer('ticket_id')->unsigned();
				$table->foreign('ticket_id')->references('id')->on('tickets');
			}
			if (Schema::hasTable('users')){
				$table->integer('created_by')->unsigned();
				$table->foreign('created_by')->references('id')->on('users');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_discussions');
    }
}
