<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_values', function (Blueprint $table){
        	$table->increments('id');
        	$table->timestamps();
        	$table->softDeletes();
        	$table->string('value');
        	if (Schema::hasTable('model_fields')){
        		$table->integer('field_id')->unsigned();
        		$table->foreign('field_id')->references('id')->on('model_fields');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_values');
    }
}
