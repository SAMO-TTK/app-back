<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWorkgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('user_workgroup', function (Blueprint $table){
			$table->increments('id');
			$table->timestamps();
			if (Schema::hasTable('users')){
				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
			}
			if (Schema::hasTable('workgroups')){
				$table->integer('workgroup_id')->unsigned();
				$table->foreign('workgroup_id')->references('id')->on('workgroups');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_workgroup');
    }
}
