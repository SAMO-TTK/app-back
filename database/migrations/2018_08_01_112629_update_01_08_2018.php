<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update01082018 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussion_notification', function(Blueprint $table){
        	$table->increments('id');
        	$table->timestamps();
        	if (Schema::hasTable('discussions')){
        		$table->integer('discussion_id')->unsigned();
        		$table->foreign('discussion_id')->references('id')->on('discussions');
			}
        	if (Schema::hasTable('notifications')){
        		$table->integer('notification_id')->unsigned();
        		$table->foreign('notification_id')->references('id')->on('notifications');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussion_notification');
    }
}
