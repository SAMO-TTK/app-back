<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rows', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('position')->default(0);
            if (Schema::hasTable('models')){
            	$table->integer('model_id')->unsigned()->nullable();
            	$table->foreign('model_id')->references('id')->on('models');
			}
			if (Schema::hasTable('fields')){
            	$table->integer('field_id')->unsigned()->nullable();
            	$table->foreign('field_id')->references('id')->on('fields');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rows');
    }
}
