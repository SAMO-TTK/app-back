<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_fields', function (Blueprint $table){
        	$table->increments('id');
        	$table->timestamps();
        	$table->softDeletes();
        	$table->string('label');
        	$table->string('type');
        	$table->integer('position');
        	$table->integer('depend')->nullable();
        	if (Schema::hasTable('ticket_models')){
        		$table->integer('model_id')->unsigned();
        		$table->foreign('model_id')->references('id')->on('ticket_models');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_fields');
    }
}
