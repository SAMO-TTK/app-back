<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
			if(Schema::hasTable('tickets')){
				$table->integer('ticket_id')->unsigned();
				$table->foreign('ticket_id')->references('id')->on('tickets');
			}
			if(Schema::hasTable('notifications')){
				$table->integer('notification_id')->unsigned();
				$table->foreign('notification_id')->references('id')->on('notifications');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_tickets');
    }
}
