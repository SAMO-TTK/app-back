<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update26092018 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table){
        	$table->dropColumn('model_id');
//        	$table->foreign('model_id')->references('id')->on('models');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('tickets', function (Blueprint $table){
//			$table->dropForeign('tickets_model_id_foreign');
//			$table->foreign('model_id')->references('id')->on('models');
		});
    }
}
