<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('columns', function (Blueprint $table){
           $table->increments('id');
           $table->timestamps();
           $table->softDeletes();
           if (Schema::hasTable('kamions')){
               $table->integer('kamion_id')->unsigned();
               $table->foreign('kamion_id')->references('id')->on('kamions');
           }
            $table->tinyInteger('numbering');
            $table->tinyInteger('reference');
            $table->tinyInteger('content');
            $table->tinyInteger('supplement');
            $table->tinyInteger('crecre');
            $table->tinyInteger('upupup');
            $table->tinyInteger('client_requirement');
            $table->tinyInteger('type');
            $table->tinyInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('columns');
    }
}
