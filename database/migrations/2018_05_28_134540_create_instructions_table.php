<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('label');
            $table->string('wana_id');
            if(Schema::hasTable('kamions')){
            	$table->integer('kamion_id')->unsigned();
            	$table->foreign('kamion_id')->references('id')->on('kamions');
			}
        });
		Schema::table('logtimes', function (Blueprint $table){
			$table->integer('instruction_id')->unsigned();
			$table->foreign('instruction_id')->references('id')->on('instructions');
		});
		Schema::table('materials', function (Blueprint $table){
			$table->integer('instruction_id')->unsigned();
			$table->foreign('instruction_id')->references('id')->on('instructions');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('materials', function($table) {
    		$table->dropForeign('materials_instruction_id_foreign');
			$table->dropColumn('instruction_id');
		});
    	Schema::table('logtimes', function($table) {
			$table->dropForeign('logtimes_instruction_id_foreign');
			$table->dropColumn('instruction_id');
		});
        Schema::dropIfExists('instructions');
    }
}
