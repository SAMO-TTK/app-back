<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_resources', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('label');
            $table->string('wana_id');
        });

        Schema::table('materials', function (Blueprint $table){
        	$table->integer('material_resource_id')->unsigned();
        	$table->foreign('material_resource_id')->references('id')->on('material_resources');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('materials', function($table) {
			$table->dropForeign('materials_material_resource_id_foreign');
			$table->dropColumn('material_resource_id');
		});
        Schema::dropIfExists('material_resources');
    }
}
