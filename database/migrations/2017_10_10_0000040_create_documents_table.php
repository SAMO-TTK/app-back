<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('title');
            $table->string('extension');
            $table->string('mfile_id');
            $table->string('mfile_object_id');
            $table->timestamp('mfile_created_at')->nullable();
            $table->timestamp('mfile_updated_at')->nullable();
            $table->string('guid')->nullable();
            if (Schema::hasTable('users')){
                $table->integer('author_id')->unsigned()->nullable();
                $table->foreign('author_id')->references('id')->on('users');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
