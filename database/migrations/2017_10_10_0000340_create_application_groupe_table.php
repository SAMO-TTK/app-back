<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationGroupeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_groupe', function(Blueprint $table){
            $table->increments('id');
            if(Schema::hasTable('applications')){
                $table->integer('application_id')->unsigned();
                $table->foreign('application_id')->references('id')->on('applications');
            }
            if(Schema::hasTable('groupes')){
                $table->integer('groupe_id')->unsigned();
                $table->foreign('groupe_id')->references('id')->on('groupes');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_groupe');
    }
}
