<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentElementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_element', function (Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            if(Schema::hasTable('documents')){
				$table->integer('document_id')->unsigned();
				$table->foreign('document_id')->references('id')->on('documents');
            }
            if(Schema::hasTable('elements')){
				$table->integer('element_id')->unsigned();
				$table->foreign('element_id')->references('id')->on('elements');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_element');
    }
}
