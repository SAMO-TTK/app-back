<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidationControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validation_controls', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->tinyInteger('is_controled')->nullable();
            $table->text('note')->nullable();
            if(Schema::hasTable('elements')){
				$table->integer('element_id')->unsigned();
				$table->foreign('element_id')->references('id')->on('elements');
			}
            if(Schema::hasTable('users')){
				$table->integer('user_id')->unsigned()->nullable();
				$table->foreign('user_id')->references('id')->on('users');
			}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validation_controls');
    }
}
