<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKamionTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamion_ticket', function(Blueprint $table){
        	$table->increments('id');
        	$table->timestamps();
			if (Schema::hasTable('kamions')){
				$table->integer('kamion_id')->unsigned();
				$table->foreign('kamion_id')->references('id')->on('kamions');
			}
			if (Schema::hasTable('tickets')){
				$table->integer('ticket_id')->unsigned();
				$table->foreign('ticket_id')->references('id')->on('tickets');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamion_ticket');
    }
}
