<?php

namespace App\Api\v1\Traits;

use Illuminate\Http\Request;
use Dingo\Api\Exception\UpdateResourceFailedException;

use App\Api\v1\Transformers\MetaTransformer;
use Dingo\Api\Routing\Helpers;


trait ApiController {

    use Helpers;

    #region - where_has
    private function where_has($length, $i, $parts, $query, $filters, $embeded, $k, $model, $mod) {
        if ($i < $length - 1){

            if (!method_exists($i == 0 ? $this->model : $model, $embeded[$i])){	//custom for multi relation (embed=relation.relation.relation)
                unset($filters[$k]);											//custom for multi relation (embed=relation.relation.relation)
                return;														//custom for multi relation (embed=relation.relation.relation)
            }

            $name = $i == 0 ? $this->model : $model;						//custom for multi relation (embed=relation.relation.relation)
            $mod = $name;
            $model = get_class((new $name)->{$embeded[$i]}()->getRelated());
            $query->whereHas(
                $embeded[$i], function($q) use ($length, $i, $parts, $filters, $embeded, $k, $model, $mod) {
                    $this->where_has($length, $i + 1 , $parts, $q, $filters, $embeded , $k, $model, $mod);
                }
            );

        } else {
            if($parts[1] == 'in') {
                return $query->whereIn($embeded[$i], explode(';', $parts[2]));

            } else {

                if(isset($parts[2])) {
                    $parts[2] = str_replace('~', '%', $parts[2]); // For 'like' operator
                }

                if($parts[2] == 'null') {
                    if($parts[1] == 'is') {
                        $query->whereNull($embeded[$i]);

                    } elseif($parts[1] == 'is not') {
                        $query->whereNotNull($embeded[$i]);
                    }

                    unset($filters[$k]);

                } else {
                    $name = $i == 0 ? $this->model : $mod;						//custom for multi relation (embed=relation.relation.relation)
                    if(isset($embeded[$i-1]) && get_class((new $name)->{$embeded[$i - 1]}()) == "Illuminate\Database\Eloquent\Relations\BelongsToMany") {
                        $parts[0] = $embeded[$i-1] . '.' . $embeded[$i];
                    } else {
                        $parts[0] = $embeded[$i];
                    }
                    $tmp[] = $parts;

                    return $query->where($tmp);
                }
            }
        }
    }
    #endregion


    #region - index()
    public function index(Request $request) {

        // Returned fields
        $q_fields   = $request->query('fields');
        $fields     = !is_null($q_fields) ? explode(',', $q_fields) : $this->index_fields;
        $query      = $this->model::select($fields);

		// Orders
		foreach($orders = explode(',', $request->query('order')) as $k => $v) {
			$o = explode('|', $v);
			if(count($o) > 1) {
				$query->orderBy($o[0], $o[1]);
			}
		}

        // Embed ressources
        $count = [];
        foreach($embed = explode(',', $request->query('embed')) as $k => $v) {
            foreach($tmp = explode('.', $v) as $key => $value) {				//custom for multi relation (embed=relation.relation.relation)
				if (!method_exists($key == 0 ? $this->model : $model, $value)){	//custom for multi relation (embed=relation.relation.relation)
					unset($embed[$k]);											//custom for multi relation (embed=relation.relation.relation)
					break;														//custom for multi relation (embed=relation.relation.relation)
				}																//custom for multi relation (embed=relation.relation.relation)
				$count[$k] = $tmp[0];
				$name = $key == 0 ? $this->model : $model;						//custom for multi relation (embed=relation.relation.relation)
				$model = get_class((new $name)->{$value}()->getRelated());		//custom for multi relation (embed=relation.relation.relation)
            }																	//custom for multi relation (embed=relation.relation.relation)
        }
        $query->with($embed)->withCount($count);


        // Filtered model's fields
        foreach($block = explode('!', $request->query('filters')) as $key => $value) {
            $query->orwhere(function ($q) use ($value) {

                foreach ($filters = explode(',', $value) as $k => $v) {
                    if (empty($v)) {
                        unset($filters[$k]);

                    } else {
                        $parts      = explode('|', $v);
                        $embeded    = explode('.', $parts[0]);
                        $length     = count($embeded);
                        $this->where_has($length, 0, $parts, $q, $filters, $embeded, $k, null, null);
                    }
                }

            });
        }

        // Filtered metas
        foreach($meta_filters = explode(',', $request->query('metas')) as $k => $v) {
            if(empty($v)) {
                unset($meta_filters[$k]);

            } else {
                $parts = explode('|', $v);

                if($parts[1] == 'all') {
                    foreach(explode(';', $parts[2]) as $meta_value) {
                        $query->whereHas('metas', function($q) use($parts, $meta_value) {
                            $q->where([
                                ['slug', '=', $parts[0]],
                                ['meta_value_id', '=', str_replace('@', '', $meta_value)]
                            ]);
                        });
                    }

                } else {
                    $query->whereHas('metas', function($q) use($parts) {
                        $filters = [
                            ['slug', '=', $parts[0]]
                        ];

                        if($parts[1] == 'in') {
                            $q->whereIn('meta_value_id', explode(';', str_replace('@', '', $parts[2])));

                        } else {
                            if(strpos($parts[2], '@') !== false) {
                                $filters[] = ['meta_value_id', $parts[1], str_replace('@', '', $parts[2])];
                            } else {
                                $filters[] = ['label', $parts[1], str_replace('~', '%', $parts[2])]; // For 'like' operator
                            }
                        }

                        $q->where($filters);
                    });
                }

            }
        }


        // Pagination, Transformation & return
        if(!is_null($request->query('page'))) {
            $per_page = !is_null($request->query('per_page')) ? $request->query('per_page') : 15;

            $query_result       = $query->paginate($per_page);

            $return = [
                'count'     => $query_result->total(),
                'per_page'  => (int)$query_result->perPage(),
                'current'   => $query_result->currentPage(),
            ];

            if($query_result->currentPage() > 1) {
                $return['prev'] = $query_result->currentPage() - 1;
            }

            if($query_result->currentPage() < $query_result->lastPage()) {
                $return['next'] = $query_result->currentPage() + 1;
            }

            $return['data'] = [];

            foreach($query_result->getCollection() as $item) {
                $return['data'][] = (new MetaTransformer)->transform($item);
            }

            return $return;

        } else {
            return $this->response->collection($query->get(), new MetaTransformer);
        }
    }
    #endregion


    #region - show()
    public function show(Request $request, $id) {
        $count = [];

        foreach($embed = explode(',', $request->query('embed')) as $k => $v) {
			foreach($tmp = explode('.', $v) as $key => $value) {				//custom for multi relation (embed=relation.relation.relation)
				if (!method_exists($key == 0 ? $this->model : $model, $value)){	//custom for multi relation (embed=relation.relation.relation)
					unset($embed[$k]);											//custom for multi relation (embed=relation.relation.relation)
					break;														//custom for multi relation (embed=relation.relation.relation)
				}
                																//custom for multi relation (embed=relation.relation.relation)
                $count[$k] = $tmp[0];
				$name = $key == 0 ? $this->model : $model;						//custom for multi relation (embed=relation.relation.relation)
				$model = get_class((new $name)->{$value}()->getRelated());		//custom for multi relation (embed=relation.relation.relation)
            }
        }

        // if(in_array('metas', $embed)) {
        //     $query_result = $this->model::with($embed)->withCount($count)->find($id);
        //
        //     dd($query_result);
        //
        // } else {
            return $this->response->item($this->model::with($embed)->withCount($count)->find($id), new MetaTransformer);
        //}
    }
    #endregion


    #region - store()
    public function store(Request $request) {

        $model  = new $this->model;
        $data   = $request->all();
		$file	= $request->file();

        foreach($data as $k => $v) {
            if(method_exists($model, $k) || in_array($v, $file)) {
                unset($data[$k]);
            }
        }

        $model
            ->fill($data)
            ->validate()
            ->save();

        return $model;
    }
    #endregion


    #region - update()
    public function update(Request $request, $id) {
        $model = $this->model::find($id);

        if($model === null) {
            throw new UpdateResourceFailedException('Could not find the ' . (new \ReflectionClass($this->model))->getShortName() . ' #' . $id);
        }

        $data = $request->all();
        foreach($data as $k => $v) {
            if(method_exists($model, $k)) {
                unset($data[$k]);
            }
        }

        $model
            ->fill($data)
            ->validate()
            ->save();

        return $model;
    }
    #endregion


    #region - delete()
    public function delete($id) {
        return $this->model::destroy($id);
    }
    #endregion


    #region - search()
    public function search($str) {
        return $this->model::search($str)->get();
    }
    #endregion
}
