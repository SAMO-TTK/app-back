<?php

namespace App\Models;

use App\Api\v1\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;


class Company extends Model {

    use ApiModel, SoftDeletes, Eloquence;

    protected $dates    = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $guarded = [

    ];

    protected $searchableColumns = [
        'name'                  => 10,
        'address.route'         => 10,
        'address.postal_code'   => 5,
        'address.locality'      => 5,
        'address.department'    => 2,
        'address.region'        => 2,
        'address.country'       => 1
    ];

    protected $rules = [
        'name'          => 'required',
        'type'          => 'required',
        'is_app_owner'  => 'required|boolean'
    ];


    #region - Relationships
    public function address() {
        return $this->belongsTo('App\Models\Address');
    }

    public function manager() {
        return $this
            ->belongsTo('App\Models\User')
            ->with('profile');
    }

    public function creator() {
        return $this
            ->belongsTo('App\Models\User', 'created_by')
            ->with('profile');
    }

    public function updater() {
        return $this
            ->belongsTo('App\Models\User', 'updated_by')
            ->with('profile');
    }

    public function agency() {
        return $this
            ->belongsTo('App\Models\Company')
            ->with('address');
    }

    public function parent() {
        return $this
            ->belongsTo('App\Models\Company')
            ->select([
                'id', 'name', 'avatar'
            ]
        );
    }

    public function children() {
        return $this
            ->hasMany('App\Models\Company', 'parent_id')
            ->select([
                'id', 'name', 'type', 'parent_id'
            ]
        );
    }

    public function current_contacts() {
        return $this
            ->belongsToMany('App\Models\Contact', 'assignments')

            ->wherePivot('deleted_at', null)
            ->wherePivot('end', null)

            ->withPivot([
                'start', 'end', 'title'
            ]
        );
    }

    public function current_opportunities() {
        return $this
            ->hasManyThrough('App\Models\Opportunity', 'App\Models\Assignment')
            ->whereNull('assignments.end')
            ->whereNull('closed_at');
    }

    public function missions() {
        return $this
            ->hasManyThrough('App\Models\Mission', 'App\Models\Assignment')
            ->whereNull('assignments.end');
    }

    public function metas() {
        return $this
            ->belongsToMany('App\Models\Meta')
            ->withPivot(['value', 'meta_value_id'])
            ->with('meta_values');
    }

	public function files() {
		return $this->belongsToMany('App\Models\File')->withPivot('type');
	}

	public function avatar() {
		return $this->files()->wherePivot('type', 'avatar');
	}
    #endregion


}
