<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <style>
            table
            {
                border-collapse: collapse;
            }
            td, th
            {
                border: 1px solid black;
            }
            th {
                font-size:bold;
            }
            div.header > p, table p {
                margin:0 0 5px 0;
            }
            ul {
                padding-right:5px;
                margin:0;
            }
        </style>
    </head>
        <body>
            <h1>Ticket {{$ticket->title}}</h1>
            <div class="header">
                @isset($ticket->kamion_id)
                    <p>Kamion : {{$ticket->kamion->wana_id}} - {{$ticket->kamion->name}}</p>
                @endisset
                <p>N° du ticket : {{$ticket->id}}</p>
                <p>Date buttoir :
                    @php
                        $deadline = new DateTime($ticket->deadline);
                    @endphp
                    {{$deadline->format('d/m/Y H:i:s')}}
                </p>
                @if (count($ticket->kamions) > 0)
                    <p>Autres kamions :</p>
                    <ul>
                        @foreach ($ticket->kamions as $kamion)
                            <li>{{$kamion->wana_id}} - {{$kamion->name}}</li>
                        @endforeach
                    </ul>
                @endif

                @if (count($ticket->categories) > 0)
                    <p>Catégories :</p>
                    <ul>
                        @foreach ($ticket->categories as $category)
                            <li>{{$category->label}}</li>
                        @endforeach
                    </ul>
                @endif
                <p>Visibilité : {{$ticket->is_public == 1 ? 'Public' : 'Privé'}}</p>
                <p>État : {{$ticket->status->label}} </p>
                <p>Modèle : {{$ticket->model->label}}</p>
                <p>Rédacteur : {{$ticket->creator->last_name}} {{$ticket->creator->first_name}}</p>
                <p>Date de création :
                @php
                    $created_at = new DateTime($ticket->created_at);
                @endphp
                    {{$created_at->format('d/m/Y H:i:s')}}
                </p>
                @if (count($ticket->users) > 0)
                    <p>Destinataire(s) : </p>
                    <ul>
                        @foreach ($ticket->users as $user)
                            <li>{{$user->last_name}} {{$user->first_name}}</li>
                        @endforeach
                    </ul>
                @endif
                @isset($ticket->manager)
                    <p>Responsable : {{$ticket->manager->last_name}} {{$ticket->manager->first_name}}</p>
                @endif
            </div>

            @if(isset($disscussion) && !empty($discussions) && count($discussions) > 0)
            <div>
                <h2>Compléments</h2>
                @foreach($discussions as $discussion)
                    <table style="width:500px">
                        <tr>
                            <th>Titre</th>
                            <th>Contenu du complément</th>
                            <th>Info</th>
                        </tr>
                        <tr>
                            <td>{{$discussion->title}}</td>
                            <td>{{$discussion->content}}</td>
                            <td>
                                <p>{{$discussion->is_open ? 'Ouvert' : 'Fermé'}}</p>
                                <p>{{$discussion->creator->last_name}} {{$discussion->creator->first_name}}</p>
                                <p>
                                    Date :
                                    @php
                                        $created_at = new DateTime($discussion->created_at);
                                    @endphp
                                    {{$created_at->format('d/m/Y H:i:s')}}
                                </p>
                            </td>
                        </tr>

                        @if (count($discussion->messages) > 0)
                        <td colspan="3">
                            <table style="width:100%;">
                                <tr>
                                    <th>Message</th>
                                    <th>Info</th>
                                </tr>

                                @foreach($discussion->messages as $message)
                                    <tr>
                                        <td>{!! $message->content !!}</td>
                                        <td>
                                            <p>{{$message->user->last_name}} {{$message->user->first_name}}</p>
                                            <p>
                                                Date :
                                                @php
                                                    $created_at = new DateTime($message->created_at);
                                                @endphp
                                                {{$created_at->format('d/m/Y H:i:s')}}
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach

                            </table>
                        </td>
                        @endif
                    </table>
                @endforeach
            </div>
            @endif

            @if(isset($fields) && !empty($fields) && count($fields) > 0)
            <div>
                <h2>Contenu du ticket</h2>
                @foreach($fields as $field)
                    @if($field['type'] === 'tableR')

                    @elseif($field['type'] === 'bool')
                        <p>
                            <u>{{$field['label']}} :</u>
                            {{$field['value'] == 1 ? 'On' : 'Off'}}
                        </p>
                    @elseif($field['type'] === 'text')
                        <p>
                            <u>{{$field['label']}} :</u><br/>
                            {!! $field['value'] !!}
                        </p>
                    @elseif($field['type'] === 'title')
                        <h3>{{$field['label']}}</h3>
                    @elseif($field['type'] === 'selectN')
                        <p>
                            <u>{{$field['label']}} :</u>
                        </p>
                        <ul>
                            @foreach($field['value'] as $value)
                                <li>{{$value}}</li>
                            @endforeach
                        </ul>
                    @else
                        <p><u>{{$field['label']}} :</u> {{$field['value']}}</p>
                    @endif
                @endforeach
            </div>
            @endif

        </body>
</html>
