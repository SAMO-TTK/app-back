	<?php
	$name_docs = 'docs';
	$controllersPath = "app/Api/v1/Controllers";
	$modelsPath = "app/Api/v1/Models";
	$migrationsPath = "database/migrations";

	function pars_json($json){
		if (!isset($json)){
			return null;
		}
		$i = 0;
		$nb_tab = 2;
		$in = false;
		$on = false;
		$return = str_repeat('	', $nb_tab);
		while(isset($json[$i])){
			if ($json[$i] == '"'){
				$return .= $json[$i];
				$in = ($in ? false : true);
			}elseif ($json[$i] == "'"){
				$return .= $json[$i];
				$on = ($on ? false : true);
			}elseif (($json[$i] == '[' || $json[$i] == '{') && ($json[$i+1] != ']' && $json[$i+1] != '}')&& !$in && !$on){
				$nb_tab++;
				$return .= $json[$i]."\n".str_repeat('	', $nb_tab);
			}elseif (($json[$i] == ']' || $json[$i] == '}') && ($json[$i-1] != '[' && $json[$i-1] != '{')&& !$in && !$on){
				$nb_tab--;
				$return .= "\n".str_repeat('	', $nb_tab).$json[$i];
			}elseif ($json[$i] == ',' && !$in && !$on){
				$return .= $json[$i]."\n".str_repeat('	', $nb_tab);
			}else{
				$return .= $json[$i];
			}
			$i++;
		}
		return $return ;
	}

	function pars_model($model_name){
		global $modelsPath;
		$lines = file($modelsPath.'/'.$model_name.'.php');
		foreach ($lines as $line):
			if(strpos($line, 'public function')):
				preg_match("/ ([^ (]+)\(\)/", $line, $tmp);
				if(isset($tmp[1]) && $tmp[1] != ''):
					$linked[] = $tmp[1];
				endif;
			endif;
			unset($tmp);
		endforeach;
		return (isset($linked) ? $linked : null);
	}

	function pars_migration($migration_name){
		global $migrationsPath;
		$ingroupe = false;
		if (strpos($migration_name, '@')){
			preg_match("/(.*)@(.*)/", $migration_name, $tmp);
			$migration_name = $tmp[1];
			$migration_group = $tmp[2];
		}
		$migrations = scandir($migrationsPath);
		foreach ($migrations as $migration):
			if (strpos($migration, $migration_name)):
				$lines = file($migrationsPath.'/'.$migration);
				if (isset($migration_group)):
					while ($tmpline = array_shift($lines)):
						if (strpos($tmpline, '@block')):
							preg_match("/::([^:]+)::/", $tmpline, $tmp);
							if ($tmp[1] == $migration_group):
								break;
							endif;
						endif;
					endwhile;
				endif;
				// echo $migration_group."\n";
				foreach ($lines as $line):
					if (!isset($migration_group)):
						if(strpos($line, "@block")):
							$ingroupe = true;
						endif;
					endif;
					if (!$ingroupe):
						if(strpos($line, '@endblock') && isset($migration_group)):
							return $fields;
						elseif(strpos($line, '->integer(') && !strpos($line, '->nullable()')):
							preg_match("/->integer\(['\"]([^)]+)['\"]\)/", $line, $tmp);
							$fields[] = array('name' => $tmp[1], 'type' => 'INT');

						elseif(strpos($line, '->tinyInteger(') && !strpos($line, '->nullable()')):
							preg_match("/->tinyInteger\(['\"]([^)]+)['\"]\)/", $line, $tmp);
							$fields[] = array('name' => $tmp[1], 'type' => 'TINYINT');

						elseif(strpos($line, '->string(') && !strpos($line, '->nullable()')):
							preg_match("/->string\(['\"]([^)]+)['\"]\)/", $line, $tmp);
							$fields[] = array('name' => $tmp[1], 'type' => 'VARCHAR(255)');

						elseif(strpos($line, '->text(') && !strpos($line, '->nullable()')):
							preg_match("/->text\(['\"]([^)]+)['\"]\)/", $line, $tmp);
							$fields[] = array('name' => $tmp[1], 'type' => 'TEXT');

						elseif(strpos($line, '->boolean(') && !strpos($line, '->nullable()')):
							preg_match("/->boolean\(['\"]([^)]+)['\"]\)/", $line, $tmp);
							$fields[] = array('name' => $tmp[1], 'type' => 'BOOLEAN');

						elseif(strpos($line, '->timestamp(') && !strpos($line, '->nullable()')):
							preg_match("/->timestamp\(['\"]([^)]+)['\"]\)/", $line, $tmp);
							$fields[] = array('name' => $tmp[1], 'type' => 'TIMESTAMP');

						elseif(strpos($line, '->dateTime(') && !strpos($line, '->nullable()')):
							preg_match("/->dateTime\(['\"]([^)]+)['\"]\)/", $line, $tmp);
							$fields[] = array('name' => $tmp[1], 'type' => 'DATETIME');

						elseif(strpos($line, '->float(') && !strpos($line, '->nullable()')):
							preg_match("/->float\(['\"]([^)]+)['\"]\)/", $line, $tmp);
							$fields[] = array('name' => $tmp[1], 'type' => 'FLOAT');
						endif;
						// var_dump($fields);
					else:
						if (strpos($line, "@endblock")):
							$ingroupe = false;
						endif;
					endif;
					unset($tmp);
				endforeach;
			endif;
		endforeach;
		return (isset($fields) ? $fields : null);
	}

	function write_index($links, $route, $body, $docs, $name){
		fwrite($docs, "\n\n## Retrieve all ".$name.' [GET '.$route.']

+ Parameters
    + fields: (array, optional) - Fields to return, comma separated list.<br/>Return all fields with `*`');
		if (isset($links)){
		    fwrite($docs, '
	+ embed: (array, optional) - Embed linked resources : ');
			foreach ($links as $link):
				fwrite($docs, '`'.$link.'` ');
			endforeach;
		}
		fwrite($docs, "
    + filters: (array, optional) - Allow to filter on the $name fields
    + order: (array, optional) - order on the $name's field return
    + page: (integer, optional) - (required if `per_page` used) - set pagination return on, default 15 data per page
		+ per_page `integer` (optional) - set the data's number per page return

+ Response 200 (application/json)");
	if ($body):
		fwrite($docs, "\n	+ Body\n");
		fwrite($docs, $body);
	endif;
	}

	function write_show($links, $route, $body, $docs, $name){
		fwrite($docs,"\n\n## Retrieve a ".$name." by its ID [GET ".$route."]

+ Parameters
    + id: (integer, required) - Path variable, id of the $name");
		if (isset($links)){
		    fwrite($docs, '
	+ embed: (array, optional) - Embed linked resources : ');
			foreach ($links as $link):
				fwrite($docs, '`'.$link.'` ');
			endforeach;
		}
		fwrite($docs, '

+ Response 200 (application/json)');
	if ($body):
		fwrite($docs, "\n	+ Body\n");
		fwrite($docs, $body);
	endif;
	}
	function write_store($fields, $route, $body, $docs, $name){
		fwrite($docs, "\n\n## Create a ".$name." [POST ".$route."]");
	if ($fields):
		fwrite($docs, "\n+ Required post field : ");
		foreach ($fields as $field):
			fwrite($docs, "\n	+ ".$field['type'].": `".$field['name']."`");
		endforeach;
	endif;

		fwrite($docs, '

+ Response 200 (application/json)');
	if ($body):
		fwrite($docs, "\n	+ Body\n");
		fwrite($docs, $body);
	endif;
	}

	function write_update($route, $body, $docs, $name){
		fwrite($docs, "\n\n## NotificationTicket a $name [PUT $route]


+ Parameters
    + id: (integer, required) - Path variable, id of the $name

+ Response 200 (application/json)");
	if ($body):
		fwrite($docs, "\n	+ Body\n");
		fwrite($docs, $body);
	endif;
	}
	function write_delete($route, $docs, $name){
		fwrite($docs, "\n\n## Delete a $name [DELETE $route]


+ Parameters
    + id: (integer, required) - Path variable, id of the $name

+ Response 200 (application/json)
    + Body

            1");

	}

	function write_custom($route, $body, $docs, $rule, $parameters = null, $field = null, $responseheader = null){
		fwrite($docs, "\n\n## $rule $route\n");
		if ($field):
			fwrite($docs, "\n+ Required fields\n");
			foreach ($field as $line):
				fwrite($docs, "	".$line);
			endforeach;
		endif;
		if ($parameters != null):
			fwrite($docs, "\n+ Parameters\n");
			foreach ($parameters as $line):
				fwrite($docs, "	".$line);
			endforeach;
		endif;
		if ($body || $responseheader):
			if ($responseheader):
				fwrite($docs, "\n+ Response 200\n");
				fwrite($docs, "	+ Headers\n");
				foreach ($responseheader as $line):
					fwrite($docs, $line);
				endforeach;
			else:
				fwrite($docs, "\n+ Response 200 (application/json)\n");
			endif;
			if ($body):
				fwrite($docs, "\n	+ Body\n");
				fwrite($docs, $body);
			endif;
		endif;
	}
	function usage(){
		echo "\nusage :
docs_generator \e[0;32m<type> \e[0;30m[\e[0;31m-oCMmnh\e[0;30m][\e[0;31m--output\e[0;30m][\e[0;31m--controllers\e[0;30m][\e[0;31m--models\e[0;30m][\e[0;31m--migrations\e[0;30m][\e[0;31m--no_html\e[0;30m][\e[0;31m--help\e[0;30m]\e[0;32m
	- standar
	- hugo_style\n";
	exit(0);
	}
	$md = (in_array('-n', $argv) || in_array('--no_html', $argv));
	if (in_array('-h', $argv) || in_array('--help', $argv)){
		usage();
		unset($pos);
	}
	if ($pos = array_search('--output', $argv)){
		$name_docs = $argv[$pos+1];
		unset($pos);
	}
	if ($pos = array_search('-o', $argv)){
		$name_docs = $argv[$pos+1];
		unset($pos);
	}
	if ($pos = array_search('--controllers', $argv)){
		$controllersPath = $argv[$pos+1];
		unset($pos);
	}
	if ($pos = array_search('-C', $argv)){
		$controllersPath = $argv[$pos+1];
		unset($pos);
	}
	if ($pos = array_search('--models', $argv)){
		$modelsPath = $argv[$pos+1];
		unset($pos);
	}
	if ($pos = array_search('-M', $argv)){
		$modelsPath = $argv[$pos+1];
		unset($pos);
	}
	if ($pos = array_search('--migrations', $argv)){
		$migrationsPath = $argv[$pos+1];
		unset($pos);
	}
	if ($pos = array_search('-m', $argv)){
		$migrationsPath = $argv[$pos+1];
		unset($pos);
	}
	if (isset($argv[1]) && ($argv[1] == "standar" || $argv[1] == "hugo_style")):
		$type = $argv[1] == "standar" ? 1 : 0;
	else:
		usage();
	endif;
	$controllers = scandir($controllersPath);
	$docs = fopen("$name_docs.md", 'w');
	echo "\e[0;33m\nstart \e[1;33mmarkdown\e[0;33m generation\n\e[m";
	foreach ($controllers as $controller):
		$first = true;
		$isBody = false;
		$isParameters = false;
		$isResponseHeader = false;
		$isField = false;
		$lines = file($controllersPath.'/'.$controller);
		foreach ($lines as $line):
			if ($isBody):
				if (strpos($line, "@endbody")):
					$isBody = false;
				else:
					$tmpbody[] = str_replace(array('//', '#', '	', "\n"), '',$line);
				endif;
			elseif ($isParameters):
				if (strpos($line, "@endparameters")):
					$isParameters = false;
				else:
					$parameters[] = str_replace(array('//','#','	'), '', $line);
				endif;
			elseif ($isResponseHeader):
				if (strpos($line, "@endresponseheader")):
					$isResponseHeader = false;
				else:
					$responseheader[] = str_replace(array('//','#'), '', $line);
				endif;
			elseif ($isField):
				if (strpos($line, "@endfield")):
					$isField = false;
				else:
					$field[] = str_replace(array('//','#','	'), '', $line);
				endif;
			else:
				if (strpos($line, "@model")):
					preg_match( "/::([^:]+)::/", $line, $tmp);
					$name = $tmp[1];
					$links = pars_model($name);
				endif;
				if (strpos($line, "@migration")):
					preg_match( "/::([^:]+)::/", $line, $tmp);
					$fields = pars_migration($tmp[1]);
				endif;
				if (strpos($line, "@group")):
					preg_match( "/::([^:]+)::/", $line, $tmp);
					$group = $tmp[1];
					if ($type == 1):
						fwrite($docs, "\n\n# Group $group\n");
					endif;
				endif;
				if (strpos($line, "@route")):
					preg_match( "/::([^:]+)::/", $line, $tmp);
					$route = $tmp[1];
				endif;
				if (strpos($line, "@rule")):
					preg_match( "/::([^:]+)::/", $line, $tmp);
					$rule = $tmp[1];
				endif;
				if (strpos($line, "@responseheader")):
					$isResponseHeader = true;
				endif;
				if (strpos($line, "@body")):
					$isBody = true;
				endif;
				if (strpos($line, "@field")):
					$isField = true;
				endif;
				if (strpos($line, "@parameters")):
					$isParameters = true;
				endif;
				if (strpos($line, "@write")):
					if (isset($tmpbody)){
						$body = "";
						foreach ($tmpbody as $linebody) {
							$body .= $linebody;
						}
					}
					preg_match( "/::([^:]+)::/", $line, $tmp);
					switch ($tmp[1]) {
						case 'index':
							if ($type == 1):
								write_index($links, $route, pars_json($body), $docs, $group);
							else:
								$module[$name]["classique"][] = 1;
								$module[$name]["route"] = $route;
							endif;
							break;
						case 'show':
							if ($type == 1):
								write_show($links, $route, pars_json($body), $docs, $name);
							else:
								$module[$name]["classique"][] = 2;
								$module[$name]["body"] = $body;
							endif;
							break;
						case 'store':
							if ($type == 1):
								write_store($fields, $route, pars_json($body), $docs, $name);
							else:
								$module[$name]["classique"][] = 3;
							endif;
							break;
						case 'update':
							if ($type == 1):
								write_update($route, pars_json($body), $docs, $name);
							else:
								$module[$name]["classique"][] = 4;
							endif;
							break;
						case 'delete':
							if ($type == 1):
								write_delete($route, $docs, $name);
							else:
								$module[$name]["classique"][] = 5;
							endif;
							break;
						case 'custom':
							if ($type == 0 && $first):
								fwrite($docs, "\n\n# Group $group\n");
								$first = false;
							endif;
							write_custom(
								$route,
								pars_json(isset($body) ? $body : null),
								$docs,
								$rule,
								(isset($parameters) ? $parameters: null),
								(isset($field) ? $field : null),
								(isset($responseheader) ? $responseheader : null));
							break;
						default:
							break;
					}
					if ($type == 0):
						$module[$name]["links"] = $links;
						$module[$name]["fields"] = $fields;
					endif;
					unset($route);
					unset($body);
					unset($tmpbody);
					unset($parameters);
					unset($field);
					unset($rule);
					unset($responseheader);
				endif;
			endif;
			unset($tmp);
		endforeach;
	endforeach;
	if ($type == 0):
		fwrite($docs, "\n\n# Group General Rules\n");
		write_index(array('show Modules section'), "/module", pars_json('{"data":[{"id":1,"content":"content"},{"id":2,"content":"content"}]}'), $docs, "Modules");
		write_show(array('show Modules section'), "/module/{id}", pars_json('{"data":{"id":1,"content":"content"}}'), $docs, "Module");
		write_store(null, "/module", pars_json('{"module":{"created":"content", "id":1}}'), $docs, "Module");
		write_update("/module/{id}", pars_json('{"module":{"id":1,"updated":"content"}}'), $docs, "Module");
		write_delete("/module/{id}", $docs, "Module");
		fwrite($docs, "\n\n# Group Modules\n");
		foreach ($module as $name => $value) {
			if ($value["classique"]){
				fwrite($docs, "\n## $name [GET ".$value['route']."]\n\n");
				if (isset($value['fields'])){
					fwrite($docs, "### Required field :\n");
					foreach ($value['fields'] as $field) {
						fwrite($docs, "+ ".$field['type']." : `".$field['name']."`\n");
					}
				}
				if (isset($value['links'])){
					fwrite($docs, "\n### Linked ressources :\n");
					foreach ($value['links'] as $link) {
						fwrite($docs, "+ `$link`\n");
					}
				}
				fwrite($docs, "\n\n+ Response 200 (application/json)
	+ Body\n".pars_json($value['body']));
			}
		}
	endif;
	echo "\e[1;33mmarkdown\e[0;33m generation is ended\n\e[m";
	// var_dump($md);
	if (!$md):
		echo "\e[0;33m\nstart \e[1;33mhtml\e[0;33m generation\n\e[m";
		shell_exec("aglio -i $name_docs.md -o $name_docs.html 2>&-");
		echo "\e[1;33mhtml\e[0;33m generation is ended\n\e[m";
	endif;
echo "\e[7;47m\nclosing docs_generator.php\n\e[m";
