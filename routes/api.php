<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');
$api->version('v1',['middleware' => ['cors']], function ($api){
	$api->post('login', 				'App\Api\v1\Controllers\AuthController@authenticate');
	$api->post('nfc', 					'App\Api\v1\Controllers\AuthController@nfc');
	$api->get('logout', 				'App\Api\v1\Controllers\AuthController@logout');
	$api->get('tickets/export',			'App\Api\v1\Controllers\TickexelController@export');
	$api->get('cda/{id}',				'App\Api\v1\Controllers\CdaController@export');
	$api->get('cda/{id}/full',			'App\Api\v1\Controllers\CdaController@full_export');
	$api->post('kamions/{id}/image',	'App\Api\v1\Controllers\KamionController@image');
	$api->post('users/{id}/image',		'App\Api\v1\Controllers\UserController@image');
	$api->get('test', 					'App\Api\v1\Controllers\UserController@update_all_user');
});
$api->version('v1',['middleware' => ['api.auth']], function ($api) {

	$routes = array(
		'announcements'         => 'AnnouncementController',
		'applications'			=> 'ApplicationController',
		'capabilities'          => 'CapabilityController',
		'categories'			=> 'CategoryController',
		'cells'					=> 'CellController',
		'chapters'              => 'ChapterController',
		'columns'               => 'ColumnController',
		'discussions'           => 'DiscussionController',
		'documents'             => 'DocumentController',
		'durations'				=> 'DurationController',
		'elements'              => 'ElementController',
		'extensions'            => 'ExtensionController',
		'favory_tickets'		=> 'FavoryTicketController',
		'field_values'			=> 'Field_valueController',
		'fields'				=> 'FieldController',
		'groupes'               => 'GroupeController',
		'impacts'				=> 'ImpactController',
		'instructions'			=> 'InstructionController',
		'instructionusers'		=> 'InstructionUserController',
		'jobs'                  => 'JobController',
		'kamions'               => 'KamionController',
		'last_projects'			=> 'Last_projectController',
		'last_tickets'			=> 'Last_ticketController',
		'last_documents'		=> 'Last_documentController',
		'last_elements'			=> 'Last_elementController',
		'last_notes'			=> 'Last_noteController',
		'logtimes'				=> 'LogtimeController',
		'logtimehistories'		=> 'LogtimeHistoryController',
		'materials'             => 'MaterialController',
		'materialresources'     => 'MaterialResourceController',
		'messages'              => 'MessageController',
		'model_fields'			=> 'Model_fieldController',
		'models'				=> 'ModelController',
		'notes'                 => 'NoteController',
		'notifications'			=> 'NotificationController',
		'quality_controls'      => 'Quality_controlController',
		'permissions'      		=> 'PermissionController',
		'researches'			=> 'ResearchController',
		'resources'             => 'ResourceController',
		'roles'                 => 'RoleController',
		'rows'					=> 'RowController',
		'settings'				=> 'SetingController',
		'stages'				=> 'StageController',
		'supplements'			=> 'SupplementController',
		'statuslogtimes'		=> 'StatusLogtimeController',
		'ticket_types'          => 'Ticket_typeController',
		'tickets'               => 'TicketController',
		'ticket_categories'		=> 'Ticket_categoryController',
		'ticket_discussions'	=> 'Ticket_discussionController',
		'ticket_fields'			=> 'Ticket_fieldController',
		'ticket_messages'		=> 'Ticket_messageController',
		'ticket_models'			=> 'Ticket_modelController',
		'ticket_status'			=> 'Ticket_statusController',
		'timecards'				=> 'TimecardController',
		'types'					=> 'TypeController',
		'updates'				=> 'NotificationTicketController',
		'update_fields'			=> 'UpdateFieldController',
		'users'                 => 'UserController',
		'user_files'			=> 'User_filesController',
		'validation_controls'   => 'Validation_controlController',
		'workgroups'			=> 'WorkgroupController'
	);
	$api->get('tickets/{id}/tickets',										'App\Api\v1\Controllers\TicketController@linked');
	$api->put('tickets/{id}/tickets',										'App\Api\v1\Controllers\TicketController@link_ticket');
	$api->get('tickets/last',												'App\Api\v1\Controllers\TicketController@last_ticket');
	$api->post('tickets/search',											'App\Api\v1\Controllers\TicketController@search');

	$api->post('cda',														'App\Api\v1\Controllers\CdaController@import');
	$api->post('cda/{id}',													'App\Api\v1\Controllers\CdaController@update');
	$api->delete('cda/{id}',												'App\Api\v1\Controllers\CdaController@delete');

	$api->get('seed',														'App\Api\v1\Controllers\FakeController@seeder');

	$api->put('notifications/users/{user_id}',								'App\Api\v1\Controllers\NotificationController@mark_all_read');
	$api->put('notifications/{id}/users/{user_id}',							'App\Api\v1\Controllers\NotificationController@mark_read');
	$api->get('notifications/tickets/{id}/users/{id_user}',					'App\Api\v1\Controllers\NotificationController@select_no_read');

	$api->get('users/{id}/notes',											'App\Api\v1\Controllers\NoteController@shared');
	$api->delete('favory_tickets/{id}/{user_id}',							'App\Api\v1\Controllers\FavoryTicketController@delete');

	$api->put('users/{id}/discussions',										'App\Api\v1\Controllers\UserController@link_discussion');
	$api->put('users/{id}/jobs',											'App\Api\v1\Controllers\UserController@link_job');
	$api->put('users/{id}/resources',										'App\Api\v1\Controllers\UserController@link_resource');
	$api->get('users/{id_user}/kamions/{id_kamion}/last_document',			'App\Api\v1\Controllers\DocumentController@last_document');
	$api->get('users/{id_user}/kamions/{id_kamion}/last_note',				'App\Api\v1\Controllers\NoteController@last_note');
	$api->get('users/{id_user}/last_document',								'App\Api\v1\Controllers\DocumentController@lastDocument');
	$api->get('users/{id_user}/kamions/{id_kamion}/last_instruction',		'App\Api\v1\Controllers\UserController@last_instruction');
	$api->get('users/{id_user}/last_instruction',							'App\Api\v1\Controllers\UserController@last_instruction_all');
	$api->post('users/instructions/{id}',									'App\Api\v1\Controllers\UserController@insert_last_instruction');
	$api->get('users/{id_user}/last_materialresource',						'App\Api\v1\Controllers\UserController@last_material_resource_all');
	$api->post('users/last_materialresource/{id}',							'App\Api\v1\Controllers\UserController@insert_last_material_resource');
	$api->put('users/wana/{id}',											'App\Api\v1\Controllers\UserController@sync');

	$api->get('kamions/users',												'App\Api\v1\Controllers\KamionController@last_projects');
	$api->get('kamions/{id}/cda',											'App\Api\v1\Controllers\KamionController@cda');
	$api->get('kamions/{id}/elements',										'App\Api\v1\Controllers\KamionController@elements');
	$api->get('kamions/{id}/projects',										'App\Api\v1\Controllers\KamionController@linked');
	$api->put('kamions/{id}/projects',										'App\Api\v1\Controllers\KamionController@link_kamion');
	$api->put('kamions/{id}/users',											'App\Api\v1\Controllers\KamionController@link_user');

	$api->get('permissions/{slug}',											'App\Api\v1\Controllers\PermissionController@get_id');
	$api->get('settings/slug/{slug}',										'App\Api\v1\Controllers\SetingController@get_id');

	$api->post('models/{id}', 												'App\Api\v1\Controllers\ModelController@duplicate');
	$api->post('rows/{id}', 												'App\Api\v1\Controllers\RowController@duplicate');

	foreach ($routes as $route => $controller):
		$api->get($route, 'App\Api\v1\Controllers\\'.$controller.'@index');
		$api->get($route.'/{id}', 'App\Api\v1\Controllers\\'.$controller.'@show');
		$api->post($route, 'App\Api\v1\Controllers\\'.$controller.'@store');
		$api->put($route.'/{id}', 'App\Api\v1\Controllers\\'.$controller.'@update');
		$api->delete($route.'/{id}', 'App\Api\v1\Controllers\\'.$controller.'@delete');
	endforeach;
});
